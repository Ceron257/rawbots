//enable if using visual studio
//#if defined _WIN32
//#include "stdafx.h"
//#endif

#include "BulletServiceSupport.h"
#include <math.h>

void PIDScalar::Reset ( float feedback ) {
    integral = 0;
    lastFeedback = feedback;
}

float PIDScalar::Compute ( float feedback, float dt ) {
    float proportional = setpoint - feedback;
    integral = integral + proportional * dt;
    float derivative = -( feedback - lastFeedback ) / dt;
    lastFeedback = feedback;
    return kp * proportional + ki * integral + kd * derivative;
}

void BulletRotationalMotor::SetVelocity(float velocity){
    constraint6Dof ->getRigidBodyA().activate(true);
    constraint6Dof ->getRigidBodyB().activate(true);
    motor->m_targetVelocity = velocity;
    motor->m_enableMotor = true;
    isActive = true;
    servoMode = false;
}

void BulletRotationalMotor::SetServoVelocity(float velocity){
    constraint6Dof ->getRigidBodyA().activate(true);
    constraint6Dof ->getRigidBodyB().activate(true);
    motor->m_targetVelocity = velocity;
    motor->m_enableMotor = true;
    isActive = true;
    servoMode = true;
}

void BulletRotationalMotor::SetTarget(float target,float scaling){
    constraint6Dof ->getRigidBodyA().activate(true);
    constraint6Dof ->getRigidBodyB().activate(true);
    
    
    controller ->setpoint = target;
    motor->m_enableMotor = true;
    isActive = true;
    servoMode = true;
}

void BulletRotationalMotor::StopMotor(){
    constraint6Dof->getRotationalLimitMotor(axisIndex) ->m_targetVelocity = 0;
    constraint6Dof->getRotationalLimitMotor(axisIndex) ->m_enableMotor = false;
    isActive = false;
}

void BulletRotationalMotor::Reset(){

    float axisAngle = constraint6Dof->getAngle(axisIndex);//cos(axisAngle)
    float targetAxisAngle = controller ->setpoint;
    btVector3 currentVectorAngle = btVector3(cos(axisAngle),sin(axisAngle),0);
    btVector3 targetVectorAngle = btVector3(cos(targetAxisAngle),sin(targetAxisAngle),0);

//  int sign =  (currentVectorAngle.cross(targetVectorAngle).getZ()  >= 0.0f)?1:-1; 

    float angle =  currentVectorAngle.angle(targetVectorAngle);//*sign;

    controller ->Reset(angle);
}

float BulletRotationalMotor::Compute(float dt){

    float axisAngle = constraint6Dof->getAngle(axisIndex);//cos(axisAngle)
    float targetAxisAngle = controller ->setpoint;
    btVector3 currentVectorAngle = btVector3(cos(axisAngle),sin(axisAngle),0);
    btVector3 targetVectorAngle = btVector3(cos(targetAxisAngle),sin(targetAxisAngle),0);

    int sign =  (currentVectorAngle.cross(targetVectorAngle).getZ()  >= 0.0f)?1:-1; 

    float angle =  currentVectorAngle.angle(targetVectorAngle);//*sign;

    


    float sample = controller ->Compute(targetAxisAngle - angle,dt);

    return sample*sign;
}

void BulletRotationalMotor::Destroy(){
    delete(controller);
}

bool BulletRotationalMotor::IsActive(){
    return isActive;
}

bool BulletRotationalMotor::InServoMode(){
    return servoMode;
}

Vector3 BulletRotationalMotor::getDebug(){


    float axisAngle = constraint6Dof->getAngle(axisIndex);//cos(axisAngle)
    //axisAngle = abs( axisAngle);
    float targetAxisAngle = controller ->setpoint;
    btVector3 currentVectorAngle = btVector3(cos(axisAngle),sin(axisAngle),0);
    btVector3 targetVectorAngle = btVector3(cos(targetAxisAngle),sin(targetAxisAngle),0);
    
//  int sign =  (currentVectorAngle.cross(targetVectorAngle).getZ()  >= 0)?-1:1; 
    float angle =  currentVectorAngle.angle(targetVectorAngle);//*sign;
    Vector3 debug;
    debug.x =axisAngle*180/3.1416;
    debug.y = targetAxisAngle*180/3.1416;
    debug.z =angle*180/3.1416;
    return debug;
}


void BulletTranslationalMotor::SetVelocity(float velocity){
    constraint6Dof ->getRigidBodyA().activate(true);
    constraint6Dof ->getRigidBodyB().activate(true);
    motor->m_targetVelocity[axisIndex] = velocity*-1.0f;
    motor->m_enableMotor[axisIndex] = true;
    isActive = true;
    servoMode = false;
}
    
void BulletTranslationalMotor::SetServoVelocity(float velocity){
    constraint6Dof ->getRigidBodyA().activate();
    constraint6Dof ->getRigidBodyB().activate();
    motor->m_targetVelocity[axisIndex] = velocity*-1.0f;
    motor->m_enableMotor[axisIndex] = true;
    isActive = true;
    servoMode = true;
}

void BulletTranslationalMotor::SetTarget(float target,float scaling){
    
    constraint6Dof ->getRigidBodyA().activate();
    constraint6Dof ->getRigidBodyB().activate();
    //constraint6Dof ->getTranslationalLimitMotor()->
    
    controller ->setpoint = target*scaling;
    motor->m_enableMotor[axisIndex] = true;
    isActive = true;
    servoMode = true;
}

void BulletTranslationalMotor::StopMotor(){
    constraint6Dof->getTranslationalLimitMotor() ->m_targetVelocity[axisIndex] = 0;
    constraint6Dof->getTranslationalLimitMotor() ->m_enableMotor[axisIndex] = false;
    isActive = false;
}

void BulletTranslationalMotor::Reset(){
    controller ->Reset(constraint6Dof ->getRelativePivotPosition(axisIndex));
}

float BulletTranslationalMotor::Compute(float dt){
    float sample = controller ->Compute(constraint6Dof ->getRelativePivotPosition(axisIndex),dt);
    return sample;
}

void BulletTranslationalMotor::Destroy(){
    delete(controller);
}

bool BulletTranslationalMotor::IsActive(){
    return isActive;
}

bool BulletTranslationalMotor::InServoMode(){
    return servoMode;
}

Vector3 BulletTranslationalMotor::getDebug(){
    Vector3 debug;
    return debug;
}

