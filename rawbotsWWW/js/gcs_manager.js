  
"use strict";

var gcs_manager = gcs_manager || {};

gcs_manager.GCS_BASE_URL = 'https://www.googleapis.com/storage/v1beta1';
gcs_manager.GCS_DOWNLOAD_URL = 'http://' + constants.GCS_BUCKET + '.storage.googleapis.com'; 

gcs_manager.createNewProfile = function(callback) {
  console.log("Creating new Profile");
    var data = player.GetLocalProfile();
    gcs_manager.insertObject(data,'profiles/'+player.profile.playerId,'application/json',function(){
      callback();
    });
};

gcs_manager.listBlueprints = function(uid,callback){
   var blueprints = [];
   var request = gapi.client.request({
        'path': '/storage/v1beta2/b/'+constants.GCS_BUCKET+'/o',
        'params': { 'delimiter' : '/' , 'prefix' : ('blueprints/' + uid + '/') } 
      });
    request.execute(function(resp){
      console.log(resp);
      if(!resp.error){
        for(i = 0 ; i < resp.items.length ; ++i){
          gcs_manager.getObject(resp.items[i].name,function(data){
              blueprints.push(data);
              if(blueprints.length == resp.items.length){
                callback(blueprints);
              }
          });
        }
      }
    });
};


gcs_manager.getObject = function(id,callback){
  $.ajax({
    url: gcs_manager.GCS_DOWNLOAD_URL + '/' + id,
    type: 'GET',
    dataType : 'json',
    success: function(data, textStatus, xhr) {
      callback(data);
    },
    error: function(data, textStatus, xhr){
      console.log(data);
    },
    beforeSend: function(xhr){
      xhr.setRequestHeader('Authorization','Bearer ' + gapi.auth.getToken().access_token);
    }
  });
};

// gcs_manager.getObject = function(fullname,callback){
//   $.ajax({
//     url: 'https://www.googleapis.com/storage/v1beta1/b/' + constants.GCS_BUCKET + '/o' +
//     '?key='+ constants.GCS_API_KEY + '&delimiter=/&prefix=' + fullname,
//     headers: {
//     'Authorization': 'Bearer ' + gapi.auth.getToken().access_token
//     },
//     dataType: 'json',
//     success: function(data, textStatus, xhr) {
//       var url = 'https://storage.cloud.google.com/'+constants.GCS_BUCKET+'/'+fullname; //This is the download link
//       console.log(data);
//       console.log(url);
//       // $('#table-blueprints tbody').append(
//       // '<tr><td>1</td><td>'+data.items[0].name+'</td><td><img id="test001" src="'+url+'" alt="Smiley face" height="42" width="42"></td><td>'+data.items[0].media.timeCreated+'</td></tr>');
//     } 
//   });
// };


gcs_manager.insertObject = function(data,name,contentType,callback){
  var boundary = '-------314159265358979323846';
  var delimiter = "\r\n--" + boundary + "\r\n";
  var close_delim = "\r\n--" + boundary + "--";

  var metadata = {
      'title': name ,
      'mimeType': contentType
    };

    var base64Data = btoa(data);
    var multipartRequestBody =
        delimiter +
        'Content-Type: application/json\r\n\r\n' +
        JSON.stringify(metadata) +
        delimiter +
        'Content-Type: ' + contentType + '\r\n' +
        'Content-Transfer-Encoding: base64\r\n' +
        '\r\n' +
        base64Data +
        close_delim;

    var request = gapi.client.request({
        'path': '/upload/storage/v1beta2/b/'+constants.GCS_BUCKET+'/o',
        'method': 'POST',
        'params': {'uploadType': 'multipart','name':name},
        'headers': {
          'Content-Type': 'multipart/mixed; boundary="' + boundary + '"'
        },
        'body': multipartRequestBody});

    request.execute(function(resp){
      callback(resp);
      gcs_manager.insertPermission(name,'allUsers','READER');
    });
};


gcs_manager.insertBase64Object = function(data,name,contentType,callback){
  var boundary = '-------314159265358979323846';
  var delimiter = "\r\n--" + boundary + "\r\n";
  var close_delim = "\r\n--" + boundary + "--";

  var metadata = {
      'title': name ,
      'mimeType': contentType
    };

    var base64Data = data;
    var multipartRequestBody =
        delimiter +
        'Content-Type: application/json\r\n\r\n' +
        JSON.stringify(metadata) +
        delimiter +
        'Content-Type: ' + contentType + '\r\n' +
        'Content-Transfer-Encoding: base64\r\n' +
        '\r\n' +
        base64Data +
        close_delim;

    var request = gapi.client.request({
        'path': '/upload/storage/v1beta2/b/'+constants.GCS_BUCKET+'/o',
        'method': 'POST',
        'params': {'uploadType': 'multipart','name':name},
        'headers': {
          'Content-Type': 'multipart/mixed; boundary="' + boundary + '"'
        },
        'body': multipartRequestBody});

    request.execute(callback);
};

gcs_manager.insertObjectFromBytes =  function(bytes,name,contentType,callback) {
  console.log("inserting file from bytes");
  
  var dataArray =[];
  dataArray.push(bytes);
  var blob = new Blob(dataArray);
  var reader = new FileReader();

  reader.onload = function(e) {
    console.log("LOAD DONE");
    gcs_manager.insertObject(reader.result,name,contentType,callback);
  }
  reader.readAsBinaryString(blob);
};

gcs_manager.insertPermission = function (objectName, entity, role) {
  var body = {
    'entity': entity,
    'role': role
  };
  var request = gapi.client.request({
    'path': 'storage/v1beta2/b/'+constants.GCS_BUCKET+'/o/'+encodeURIComponent(objectName)+'/acl',
    'method': 'POST',
    'body': body
  });

  console.log(request);

  request.execute(function(resp){
    console.log(resp);
  });
}