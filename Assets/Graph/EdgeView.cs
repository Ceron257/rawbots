using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class EdgeView : MonoBehaviour {

    public NodeView tail;
    public NodeView head;
    public Relationship relationship;
    public new Transform transform;

}
