using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SignalView : MonoBehaviour {

    /*
    // TODO: this code shares functionality with Oscilloscope, refactor accordingly

    public EdgeView edgeView;
    public new LineRenderer renderer;
    private LineRenderer fiber;
    public GameObject childFiber;
    public new Transform transform;
    public int sample = 4;
    public float delta = 0;
    public float signalScale = 0.1f;
    OscilloscopeSignal signalView;
 
    void Start () {
        signalView = new OscilloscopeSignal();
        fiber = childFiber.GetComponent<LineRenderer>();
//        var outbox = edgeView.head.proxy.GetComponent< Operants.Outbox >();
//        outbox.For< float >( true, value => signalView.SetPoints( value ) );
    }

    void OnSuppress ( bool suppress ) {
        this.enabled = !suppress;
    }

    void Update () {
        DrawRender();
        DrawFiberRender();
    }
    
    void DrawRender () {
        var lhsPos = edgeView.tail.transform.position;
        var rhsPos = edgeView.head.transform.position;
        Vector3 direction = rhsPos - lhsPos;
        direction.Normalize();
        lhsPos += direction * edgeView.tail.radius;
        rhsPos += direction * edgeView.head.radius * -1;
        direction = rhsPos - lhsPos;
        var magnitude = direction.magnitude;
        direction.Normalize(); 
        var signal = Vector3.Cross( Vector3.forward, direction );
        renderer.SetVertexCount( signalView.samples.Count );
        for ( int i = 0; i < signalView.samples.Count; i++ ) {
            var position = signalView.samples[ i ];
            position.x *= magnitude / sample;
            position.y -= signalView.delta;
            if ( Mathf.Abs( signalView.max - signalView.min ) > 0f ) {
                position.y *= ( signalScale / Mathf.Abs( signalView.max - signalView.min ) );
            }
            var temp = lhsPos + direction * position.x;
            var tempY = temp + signal * position.y;
            renderer.SetPosition( i, tempY );
        }
    }

    void DrawFiberRender () {
        var lhsPos = edgeView.tail.transform.position;
        var rhsPos = edgeView.head.transform.position;
        Vector3 direction = rhsPos - lhsPos;
        direction.Normalize();
        lhsPos += direction * edgeView.tail.radius;
        rhsPos += direction * edgeView.head.radius * -1;
        fiber.SetVertexCount( 2 );
        fiber.SetPosition( 0, lhsPos );
        fiber.SetPosition( 1, rhsPos );
    }*/
}
