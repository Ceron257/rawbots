using UnityEngine;
using System.Collections;

public class EditDetach : MonoBehaviour {

    public new GameObject light;

	void Start () {
        GetComponent<AudioSource>().pitch = Random.Range( 0.9f, 1.2f );
        Destroy( light, 0.3f );
        Destroy( gameObject, 2f );
	}
    
}
