using UnityEngine;
using System.Collections;

public class Collectable : MonoBehaviour {

    public GameObject fxAsset;

    void OnTriggerEnter () {
        GetComponent<Collider>().enabled = false;
        foreach ( var r in gameObject.GetComponentsInChildren< Renderer >() ) {
            r.enabled = false;
        }
        Instantiate( fxAsset, transform.position, transform.rotation );
        Destroy( gameObject, 3 );
    }
}
