using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;
using System.Text.RegularExpressions;

public class HexOutputTile : HexGridTileTypeHandler {

    HexGridTile outputOfTile;
    HexGridLine outputOfLine;

    public override void Init (Entity etype) {
        tile.tileType = HexGridTileType.Output;
        tile.entity.ForInferredProperty< string >( "property", Util.AsIs, p => tile.label.text = p );
        outbox = tile.entity.proxy.GetComponent< Outbox >();

        tile.entity.ForInferredHead( "output_float", Util.AsIs, head => {
            this.etype = head;
            outbox.For< float >( true, OnOutputFloat );
        } );
        
        tile.entity.ForInferredHead( "output_string", Util.AsIs, head => {
            this.etype = head;
            outbox.For< string >( true, OnOutputString );
        } );
        
        tile.entity.ForInferredHead( "output_color", Util.AsIs, head => {
            this.etype = head;
            outbox.For< Color >( true, OnOutputColor );
        } );
        
        tile.entity.ForInferredHead( "output_part", Util.AsIs, head => {
            this.etype = head;
            outbox.For< PartProxy >( true, OnOutputPart );
        } );

        tile.entity.ForInferredHead( "output_label", Util.AsIs, head => {
            this.etype = head;
            outbox.For< NodeProxy >( true, OnOutputOperant );
        } );

        tile.entity.ForInferredHead( "output_texture", Util.AsIs, head => {
            this.etype = head;
            outbox.For< NodeProxy >( true, OnOutputOperant );
        } );

        tile.entity.ForInferredHead( "output_movie", Util.AsIs, head => {
            this.etype = head;
            outbox.For< NodeProxy >( true, OnOutputOperant );
        } );

        outputOfTile = tile.grid.tiles[ tile.entity.Heads( p => p == "output_for" ).First() ];
        outputOfLine = ( Instantiate( tile.outputLineAsset ) as GameObject ).GetComponent< HexGridLine >();
        outputOfLine.transform.parent = transform;
        outputOfLine.grid = tile.grid;
        outputOfLine.tail = this.transform;
        outputOfLine.head = outputOfTile.transform;

        SetMaterial();
    }
   
    public override void SetMaterial () {
        tile.renderer.sharedMaterial = tile.grid.outputMaterial;
    }

    void OnOutputFloat ( float value ) {
        tile.value.text = value.ToString( "0.###" );
    }
    
    void OnOutputString ( string value ) {
        tile.value.text = value;
    }
    
    void OnOutputPart ( PartProxy part ) {
        if ( part != default(PartProxy) ) {
            tile.value.text = part.entity.Heads( p => p == "as" ).First().id;
        }
        else {
            tile.value.text = "";
        }
    }

    void OnOutputOperant ( NodeProxy operant ) {
        if ( operant != default(NodeProxy) ) {
            tile.value.text = operant.entity.Heads( p => p == "as" ).First().id;
        }
        else {
            tile.value.text = "";
        }
    }
    
    void OnOutputEntity ( Entity entity ) {
        tile.value.text = entity.Heads( p => p == "as" ).First().id;
    }
    
    void OnOutputColor ( Color value ) {
        tile.value.text = Util.ColorToProperty( value );
    }

    public override void DeleteStep (bool step) {
        if ( outbox != null ) {
            var deletedSomething = false;
            var inboxes = outbox.Proxy.entity.Tails( p => p == "consumes" )
                .Where( i => i.proxy != null )
                    .Select( e => e.proxy.GetComponent< Inbox >() ).ToList();
            foreach ( var i in inboxes ) {
                i.Proxy.OnInboxDisconnect( i );
                deletedSomething = step;
                outbox.inboxes.Remove( inbox );
                i.Proxy.entity.Relationships()
                    .Where( r => r.Tail == i.Proxy.entity && r.Predicate == "consumes" && r.Head == outbox.Proxy.entity )
                        .First().Break();
                HexGridTile otherTile;
                if ( tile.grid.tiles.TryGetValue( i.Proxy.entity, out otherTile ) ) {
                    otherTile.handler.Consumes();
                }
            }
            outbox.inboxes.Clear();
            if ( !deletedSomething ) {
                var output_for = outbox.Proxy.entity.Relationships()
                    .Where( r => r.Tail == outbox.Proxy.entity && r.Predicate == "output_for" ).First();
                output_for.Break();
                tile.DestroyTile();
            }
        }
    }

    public override void OnMouseUpCustom ( TouchData touch ) {
        base.OnMouseUpCustom( touch );
        var leftControl = Input.GetKey( KeyCode.LeftControl );
        var leftAlt = Input.GetKey( KeyCode.LeftAlt );
        var leftShift = Input.GetKey( KeyCode.LeftShift );
        if ( touch.dragging && leftControl && !leftAlt && !leftShift) {
            var other = GetTileAtTouchPosition(touch);
            if(other != null && other != tile && other.tileType == HexGridTileType.Input){
                ConnectIOs(other,tile);
            }
        }
    }

    public override void RequestOnConsole () {
        var leftControl = Input.GetKey( KeyCode.LeftControl );
        var leftAlt = Input.GetKey( KeyCode.LeftAlt );
        var leftShift = Input.GetKey( KeyCode.LeftShift );
        
        if(!leftControl && !leftAlt && !leftShift && Gaia.scene > 0 ){
            Sec.ForPermission( tile.entity, Sec.Permission.edit, () => {
                onConsole();
            }, () => {
                Console.PushError("tile", new string[]{  "access denied - check permissions" } );
            } );
        }
    }


    public override void OnDestroyEventHandler () {
        if ( outbox != null ) {
            outbox.RemoveAction< float >( OnOutputFloat );
            outbox.RemoveAction< string >( OnOutputString );
            outbox.RemoveAction< PartProxy >( OnOutputPart );
            outbox.RemoveAction< Color >( OnOutputColor );
            outbox.RemoveAction< NodeProxy >( OnOutputOperant );
        }
    }

}
