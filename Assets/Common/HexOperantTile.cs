using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;
using System.Text.RegularExpressions;

public class HexOperantTile : HexGridTileTypeHandler {

    public override void Init (Entity etype) {
        base.Init(etype);
        tile.originalIconColor = ApplyAlphaFactor(tile.iconRenderer,0.5f);
        tile.tileType = HexGridTileType.Operant;
        io = tile.entity.proxy.GetComponent< IOComponent >();
        InitializeIO();
        SetMaterial();
    }
 
    public override void SetMaterial () {
        tile.renderer.sharedMaterial = tile.grid.operantMaterial;
    }

    public override void DeleteStep (bool step) {
        DestroyIOs();
    }

    public override void RequestOnConsole () {
        var leftControl = Input.GetKey( KeyCode.LeftControl );
        var leftAlt = Input.GetKey( KeyCode.LeftAlt );
        var leftShift = Input.GetKey( KeyCode.LeftShift );

        if(Gaia.scene > 0){
            Sec.ForPermission( tile.entity, Sec.Permission.edit, () => {
                if(!leftAlt && !leftShift){
                    if(leftControl){
                        tile.grid.GenerateTilesIO( tile );
                    }
                    onConsole();
                }
            }, () => {
                Console.PushError("tile", new string[]{  "access denied - check permissions" } );
            } );
        }
    }
}
