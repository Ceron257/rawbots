using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;
using System.Text.RegularExpressions;

public class HexInputTile : HexGridTileTypeHandler {

    HexGridTile inputOfTile;
    HexGridLine inputOfLine;

    public override void Init(Entity etype) {
        tile.tileType = HexGridTileType.Input;
        tile.entity.ForInferredProperty< string >( "property", Util.AsIs, p => tile.label.text = p );
        inbox = tile.entity.proxy.GetComponent< Inbox >();

        tile.entity.ForInferredHead( "input_amount", Util.AsIs, head => {
            this.etype = head;
            inbox.ForQueued< float >( true, OnInputAmount );
            onConsole = () => {
                tile.grid.console.ForInput( tile.label.text, tile.value.text, options, input => {
                    var floatValue = 0f;
                    if ( float.TryParse( input.Trim(), out floatValue ) ) {
                        var gridTile = HexGridTile.Find( tile.entity.id );
                        if ( gridTile != null ) {
                            tile.entity.SetProperty( "value", floatValue );
                            gridTile.value.text = floatValue.ToString( "0.###" );
                            if ( inbox != null ) {
                                inbox.Consume< float >( null, floatValue );    //Further user input value constraints could be put here
                            }
                        }
                    }
                } );
            };
        } );
        
        tile.entity.ForInferredHead( "input_string", Util.AsIs, head => {
            this.etype = head;
            inbox.For< string >( true, OnInputString );
            onConsole = () => {
                tile.grid.console.ForInput( tile.label.text, tile.value.text, options, input => {
                    var gridTile = HexGridTile.Find( tile.entity.id );
                    if ( gridTile != null ) {
                        tile.entity.SetProperty( "value", input );
                        gridTile.value.text = input;
                        if ( inbox != null ) {
                            inbox.Consume< string >( null, input );
                        }
                    }
                } );
            };
        } );
        
        tile.entity.ForInferredHead( "input_list", Util.AsIs, head => {
            this.etype = head;
            tile.entity.ForInferredProperty< string >( "enum", Util.AsIs, e => {
                options.AddRange( System.Enum.GetNames( System.Type.GetType( e ) ) );
            } );
            inbox.For< string >( true, OnInputString );
            onConsole = () => {
                tile.grid.console.ForInput( tile.label.text, tile.value.text, options, input => {
                    var gridTile = HexGridTile.Find( tile.entity.id );
                    if ( gridTile != null ) {
                        tile.entity.SetProperty( "value", input );
                        gridTile.value.text = input;
                        if ( inbox != null ) {
                            inbox.Consume< string >( null, input );
                        }
                    }
                } );
            };
        } );
        
        tile.entity.ForInferredHead( "input_color", Util.AsIs, head => {
            this.etype = head;
            inbox.For< Color >( true, OnInputColor );
            onConsole = () => {
                tile.grid.console.ForInput( tile.label.text, tile.value.text, options, input => {
                    var gridTile = HexGridTile.Find( tile.entity.id );
                    if ( gridTile != null ) {
                        tile.entity.SetProperty( "value", input );
                        gridTile.value.text = input;
                        if ( inbox != null ) {
                            var colorRegex = "#[A-Fa-f0-9]{6}";//previously, length wasn't checked as long as 6 char matched. This meant for instance,
                            var isValid = Regex.Match( input, colorRegex ).Success && input.Length == 7;//"#000000whatever" would've been valid. -z26
                            if ( isValid ) {
                                inbox.Consume< Color >( null, Util.PropertyToColor( input ) );
                            }
                            else {
                                Console.PushError("color", new string[]{ "Invalid format, please use an hexadecimal color code or the colouriser tile" } );
                            }
                        }
                    }
                } );
            };
        } );
        
        tile.entity.ForInferredHead( "input_part", Util.AsIs, head => {
            this.etype = head;
            inbox.For< PartProxy >( true, OnInputPart );
        } );

        tile.entity.ForInferredHead( "input_label", Util.AsIs, head => {
            this.etype = head;
            inbox.For< NodeProxy >( true, OnInputOperant );
        } );

        tile.entity.ForInferredHead( "input_texture", Util.AsIs, head => {
            this.etype = head;
            inbox.For< NodeProxy >( true, OnInputOperant );
        } );

        tile.entity.ForInferredHead( "input_movie", Util.AsIs, head => {
            this.etype = head;
            inbox.For< NodeProxy >( true, OnInputOperant );
        } );

        inputOfTile = tile.grid.tiles[ tile.entity.Heads( p => p == "input_for" ).First() ];
        inputOfLine = ( Instantiate( tile.inputLineAsset ) as GameObject ).GetComponent< HexGridLine >();
        inputOfLine.grid = tile.grid;
        inputOfLine.transform.parent = transform;
        inputOfLine.tail = this.transform;
        inputOfLine.head = inputOfTile.transform;
        Consumes();
        SetMaterial();
    }

    void OnInputString ( Outbox outbox, string value ) {    
        tile.value.text = value;
        if(tile.label.text == "comment") {
            tile.label.text = "comment\n(hover to view)";
        }
        if(tile.label.text == "comment\n(hover to view)") {  //Added this here, work in progress. -z26
            tile.value.gameObject.SetActive(false);
        }
    }
    
    void OnInputAmount ( Outbox outbox, List< float > value ) {
        var sum = Util.Sum(value);// value.Sum( v => v );
        tile.value.text = sum.ToString( "0.###" );
    }
    
    void OnInputColor ( Outbox outbox, Color value ) {
        tile.value.text = Util.ColorToProperty( value );
    }
    
    void OnInputPart ( Outbox outbox, PartProxy part ) {
        if ( part != default(PartProxy) ) {
            tile.value.text = part.entity.Heads( p => p == "as" ).First().id;
        }
        else {
            tile.value.text = "";
        }
    }

    void OnInputOperant ( Outbox outbox, NodeProxy operant ) {
        if ( operant != default(NodeProxy) ) {
            tile.value.text = operant.entity.Heads( p => p == "as" ).First().id;
        }
        else {
            tile.value.text = "";
        }
    }

    void OnInputEntity ( Outbox outbox, Entity entity ) {
        tile.value.text = entity.Heads( p => p == "as" ).First().id;
    }

    public override void SetMaterial( ) {
        tile.renderer.sharedMaterial = tile.grid.inputMaterial;
    }

    public override void DeleteStep (bool step) {
        if ( inbox != null ) {
            var deletedSomething = false;
            var outboxes = inbox.Proxy.entity.Heads( p => p == "consumes" )
                .Where( o => o.proxy != null )
                    .Select( e => e.proxy.GetComponent< Outbox >() ).ToList();
            var proxies = inbox.Proxy.entity.Heads( p => p == "input_for" ).Select( e => e.proxy );
            if(outboxes.Count > 0){
                foreach ( var proxy in proxies ) {
                    proxy.OnInboxDisconnect( inbox );
                }
            }
            foreach ( var o in outboxes ) {
                deletedSomething = step;
                o.inboxes.Remove( inbox );
                inbox.Proxy.entity.Relationships()
                    .Where( r => r.Tail == inbox.Proxy.entity && r.Predicate == "consumes" && r.Head == o.Proxy.entity )
                        .First().Break();
            }
            if ( !deletedSomething ) {
                foreach ( var proxy in proxies ) {
                    proxy.OnInboxDestroy( inbox );
                }
                var input_for = inbox.Proxy.entity.Relationships()
                    .Where( r => r.Tail == inbox.Proxy.entity && r.Predicate == "input_for" ).First();
                input_for.Break();
                tile.DestroyTile();
            }
            Consumes();
        }
    }

    public override void OnMouseUpCustom ( TouchData touch ) {
        base.OnMouseUpCustom( touch );
        var leftControl = Input.GetKey( KeyCode.LeftControl );
        var leftAlt = Input.GetKey( KeyCode.LeftAlt );
        var leftShift = Input.GetKey( KeyCode.LeftShift );
        if ( touch.dragging && leftControl && !leftAlt && !leftShift) {
            var other = GetTileAtTouchPosition(touch);
            if(other != null && other != tile && other.tileType == HexGridTileType.Output){
                ConnectIOs(tile,other);
            }
        }
    }

    public override void RequestOnConsole () {
        var leftControl = Input.GetKey( KeyCode.LeftControl );
        var leftAlt = Input.GetKey( KeyCode.LeftAlt );
        var leftShift = Input.GetKey( KeyCode.LeftShift );

        if(!leftControl && !leftAlt && !leftShift && Gaia.scene > 0){
            Sec.ForPermission( tile.entity, Sec.Permission.edit, () => {
                onConsole();
            }, () => {
                Console.PushError("tile", new string[]{  "access denied - check permissions" } );
            } );
        }
    }

    public override void OnDestroyEventHandler () {
        if ( inbox != null ) {
            inbox.RemoveAction< string >( OnInputString );
            inbox.RemoveAction< List< float > >( OnInputAmount );
            inbox.RemoveAction< Color >( OnInputColor );
            inbox.RemoveAction< PartProxy >( OnInputPart );
            inbox.RemoveAction< NodeProxy >( OnInputOperant );
        }
    }

}
