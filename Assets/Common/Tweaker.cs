using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class Tweaker : MonoBehaviour {

    public enum ColorComponent {
        Hue,
        Saturation,
        Brightness,
        Alpha,
    }

    [System.Serializable]
    public class ColorTween {
        public string colorName;
        public ColorComponent colorComponent;
        public Waveform colorWave;
        internal float initialValue;
        internal Color initialColor;
    }

    public Vector3 angularAxis;
    public Waveform angularWave;
    public Vector3 linearAxis;
    public Waveform linearWave;
    public Vector3 scaleAxis;
    public Waveform scaleWave;
    Quaternion initialRotation;
    Vector3 initialPosition;
    public Vector3 initialScale;
    public List< ColorTween > colorTweens;

    void Start () {
        initialRotation = transform.localRotation;
        initialPosition = transform.localPosition;
        initialScale = transform.localScale;
        foreach ( var colorTween in colorTweens ) {
            colorTween.initialColor = GetComponent<Renderer>().material.GetColor( colorTween.colorName );
            colorTween.initialValue = GetComponentValue( colorTween.colorComponent, colorTween.initialColor );
        }
    }

    void Update () {
        float angularTween = angularWave.Sample( Time.deltaTime ).sample;
        transform.localRotation = initialRotation * Quaternion.AngleAxis( angularTween, angularAxis );
        float linearTween = linearWave.Sample( Time.deltaTime ).sample;
        transform.localPosition = initialPosition + linearAxis * linearTween;
        float scaleTween = scaleWave.Sample( Time.deltaTime ).sample;
        transform.localScale = initialScale + scaleAxis * scaleTween;
        foreach ( var colorTween in colorTweens ) {
            var componentTween = colorTween.colorWave.Sample( Time.deltaTime ).sample;
            var componentValue = colorTween.initialValue + componentTween;
            var color = SetComponentValue( colorTween.colorComponent, colorTween.initialColor, componentValue );
            GetComponent<Renderer>().material.SetColor( colorTween.colorName, color );
        }
    }

    float GetComponentValue ( ColorComponent component, Color color ) {
        var hsb = new HSBColor( color );
        if ( component == ColorComponent.Hue ) {
            return hsb.h * 360;
        }
        else if ( component == ColorComponent.Saturation ) {
            return hsb.s * 100;
        }
        else if ( component == ColorComponent.Brightness ) {
            return hsb.b * 100;
        }
        else if ( component == ColorComponent.Alpha ) {
            return hsb.a * 100;
        }
        return 0;
    }

    Color SetComponentValue ( ColorComponent component, Color color, float value ) {
        var hsb = new HSBColor( color );
        if ( component == ColorComponent.Hue ) {
            hsb.h = value / 360;
        }
        else if ( component == ColorComponent.Saturation ) {
            hsb.s = value / 100;
        }
        else if ( component == ColorComponent.Brightness ) {
            hsb.b = value / 100;
        }
        else if ( component == ColorComponent.Alpha ) {
            hsb.a = value / 100;
        }
        return hsb.ToColor();
    }
}
