using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*by z26
This script displays a description when the mouse is hovering on top of an operand.
I want to make visual programming more powerful in the future but more powerful oftens equals more complicated.
Hopefully this feature can allow newbies to only need a simple tutorial to get into rawbots regardless.


Despite describing the operands on the hex grid, the description is displayed in the overlay layer.
Its size is therefore unaffected by zooming in/out the vp grid.*/


public class TileDescript3D : MonoBehaviour {

	public GameObject tPanel;
	public TextMesh tMesh;
	public Camera gridCam;
	HexGridTile currentTile;
	float initialZoom;
	public int hoverTimer;
	float ratio = 1; //Compensates for zooming in/out the grid.

	void Start () {
		initialZoom = gridCam.orthographicSize;
	}
	
	// Update is called once per frame
	void Update () {

		//Reset if tile isnt visible.
		if(currentTile != null && currentTile.gameObject.activeInHierarchy == false) {
			hoverTimer = 0;
			currentTile = null;
		}

		if (currentTile) {
			hoverTimer++;
		} else {
			hoverTimer = 0;
		}

		if (hoverTimer > 30 ) {

			tMesh.GetComponent<Renderer>().enabled = true;
			tPanel.GetComponent<Renderer>().enabled = true;

			ratio = gridCam.orthographicSize / initialZoom;
			tMesh.transform.localScale = new Vector3(ratio, ratio, 1f);


			float halfHeight = tPanel.GetComponent<Renderer>().bounds.size.y / 2;
			float halfTileHeight = currentTile.renderer.bounds.size.y / 2;
			float border = 0.2f*ratio;
			float offset = -(halfHeight + halfTileHeight + border/2);
			transform.position = currentTile.transform.position + new Vector3 (0f,offset,0f);


			//Resize the background panel to match the text + margins.
			var bounds = tMesh.GetComponent<Renderer>().bounds;
			tPanel.transform.localScale = new Vector3(bounds.size.x + border*2, bounds.size.y + border*2, 1f);

		} else {
			tMesh.GetComponent<Renderer>().enabled = false;
			tPanel.GetComponent<Renderer>().enabled = false;
		}	
	}






	 protected void setText(string input) {
	 /*Found online. https://answers.unity.com/questions/223906/textMesh-wordwrap.html 
	 Text wrapping text isnt properly supported by text meshes, so
	 I'm using this hack that computes the text mesh each time a new word is added.
	 A newline is added if the text is wider than a threshold.*/    
	//Turn fake newlines in assembly.txt into real ones.
	var inputText = input.Replace("\\n","\n");

	float MaxWidth = 6f * ratio;
     
    string[] words = inputText.Split(new char [] {' ','\n'} );
    string temp = "";
    tMesh.text = "";

    for (int i = 0; i < words.Length; i++) {
		temp = tMesh.text;
		if (i > 0) { tMesh.text += " "; } //Dont put a space before the first word.
    	tMesh.text += words[i];

		
		//The "&& i > 0" is in order to never put a newline before the 1st word.
		if (tMesh.GetComponent<Renderer>().bounds.size.x > MaxWidth && i > 0) {
        	temp += "\n";
        	temp += words[i];
        	tMesh.text = temp;
        }
       
	}
   }
   


	public void showDescript(HexGridTile tile, string description)
	{
		currentTile = tile;
		setText(description);
	}

}
