using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Operants;

//Rewritten by z26.

public class Laser : MonoBehaviour {

    //Hide all of these, cause the laserProxy dictates what they are.
    [HideInInspector]
    public int damageEachFrame;
    [HideInInspector]
    public float inputLength;
    [HideInInspector]
    public float maxLength;
    [HideInInspector]
    public float thickness;
    [HideInInspector]
    public Color beamColor;

    public float currentLength { get; private set; } //read-only

    public new Light light;
    public Transform top;
    public Transform body;
    public Transform core;
    public Transform laserBeam;
    public GameObject contactFXAsset;
    public AudioClip[] hits;
    public AudioClip[] swings;
    public AudioSource hitAudio;
    public AudioSource swingSource;
    public List<Renderer> beamRenderers;
    Vector3 bodyInitialPos;
    float initialLightIntensity;
    float initialLightRange;
    float worldLength;
    public Color defaultColor = new Color( 1, 0.13f, 0, 0.2f );
	public Renderer bodyRenderer;

    public BulletLayers raycastGroup = BulletLayers.Part;
    public BulletLayers raycastMask = BulletLayers.Part | BulletLayers.HexBlock | BulletLayers.World | BulletLayers.Structure;
    BulletRaycastHitDetails hitDetails;
    public PartProxy detected;
    float lastFx;

    void Awake () {
        beamColor = defaultColor;
        SetBeamColor();
    }

    void Start () {
        bodyInitialPos = body.localPosition;
        initialLightIntensity = light.intensity * 10;
        initialLightRange = light.range * 10;
    }

    void OnBuild(){
    }
 
    void FixedUpdate () {
        float weight = Mathf.Clamp(inputLength / Bullet.instance.worldScale / maxLength, 0, 1);
        light.intensity = weight * initialLightIntensity*0.1f;
        light.range = weight * initialLightRange;
        SetBeamColor();

        //Laser is always at least 1 unit long. This is so that the beginning of the raycast is inside the
        //laser's body. Raycasts don't detect a hit with a shape if they start from inside that shape.
        worldLength = Mathf.Clamp(inputLength / Bullet.instance.worldScale, 0 , maxLength);
        worldLength++;

        Raycast();
        Woosh();
    }


    void Raycast() {
        var beam = new Ray (laserBeam.position, laserBeam.forward);
        var length = worldLength;
        var result = Bullet.Raycast (beam, out hitDetails, length, raycastGroup, raycastMask );
        detected = default(PartProxy);

        if(Mathf.Approximately(inputLength, 0)) {
            Resize(0);
            currentLength = 0;
            return; //Don't raycast and hide laser.
        }

        if (result) {
            var plasma = hitDetails.bulletRigidbody.gameObject.GetComponent<PlasmaBlast>();

            if (damageEachFrame != 0) {
                hitDetails.bulletRigidbody.SendMessage("OnHit", damageEachFrame, SendMessageOptions.DontRequireReceiver );
                Sparkle();
                
                if(plasma != null) {//Destroy if object is a plasma projectile.
                    plasma.Impact(hitDetails.point, plasma.gameObject.transform.rotation);
                }
            }
            
            //Return part output.
            if(plasma == null && hitDetails.bulletRigidbody.gameObject.GetComponent<NodeView>().proxy is PartProxy) {
                detected = (PartProxy)hitDetails.bulletRigidbody.gameObject.GetComponent<NodeView>().proxy;
            }
        }

        float interruptLength = Vector3.Distance (laserBeam.position, hitDetails.point);
        Resize (Mathf.Clamp(interruptLength, 1, worldLength ));
    }


    void Resize(float length) {

            if(damageEachFrame == 0) {
                core.gameObject.SetActive(false); //hide bright core if laser is harmless.
            } else {
                core.gameObject.SetActive(true);
            }

            laserBeam.localScale = new Vector3( thickness, thickness, length );
            currentLength = (laserBeam.localScale.z-1) * Bullet.instance.worldScale;

            top.localScale = new Vector3( thickness, 1, thickness);//Different order because transform oriented differently.
            top.localPosition = laserBeam.localPosition + new Vector3( 0, length, 0);
    }


    void Sparkle() {
        if(Time.time - lastFx > 0.02f){
            Instantiate( contactFXAsset, hitDetails.point, Quaternion.identity );
            lastFx = Time.time;
        }
        hitAudio.clip = hits[ Random.Range( 0, hits.Length ) ];
        hitAudio.pitch = Random.Range( 0.9f, 1.5f );
        hitAudio.Play();
    }



    void Woosh() {
            var angularVelocity = GetComponent<BulletRigidBody>().GetAngularVelocity().magnitude;
        if ( angularVelocity > 1 ) {
            if ( !swingSource.isPlaying ) {
                swingSource.Play();
            }
            swingSource.volume = angularVelocity / 10;
            swingSource.pitch = Mathf.Clamp( angularVelocity / 5, 0.8f, 1.2f );
        }
        else if ( angularVelocity < 1 && swingSource.isPlaying ) {
            swingSource.Stop();
        }
    }


    public void SetBeamColor () {
        beamColor.a = defaultColor.a;
        beamRenderers.ForEach( r => r.material.SetColor( "_TintColor", beamColor ) );
        light.color = beamColor;
    }

}
