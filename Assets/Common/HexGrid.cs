using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;

public enum HexGridTileType {
    Operant,
    Part,
    Input,
    Output,
    OperantAdder,
    InputAdder,
    OutputAdder,
    PartAdder,
    Empty,
    HexElement,
    TileAction,
    StructureType
}

public class HexGrid : MonoBehaviour {

    public enum GridState {
        Idle,
        Subgrid,
        Selecting,
        Selected,
        PivotSet,
        Dragging,
        Clear
    }

    public GridState state = GridState.Idle;
    public new GridCamera camera;
    public Console console;
    public GameObject tileAsset;
    public Material partMaterial;
    public Material operantMaterial;
    public Material inputMaterial;
    public Material outputMaterial;
    public Material actionMaterial;
    public GameObject layerObject; //that grid is the gray backdrop when spawning stuff -z26
    public Dictionary< HexGridTile, Vector3 > selected = new Dictionary<HexGridTile, Vector3>();
    public Dictionary< string, string[] > ioValidations = new Dictionary< string, string[] > {
        { "output_float",   new [] { "input_amount" } },
        { "output_part",    new [] { "input_part" } },
        { "output_color",   new [] { "input_color" } },
        { "output_string",  new [] { "input_string" } },
        { "output_label",   new [] { "input_label" } },
        { "output_texture", new [] { "input_texture" } },
    };

    void Start () {
        transform.parent = camera.transform;
        transform.localPosition = new Vector3( 0, 0, 15 );
        transform.localRotation = Quaternion.Euler( 0, 180, 0 );
        layerObject.SetActive( false );
    }

    public void ShowGrid ( Entity entity ) {
        if ( entity != null ) {
            GenerateTiles( entity );
        }
    }

    Vector3 stride = new Vector3( 1.7f, 2, 2 );

    public Vector3 ToTiling ( Vector3 position ) {
        var tiling = position;
        tiling.x = Mathf.Round( position.x / stride.x );
        tiling.y = Mathf.Round( position.y / stride.y + ( tiling.x % 2 == 0 ? 0 : 0.5f ) );
        tiling.z = Mathf.Round( position.z / stride.z );
        return tiling;
    }

    public Vector3 FromTiling ( Vector3 tiling ) {
        float x = ( int )tiling.x;
        float y = ( int )tiling.y - ( x % 2 == 0 ? 0 : 0.5f );
        float z = ( int )tiling.z;
        return new Vector3( x * stride.x, y * stride.y, z * stride.z );
    }

    public Dictionary< Entity, HexGridTile > tiles = new Dictionary< Entity, HexGridTile >();
    public List< HexGridTile > partTiles = new List< HexGridTile >();

    public void GenerateTilesParts ( Vector3 pivot, Vector3 spawnPosition, Quaternion spawnRotation ) {
        HideGrid();
        layerObject.SetActive( true );
        var loadedParts = Gaia.Instance().entities[ "part" ].Tails( p => p == "is" );
        var spawnableParts = loadedParts.Where(p => console.spawnableParts.Contains(p.id));
        //(to set spawning godmode, use loadedParts in this foreach.) -z26
        var position = FromTiling( pivot );
        foreach ( var part in spawnableParts ) {
            var tiling = FindAvailableSpaceAround( pivot + new Vector3( 0, 0, 1 ) );
            var tileObj = Instantiate( tileAsset ) as GameObject;
            tileObj.transform.parent = transform;
            tileObj.transform.localPosition = position;
            tileObj.transform.localRotation = Quaternion.identity;
            var tile = tileObj.GetComponent< HexGridTile >();
            tile.tileType = HexGridTileType.PartAdder;
            tile.entity = part; //z26
            tile.tiling = tiling;
            tile.grid = this;
            tile.spawnPosition = spawnPosition;
            tile.spawnRotation = spawnRotation;
            tile.targetTiling = pivot;
            tiles[ tile.entity ] = tile;
            partTiles.Add( tile );
        }
    }

    public List< HexGridTile > operantTiles = new List< HexGridTile >();

    public void GenerateTilesOperants ( Vector3 pivot ) {
        HideGrid();
        layerObject.SetActive( true );
        var operants = Gaia.Instance().entities[ "operant" ].Tails( p => p == "is" )
            .Where( e => !e.Heads( p => p == "hidden_from" ).Any() );
        var position = FromTiling( pivot );
        foreach ( var operant in operants ) {
            var tiling = FindAvailableSpaceAround( pivot + new Vector3( 0, 0, 1 ) );
            var tileObj = Instantiate( tileAsset ) as GameObject;
            tileObj.transform.parent = transform;
            tileObj.transform.localPosition = position;
            tileObj.transform.localRotation = Quaternion.identity;
            var tile = tileObj.GetComponent< HexGridTile >();
            tile.tileType = HexGridTileType.OperantAdder;
            tile.grid = this;
            tile.entity = operant;
            tile.tiling = tiling;
            tile.targetTiling = pivot;
            tiles[ operant ] = tile;
            operantTiles.Add( tile );
        }
    }


    public List< HexGridTile > tileActionTiles = new List< HexGridTile >();

    public void GenerateTilesActions ( HexGridTile tile ) {
        HideGrid();
        var pivot = tile.tiling;
        layerObject.SetActive( true );
        var position = FromTiling( pivot );
        foreach ( var action in HexGridTile.actions ) {
            var tiling = FindAvailableSpaceAround( pivot + new Vector3( 0, 0, 1 ) );
            var tileObj = Instantiate( tileAsset ) as GameObject;
            tileObj.transform.parent = transform;
            tileObj.transform.localPosition = position;
            tileObj.transform.localRotation = Quaternion.identity;
            var tileAction = tileObj.GetComponent< HexGridTile >();
            tileAction.tileType = HexGridTileType.TileAction;
            tileAction.grid = this;
            tileAction.actionTarget = tile;
            tileAction.entity = new Entity( action );
            tileAction.tiling = tiling;
            tileAction.targetTiling = pivot;
            tiles[ tileAction.entity ] = tileAction;
            tileActionTiles.Add( tileAction );
        }
    }


    public List< HexGridTile > hexElementTiles = new List< HexGridTile >();
    
    public void GenerateHexElementOperants ( Vector3 pivot ) {
        HideGrid();
        layerObject.SetActive( true );
        var elements = System.Enum.GetNames(typeof(HexElement));

        var position = FromTiling( pivot );
        for ( int i = 0 ; i < elements.Length ; ++i) {
            var element = elements[i];
            var tiling =  FindAvailableSpaceAround(pivot + new Vector3( 0, -i, 0 ),true);
            var tileObj = Instantiate( tileAsset ) as GameObject;
            tileObj.transform.parent = transform;
            tileObj.transform.localPosition = position;
            tileObj.transform.localRotation = Quaternion.identity;
            var tile = tileObj.GetComponent< HexGridTile >();
            tile.tileType = HexGridTileType.HexElement;
            tile.grid = this;
            tile.entity = new Entity(element);
            tile.tiling = tiling;
            tile.targetTiling = pivot;
            tiles[ tile.entity ] = tile;
            hexElementTiles.Add( tile );
        }
    }

    public List< HexGridTile > structureTiles = new List< HexGridTile >();

    public void GenerateStructureTiles ( Vector3 pivot ) {
        HideGrid();
        layerObject.SetActive( true );
        var structures = System.Enum.GetNames(typeof(Structures));

        var position = FromTiling( pivot );
        for ( int i = 0 ; i < structures.Length ; ++i) {
            var structure = structures[i];
            var tiling =  FindAvailableSpaceAround(pivot + new Vector3( 0, -i, 0 ),true);
            var tileObj = Instantiate( tileAsset ) as GameObject;
            tileObj.transform.parent = transform;
            tileObj.transform.localPosition = position;
            tileObj.transform.localRotation = Quaternion.identity;
            var tile = tileObj.GetComponent< HexGridTile >();
            tile.tileType = HexGridTileType.StructureType;
            tile.grid = this;
            tile.entity = new Entity(structure);
            tile.tiling = tiling;
            tile.targetTiling = pivot;
            tiles[ tile.entity ] = tile;
            structureTiles.Add( tile );
        }
    }


    public List< HexGridTile > ioTiles = new List< HexGridTile >();

    public void GenerateTilesIO ( HexGridTile ioTile ) {
        HideGrid();
        layerObject.SetActive( true );

        var ios = ioTile.entity.Tails( p => p == "input_for" || p == "output_for" )
            .SelectMany( io => io.Heads( p => p == "as" ) );
        var inputs = new List< Entity >();
        var outputs = new List< Entity >();
        var types = new List< Entity >();
        ioTile.entity.ForHeads( Util.AsIs, head => types.Add( head ) );
        types = types.Where( t => t.Heads( p => p == "as" ).Count() == 0 ).ToList();
        foreach ( var e in types.SelectMany( e => e.Tails( p => p == "input_for" ) )
            .Where( e => !e.Heads( p => p == "hidden_from" ).Any() ) ) {
            inputs.Add( e );
        }
        foreach ( var e in types.SelectMany( e => e.Tails( p => p == "output_for" ) )
            .Where( e => !e.Heads( p => p == "hidden_from" ).Any() ) ) {
            outputs.Add( e );
        }
        inputs = inputs.Where( i => !ios.Any( io => io == i ) ).ToList();
        outputs = outputs.Where( o => !ios.Any( io => io == o ) ).ToList();

        var pivot = ioTile.tiling;
        var position = FromTiling( pivot );
        foreach ( var input in inputs ) {
            var tiling = FindAvailableSpaceAround( pivot + new Vector3( 0, 0, 1 ) );
            var tileObj = Instantiate( tileAsset ) as GameObject;
            tileObj.transform.parent = transform;
            tileObj.transform.localPosition = position;
            tileObj.transform.localRotation = Quaternion.identity;
            var tile = tileObj.GetComponent< HexGridTile >();
            tile.tileType = HexGridTileType.InputAdder;
            tile.grid = this;
            tile.entity = input;
            tile.tiling = tiling;
            tile.ioTarget = ioTile;
            tiles[ input ] = tile;
            ioTiles.Add( tile );
        }
        foreach ( var output in outputs ) {
            var tiling = FindAvailableSpaceAround( pivot + new Vector3( 0, 0, 1 ) );
            var tileObj = Instantiate( tileAsset ) as GameObject;
            tileObj.transform.parent = transform;
            tileObj.transform.localPosition = position;
            tileObj.transform.localRotation = Quaternion.identity;
            var tile = tileObj.GetComponent< HexGridTile >();
            tile.tileType = HexGridTileType.OutputAdder;
            tile.grid = this;
            tile.entity = output;
            tile.tiling = tiling;
            tile.ioTarget = ioTile;
            tiles[ output ] = tile;
            ioTiles.Add( tile );
        }
    }

    void GenerateTiles ( Entity entity) {
        HexGridTile tile = null;
        if(entity.deprecated || tiles.TryGetValue( entity, out tile )){
            return;
        }

        CreateTile( entity );
       
        foreach ( var r in entity.Relationships().Where( r =>
            r.Predicate == "input_for" ||
            r.Predicate == "output_for" ||
            r.Predicate == "consumes" ) ) {
            var e = r.Head == entity ? r.Tail : r.Head;
            GenerateTiles( e );
        }
    }

    public HexGridTile FindTileAt ( Vector3 tiling ) {
        foreach ( var tile in tiles.Values ) {
            if ( Mathf.Approximately( tile.tiling.z, tiling.z ) ) {
                if ( ( tile.tiling - tiling ).sqrMagnitude < 1f ) {
                    return tile;
                }
            }
        }
        return null;
    }

    void CreateTile(Entity entity){

        UnityEngine.Profiling.Profiler.BeginSample( "FindAvailableSpaceAround()" );
        var tiling = Vector3.zero;// = FindAvailableSpaceAround( Vector3.zero, true ); This line seems useless and terrible for performance -z26
        UnityEngine.Profiling.Profiler.EndSample();

        entity.ForInferredProperty< string >( "tiling", Util.AsIs, tilingProperty => {
            tiling = Util.PropertyToVector3( tilingProperty );
        } );


        var tileObj = Instantiate( tileAsset ) as GameObject;
        tileObj.transform.parent = transform;
        tileObj.transform.localPosition = Vector3.zero;
        tileObj.transform.localRotation = Quaternion.identity;
        var tile = tileObj.GetComponent< HexGridTile >();
        tile.grid = this;
        tile.entity = entity;
        tile.tiling = tiling;
        tiles[ entity ] = tile;
    }

    //This method (has something to do with placing tiles at specific spots) is terrible for
    //performance when loading large vp programs, I assume because of all the math.
    //It isn't even necessary as far as I know, so I simply removed the line calling it. -z26
    public Vector3 FindAvailableSpaceAround ( Vector3 tiling, bool useCenter = false ) {
        var position = FromTiling( tiling );
        var start = useCenter ? 0 : 2;
        for ( var d = start; d < 20; d += 2 ) {
            for ( var i = 0f; i < Mathf.PI * 2; i += Mathf.PI / 10 ) {
                var dir = new Vector3( Mathf.Sin( i ), Mathf.Cos( i ), 0 );
                var newTiling = ToTiling( position + dir.normalized * d );
                var other = FindTileAt( newTiling );
                if ( other == null ) {
                    var worldPos = transform.TransformPoint( FromTiling( newTiling ) );
                    var viewportPoint = camera.GetComponent<Camera>().WorldToViewportPoint( worldPos );
                    if ( viewportPoint.x < 0.03f || viewportPoint.x > 0.98f ||
                        viewportPoint.y < 0.1f || viewportPoint.y > 0.95f ) {
                        continue;
                    }
                    return newTiling;
                }
            }
        }
        return tiling;
    }

    public void HideGrid () {
        foreach ( var tile in operantTiles ) {
            tiles.Remove( tile.entity );
            Destroy( tile.gameObject );
        }
        foreach ( var tile in ioTiles ) {
            tiles.Remove( tile.entity );
            Destroy( tile.gameObject );
        }
        foreach ( var tile in partTiles ) {
            tiles.Remove( tile.entity );
            Destroy( tile.gameObject );
        }
        foreach ( var tile in hexElementTiles ) {
            tiles.Remove( tile.entity );
            Destroy( tile.gameObject );
        }
        foreach ( var tile in tileActionTiles ) {
            tiles.Remove( tile.entity );
            Destroy( tile.gameObject );
        }
        foreach ( var tile in structureTiles ) {
            tiles.Remove( tile.entity );
            Destroy( tile.gameObject );
        }
        operantTiles.Clear();
        ioTiles.Clear();
        partTiles.Clear();
        hexElementTiles.Clear();
        tileActionTiles.Clear();
        structureTiles.Clear();
        layerObject.SetActive( false );
    }

    IEnumerator CreateTask ( Vector3 target, Vector3 start, System.Action< Entity > pre, System.Action< Entity > post ) {

        var entity = new Entity();
        entity.SetProperty( "target", Util.Vector3ToProperty( target ) );
        Gaia.Instance().recentlyAddedEntities.Add( entity );
        entity.SetProperty( "permissions", ( float )Sec.Permission.all );
        pre( entity );

        while ( Gaia.Instance().recentlyAddedEntities.IndexOf( entity ) >= 0 ) {
            yield return 0;
        }

        post( entity );

        var tileObj = Instantiate( tileAsset ) as GameObject;
        tileObj.transform.parent = transform;
        tileObj.transform.localPosition = FromTiling( start );
        tileObj.transform.localRotation = Quaternion.identity;

        var tile = tileObj.GetComponent< HexGridTile >();
        tile.tiling = target;
        tile.entity = entity;
        tile.grid = this;
        entity.SetProperty( "tiling", Util.Vector3ToProperty( target ) );
        tiles[ entity ] = tile;

        yield return 0;

        tile.handler.onConsole();
    }

    public void CreateOperant ( string operant, Vector3 target, Vector3 start ) {
        StartCoroutine( CreateTask( target, start, entity => {
            new Relationship( entity, "as", Gaia.Instance().entities[ operant ] );
        }, entity => {
            var inputSampler = entity.proxy as InputSamplerProxy;
            if ( inputSampler != default( InputSamplerProxy ) ) {
                inputSampler.hasFocus = true;
            }
        } ) );
    }

    public void CreateInput ( Entity input, Entity input_for, Vector3 target, Vector3 start ) {
        if ( !tiles.ContainsKey( input_for ) ) {
            return;
        }
        if ( input_for.Tails( p => p == "input_for" )
            .SelectMany( t => t.Heads( p => p == "as" ) )
            .Any( a => a == input ) ) {
            return;
        }
        StartCoroutine( CreateTask( target, start, entity => {
            new Relationship( entity, "as", input );
            new Relationship( entity, "input_for", input_for );
        }, entity => {
            var inbox = entity.proxy.GetComponent< Inbox >();
            input_for.proxy.OnInbox( inbox );
        } ) );
    }

    public void CreateOutput ( Entity output, Entity output_for, Vector3 target, Vector3 start ) {
        if ( !tiles.ContainsKey( output_for ) ) {
            return;
        }
        if ( output_for.Tails( p => p == "output_for" )
            .SelectMany( t => t.Heads( p => p == "as" ) )
            .Any( a => a == output ) ) {
            return;
        }
        StartCoroutine( CreateTask( target, start, entity => {
            new Relationship( entity, "as", output );
            new Relationship( entity, "output_for", output_for );
        }, entity => {
            var outbox = entity.proxy.GetComponent< Outbox >();
            output_for.proxy.GetComponent< IOComponent >().outboxes.Add( outbox );
            entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                outbox.property = property;
            } );
        } ) );
    }

}
