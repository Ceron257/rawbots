using UnityEngine;
using System.Collections;

public class VoltaicAnim : MonoBehaviour {

	public Material voltaic;
	public float speed = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		var offset = voltaic.mainTextureOffset;
		offset.x += Time.deltaTime * speed;
		voltaic.mainTextureOffset = offset;
	}
}
