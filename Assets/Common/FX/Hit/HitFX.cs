using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class HitFX : MonoBehaviour {

	public List< GameObject > planes;
	public List< AudioClip> clips;

	static int vfxIndex = 0;
	static int sfxIndex = 0;

	void Start () {
		Destroy( GetComponent<Light>(), 0.1f );
		for ( int i = 0; i < planes.Count; ++i ) {
			if ( i != vfxIndex ) {
				planes[ i ].GetComponent<Renderer>().enabled = false;
			}else{
                var position = Vector3.zero;
                position.x = UnityEngine.Random.Range(-1f,1f);
                position.y = UnityEngine.Random.Range(-1f,1f);
                position.z = UnityEngine.Random.Range(-1f,1f);
                planes[i].transform.localPosition = position;
            }
			Destroy( planes[ i ], 0.2f );
		}
		++vfxIndex;
		if ( vfxIndex >= planes.Count ) {
			vfxIndex = 0;
		}
		Destroy( gameObject, 1.5f );
		GetComponent<AudioSource>().clip = clips[ Random.Range( 0, clips.Count ) ];
		GetComponent<AudioSource>().Play();
		++sfxIndex;
		if ( sfxIndex >= clips.Count ) {
			sfxIndex = 0;
		}
	}
	
}
