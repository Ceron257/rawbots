using UnityEngine;
using System.Collections;

public class EditAttachFailed : MonoBehaviour {

    public new GameObject light;

    void Start () {
        Destroy( light, 0.3f );
        Destroy( gameObject, 2f );
    }
    
}
