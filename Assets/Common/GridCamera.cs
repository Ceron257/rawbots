using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;

public class GridCamera : MonoBehaviour {

    enum MouseButton{
        none,
        left,
        right,
        middle,
    }

    class TouchContext {
        public Camera camera;
        public LayerMask mask;
        public GameObject pickObject;
        public GameObject hoverObject;
        public Highlight highlight;
        public bool dragging = false;
        public MouseButton button = MouseButton.none;
    }

    public enum GridType {
        Programming,
        SpawnPart,
        HexCreation,
        StructureCreation,
    }

    enum TouchCamera {
        Grid = 0,
        World = 1,
    }

    public PID gridPID;
    public LayerMask pickMask;
    public GameCamera gameCamera;
    public Camera pinned;
    public Console console;
    Vector3 oldMousePosition = Vector3.zero;
    public HexGrid grid;
    HexGrid backupGrid;
    Vector3 startOfSelection;
    Vector3 endOfSelection;
    GameObject deletee;
    public Vector3 gridPosition;
    public GameObject tileAsset;
    List< TouchContext > touchContexts = new List< TouchContext >();
    HexGridTile tile = default(HexGridTile);
    public GridType activeGrid = GridType.Programming;
    float defaultCameraSize;
    float backUpSize;
    GameObject selectionPlane;

    public TileDescript3D tileDescript; //z26

    void Awake () {
        var mode = new GameModeManager.Mode(){
            id = GameModeManager.ModeId.Grid,
            transitions = new List<GameModeManager.Transition>(){
                new GameModeManager.Transition(){ key = KeyCode.Escape , state = GameModeManager.KeyState.KeyUp , mode = GameModeManager.ModeId.World},
            }};
        GameModeManager.RegisterMode( mode );
        backUpSize = defaultCameraSize = GetComponent<Camera>().orthographicSize;
        ResetGrid();
    }

    void Start () {
        touchContexts = new List< TouchContext > {
            new TouchContext() { camera = GetComponent<Camera>(), mask = pickMask },
            new TouchContext() { camera = gameCamera.GetComponent<Camera>(), mask = 1 << LayerMask.NameToLayer("part")},
        };
    }

    void Update () {

        if ( GameModeManager.InMode( m => m != GameModeManager.ModeId.Grid 
                                    && m != GameModeManager.ModeId.CreateStructure
                                    && m != GameModeManager.ModeId.CreateAddon
                                    && m != GameModeManager.ModeId.SpawnPart ) ) {
            foreach ( var tx in touchContexts ) {
                if ( tx.highlight != default( Highlight ) ) {
                    tx.highlight.SetHighlight( false );
                    tx.highlight = default( Highlight );
                }
                if ( tx.hoverObject != null ) {
                    tx.hoverObject.SendMessage( "OnMouseExitCustom", new TouchData(), SendMessageOptions.DontRequireReceiver );
                    tx.hoverObject = null;
                }
            }
            grid.gameObject.SetActive( false ); //Stop rendering to spare gpu resources.-z26
            return;
        }
        grid.gameObject.SetActive( true ); //z26

        var touch = new TouchData();
        touch.deltaPosition = oldMousePosition - Input.mousePosition;
        touch.position = Input.mousePosition;

        var prevtx = default( TouchContext );

        for ( int i = 0; i < touchContexts.Count; ++i ) {
            var tx = touchContexts[ i ];
            var cast = true;
            if ( prevtx != null ) {
                if ( prevtx.dragging || prevtx.hoverObject != null ) {
                    cast = false;
                }
            }

            var pickRay = tx.camera.ScreenPointToRay( Input.mousePosition );
            touch.camera = tx.camera;
            touch.dragging = tx.dragging;
            touch.touchId = ( int )tx.button;
            GameObject hitObject = null;
            RaycastHit hit;
            BulletRaycastHitDetails hitResult = null;

            bool wasHit = false;
            if ( i == ( int )TouchCamera.World && GameModeManager.InMode( m => m == GameModeManager.ModeId.Grid ) ) {
                wasHit = Bullet.Raycast( pickRay, out hitResult, 200, ( BulletLayers.Part | BulletLayers.Structure), ( BulletLayers.Part | BulletLayers.Structure) );//change this map to allow loading terrain hexes in the grid -z26
                if ( cast && wasHit ) {
                    touch.worldPosition = hitResult.point;
                    var highlight = hitResult.bulletRigidbody.nodeview.highlight;
                    hitObject = highlight.gameObject;
                }
            }
            else {
                if ( cast && Physics.Raycast( pickRay, out hit, 200, tx.mask ) ) {
                    touch.worldPosition = hit.point;
                    hitObject = hit.collider.gameObject;
                }
            }

            if ( tx.pickObject != null && !Input.GetMouseButton( 0 ) && !Input.GetMouseButton( 1 ) ) {
                if ( grid.state == HexGrid.GridState.Dragging) {
                    var pivot = tx.pickObject.GetComponent<HexGridTile>();
                    pivot.handler.OnMouseUpCustom( touch );
                    foreach ( var t in grid.selected.Keys.ToList() ) {
                        t.handler.OnMouseUpCustom( GenerateTouchDataWithOffset(pivot.transform,touch,grid.selected[t]) );
                    }
                    grid.state = HexGrid.GridState.Clear;
                }else if( grid.state == HexGrid.GridState.PivotSet ){
                    grid.state = HexGrid.GridState.Clear;
                }
                else{
                    tx.pickObject.SendMessage( "OnMouseUpCustom", touch, SendMessageOptions.DontRequireReceiver );
                }
                tx.pickObject = null;
                if ( !tx.dragging && wasHit ) {
                    var nodeView = hitResult.bulletRigidbody.nodeview;
                    if ( nodeView != null ) {
                        ShowGrid( nodeView.proxy.entity );
                    }
                }
                tx.button = MouseButton.none;
                touch.touchId = ( int )tx.button;
                tx.dragging = false;
            }
            else if(i == (int) TouchCamera.Grid && tx.pickObject == null && tx.hoverObject == null 
                    && !tx.dragging && Input.GetMouseButton( 0 ) ){
                if ( touch.deltaPosition.sqrMagnitude > 1 && Input.GetKey(KeyCode.LeftShift)) {
//                    Debug.Log( "STARTdrag" );
                    startOfSelection = touch.position;
                    if ( grid.state == HexGrid.GridState.Idle ) {
                        grid.state = HexGrid.GridState.Selecting;
                    }
//                    selecting = true;
//                    clear = false;
                    selectionPlane = GameObject.Instantiate( Resources.Load("FX/Select")) as GameObject ;
                    tx.dragging = true;
                } else if ( grid.state == HexGrid.GridState.Selected ){//grid.selected.Keys.Count > 0 ) {
//                    Debug.Log( "Clear Selection" );
                    grid.state = HexGrid.GridState.Clear;
                }
            }else if(i == (int) TouchCamera.Grid && tx.pickObject == null && tx.hoverObject == null 
                     && tx.dragging && !Input.GetMouseButton( 0 ) ){
//                Debug.Log("END drag");
                if ( selectionPlane != null ) {
//                    Debug.Log("Destroying");
                    Destroy( selectionPlane );
                }
                grid.state = HexGrid.GridState.Selected;
//                selecting = false;
                tx.dragging = false;
            }
            else if ( tx.pickObject != null && tx.hoverObject != null && touch.deltaPosition.sqrMagnitude > 1 ) {
                tx.dragging = true;
                if ( grid.state == HexGrid.GridState.PivotSet ) {
                    grid.state = HexGrid.GridState.Dragging;
                }
                if(grid.state == HexGrid.GridState.Dragging){
                    var pivot = tx.pickObject.GetComponent<HexGridTile>();
                    pivot.handler.OnMouseDragCustom( touch );
                    foreach(var t in grid.selected.Keys.ToList() ){
                        t.handler.OnMouseDragCustom( GenerateTouchDataWithOffset(pivot.transform,touch,grid.selected[t]) );
                    }
                }else{
                    tx.pickObject.SendMessage( "OnMouseDragCustom", touch, SendMessageOptions.DontRequireReceiver );
                }
            }
            else if ( tx.pickObject != null ) {
                tx.pickObject.SendMessage( "OnMouseStayCustom", touch, SendMessageOptions.DontRequireReceiver );
            }
            else if ( tx.pickObject == null && hitObject != null && tx.hoverObject != null 
                     && ( Input.GetMouseButton( 0 ) || Input.GetMouseButton( 1 ) ) ) {
                tx.dragging = false;
                tx.pickObject = hitObject;
                tx.button = Input.GetMouseButton( 0 ) ? MouseButton.left : MouseButton.right;
                touch.touchId = ( int )tx.button;
                if ( grid.state == HexGrid.GridState.Selected ) {
                    var pivot = tx.pickObject.GetComponent<HexGridTile>();
                    if(!grid.selected.ContainsKey(pivot)){
                        grid.state = HexGrid.GridState.Clear;
                    }else{
                        grid.selected.Remove( pivot );
                        foreach ( var t in grid.selected.Keys.ToList() ) {
                            grid.selected[ t ] = t.transform.position - pivot.transform.position;
                        }
                        grid.state = HexGrid.GridState.PivotSet;
                    }
                }
                tx.pickObject.SendMessage( "OnMouseDownCustom", touch, SendMessageOptions.DontRequireReceiver );
            }
            else if ( tx.pickObject == null && tx.hoverObject != null && hitObject != tx.hoverObject && !tx.dragging ) {
                tx.hoverObject.SendMessage( "OnMouseExitCustom", touch, SendMessageOptions.DontRequireReceiver );
                tx.hoverObject = null;
            }
            else if ( tx.pickObject == null && tx.hoverObject == null && hitObject != null && !tx.dragging) {
                tx.hoverObject = hitObject;
                tx.hoverObject.SendMessage( "OnMouseEnterCustom", touch, SendMessageOptions.DontRequireReceiver );
                tx.hoverObject.SendMessage( "hoverDescription", SendMessageOptions.DontRequireReceiver );
            }
            else if ( tx.pickObject == null && tx.hoverObject != null ) {
                tx.hoverObject.SendMessage( "OnMouseMoveCustom", touch, SendMessageOptions.DontRequireReceiver );
            }

            if ( tx.hoverObject == null ) {
                if ( tx.highlight != null ) {
                    tx.highlight.SetHighlight( false );
                    tx.highlight = null;
                }
            }
            else {
                if ( tx.highlight == null ) {
                    var btRigidbody = tx.hoverObject.GetComponent<BulletRigidBody>();
                    if ( btRigidbody != null ) {
                        var highlight = btRigidbody.nodeview.highlight;
                        highlight.SetHighlight( true, Highlight.HighlightType.Normal );
                        tx.highlight = highlight;
                    }
                        
                }
            }

            prevtx = tx;
        }

        if ( grid != null ) {
            if ( Input.GetKey( KeyCode.LeftControl ) || Input.GetKey( KeyCode.RightShift ) ) {
                if ( grid.state != HexGrid.GridState.Subgrid) {
//                    editing = true;
                    grid.state = HexGrid.GridState.Subgrid;
                }               //what is that? -z26
            }
            else if ( grid.state == HexGrid.GridState.Subgrid ) {
//                editing = false;
                grid.state = HexGrid.GridState.Idle;
                grid.HideGrid();
            }

            if ( grid.state == HexGrid.GridState.Selecting ) {
                if(Input.GetMouseButton(0)){
                    endOfSelection = touch.position;
                }
                float left = Mathf.Min(startOfSelection.x,endOfSelection.x);
                float bottom = Mathf.Min(startOfSelection.y,endOfSelection.y);
                float width = Mathf.Abs( endOfSelection.x - startOfSelection.x );
                float height = Mathf.Abs( endOfSelection.y - startOfSelection.y );
                float right = left + width;
                float top = bottom + height;


                Vector3 topLeft = GetComponent<Camera>().ScreenToWorldPoint(new Vector3(left,top,GetComponent<Camera>().nearClipPlane + 10));
                Vector3 topRight = GetComponent<Camera>().ScreenToWorldPoint(new Vector3( left + width, top, GetComponent<Camera>().nearClipPlane + 10 ));
                Vector3 bottomLeft = GetComponent<Camera>().ScreenToWorldPoint(new Vector3( left, top - height, GetComponent<Camera>().nearClipPlane + 10 ));

                var center = GetComponent<Camera>().ScreenToWorldPoint(new Vector3( left + width/2, top - height/2, GetComponent<Camera>().nearClipPlane + 10 ));
                selectionPlane.transform.position = center;
                selectionPlane.transform.localScale = new Vector3( (topRight.x - topLeft.x)/2 , (topLeft.y - bottomLeft.y)/2,1 );
                foreach ( var e in grid.tiles.Keys ) {
                    var tile = grid.tiles[ e ];
                    if(tile == null){
                        continue;
                    }
                    Vector3 position = GetComponent<Camera>().WorldToScreenPoint( tile.transform.position );
                    if ( position.x > left && position.x < right && position.y < top && position.y > bottom) {
                        if( !grid.selected.ContainsKey( tile )){
                            grid.selected.Add( tile, Vector3.zero );
                        }
                    } else if(grid.selected.ContainsKey( tile )) {
                        tile.handler.OnMouseExitCustom( touch );
                        grid.selected.Remove(tile);
                    }
                }
            } else {
                if(grid.state == HexGrid.GridState.Clear){
                    foreach ( var k in grid.selected.Keys ) {
                        if(k == null){
                            continue;
                        }
                        k.handler.OnMouseExitCustom( touch );
                    }
                    if(grid.selected.Keys.Count > 0){
//                        Debug.Log("Deleting");
//                        HexGridTileTypeHandler.pivot = null;
                        grid.selected.Clear();
                    }
                    grid.state = HexGrid.GridState.Idle;
                }
            }

            foreach ( var k in grid.selected.Keys ) {
                if(k == null){
                    continue;
                }
                k.handler.OnMouseEnterCustom(touch);
            }
        }
        else {
            ShowGrid( null );
        }

        if ( grid != null && GameModeManager.InMode( m => m == GameModeManager.ModeId.Grid ) ) {
            gridPID.setpoint = gridPosition;
            grid.transform.localPosition += gridPID.Compute( grid.transform.localPosition, Time.deltaTime ) * Time.deltaTime;
            var screenPos = Input.mousePosition;
            screenPos.z = gridPosition.z;
            if ( Input.GetMouseButton( 2 ) ) {
                var localScreenPos = transform.InverseTransformPoint( GetComponent<Camera>().ScreenToWorldPoint( screenPos ) );
                if ( draggingGrid ) {
                    gridPosition = localScreenPos + gridDragOffset;
                }
                else {
                    draggingGrid = true;
                    gridDragOffset = gridPosition - localScreenPos;
                }
            }
            else {
                draggingGrid = false;
                gridPosition.x = Mathf.Clamp( gridPosition.x, -50, 50 );
                gridPosition.y = Mathf.Clamp( gridPosition.y, -50, 50 );
                gridPosition.z += Input.GetAxis( "Mouse ScrollWheel" );
                gridPosition.z = Mathf.Clamp( gridPosition.z, 5, 20 );
# if !UNITY_STANDALONE_LINUX    //interesting -z26            
                var size = Mathf.Clamp( GetComponent<Camera>().orthographicSize, 5, 60 ) + Input.GetAxis( "Mouse ScrollWheel" ) * 50;//set 15 to 60 for greater zoom and
                GetComponent<Camera>().orthographicSize = Mathf.Lerp( GetComponent<Camera>().orthographicSize, size, Time.deltaTime * 2 );//enlarged grey "Layer" 10 times to hide edges
                pinned.orthographicSize = Mathf.Lerp( GetComponent<Camera>().orthographicSize, size, Time.deltaTime * 2 );//(the one appearing when spawning tiles) -z26
#endif
            }

            if ( Input.GetKey( KeyCode.LeftControl ) ) {//cursor tile=visible and mess up clicking for loading tiles -z26
                var tiling = grid.ToTiling( grid.transform.InverseTransformPoint( GetComponent<Camera>().ScreenToWorldPoint( screenPos ) ) );
                if ( tile == null ) {
                    if ( grid.FindTileAt( tiling ) == null ) {
                        var tileObj = Instantiate( tileAsset ) as GameObject;
                        tileObj.transform.parent = grid.transform;
                        tileObj.transform.localPosition = grid.FromTiling( tiling );
                        tileObj.transform.localRotation = Quaternion.identity;
                        tile = tileObj.GetComponent< HexGridTile >();
                        tile.tiling = tiling;
                        tile.entity = default(Entity);
                        tile.grid = grid;
                    }
                }
                if ( tile != null ) {
                    if ( grid.FindTileAt( tiling ) != null ) {
                        Destroy( tile.gameObject );
                    }
                    else {
                        tile.transform.localPosition = grid.FromTiling( tiling );
                        tile.tiling = tiling;
                    }
                }
            }
            else if ( tile != null ) {
                Destroy( tile.gameObject );
            }
        }

        oldMousePosition = touch.position;
    }

    bool draggingGrid = false;
    Vector3 gridDragOffset;

//    void OnDrawGizmos(){
//        if ( selecting ) {
//            Gizmos.color = Color.blue;
//            float left = Mathf.Min(startOfSelection.x,endOfSelection.x);
//            float top = Mathf.Max(startOfSelection.y,endOfSelection.y);
//            float width = Mathf.Abs( endOfSelection.x - startOfSelection.x );
//            float height = Mathf.Abs( endOfSelection.y - startOfSelection.y );
//            Vector3 A = camera.ScreenToWorldPoint(new Vector3(left,top,camera.near + 10));
//            Vector3 B = camera.ScreenToWorldPoint(new Vector3( left + width, top, camera.near + 10 ));
//            Vector3 C = camera.ScreenToWorldPoint(new Vector3( left, top - height, camera.near + 10 ));
//            Gizmos.DrawLine(A,B);
//            Gizmos.DrawLine(A,C);
//        }
//    }

    public void ShowGrid ( Entity entity ) { //when shift is held add to already loaded tiles instead of replacing them -z26
        if ( grid == null || !Input.GetKey( KeyCode.LeftShift ) ) {
            ResetGrid();
        }
        grid.ShowGrid( entity );
    }

    void ResetGrid () {
        if ( grid != null ) {
            Destroy( grid.gameObject );
        }
        grid = CreateNewGrid();
    }

    public void SwitchGrid ( GridCamera.GridType toGrid ) {
//        Debug.Log("Current " + activeGrid + " Requested " + toGrid);
        if ( activeGrid == toGrid ) {
            return;
        }

        if ( grid == null ) {
//            Debug.Log("New Creating");
            ResetGrid();
            activeGrid = toGrid;
        }
        else {
            if ( backupGrid == null && activeGrid == GridType.Programming ) {
//                Debug.Log("Backing up main");
                backUpSize = GetComponent<Camera>().orthographicSize;
                backupGrid = grid;
                backupGrid.gameObject.SetActive( false );
                grid = null;
                ResetGrid();
                activeGrid = toGrid;
                GetComponent<Camera>().orthographicSize = defaultCameraSize;
            }
            else {
                if ( activeGrid != GridType.Programming ) {
//                    Debug.Log("Restoring main");
                    Destroy( grid.gameObject );
                    grid = backupGrid;
                    grid.gameObject.SetActive( true );
                    backupGrid = null;
                    activeGrid = toGrid;
                    GetComponent<Camera>().orthographicSize = backUpSize;
                }
            }
        }
    }

    HexGrid CreateNewGrid () {
        var gridObj = Instantiate( Resources.Load( "HexGrid", typeof( GameObject ) ) ) as GameObject;
        var newGrid = gridObj.GetComponent< HexGrid >();
        newGrid.console = console;
        newGrid.camera = this;
        return newGrid;
    }

    public void RemoveDeprecated () {
        if ( grid != null ) {
            var tiles = grid.transform.GetComponentsInChildren<HexGridTile>();
            foreach ( var tile in tiles ) {
                if ( tile.entity == default(Entity) ) {
                    continue;
                }
                if ( tile.entity.deprecated ) {
                    tile.DestroyTile();
                }
            }
        }
    }

    TouchData GenerateTouchDataWithOffset (Transform pivot, TouchData touch, Vector3 offset ) {
        var t = new TouchData();
        t.touchId = touch.touchId;
        t.camera = touch.camera;
        t.worldPosition = pivot.position + offset;
        t.deltaPosition = touch.deltaPosition;
        t.dragging = touch.dragging;
        t.position = touch.camera.WorldToScreenPoint( pivot.position + offset );
        return t;
    }

}

