using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Operants;

public class HoloProjector : MonoBehaviour {

    public Transform plane;
    public Transform halo;
    public Renderer projectorMesh;
    float targetIntensity;
    float currentIntensity;
    public BulletRigidBody btRigidBody;
    float maxIllumpower = 1.0f;
    float maxIntensity = 0.5f;
    float muliplier = 2f;
    const int maxItemSize = 4;

    public List<LabelProxy> inFrameLabelProxies = new List<LabelProxy>(maxItemSize);
    Dictionary<LabelProxy,TextMesh> labels = new Dictionary<LabelProxy, TextMesh>(maxItemSize);
    public List<TextureProxy> inFrameTextureProxies = new List<TextureProxy>(maxItemSize);
    Dictionary<TextureProxy,Renderer> textures = new Dictionary<TextureProxy, Renderer>(maxItemSize);

    void Awake () {
        projectorMesh.material.SetFloat( "_IllumPower", 0 );
    }

    public void SetIntensity ( float value ) {
        targetIntensity = Mathf.Clamp01( value );
    }

    void Update(){
        UpdateIntensity();
        UpdateLabels();
        UpdateTextures();
    }

    void UpdateIntensity () {
        if(projectorMesh.material.shader.name != "Rozgo/Part"){
            return;
        }
        var haloColor = halo.GetComponent<Renderer>().material.GetColor( "_TintColor" );
        var fromIntensity = currentIntensity = haloColor.a;
        var toIntensity = Util.MapValue0X( targetIntensity, 1.0f, 0, maxIntensity );
        var fromIllum = projectorMesh.material.GetFloat( "_IllumPower" );
        var toIllum = Util.MapValue0X( targetIntensity, 1.0f, 0, maxIllumpower );
        currentIntensity = Mathfx.Berp( fromIntensity, toIntensity, Time.deltaTime * muliplier );
        var newIllum = Mathfx.Berp( fromIllum, toIllum, Time.deltaTime * muliplier );
        var newHaloColor = haloColor;
        newHaloColor.a = currentIntensity;
        halo.GetComponent<Renderer>().material.SetColor( "_TintColor", newHaloColor );
        projectorMesh.material.SetFloat( "_IllumPower", newIllum );
    }

    void UpdateLabels(){
        var loadedLabels = labels.Keys;
        var toDelete = loadedLabels.Except(inFrameLabelProxies).ToList();
        
        for(int i = 0 ; i < toDelete.Count ; ++i){
            var label = toDelete[i];
            var mesh = labels[label];
            labels.Remove(label);
            Destroy(mesh.transform.parent.gameObject);
            
        }
        
        var newLabels = inFrameLabelProxies.Except(loadedLabels).ToList();
        var avialableSlots = maxItemSize - newLabels.Count;
        avialableSlots = avialableSlots < 0 ? 0 : avialableSlots;
        var itLimit = Mathf.Min(avialableSlots,newLabels.Count);
        
        for(int i = 0 ; i < itLimit ; ++i){
            CreateNewLabel(newLabels[i]);
        }
        
        foreach(var label in loadedLabels){
            var textMesh = labels[label];
            textMesh.text = label.text;
            var color = label.color;
            color.a = currentIntensity;
            textMesh.GetComponent<Renderer>().material.color = color;
            textMesh.transform.parent.localPosition = label.position;
            textMesh.transform.parent.localScale = label.size;
        }
        inFrameLabelProxies.Clear();
    }

    void CreateNewLabel( LabelProxy label ){
        var newLabel = GameObject.Instantiate(Resources.Load("Operants/Label")) as GameObject;
        var textMesh = newLabel.GetComponentInChildren<TextMesh>();
        newLabel.transform.parent = plane;
        newLabel.transform.localPosition = Vector3.zero;
        newLabel.transform.localRotation = Quaternion.identity;
        labels.Add(label,textMesh);
    }


    void UpdateTextures(){
        var loaded = textures.Keys;
        var toDelete = loaded.Except(inFrameTextureProxies).ToList();
        
        for(int i = 0 ; i < toDelete.Count ; ++i){
            var texture = toDelete[i];
            var renderer = textures[texture];
            textures.Remove(texture);
            Destroy(renderer.transform.parent.gameObject);
            
        }
        
        var newTextures = inFrameTextureProxies.Except(loaded).ToList();
        var avialableSlots = maxItemSize - newTextures.Count;
        avialableSlots = avialableSlots < 0 ? 0 : avialableSlots;
        var itLimit = Mathf.Min(avialableSlots,newTextures.Count);
        
        for(int i = 0 ; i < itLimit ; ++i){
            CreateNewTexture(newTextures[i]);
        }
        
        foreach(var texture in loaded){
            var renderer = textures[texture];
            var color = renderer.material.GetColor( "_Color" );
            color.a = renderer.material.mainTexture == null ? 0 : currentIntensity;
            renderer.material.SetColor( "_Color", color );
            renderer.transform.parent.localPosition = texture.position;
            if(!texture.isMovie){
                renderer.transform.parent.localScale = texture.size;
            }
            if(texture.refresh){
                StartCoroutine(LoadTexture(texture));
            }
        }

        inFrameTextureProxies.Clear();
    }

    void CreateNewTexture( TextureProxy texture ){
        var newTexture = GameObject.Instantiate( Resources.Load("Operants/Texture") ) as GameObject;
        var renderer = newTexture.GetComponentInChildren<Renderer>();
        newTexture.transform.parent = plane;
        newTexture.transform.localPosition = Vector3.zero;
        newTexture.transform.localRotation = Quaternion.identity;
        textures.Add(texture,renderer);
        StartCoroutine(LoadTexture(texture));
    }


    IEnumerator LoadTexture ( TextureProxy texture ) {
        texture.refresh = false;
        if(texture.url == string.Empty){
            textures[texture].material.mainTexture = null;
            yield break;
        }
        if(texture.tryLocal){
            
            var local = Resources.Load("Default/"+texture.url) as Texture2D; //line below added by me -z26
            //var userfolder = Resources.Load( Path.Combine( Util.userDataPath, "blueprints/" + texture.url) ) as Texture2D;
            if(local != default(Texture2D)){
                textures[texture].material.SetTexture( "_MainTex", local );
                yield break;
            }
            
            
            var movieLocal = Resources.Load("Default/"+texture.url) as MovieTexture;
            if(movieLocal != default(MovieTexture)){
                var renderer = textures[texture];
                texture.isMovie = true;
                renderer.material.SetTexture( "_MainTex", movieLocal );
                movieLocal.loop = true;
                movieLocal.Play();
                var ratio = (float)movieLocal.width / (float)movieLocal.height;
                var scale = new Vector3( 3.2f*ratio, 3.2f , 3.2f );
                renderer.transform.parent.localScale = scale;
                yield break;
            }
            
        }
        
        
        
        
        
        Texture2D newTexture = new Texture2D(128,128,TextureFormat.DXT5,false);
        var  www = new WWW( texture.url );
        yield return www;
        if(www.error != null){
            Console.PushError("www",new string[]{ www.error });
        }else{
            www.LoadImageIntoTexture(newTexture);
            var width = newTexture.width;
            var height = newTexture.height;
            if(width <= 128 && height <= 128){
                textures[texture].material.SetTexture( "_MainTex", newTexture );
            }else{
                Console.PushError("texture",new string[]{"texture size is too big keep it under 128x128 px"});
                Destroy(newTexture);
            }
        }
        www.Dispose();
        www = null;
    }

}
