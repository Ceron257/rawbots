using UnityEngine;
using System.Collections;

public class Hook : MonoBehaviour
{


	public Part part;
	public GameObject constraintHolder;
	public BulletRigidBody bulletRigidbody;
	BulletGeneric6DofConstraint constraint;
	BulletGenericMotor motor;
	private int jointState = 0;
	public bool closeToSurface;
	public bool isActive = false;
	public float distance = 0;
	public float angle = 0;
	private Ray rayTest;
	private BulletRaycastHitDetails hitDetails;
	private Vector3 holderOriginalPos;
	public GameObject hookFeedback;
	public GameObject hookPreFeedback;
	public bool attachAtBeginning = false;
	public Vector3 initialAttachedPos = Vector3.zero;
	public Quaternion initialAttachedRot = Quaternion.identity;

	void Start ()
	{
		holderOriginalPos = constraintHolder.transform.localPosition;
		hitDetails = null;
	}

	public void SetJointState (int jointStateFromInbox)
	{
		jointState = jointStateFromInbox;
	}

	public void Update ()
	{
		if (attachAtBeginning) {
			bulletRigidbody.SetRotation (initialAttachedRot);
			bulletRigidbody.SetPosition (initialAttachedPos);

			CreateJoint ();
			isActive = true;
			hookFeedback.SetActive (true);
			attachAtBeginning = false;
		}
		rayTest = new Ray (transform.position, transform.up * -1f);

       
		var result = Bullet.Raycast (rayTest, out hitDetails, 100f, BulletLayers.Camera, BulletLayers.HexBlock | BulletLayers.World | BulletLayers.Structure);
        
		if (result) {

			
			distance = Vector3.Distance (constraintHolder.transform.position, hitDetails.point);
			angle = Vector3.Angle (hitDetails.normal, bulletRigidbody.transform.up);

            if (distance < 1.2f) {
				closeToSurface = true;
				hookPreFeedback.SetActive (true);
			} else {
				closeToSurface = false;
				hookPreFeedback.SetActive (false);
			}

		} else {
			closeToSurface = false;
			distance = 31415;
			angle = 0;
		}
		
		if (jointState == 0) {
			
			hookPreFeedback.SetActive (false);
            
		} else {
			hookPreFeedback.SetActive (true);
		}

		if (jointState == 1 && !isActive && closeToSurface) {

			var normal = hitDetails.normal;

			if (angle < 30) {
				Oracle.Entity entity = GetComponent<Part> ().proxy.entity;
				entity.SetProperty ("hook_point", Util.Vector3ToProperty (transform.position));
				entity.SetProperty ("hook_rotation", Util.Vector3ToProperty (transform.eulerAngles));
				entity.SetProperty ("attached", 1.0f);
				
				constraintHolder.transform.position = hitDetails.point;
				constraintHolder.transform.up = normal * -1f;
				CreateJoint ();
				isActive = true;
				hookFeedback.SetActive (true);
			}

		} else if (jointState == 0 && isActive) {
			
			Oracle.Entity entity = GetComponent<Part> ().proxy.entity;
			
			entity.SetProperty ("attached", 0);
			constraintHolder.transform.localPosition = holderOriginalPos;
			DestroyJoint ();
			bulletRigidbody.AddForce (hitDetails.normal, Vector3.zero);
			hookFeedback.SetActive (false);
			isActive = false;
		}
	}

	public void CreateJoint ()
	{
		constraint = constraintHolder.AddComponent<BulletGeneric6DofConstraint> ();
		constraint.bulletRbA = bulletRigidbody;
		constraint.solverIterations = 20;
		constraint.rbAPos = bulletRigidbody.transform.position;
		constraint.rbARot = bulletRigidbody.transform.rotation;


		constraint.lowerLimit = Vector3.zero;// Vector3.one*-0.3f;
		constraint.lowerLimit.z = 0;//1
		constraint.upperLimit = Vector3.zero;//  Vector3.one*1f;
		constraint.upperLimit.z = 0;
		constraint.useLinearReferenceFrameA = false;
		constraint.disableCollisionBetweenBodies = false;
		constraint.angularERPValue = Vector3.one * 0.5f;
		constraint.linearERPValue = Vector3.one * 0.5f;
		constraint.angularCFMValue = Vector3.zero;
		constraint.linearCFMValue = Vector3.zero;
		constraint.constraintToWorld = true;
		constraint.enableSpring = true;
		constraint.springAxis = BulletGeneric6DofConstraint.Axis.Y;
		constraint.damping = 0.2f;
		constraint.stiffness = 30;

		constraint.Initialize ();
	}

	public void DestroyJoint ()
	{
		constraint.RemoveBulletConstraint ();
		Destroy (constraint);
	}

}
