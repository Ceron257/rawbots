using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class Floater : MonoBehaviour
{

	public PID pid;
	Water water;
	public BulletRigidBody btRigidBody;
    public float submergedDepth;
    public Vector3 surfacePoint;
    public float depth;
    public int inWater;
    //PhysicalObject physicalObject;

    private Ray rayTest;
    private BulletRaycastHitDetails hitDetails;


    void Start ()
	{
		
		btRigidBody.AddOnTriggerDelegate (OnTrigger);
	}

    void FixedUpdate()
    {

        if ((water != default(Water)) || (waterOnTop)) {
            inWater = 1;
            //if (water != default(Water))
            //{
            //    surfacePoint = water.getSurfacePoint(transform.position);   //Surface of containing body
            //}
            //else
            //{
            //    surfacePoint
            //}
            surfacePoint = transform.position;
            Vector3 direction = GetComponent<PhysicalObject>().Up(); //physicalObject.Up();//Vector3.Normalize(surfacePoint - transform.position);
            Vector3 testPoint = transform.position;
            int count = 0;
            do
            {
                testPoint += (direction * 100.0f);          //Cast down from outside water
                rayTest = new Ray(testPoint, -direction);
                var hit = Bullet.Raycast(rayTest, out hitDetails, 100.0f, BulletLayers.Camera, BulletLayers.Water/* | BulletLayers.HexBlock*/);

                if (hit)
                {
                    surfacePoint = hitDetails.point;
                }
                count++;
            }
            while ((Vector3.Magnitude(testPoint - surfacePoint) < 15) && (count < 10));   //Allows for ~45degrees tilt of water hex. If too close try from 100 further

            depth = Vector3.Magnitude(surfacePoint - transform.position);
            if (depth > submergedDepth)
            {
                pid.setpoint = surfacePoint - ((surfacePoint - transform.position).normalized * ((depth - submergedDepth) * 0.998f));   //Limit lift scaling to 0.2% once submerged
            }
            else
            {
                pid.setpoint = surfacePoint;
            }

            //         if (waterOnTop)
            //         {
            //             pid.setpoint = transform.position + (surfacePoint - transform.position).normalized * 6f; //Now redundant as main branch handles both cases
            //}

            var force = pid.Compute(transform.position, Time.deltaTime);
            btRigidBody.AddForce(force, Vector3.zero);


        }
        else
        {
            inWater = 0;
        }
		waterOnTop = false;
	}

	void OnWaterEnter (Water water)
	{
		this.water = water;
		pid.Reset (transform.position);
	}

	void OnWaterExit (Water water)
	{
		this.water = default( Water );
	}
	
	bool waterOnTop = false;
	// checking if water on top
	public void OnTrigger (Vector3 relativeVelocity, BulletRigidBody other, Vector3 contactPoint, Vector3 lineOfAction)
	{
		if (other.collisionGroup == BulletLayers.Water) {
			if (water != default(Water)) {
				if (water.gameObject != other.gameObject) {
					var proj = Vector3.Project ((other.transform.position - water.transform.position), water.transform.up);
					if (proj.sqrMagnitude > 110) {
						waterOnTop = true;
					}
				}
			}
		}
	}
	
}
