using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Operants;

public class HyperCubeHotSpot : MonoBehaviour {

    public BulletRigidBody btRigidBody;
    public HyperCube hyperCube;
    float spotYOffset = 5.5f;
    
    void Start(){
        btRigidBody.SetPosition(transform.position);
        btRigidBody.SetRotation(hyperCube.transform.rotation);
        btRigidBody.AddOnTriggerDelegate( OnTrigger );
        //transform.parent = hyperCube.transform.parent;

    }

    void Update(){
        transform.position =hyperCube.transform.position + hyperCube.transform.up * spotYOffset;
        btRigidBody.SetPosition(transform.position);
        btRigidBody.SetRotation(hyperCube.transform.rotation);
    }


    public void OnTrigger( Vector3 relativeVelocity,BulletRigidBody other,Vector3 contactPoint,Vector3 lineOfAction ) {
        if(other == hyperCube.btRigidBody){
            return;
        }

        var part = other.GetComponent<Part>();
        var subPart = other.GetComponent<Subpart>();
        if(subPart !=  null){
            part = subPart.part;
        }
        if(part != default(Part)){
            if(part.proxy.Parts().Count() <= 20 && 
               !hyperCube.partsToTransport.Contains(part.proxy)){
                hyperCube.partsToTransport.Add(part.proxy);
            }
        }
    }
}

