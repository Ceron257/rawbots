using UnityEngine;
using System.Collections;
using Operants;

public class CameraView : MonoBehaviour {

    public SensorTrigger trigger;
    public MeshFilter filter;
    public MeshRenderer triggerRenderer;
    public System.Action< float > produceActivity = v => {};
    public System.Action< PartProxy > produceVisiblePart = v => {};
    public float fov = 30;
    public float size = 5;
    public float near = 1;
    public float far = 30;
    public new Transform transform;

    void Awake () {
        HideTriggerRender();
    }
	
	
	void _OnDrawGizmos ()
	{
		var physicalObject = GetComponent<PhysicalObject>();
		Gizmos.color = Color.green;
		Gizmos.DrawLine (transform.position, transform.position + physicalObject.Gravity()/5f);
	}
	
//	void Update(){
////		var gravity =GetComponent<PhysicalObject>().Gravity();
//	//Debug.Log("gravity "+gravity);
//	}
	

    public void SetFOV ( float fov ) {
        fov = Mathf.Clamp( fov, 1, 180 );
        if ( !Mathf.Approximately( this.fov, fov ) ) {
            this.fov = fov;
        }
    }

    public void SetSize ( float size ) {
        size = Mathf.Clamp( size, 1, 128 ); //xfm value pline
        if ( !Mathf.Approximately( this.size, size ) ) {
            this.size = size;
        }
    }

    public void SetNearPlane ( float near ) {
        near = Mathf.Clamp( near, 1, far - 1 );
        if ( !Mathf.Approximately( this.near, near ) ) {
            this.near = near;
        }
    }

    public void SetFarPlane ( float far ) {
        far = Mathf.Clamp( far, near + 1, 200 );
        if ( !Mathf.Approximately( this.far, far ) ) {
            this.far = far;
        }
    }

    public void ShowTriggerRender () {
        TriggerRenderStatus( true );
    }

    public void HideTriggerRender () {
        TriggerRenderStatus( false );
    }

    void TriggerRenderStatus ( bool status ) {
        triggerRenderer.enabled = status;
    }

    void OnGridTileEnter () {
        ShowTriggerRender();
    }

    void OnGridTileExit () {
        HideTriggerRender();
    }
}

public static class CameraExtention {

    private static int[] m_VertOrder = new int[24]
    {
        0,1,2,3, // near
        6,5,4,7, // far
        0,4,5,1, // left
        3,2,6,7, // right
        1,5,6,2, // top
        0,3,7,4  // bottom
    };
    private static int[] m_Indices = new int[36]
    {
         0,  1,  2,  3,  0,  2, // near
         4,  5,  6,  7,  4,  6, // far
         8,  9, 10, 11,  8, 10, // left
        12, 13, 14, 15, 12, 14, // right
        16, 17, 18, 19, 16, 18, // top
        20, 21, 22, 23, 20, 22, // bottom
    }; //              |______|---> shared vertices

    public static Mesh GenerateFrustumMesh ( this Camera cam ) {
        Mesh mesh = new Mesh();
        Vector3[] v = new Vector3[8];
        v[ 0 ] = v[ 4 ] = new Vector3( 0, 0, 0 );
        v[ 1 ] = v[ 5 ] = new Vector3( 0, 1, 0 );
        v[ 2 ] = v[ 6 ] = new Vector3( 1, 1, 0 );
        v[ 3 ] = v[ 7 ] = new Vector3( 1, 0, 0 );
        v[ 0 ].z = v[ 1 ].z = v[ 2 ].z = v[ 3 ].z = cam.nearClipPlane;
        v[ 4 ].z = v[ 5 ].z = v[ 6 ].z = v[ 7 ].z = cam.farClipPlane;
        // Transform viewport --> world --> local
        for ( int i = 0; i < 8; i++ ) {
            v[ i ] = cam.transform.InverseTransformPoint( cam.ViewportToWorldPoint( v[ i ] ) );
        }

        Vector3[] vertices = new Vector3[24];
        Vector3[] normals = new Vector3[24];
        // Split vertices for each face (8 vertices --> 24 vertices)
        for ( int i = 0; i < 24; i++ ) {
            vertices[ i ] = v[ m_VertOrder[ i ] ];
        }
        // Calculate facenormal
        for ( int i = 0; i < 6; i++ ) {                                   
            Vector3 faceNormal = Vector3.Cross( vertices[ i * 4 + 2 ] - vertices[ i * 4 + 1 ], vertices[ i * 4 + 0 ] - vertices[ i * 4 + 1 ] );
            normals[ i * 4 + 0 ] = normals[ i * 4 + 1 ] = normals[ i * 4 + 2 ] = normals[ i * 4 + 3 ] = faceNormal;
        }
        mesh.vertices = vertices;
        mesh.normals = normals;
        mesh.triangles = m_Indices;
        return mesh;
    }
}
