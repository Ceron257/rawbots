using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class Elbow : MonoBehaviour {

    public NodeView nodeView;
    public Part part;
    public GameObject rotorObject;
    private float force = 100;
    public System.Action<float> producePartAngle = (notAForce) => {};
    public BulletGenericMotor motor;
    public BulletGeneric6DofConstraint constraint;

    private Vector3 rotorPosition;
    private Quaternion rotorRotation;

    void Awake () {
        rotorPosition = rotorObject.transform.localPosition;
        rotorRotation = rotorObject.transform.localRotation;
//        part.subparts[0].localPosition = rotorPosition;
//        part.subparts[0].localRotation = rotorRotation;
    }




    void Update () {
        var rotorCurrentRotation = Quaternion.Inverse( transform.rotation ) * rotorObject.transform.rotation;
		var reportedAngle = rotorCurrentRotation.eulerAngles.y;
		var testVector = new Vector3(Mathf.Cos(reportedAngle*Mathf.PI/180f),Mathf.Sin(reportedAngle*Mathf.PI/180f),0);
		var referenceVector = new Vector3(Mathf.Cos(0),Mathf.Sin(0),0);
		var sign =Mathf.Sign(  Vector3.Cross(referenceVector,testVector).z);
        producePartAngle( Quaternion.Angle(transform.rotation,rotorObject.transform.rotation)*sign );
    }

    public void SetForce ( float force ) {
        this.force = force;
    }

    public void SetElbowAngle ( float value ) {
        motor.SetTarget(value*Mathf.PI/180f);

    }

    public void SetElbowVelocity ( float value ) {
       motor.SetVelocity(value);
    }

    public void StoreElbowStatus () {
        //rotorPosition = transform.InverseTransformPoint( rotorObject.transform.position );
        //rotorRotation = Quaternion.Inverse( transform.rotation ) * rotorObject.transform.rotation;
    }

    public void ResetElbowJoint () {
        //rotorObject.transform.position =  transform.TransformPoint( rotorPosition );
        //rotorObject.transform.rotation = transform.rotation * rotorRotation;
    }
}
