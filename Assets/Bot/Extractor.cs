using UnityEngine;
using System.Collections;

public class Extractor : MonoBehaviour {

    public GameObject crystalPopReference;
    public GameObject[] crystals;
    public GameObject fx;
    public Transform drill;
    public float period = 3f;
    public System.Func<string,NodeView> onRequest = (arg) => {
        return null;};

    public enum Controls {
        TurnOn,
        TurnOff
    }

    public PhysicalObject gravityObject;
    Controls command = Controls.TurnOn;
    Controls currentCommand = Controls.TurnOff;

    void Start () {
        fx.SetActive(false); //fx.SetActiveRecursively( false );
    }

    void Update () {
        if ( command != currentCommand ) {
            currentCommand = command;
            if ( command == Controls.TurnOn ) {
                fx.SetActive(true); //fx.SetActiveRecursively( true );
            }
            else if ( command == Controls.TurnOff ) {
                fx.SetActive(false); //fx.SetActiveRecursively( false );
            }
        }

        if ( command == Controls.TurnOn ) {
            if ( Vector3.Angle( gravityObject.Gravity(), transform.up * -1f ) > 25f ) {
                command = Controls.TurnOff;
            }
            var targetRotation = drill.localRotation * Quaternion.Euler( 0, 0, 100 );
            drill.localRotation = Quaternion.Lerp( drill.localRotation, targetRotation, Time.deltaTime * 10 );
            PopCrystal( Random.Range( 0, crystals.Length ) );
            //CheckSoil();
        }


    }

    public void Command ( Controls command ) {
        this.command = command;
    }

    float rawMat = 0f;

    public void PopCrystal ( int crystalType ) {

        rawMat += Time.deltaTime;
        if ( rawMat > period ) {


            NodeView nodeView = onRequest( "crystal"+Time.time.GetHashCode() );

            if ( nodeView != null ) {
                var crystal = nodeView.gameObject;
                crystal.transform.position = crystalPopReference.transform.position;
                crystal.transform.localScale = Random.Range( 1f, 3f ) * Vector3.one;
                crystal.GetComponent<Rigidbody>().AddForceAtPosition( crystalPopReference.transform.forward * 400f +
                300 * crystalPopReference.transform.up * Random.Range( -1f, 1f ), crystal.transform.position + Vector3.up * Random.Range( -0.15f, 0.15f ) );
                GetComponent<Rigidbody>().AddForceAtPosition( -crystalPopReference.transform.forward * 400f, crystalPopReference.transform.position );


            }
            rawMat = 0f;
        }
    }

    public void CheckSoil () {



    }




}
