using UnityEngine;
using System.Collections;

public class SpotLight : MonoBehaviour {

    public NodeView nodeView;
    public Light light;
    public Transform cone;
    public Renderer spotLightRenderer;
    float irisState = 0;
    float maxLightRange = 300;
    float minLightRange = 100;
    float maxLightAperture = 130;
    float minLightAperture = 5f;

    float maxIllumPower = 25f;
    float minIllumPower = 0f;

    float intensity = 3;
    float range;
    float aperture;
    Material lensMaterial;

    public void Awake(){
        lensMaterial = spotLightRenderer.material;
    }


    public void SetProperties ( float value ) {
        irisState = Mathf.Clamp01( value );
        range = Util.MapValue0X( irisState, 1, maxLightRange, minLightRange );
        aperture = Util.MapValue0X( irisState, 1, minLightAperture, maxLightAperture );
        var spotLightBulbIllumPower = Util.MapValue0X( irisState, 1, minIllumPower, maxIllumPower );
        light.range = range;
        light.spotAngle = aperture;
        light.intensity = irisState == 0 ? 0 : intensity;
        lensMaterial.SetFloat( "_IllumPower", spotLightBulbIllumPower );
        var renderer = cone.GetComponent<MeshRenderer>();

        if ( aperture < 5.1f && renderer.enabled ) {
            cone.GetComponent<MeshRenderer>().enabled = false;
        }
        else if ( aperture > 5.2f && !renderer.enabled ) {
            cone.GetComponent<MeshRenderer>().enabled = true;
        }
        cone.localScale = new Vector3( aperture * 2, aperture * 2, range / 2 );
    }
}
