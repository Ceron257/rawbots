using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Oracle;
using Operants;

public class HyperCube : MonoBehaviour {

    public HyperCubeHotSpot hotspot;
    public NodeView nodeView;
    public Renderer glow;
    public Color glowColor;
    public Operants.HyperCubeProxy proxy;
    public GameObject bagExit;
    public List<PartProxy> partsToTransport = new List<PartProxy>();
    public BulletRigidBody btRigidBody;

    void OnCreateView ( HyperCubeProxy proxy ) {
        this.proxy = proxy;
    }

    bool dragging = true;

    void OnMouseDragCustom ( TouchData touch ) {
        dragging = true;
    }

    void OnMouseUpCustom ( TouchData touch ) {
        if ( !dragging ) {

            var containedEntity = proxy.entity.Tails( p => p == "contained_in" ).FirstOrDefault();
            if ( containedEntity != default(Entity) ) {
                Pop( containedEntity );
            }

//            if ( proxy.bag.Count > 0 ) {
//                var entity = proxy.bag[ 0 ];
//                proxy.bag.RemoveAt( 0 );
//                Pop( entity );
//            }
        }
        dragging = false;
    }

    void Suck ( Part root ) {

        var result = Hyperspace.DestroyPartsView( root, proxy );
        if ( result ) {
            var effect = ( GameObject )Instantiate( Resources.Load( "FX/Suck", typeof( GameObject ) ),
            transform.position, transform.rotation );
            effect.transform.parent = gameObject.transform;

        }
    }

    public void Pop ( Entity entity ) {
        var effect = ( GameObject )Instantiate( Resources.Load( "FX/Spit", typeof( GameObject ) ),
            transform.position, transform.rotation );
        effect.transform.parent = gameObject.transform;
        Hyperspace.RecoverPartsView( entity, bagExit.transform );
    }

    void Update () {
        if ( partsToTransport.Count > 0 ) {
            var partProxy = partsToTransport[ 0 ];
            partsToTransport.RemoveAt( 0 );
            if ( partProxy.nodeView != null ) {
                Suck( partProxy.nodeView.GetComponent<Part>() );
            }
        }
    }


    
}

