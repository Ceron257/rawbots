using UnityEngine;
using Oracle;

public class PistonBase : MonoBehaviour {

    public GameObject constraintHolder;
    public BulletGenericMotor motor;
    public GameObject pistonHead;
    public System.Action<float> producePartLocalPosition = (position) => {};
    public GameObject visualFeedback;

    public void ChangePosition ( float value ) {
        motor.SetTarget( value * 2.0f );// in order to fit continuum size
    }

    void Update () {
        var center = ( pistonHead.transform.position + gameObject.transform.position ) / 2.0f;
        visualFeedback.transform.position = center;
        var pos = gameObject.transform.InverseTransformPoint( pistonHead.transform.position ).y / 2.0f;
        producePartLocalPosition( pos );

        if ( Vector3.Distance( pistonHead.transform.position, center ) > 0.2f ) {
            var feedbackDirection = pistonHead.transform.position - center;
            visualFeedback.transform.forward = feedbackDirection.normalized;
            visualFeedback.transform.localScale = new Vector3( 1, 1, pos * 1.5f );
        }

    }

}
