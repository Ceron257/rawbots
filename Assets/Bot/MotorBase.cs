using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class MotorBase : MonoBehaviour {

   
    public GameObject constraintHolder;
    public GameObject rotor;
    public BulletGenericMotor motor;
    public BulletGeneric6DofConstraint constraint;
    public System.Action< float > producePartAngle = ( notAForce ) => {};
    public float safeTimer = 0f;
    public float idle = 0.0f;
    void Awake () {
        safeTimer = Time.time;
    }

    void Update () {
        var velocity = Mathf.Abs(motor.velocity) * 0.1f;
        if (idle >= 0.5f)
        {
            motor.Stop();
            velocity = 0;
        }
        GetComponent<AudioSource>().volume = velocity * velocity;
        GetComponent<AudioSource>().pitch = Mathf.Clamp( velocity * 2, 0.8f, 3 );

        var rotorRotation = Quaternion.Inverse( transform.rotation ) * rotor.transform.rotation;
        producePartAngle(rotorRotation.eulerAngles.y );
    }

    public void SetIdle(float idle)
    {
        this.idle = idle;
    }

    public void SetMotorAngle ( float value ) {
        if (idle < 0.5f)
        {
            motor.SetTarget(value * Mathf.PI / 180.0f);
        }
        else
        {
            motor.Stop();
        }
    }

    public void SetMotorVelocity ( float value ) {
        if (idle < 0.5f)
        {
            motor.SetVelocity(value);
        }
        else
        {
            motor.Stop();
        }
    }

    

}
