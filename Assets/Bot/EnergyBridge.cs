using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnergyBridge : MonoBehaviour {

    public Transform bridge;
	public Transform bridgeCollider;
	public BulletRigidBody energyBridge;
	public Transform bridgeRealCollider;
    public float length = 0;
	public float width = 0;
    public bool on = false;
	

    public void SwitchState ( float value ) {
        if ( value >= 0.9f ) {
            on = true;
        }
        else {
            on = false;
        }
    }
	public void SetInitialScale(Vector3 initialScale){
		
		bridge.localScale = initialScale;
		energyBridge.SetCollisionShapeScale(initialScale);
	}
    
    void Update () {
		var distance = 0f;
		var currentWidth = 0f;
        //length = 0;
		
		
		if(on){
			length = Mathf.Clamp(length,0,40);
			distance = length+1;
			width = Mathf.Clamp(width,0,30);
			currentWidth =width+5;
			
		}else{
			distance = 1;
			currentWidth =width+5;
		}
        
        var current = bridge.localScale;
		var currentCollider = bridgeCollider.localScale;
        var scale = Vector3.one;
        scale.y = distance;
		scale.x = currentWidth;
		
		if(Mathf.Abs(currentCollider.y-current.y) >= 0.5f){
			
			currentCollider.y = current.y;
			bridgeCollider.localScale = currentCollider;
			energyBridge.SetCollisionShapeScale(currentCollider);
		}
		if(Mathf.Abs(currentCollider.x-current.x) >= 0.5f){
			currentCollider.x = current.x;
			bridgeCollider.localScale = currentCollider;
			energyBridge.SetCollisionShapeScale(currentCollider);
		}
		
        if ( !Mathf.Approximately( current.y, scale.y ) ) {
			Oracle.Entity entity = GetComponent<NodeView> ().proxy.entity;
            bridge.localScale = Vector3.Lerp( current, scale, Time.deltaTime );
			entity.SetProperty ("scale", Util.Vector3ToProperty (scale));
        }
		if ( !Mathf.Approximately( current.x, scale.x ) ) {
			Oracle.Entity entity = GetComponent<NodeView> ().proxy.entity;
            bridge.localScale = Vector3.Lerp( current, scale, Time.deltaTime );
			entity.SetProperty ("scale", Util.Vector3ToProperty (scale));
        }
    }

}
