using UnityEngine;
using System.Collections;
using Oracle;

public class DragComponent : MonoBehaviour {


    public PID positionPID;
    int dragging = 0;
    Vector3 dragPosition;
    Vector3 touchOffset;
    Vector3 targetPosition;
    public bool mouseOverPart = false;
    private bool dragPermission = false;
    bool dragHighlightActive = false;
    bool dragDisconnect = false;
    float draggingTime = 0;
    public Part part;
    public BulletRigidBody bulletRigidbody;
    new Transform transform;
    private GrabPoint currentGrabPoint;
    private float mass;

    void Awake () {
        transform = gameObject.transform;
        positionPID = new PID();
    }

    void setPIDMass() { //For some reason, the mouse drags a separate rigidbody thats phyisically connected to the part to drag
        //instead of dragging the part directly. The code below increases the mass of that rigidbody so that its the same as the dragged
        //object. That way, the physical joint should behave well. -z26

        mass = bulletRigidbody.mass;
        positionPID.kp = 80 * mass;
        positionPID.ki = 15 * mass;
        positionPID.kd = 20 * mass;
        currentGrabPoint.bulletRigidbody.SetBodyMass(mass,currentGrabPoint.bulletRigidbody.compoundShape.CalculateInertia());
    }

    void OnMouseEnterCustom ( TouchData touchData ) {
        mouseOverPart = true;
        dragPermission = Sec.HasPermission( part.proxy.entity, Sec.Permission.drag );
    }

    void OnMouseExitCustom ( TouchData touchData ) {
        mouseOverPart = false;
        if ( dragHighlightActive ) {
            part.gameObject.GetComponent<Highlight>().SetHighlight( false );
            dragHighlightActive = false;
        }
    }

    void OnMouseDownCustom ( TouchData touchData ) {
        if ( GameModeManager.InMode( m => m != GameModeManager.ModeId.Grid ) ) {
            touchOffset = Util.TouchToWorld( transform.position, touchData ) - transform.position;
            
            if ( currentGrabPoint != null ) {
                Debug.LogWarning( "This should not happen, check carefully" );
                
                currentGrabPoint.DisconnectGrabPoint();
                Destroy( currentGrabPoint.gameObject );
            }
            
            
            var newGrabPoint = ( GameObject )Instantiate( Resources.Load( "GrabPoint" ), touchData.worldPosition, Quaternion.identity );
            currentGrabPoint = newGrabPoint.GetComponent<GrabPoint>();
            currentGrabPoint.PlaceGrabPoint( bulletRigidbody );


            targetPosition = currentGrabPoint.transform.position;

            positionPID.Reset( targetPosition );
        }
    }

    void OnMouseDragCustom ( TouchData touchData ) {
        if ( GameModeManager.InMode( m => m != GameModeManager.ModeId.Grid ) ) {
            dragging = dragging <= 1 ? 1 : dragging;
            dragPosition = Util.TouchToWorld( transform.position, touchData ) - touchOffset;
            targetPosition = dragPosition;
        }
    }

    void OnMouseStayCustom ( TouchData touchData ) {
        if ( GameModeManager.InMode( m => m != GameModeManager.ModeId.Grid ) ) {
            dragging = dragging <= 1 ? 1 : dragging;
            dragPosition = Util.TouchToWorld( transform.position, touchData ) - touchOffset;
            targetPosition = dragPosition;
        }
    }

    void OnMouseUpCustom ( TouchData touchData ) {
        dragging = 0;
        if ( currentGrabPoint != null ) {
            //currentGrabPoint.DisconnectGrabPoint(); //cause of this light continuums will experience a force when the mouse is released.
            //The only thing this function (in GrabPoint.cs) does is to apply an upwards force, dunno the point of that so I commented it out. -z26
            Destroy( currentGrabPoint.gameObject );
        }
    }

    void FixedUpdate () {
        if ( GameModeManager.InMode( m => m == GameModeManager.ModeId.World ) && ( dragging != 2 ) && dragPermission ) {
            var highlightComponent = part.gameObject.GetComponent<Highlight>();
            if ( !dragHighlightActive && mouseOverPart && !highlightComponent.highlighted ) {
                highlightComponent.SetHighlight( true, Highlight.HighlightType.Blue,false );
                dragHighlightActive = true;

            }
        }
        if ( dragging == 2 || GameModeManager.InMode( m => m != GameModeManager.ModeId.World ) ) {

            if ( dragHighlightActive ) {
                part.gameObject.GetComponent<Highlight>().SetHighlight( false, Highlight.HighlightType.Normal, false );
                dragHighlightActive = false;
                mouseOverPart = false;

            }
        }
        if ( dragging == 1 && dragPermission ) {
            draggingTime += Time.deltaTime;
            positionPID.setpoint = targetPosition;
            setPIDMass(); //run every frame, cause a continuum could change its mass at any time. Dunno if thats worth it.
            var force = positionPID.Compute( currentGrabPoint.transform.position, Time.deltaTime );
            force = Vector3.ClampMagnitude( force, 2500 * mass );// Max force should depend on the mass of the dragged object
            //Debug.Log(force);

            currentGrabPoint.bulletRigidbody.AddForce( force, Vector3.zero );
            /*if ( ( draggingTime > 10 | force.magnitude > 1500  ) && !dragDisconnect ) {    removed the time limit -z26
                dragDisconnect = true;
                dragging = 2;
            } */
        } else {
            dragDisconnect = false;
            draggingTime = 0;

        }

    }
    
    void OnDestroy () {
        if ( currentGrabPoint != null ) {
            Destroy( currentGrabPoint.gameObject );
        }
        
    }


}
