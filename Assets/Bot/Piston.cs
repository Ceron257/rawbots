using UnityEngine;
using Oracle;

public class Piston : MonoBehaviour {

    public GameObject pistonObject;
    public LineRenderer link;
    public BulletGenericMotor motor;
    public BulletGeneric6DofConstraint constraint;
    private Vector3 pistonPosition;
    private Quaternion pistonRotation;
	
	public System.Action<float> producePartLocalPosition = (position) => {};
	
    public GameObject visualFeedback;
    public Part part;


    public void ChangePosition ( float value ) {
        motor.SetTarget(value*2.0f);// in order to fit continuum size
		
    }
    void Awake(){
        pistonPosition = pistonObject.transform.localPosition;
        pistonRotation = pistonObject.transform.localRotation;
//        part.subparts[0].localPosition = pistonPosition;
//        part.subparts[0].localRotation = pistonRotation;

    }
    void OnBuild(){
        constraint.Initialize();
    }
    void Start () {
        //UpdateLineRenderer();
    }

    void Update () {

        var center = (pistonObject.transform.position + gameObject.transform.position)/2.0f;
        //localPos.y = gameObject.transform.InverseTransformPoint(center).y;
        //visualFeedback.transform.localPosition = localPos;
		visualFeedback.transform.position = center;
		var pos =  gameObject.transform.InverseTransformPoint(pistonObject.transform.position).y /2.0f;
		producePartLocalPosition( pos);
		
		var feedbackDirection = pistonObject.transform.position - center;
		visualFeedback.transform.forward = feedbackDirection.normalized;
		
		
		visualFeedback.transform.localScale = new Vector3(1,1,pos*1.5f);
    }



    void UpdateLineRenderer () {
        link.SetVertexCount( 2 );
        link.SetPosition( 0, gameObject.transform.position );
        link.SetPosition( 1, pistonObject.transform.position );
        var offset = link.material.mainTextureOffset;
        offset.x += Time.deltaTime * 5f;
        link.material.mainTextureOffset = offset;
    }

    public void StorePistonStatus () {
//        pistonPosition = transform.InverseTransformPoint( pistonObject.transform.position );
//        pistonRotation = Quaternion.Inverse( transform.rotation ) * pistonObject.transform.rotation;
    }

    public void ResetPistonJoint () {
//        pistonObject.transform.position =  transform.TransformPoint( pistonPosition );
//        pistonObject.transform.rotation = transform.rotation * pistonRotation;
    }
}
