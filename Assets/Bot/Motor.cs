using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class Motor : MonoBehaviour {

    public NodeView nodeView;
    public Part part;
    public GameObject rotorObject;
    private float force = 100;
    public System.Action< float > producePartAngle = ( notAForce ) => {};
    public List< float > velocities = new List< float >();

    public BulletGenericMotor motor;
    public BulletGeneric6DofConstraint constraint;
    private Vector3 rotorPosition;
    private Quaternion rotorRotation;
    public float idle = 0.0f;

    void Awake () {
        rotorPosition = rotorObject.transform.localPosition;
        rotorRotation = rotorObject.transform.localRotation;
//        part.subparts[0].localPosition = rotorPosition;
//        part.subparts[0].localRotation = rotorRotation;
    }

    void OnBuild(){
        //constraint.Initialize();
    }

    void Update () {
        var velocity = Mathf.Abs( motor.velocity ) * 0.2f;
        if (idle >= 0.5f)
        {
            motor.Stop();
            velocity = 0;
        }
        GetComponent<AudioSource>().volume = velocity * velocity;
        GetComponent<AudioSource>().pitch = Mathf.Clamp( velocity * 2, 0.8f, 3 );

        var rotorRotation = Quaternion.Inverse( transform.rotation ) * rotorObject.transform.rotation;
        producePartAngle( rotorRotation.eulerAngles.y );
    }

    public void SetForce ( float force ) {
        this.force = force;
    }

    public void SetIdle(float idle)
    {
        this.idle = idle;
    }

    public void SetMotorAngle ( float value ) {
        if (idle < 0.5f)
        {
            motor.SetTarget(value * Mathf.PI / 180.0f);
        }
        else
        {
            motor.Stop();
        }
    }

    public void SetMotorVelocity ( float value ) {
        if (idle < 0.5f)
        {
            motor.SetVelocity(value);
        }
        else
        {
            motor.Stop();
        }
    }

    public void StoreMotorStatus () {
        rotorPosition = transform.InverseTransformPoint( rotorObject.transform.position );
        rotorRotation = Quaternion.Inverse( transform.rotation ) * rotorObject.transform.rotation;
    }

    public void ResetMotorJoint () {
        rotorObject.transform.position =  transform.TransformPoint( rotorPosition );
        rotorObject.transform.rotation = transform.rotation * rotorRotation;
    }

}
