using UnityEngine;
using System.Collections;
using Operants;

public class Speaker : MonoBehaviour {

    public AudioSource source;
    public Renderer feedback;
    bool loading = false;
    bool loaded = false;
    float volume = 0.0f;
    float maxIllumpower = 25;
    string loadedURL = string.Empty;
    string url;
    Operants.AudioFormat format = Operants.AudioFormat.wav;

    float requestPlay = 0;
    float lastPlayValue = 0;

    void Awake(){
        source.priority = 0;
        source.loop = false;
    }

    public void LoadSource(string url){
        if(url == string.Empty && GetComponent<AudioSource>().clip != null){
            if(GetComponent<AudioSource>().isPlaying){
                GetComponent<AudioSource>().Stop();
            }
            loaded = false;
            GetComponent<AudioSource>().clip = null;
            return;
        }else if ( loadedURL == url){
            return;
        }

        if ( loading ) {
            StopCoroutine("Load");
        }

        loading = true;
        loaded = false;
        this.url = url;
        StartCoroutine( "Load" , url);
    }

    public void SetAudioFormat(Operants.AudioFormat value){
        if ( format != value ) {
            StopCoroutine("Load");
            format = value;
            StartCoroutine("Load",url);
        }
    }

    public void Play( int value ){
        if(value != lastPlayValue){
            lastPlayValue = value;
            requestPlay = value;
        }
    }

    public void SetVolume( float value ){
        value = Mathf.Clamp01(value);
        volume = value;
        source.volume = volume;
    }

    IEnumerator Load ( string url ) {
        loadedURL = string.Empty;
        var www = new WWW( url );
        yield return www;
        if(www.error != null){
            Console.PushError("www",new string[]{ www.error });
        }else {
            UnityEngine.AudioType audioFormat = format == Operants.AudioFormat.wav ? UnityEngine.AudioType.WAV : UnityEngine.AudioType.OGGVORBIS;
            var audio = www.GetAudioClip(true, false, audioFormat);
            if ( audio != null ) {
                loaded = true;
                loadedURL = url;
                source.clip = audio;
            }
        }
        loading = false;
        www.Dispose();
        www = null;
    }

    void Update() {
        if(!loaded){
            feedback.material.SetFloat( "_IllumPower", 0);
            return;
        }
        if ( requestPlay == -1 ) {
            if ( !source.isPlaying ) {
                source.Play();
            }
        } else if(requestPlay > 0) {
            if(!source.isPlaying ){
                source.Play();
                --requestPlay;
            }
        }
        else if ( lastPlayValue == 0 ) {
            if ( source.isPlaying ) {
                source.Stop();
            }
        }

        float maxValue = float.MinValue;
        float[] spectrum = new float[64];
        source.GetSpectrumData(spectrum, 0, FFTWindow.Rectangular);
        for(int i = 0; i < 63 ; ++i){
            var value = 1.01f - Mathf.Pow(Mathf.Epsilon, spectrum[i]);
            maxValue = value > maxValue ? value : maxValue;
            var illum = Util.MapValue0X( value , 1.0f, 0, maxIllumpower );
            feedback.material.SetFloat( "_IllumPower", illum);
        }
    }

}
