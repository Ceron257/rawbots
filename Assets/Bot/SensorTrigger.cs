using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Operants;
using System.Linq;

public class SensorTrigger : MonoBehaviour {
    
    public CameraView view;
    public BulletRigidBody btRigidBody;
    Part viewPart;
    List< Part > parts = new List< Part >();
    BulletLayers raycastMask = BulletLayers.World | BulletLayers.HexBlock;
    //BulletLayers.Part : I've turned off visibility occlusion by parts.
    //BulletLayers.Structure : Removed cause pline wanted bridges to be see-through.
    //This mean you can't use the structure group for non-transparent objects.
	new Transform transform;
	
	void Awake(){
		transform = gameObject.transform;
        viewPart = view.GetComponent<Part>();
	}
	
    void Start(){
        btRigidBody.SetPosition(transform.position);
        btRigidBody.SetRotation(view.transform.rotation);
        btRigidBody.AddOnTriggerDelegate( OnTrigger );
    }

    void OnTrigger ( Vector3 relativeVelocity,BulletRigidBody other,Vector3 contactPoint,Vector3 lineOfAction ) {
        var part = other.GetComponent<Part>();
        //added 3rd condition so that the camera can't see (and don't count) the second half of actuators.
        if(part == default(Part) || part == viewPart || PartProxy.DetectIfSubpart(part.proxy.entity)){
            return;
        }

        if(CameraFilter(part) == false) {
            return;
        }

        if ( parts.IndexOf( part ) < 0 ) {
            parts.Add( part );
        }
    }

    void FixedUpdate () {
        var newScale = new Vector3( view.size, view.size, view.far - view.near);
        transform.localPosition = Vector3.forward * view.near;
        btRigidBody.SetPosition(transform.position);
        btRigidBody.SetRotation(view.transform.rotation);
        if ( !Mathf.Approximately( transform.localScale.x , newScale.x ) ||
             !Mathf.Approximately( transform.localScale.y , newScale.y ) ||
             !Mathf.Approximately( transform.localScale.z , newScale.z )) {
            transform.localScale = newScale;
            btRigidBody.SetCollisionShapeScale(newScale);
        }
        var activity = 0;
        for ( var i = 0; i < parts.Count; ) {
            var part = parts[ i ];
            if ( part == default( Part ) ) {
                parts.RemoveAt( i );
                continue;
            }
            else if ( Visibility( part ) ) {
                view.produceVisiblePart( part.proxy );
                ++activity;
            }
            ++i;
        }
        view.produceActivity( activity );
        if ( activity == 0 ) {
            view.produceVisiblePart( default( PartProxy ) );
        }
        parts.Clear();
    }


    bool CameraFilter (Part part) {//by z26
        if(part.proxy.Energy() < 1) {
            return false;
        }
        return true;
    }


    bool Visibility ( Part part ) {//reworked this method, its simpler and detects terrain properly -z26
        BulletRaycastHitDetails hit;
        var ray = new Ray();
        ray.origin = view.transform.InverseTransformPoint( part.transform.position );
        ray.origin = new Vector3( 0,0, view.near );
        ray.origin = view.transform.TransformPoint( ray.origin );

        Vector3 originToTarget = part.transform.position - ray.origin;
        ray.direction = originToTarget;
        //This didnt work with terrain partly because the "layer" argument (second to last) wasnt properly set
        bool intersect = Bullet.Raycast( ray, out hit, originToTarget.magnitude, raycastMask, raycastMask );

        if (intersect && hit.bulletRigidbody != part.bulletRigidBody) {//if intersecting with something that isnt the part itself
            //Debug.DrawRay( ray.origin, originToTarget, Color.red );
            return false;
        } else {//if intersecting with the part itself or nothing at all (depends on whenever raycastMask includes parts)
            //Debug.DrawRay( ray.origin, originToTarget, Color.green );
            return true;
        }
    }

}