using UnityEngine;
using System.Collections;

public class EnergyProcessor : MonoBehaviour {

    public GameObject rotor;
    public float anglePerSecond = 90;

    void Update(){
        MoveRotor();
    }

    public void MoveRotor(){
        rotor.transform.Rotate(Vector3.forward,anglePerSecond*Time.deltaTime);
    }


    void OnTriggerEnter ( Collider other ) {
        Part otherPart = other.GetComponent<Part>();
        if( otherPart != null ) {

            Debug.Log("This is a crystal");
        }
    }


}
