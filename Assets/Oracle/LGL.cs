using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Oracle {

    [System.Flags]
    public enum LGLResult {
        None = 0,
        Subject = 1,
        SubjectProperty = 2,
        SubjectPropertyValue = 4,
        Predicate = 8,
        PredicateProperty = 16,
        PredicatePropertyValue = 32,
        Object = 64,
        All = Subject | SubjectProperty | SubjectPropertyValue |
            Predicate | PredicateProperty | PredicatePropertyValue |
            Object,
    }

    public class LGL {/*

        Graph graph;
        Regex literalPattern;
        Regex wildcardPattern;

        List< System.Action > actions = new List< System.Action >();

        public LGL ( Graph graph ) {
            this.graph = graph;
            literalPattern = new Regex( @"^[a-z][a-z0-9_]*$" );
            wildcardPattern = new Regex( @"^\*$" );
        }

        public IEnumerable< string > Execute ( string expression ) {
            foreach ( var p in UnfoldExpression( expression ) ) {
                yield return p;
            }
            foreach ( var action in actions ) {
                action();
            }
            actions.Clear();
        }

        IEnumerable< string > UnfoldExpression ( string expression ) {
            Debug.Log("1-unfold expression "+ expression);
            expression = expression.Trim();
            if ( expression.Length == 0 ) {
                yield break;
            }
            else if ( expression.StartsWith( "--" ) || expression.StartsWith( "#" ) ) {
                yield break;
            }

            // parse expression into sub-expressions
            List< string > parts = new List< string >();

            int tokensOffset = 0;
            Debug.Log("2-unfold expression "+ expression);
            while ( tokensOffset < expression.Length ) {

                string part = string.Empty;
                var parenthesis = 0;
                var quotes = 0;
                var characters = 0;

                while ( tokensOffset + characters < expression.Length ) {

                    if ( expression[ tokensOffset + characters ] == '(' ) {
                        part += expression[ tokensOffset + characters ];
                        ++parenthesis;
                    }
                    else if ( expression[ tokensOffset + characters ] == ')' ) {
                        --parenthesis;
                        part += expression[ tokensOffset + characters ];
                        if ( parenthesis == 0 ) {
                            break;
                        }
                    }
                    else if ( parenthesis > 0 ) {
                        part += expression[ tokensOffset + characters ];
                    }
                    else if ( expression[ tokensOffset + characters ] == '\"' ) {
                        part += expression[ tokensOffset + characters ];
                        ++quotes;
                    }
                    else if ( expression[ tokensOffset + characters ] == '\"' ) {
                        --quotes;
                        part += expression[ tokensOffset + characters ];
                        if ( quotes == 0 ) {
                            break;
                        }
                    }
                    else if ( quotes > 0 ) {
                        part += expression[ tokensOffset + characters ];
                    }
                    else if ( expression[ tokensOffset + characters ] == ' ' ) {
                        break;
                    }
                    else {
                        part += expression[ tokensOffset + characters ];
                    }
                    ++characters;
                }

                tokensOffset = tokensOffset + characters + 1;
                if ( part != string.Empty && part != " " ) {
                    parts.Add( part );
                }
            }
            Debug.Log("parts "+ parts.Count);
            // determine expression function
            string func = string.Empty;
            if ( parts[ 0 ] == "?" ) {
                func = "?";
            }
            else if ( parts[ 0 ] == "-" ) {
                func = "-";
            }
            else if ( parts[ 0 ] == "+" ) {
                func = "+";
            }
            else if ( parts.Count > 0 ) {
                parts.Insert( 0, "+" );
            }
            else {
                Debug.LogError( "LGL Error\t" + expression );
                yield return "Error\t" + expression;
                yield break;
            }
            Debug.Log("parts "+ parts.Count);
            // is entity or relationship expression
            bool isEntity = false;

            if ( parts.Count == 2 || ( parts.Count >= 4 && parts[ 2 ] == "." ) ) {
                isEntity = true;
            }
            else if ( parts.Count == 3 ) {
                Debug.LogError( "LGL Error\t" + expression );
                yield return "Error\t" + expression;
                yield break;
            }

            if ( isEntity ) {

                var subPart = parts[ 1 ];
                var proPart = parts.Count >= 4 ? parts[ 3 ] : default( string );
                var valPart = parts.Count >= 5 ? parts[ 4 ] : default( string );

                ExpressionType subType = GetExpressionType( subPart );
                ExpressionType proType = proPart == default( string ) ? ExpressionType.Literal :
                    GetExpressionType( proPart );
                ExpressionType valType = valPart == default( string ) ? ExpressionType.Literal :
                    GetExpressionType( valPart );
                if ( subType == ExpressionType.Error ||
                    proType == ExpressionType.Error ||
                    valType == ExpressionType.Error ) {
                    Debug.LogError( "LGL Error\t" + expression );
                    yield return "Error\t" + expression;
                    yield break;
                }

                var subs = GenerateSubjects( func, subPart, subType );
                IEnumerable< string > pros = ( new List< string >() { default( string ) } ).AsEnumerable();
                if ( proPart != default( string ) ) {
                    pros = GenerateSubjectPros( func, proPart, proType );
                }
                IEnumerable< string > vals = ( new List< string >() { default( string ) } ).AsEnumerable();
                if ( valPart != default( string ) ) {
                    vals = GenerateSubjectProVals( func, valPart, valType );
                }

                var result = LGLResult.None;
                if ( subType != ExpressionType.Literal ) {
                    result |= LGLResult.Subject;
                }
                if ( proType != ExpressionType.Literal ) {
                    result |= LGLResult.SubjectProperty;
                }
                var valueMask = ExpressionType.Literal | ExpressionType.String | ExpressionType.Number;
                if ( ( valueMask & valType ) == 0 ) {
                    result |= LGLResult.SubjectPropertyValue;
                }
                if ( result == LGLResult.None ) {
                    result = LGLResult.All;
                }
                            Debug.Log("continuing");
                bool foundResult = false;
                foreach ( var sub in subs ) {
                    foreach ( var pro in pros ) {
                        foreach ( var val in vals ) {
                            if ( func == "+" ) {
                                AddEntity( sub, pro, val );
                                foundResult = true;
                                yield return "+ " +
                                    BuildResult( LGLResult.All, sub, pro, val, null, null, null, null );
                            }
                            else if ( func == "-" ) {
                                RemoveEntity( sub, pro, val );
                                foundResult = true;
                                yield return "- " +
                                    BuildResult( LGLResult.All, sub, pro, val, null, null, null, null );
                            }
                            else if ( func == "?" ) {
                                var e = graph.FindEntity( sub );
                                if ( e != default( Entity ) ) {
                                    bool success = true;
                                    if ( pro != default( string ) ) {
                                        var ePro = e.properties.FirstOrDefault( p => p.Name == pro );
                                        if ( ePro == default( Property ) ||
                                            ( val != default( string ) && ePro.Value.ToString() != val ) ) {
                                            success = false;
                                        }
                                    }
                                    if ( success ) {
                                        foundResult = true;
                                        yield return
                                            BuildResult( result, sub, pro, val, null, null, null, null );
                                    }
                                }
                            }
                        }
                    }
                }

                if ( !foundResult && func == "?" ) {
                    yield return "NotFound";
                    yield break;
                }
            }

            // is relationship
            else {

                var subPart = parts[ 1 ];
                var prePart = parts[ 2 ];
                var objPart = parts[ 3 ];
                var proPart = parts.Count >= 6 ? parts[ 5 ] : default( string );
                var valPart = parts.Count >= 7 ? parts[ 6 ] : default( string );

                if ( parts.Count >= 5 && parts[ 4 ] != "." ) {
                    Debug.LogError( "LGL Error\t" + expression );
                    yield return "Error\t" + expression;
                    yield break;
                }

                ExpressionType subType = GetExpressionType( subPart );
                ExpressionType preType = GetExpressionType( prePart );
                ExpressionType objType = GetExpressionType( objPart );
                ExpressionType proType =
                    proPart == default( string ) ? ExpressionType.Literal : GetExpressionType( proPart );
                ExpressionType valType =
                    valPart == default( string ) ? ExpressionType.Literal : GetExpressionType( valPart );
                if ( subType == ExpressionType.Error ||
                    preType == ExpressionType.Error ||
                    objType == ExpressionType.Error ||
                    proType == ExpressionType.Error ||
                    valType == ExpressionType.Error ) {
                    Debug.LogError( "LGL Error\t" + expression );
                    yield return "Error\t" + expression;
                    yield break;
                }

                var subs = GenerateSubjects( func, subPart, subType );
                var pres = GeneratePredicates( func, prePart, preType );
                var objs = GenerateObjects( func, objPart, objType );
                IEnumerable< string > pros = ( new List< string >() { default( string ) } ).AsEnumerable();
                if ( proPart != default( string ) ) {
                    pros = GeneratePredicatePros( func, proPart, proType );
                }
                IEnumerable< string > vals = ( new List< string >() { default( string ) } ).AsEnumerable();
                if ( valPart != default( string ) ) {
                    vals = GeneratePredicateProVals( func, valPart, valType );
                }

                var result = LGLResult.None;
                if ( subType != ExpressionType.Literal ) {
                    result |= LGLResult.Subject;
                }
                if ( preType != ExpressionType.Literal ) {
                    result |= LGLResult.Predicate;
                }
                if ( objType != ExpressionType.Literal ) {
                    result |= LGLResult.Object;
                }
                if ( proType != ExpressionType.Literal ) {
                    result |= LGLResult.PredicateProperty;
                }
                var valueMask = ExpressionType.Literal | ExpressionType.String | ExpressionType.Number;
                if ( ( valueMask & valType ) == 0 ) {
                    result |= LGLResult.PredicatePropertyValue;
                }
                if ( result == LGLResult.None ) {
                    result = LGLResult.All;
                }

                bool foundResult = false;
                foreach ( var sub in subs ) {
                    foreach ( var pre in pres ) {
                        foreach ( var obj in objs ) {
                            if ( sub == obj ) {
                                // TODO: make this break instead
                                continue;
                            }
                            foreach ( var pro in pros ) {
                                foreach ( var val in vals ) {
                                    if ( func == "+" ) {
                                        AddRelationship( sub, pre, obj, pro, val );
                                        foundResult = true;
                                        yield return "+ " +
                                            BuildResult( LGLResult.All, sub, null, null, pre, pro, val, obj );
                                    }
                                    else if ( func == "-" ) {
                                        RemoveRelationship( sub, pre, obj, pro, val );
                                        foundResult = true;
                                        yield return "- " +
                                            BuildResult( LGLResult.All, sub, null, null, pre, pro, val, obj );
                                    }
                                    else if ( func == "?" ) {
                                        var r = graph.FindRelationship( sub, pre, obj );
                                        if ( r != default( Relationship ) ) {
                                            bool success = true;
                                            if ( pro != default( string ) ) {
                                                var rPro = r.properties.FirstOrDefault( p => p.Name == pro );
                                                if ( rPro == default( Property ) || ( val != default( string )
                                                    && rPro.Value.ToString() != val ) ) {
                                                    success = false;
                                                }
                                            }
                                            if ( success ) {
                                                foundResult = true;
                                                yield return
                                                    BuildResult( result, sub, null, null, pre, pro, val, obj );
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if ( !foundResult && func == "?" ) {
                    yield return "NotFound";
                    yield break;
                }
            }
        }

        void AddEntity ( string sub, string pro, string val ) {
            actions.Add( () => {
                var e = graph.AddEntity( sub );
                if ( pro != default( string ) ) {
                    var type = GetExpressionType( val );
                    if ( ( type & ( ExpressionType.Number ) ) > 0 ) {
                        e.SetProperty( pro, float.Parse( val ) );
                    }
                    else {
                        e.SetProperty( pro, val.Replace( "\"", "" ) );
                    }
                }
            } );
        }

        void RemoveEntity ( string sub, string pro, string val ) {
            actions.Add( () => {
                if ( pro == default( string ) ) {
                    graph.RemoveEntity( sub );
                }
                else {
                    var e = graph.FindEntity( sub );
                    e.RemoveProperty( pro );
                }
            } );
        }

        void AddRelationship ( string sub, string pre, string obj, string pro, string val ) {
            actions.Add( () => {
                var r = graph.AddRelationship( sub, pre, obj );
                if ( pro != default( string ) ) {
                    var type = GetExpressionType( val );
                    if ( ( type & ( ExpressionType.Number ) ) > 0 ) {
                        r.SetProperty( pro, double.Parse( val ) );
                    }
                    else {
                        r.SetProperty( pro, val.Replace( "\"", "" ) );
                    }
                }
            } );
        }

        void RemoveRelationship ( string sub, string pre, string obj, string pro, string val ) {
            actions.Add( () => {
                if ( pro == default( string ) ) {
                    graph.RemoveRelationship( sub, pre, obj );
                }
                else {
                    var r = graph.FindRelationship( sub, pre, obj );
                    r.RemoveProperty( pro );
                }
            } );
        }

        static bool IsResult ( LGLResult mask, LGLResult result ) {
            return ( mask & result ) > 0;
        }

        string BuildResult ( LGLResult result,
            string sub, string subPro, string subProVal,
            string pre, string prePro, string preProVal,
            string obj ) {

            var build = string.Empty;

            if ( IsResult( result, LGLResult.Subject ) && sub != null ) {
                build += build.Length > 0 ? " " + sub : sub;
            }
            if ( IsResult( result, LGLResult.SubjectProperty ) && subPro != null ) {
                build += build.Length > 0 ? " . " + subPro : subPro;
            }
            if ( IsResult( result, LGLResult.SubjectPropertyValue ) && subProVal != null ) {
                build += build.Length > 0 ? " " + subProVal : subProVal;
            }
            if ( IsResult( result, LGLResult.Predicate ) && pre != null ) {
                build += build.Length > 0 ? " " + pre : pre;
            }
            if ( IsResult( result, LGLResult.Object ) && obj != null ) {
                build += build.Length > 0 ? " " + obj : obj;
            }
            if ( IsResult( result, LGLResult.PredicateProperty ) && prePro != null ) {
                build += build.Length > 0 ? " . " + prePro : prePro;
            }
            if ( IsResult( result, LGLResult.PredicatePropertyValue ) && preProVal != null ) {
                build += build.Length > 0 ? " " + preProVal : preProVal;
            }

            return build;
        }

        IEnumerable< string > GenerateSubjects ( string func, string sub, ExpressionType subType ) {
            var subs = graph.entities.Select( e => e.id );
            if ( subType == ExpressionType.Literal ) {
                if ( func == "+" ) {
                    subs = ( new List< string >(){ sub } ).AsEnumerable();
                }
                else {
                    subs = subs.Where( s => s == sub );
                }
            }
            else if ( subType == ExpressionType.Wildcard ) {
            }
            else if ( subType == ExpressionType.Group ) {
                var query = "? " + sub.Substring( 1, sub.Length - 2 );
                subs = UnfoldExpression( query ).Distinct();
            }
            return subs;
        }

        IEnumerable< string > GenerateSubjectPros ( string func, string pro, ExpressionType proType ) {
            var subPros = graph.entities
                .SelectMany( e => e.properties.Select( p => p.Name ) ).Distinct();
            if ( proType == ExpressionType.Literal ) {
                if ( func == "+" ) {
                    subPros = ( new List< string >(){ pro } ).AsEnumerable();
                }
                else {
                    subPros = subPros.Where( s => s == pro );
                }
            }
            else if ( proType == ExpressionType.Wildcard ) {
            }
            else if ( proType == ExpressionType.Group ) {
                var query = "? " + pro.Substring( 1, pro.Length - 2 );
                subPros = UnfoldExpression( query ).Distinct();
            }
            return subPros;
        }

        IEnumerable< string > GenerateSubjectProVals ( string func, string val, ExpressionType valType ) {
            var subProVals = graph.entities
                .SelectMany( e => e.properties.Select( p => p.Value.ToString() ) ).Distinct();
            if ( ( valType & ( ExpressionType.Literal | ExpressionType.String | ExpressionType.Number ) ) > 0 ) {
                if ( func == "+" ) {
                    subProVals = ( new List< string >(){ val } ).AsEnumerable();
                }
                else {
                    subProVals = subProVals
                        .Where( s => s == val )
                        .Select( s => s.Contains( ' ' ) ? "\"" + s + "\"" : s );
                }
            }
            else if ( valType == ExpressionType.Wildcard ) {
            }
            else if ( valType == ExpressionType.Group ) {
                var query = "? " + val.Substring( 1, val.Length - 2 );
                subProVals = UnfoldExpression( query ).Distinct();
            }
            return subProVals;
        }

        IEnumerable< string > GeneratePredicates ( string func, string pre, ExpressionType preType ) {
            var pres = graph.relationships.Select( r => r.Predicate ).Distinct();
            if ( preType == ExpressionType.Literal ) {
                if ( func == "+" ) {
                    pres = ( new List< string >(){ pre } ).AsEnumerable();
                }
                else {
                    pres = pres.Where( p => p == pre );
                }
            }
            else if ( preType == ExpressionType.Wildcard ) {
            }
            else if ( preType == ExpressionType.Group ) {
                var query = "? " + pre.Substring( 1, pre.Length - 2 );
                pres = UnfoldExpression( query ).Distinct();
            }
            return pres;
        }

        IEnumerable< string > GeneratePredicatePros ( string func, string pro, ExpressionType proType ) {
            var prePros = graph.relationships
                .SelectMany( r => r.properties.Select( p => p.Name ) ).Distinct();
            if ( proType == ExpressionType.Literal ) {
                if ( func == "+" ) {
                    prePros = ( new List< string >(){ pro } ).AsEnumerable();
                }
                else {
                    prePros = prePros.Where( s => s == pro );
                }
            }
            else if ( proType == ExpressionType.Wildcard ) {
            }
            else if ( proType == ExpressionType.Group ) {
                var query = "? " + pro.Substring( 1, pro.Length - 2 );
                prePros = UnfoldExpression( query ).Distinct();
            }
            return prePros;
        }

        IEnumerable< string > GeneratePredicateProVals ( string func, string val, ExpressionType valType ) {
            var preProVals = graph.relationships
                .SelectMany( r => r.properties.Select( p => p.Value.ToString() ) ).Distinct();
            if ( ( valType & ( ExpressionType.Literal | ExpressionType.String | ExpressionType.Number ) ) > 0 ) {
                if ( func == "+" ) {
                    preProVals = ( new List< string >(){ val } ).AsEnumerable();
                }
                else {
                    preProVals = preProVals
                        .Where( p => p == val )
                        .Select( p => p.Contains( ' ' ) ? "\"" + p + "\"" : p );
                }
            }
            else if ( valType == ExpressionType.Wildcard ) {
            }
            else if ( valType == ExpressionType.Group ) {
                var query = "? " + val.Substring( 1, val.Length - 2 );
                preProVals = UnfoldExpression( query ).Distinct();
            }
            return preProVals;
        }

        IEnumerable< string > GenerateObjects ( string func, string obj, ExpressionType objType ) {
            var objs = graph.entities.Select( e => e.id );
            if ( objType == ExpressionType.Literal ) {
                if ( func == "+" ) {
                    objs = ( new List< string >(){ obj } ).AsEnumerable();
                }
                else {
                    objs = objs.Where( o => o == obj );
                }
            }
            else if ( objType == ExpressionType.Wildcard ) {
            }
            else if ( objType == ExpressionType.Group ) {
                var query = "? " + obj.Substring( 1, obj.Length - 2 );
                objs = UnfoldExpression( query ).Distinct();
            }
            return objs;
        }

        [System.Flags]
        enum ExpressionType {
            Error = 0,
            Literal = 2,
            String = 4,
            Number = 8,
            Wildcard = 16,
            Group = 32,
        }

        ExpressionType GetExpressionType ( string expression ) {
            double number;
            if ( literalPattern.Match( expression ).Success ) {
                return ExpressionType.Literal;
            }
            else if ( wildcardPattern.Match( expression ).Success ) {
                return ExpressionType.Wildcard;
            }
            else if ( expression[ 0 ] == '(' ) {
                return ExpressionType.Group;
            }
            else if ( double.TryParse( expression, out number ) ) {
                return ExpressionType.Number;
            }
            else if ( expression[ 0 ] == '\"' || !expression.Contains( ' ' ) ) {
                return ExpressionType.String;
            }
            return ExpressionType.Error;
        }*/
    }
}
