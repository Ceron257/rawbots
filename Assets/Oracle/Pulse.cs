﻿using System.Diagnostics;
using UnityEngine;

namespace Oracle {

    [System.Serializable]
    public class Pulse {
//The purpose of this class is to trigger the action argument only if the signal just transistioned from 0 to 1. -z26
        int direction = 1;

        public void ForPulse ( float signal, System.Action action ) {
            ForPulse( Mathf.RoundToInt( Mathf.Clamp01( signal ) ), action );
        }

        public void ForPulse ( int signal, System.Action action ) {
            if ( direction == 0 && signal == 1 ) {
                direction = 1;
                action();
            }
            else if ( direction == 1 && signal == 0 ) {
                direction = 0;
            }
        }

    }

}
