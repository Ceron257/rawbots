using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Oracle;

public class Dijkstra {
 
    private static int infinity = int.MaxValue;
    
    public static List< T > GetPathFor < T > (
        List< T > graph, T origin, T destination, System.Func< T, List< T > > neighbors )where T : class {

        if(origin == destination){
            return new List< T >() { origin };
        }

        Dictionary< T, int > distances = new Dictionary< T, int >();
        Dictionary< T, T > previous = new Dictionary< T, T >();
        List< T > set = new List< T >();
        int alt;
        
        foreach ( var i in graph ) {
            distances.Add( i, infinity );
            previous.Add( i, default( T ) );
            set.Add( i );
        }

        distances[ origin ] = 0;
        
        while ( set.Count > 0 ) {
            var u = GetWSmallestDistance< T >( set, distances );
            if ( distances[ u ] == infinity ) {
                break;
            }
            set.Remove( u );
            
            var n = neighbors(u);
            
            foreach ( var neighbor in n ) {
                alt = distances[ u ] + GetNeighborWeight<T>( neighbor, u );
                if ( !distances.ContainsKey( neighbor ) ) {
                    continue;
                }
                if ( alt < distances[ neighbor ] ) {
                    distances[ neighbor ] = alt;
                    previous[ neighbor ] = u;
                }
            }

        }
        
        var path = new List< T >(); 
        
        while ( previous[ destination ] != null ) {
            path.Add( destination );
            destination = previous[ destination ];
        }
        if(path.Count > 0){
            path.Add( origin );
        }
        path.Reverse();
        
        return path;
    }
    
    private static int GetNeighborWeight< T > ( T fromEntity, T toEntity ) {
        //TODO: Implement weight for neighbours
        return 1;
    }
    
    private static T GetWSmallestDistance< T > ( List< T > set, Dictionary< T, int > distances )where T : class {
        
        int smallest = distances[ set.First() ];
        var closets = default( T );
        
        foreach ( var entity in set ) {
            if ( smallest > distances[ entity ] ) {
                smallest = distances[ entity ];
            }
        }
        
        foreach ( var key in distances.Keys ) {
            if ( distances[key] == smallest && set.Any( e => e == key) ) {
                closets = key;
            }
        }
        
        return closets;
    }

    

}