#include Assembler/Assembly
#include Assembler/Touch
#include Bot/Bot

#####################################################
# Factory Things
#####################################################

factory_operant is operant
factory_input is input
button_operant is operant
button_output is output
factory is factory_operant
factory . proxy Tests/Factory
factory_button_input input_for factory
factory_button_input . label factory_button
factory_button_input . property factory_button
factory_button_input is factory_input
button is button_operant
button . proxy Tests/Button
button_output output_for button
button_output . label button_tap
button_output . property button_tap
button_output is button_output
fac01 as factory
fac01 . position [2,7,0]
fac01_input input_for fac01
fac01_input as factory_button_input
btn001 as button
btn001 . label Disassemble
btn001 . position [0,7,0]
btn001_output output_for btn001
btn001_output as button_output
btn002 as button
btn002 . label Save
btn002 . position [0,9,0]
btn002_output output_for btn002
btn002_output as button_output

#cam01 as camera_control
#cam01 . position [-8,4,0]
#cam01_two_finger_move input_for cam01
#cam01_two_finger_move as camera_control_two_finger_move

fac01_input consumes btn001_output
fac01_input consumes btn002_output
# cam01_two_finger_move consumes tf01_output

#####################################################
# touches
#####################################################

btn001_touch_event input_for btn001
btn001_touch_event as touch_machine_touch_event
btn001_touch_event consumes te01_touch_event

btn002_touch_event input_for btn002
btn002_touch_event as touch_machine_touch_event
btn002_touch_event consumes te01_touch_event

