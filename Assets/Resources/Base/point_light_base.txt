pointlight_xform as xform
pointlight_xform local_to gaia

################################################
# point_light001
################################################
point_light001 . position [0,0,0]
point_light001 . rotation [0,0,0]
point_light001 . permissions 31
point_light001 as point_light
point_light001 local_to pointlight_xform