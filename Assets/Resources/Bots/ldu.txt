################################################
##core00
################################################
core00 . position [0,0,0]
core00 . rotation [0,0,0]
core00 as core

core00_pos output_for core00
core00_pos as core_pos

core00_slot_0 slot_for core00
core00_slot_0 as core_slot_0
core00_slot_1 slot_for core00
core00_slot_1 as core_slot_1
core00_slot_2 slot_for core00
core00_slot_2 as core_slot_2
core00_slot_3 slot_for core00
core00_slot_3 as core_slot_3
core00_slot_4 slot_for core00
core00_slot_4 as core_slot_4
core00_slot_5 slot_for core00
core00_slot_5 as core_slot_5

################################################
##center_right_motor
################################################
center_right_motor . position [2,0,0]
center_right_motor . rotation [0,0,270]
center_right_motor as motor

center_right_motor_slot_0 slot_for center_right_motor
center_right_motor_slot_0 as motor_slot_0
center_right_motor_slot_1 slot_for center_right_motor
center_right_motor_slot_1 as motor_slot_1

center_right_motor_angle input_for center_right_motor
center_right_motor_angle as motor_angle
center_right_motor_angle . value 0

center_right_motor_velocity input_for center_right_motor
center_right_motor_velocity as motor_velocity



################################################
##center_right_wheel
################################################
center_right_wheel . position [4,0,0]
center_right_wheel . rotation [0,90,0]
center_right_wheel as wide_wheel_small

center_right_wheel_slot_0 slot_for center_right_wheel
center_right_wheel_slot_0 as wide_wheel_small_slot_0
center_right_wheel_slot_1 slot_for center_right_wheel
center_right_wheel_slot_1 as wide_wheel_small_slot_1
center_right_motor_slot_1 extends core00_slot_0
center_right_wheel_slot_1 extends center_right_motor_slot_0


################################################
##center_left_motor
################################################
center_left_motor . position [-2,0,0]
center_left_motor . rotation [0,0,90]
center_left_motor as motor

center_left_motor_slot_0 slot_for center_left_motor
center_left_motor_slot_0 as motor_slot_0
center_left_motor_slot_1 slot_for center_left_motor
center_left_motor_slot_1 as motor_slot_1

center_left_motor_angle input_for center_left_motor
center_left_motor_angle as motor_angle
center_left_motor_angle . value 0

center_left_motor_velocity input_for center_left_motor
center_left_motor_velocity as motor_velocity




################################################
##center_left_wheel
################################################
center_left_wheel . position [-4,0,0]
center_left_wheel . rotation [0,90,0]
center_left_wheel as wide_wheel_small

center_left_wheel_slot_0 slot_for center_left_wheel
center_left_wheel_slot_0 as wide_wheel_small_slot_0
center_left_wheel_slot_1 slot_for center_left_wheel
center_left_wheel_slot_1 as wide_wheel_small_slot_1
center_left_motor_slot_1 extends core00_slot_3
center_left_wheel_slot_0 extends center_left_motor_slot_0

################################################
##front_wheel_support
################################################
front_wheel_support . position [0,0,4]
front_wheel_support . rotation [0,0,0]
front_wheel_support as continuum

front_wheel_support_slot_0 slot_for front_wheel_support
front_wheel_support_slot_0 as continuum_slot_0
front_wheel_support_slot_1 slot_for front_wheel_support
front_wheel_support_slot_1 as continuum_slot_1
front_wheel_support_slot_2 slot_for front_wheel_support
front_wheel_support_slot_2 as continuum_slot_2
front_wheel_support_slot_3 slot_for front_wheel_support
front_wheel_support_slot_3 as continuum_slot_3
front_wheel_support_slot_4 slot_for front_wheel_support
front_wheel_support_slot_4 as continuum_slot_4
front_wheel_support_slot_5 slot_for front_wheel_support
front_wheel_support_slot_5 as continuum_slot_5
front_wheel_support_slot_5 extends front_support_slot_2


################################################
##front_right_motor
################################################
front_right_motor . position [2,0,4]
front_right_motor . rotation [0,0,270]
front_right_motor as motor

front_right_motor_slot_0 slot_for front_right_motor
front_right_motor_slot_0 as motor_slot_0
front_right_motor_slot_1 slot_for front_right_motor
front_right_motor_slot_1 as motor_slot_1

front_right_motor_angle input_for front_right_motor
front_right_motor_angle as motor_angle
front_right_motor_angle . value 0

front_right_motor_velocity input_for front_right_motor
front_right_motor_velocity as motor_velocity



################################################
##front_right_wheel
################################################
front_right_wheel . position [4,0,4]
front_right_wheel . rotation [0,90,0]
front_right_wheel as wide_wheel_small

front_right_wheel_slot_0 slot_for front_right_wheel
front_right_wheel_slot_0 as wide_wheel_small_slot_0
front_right_wheel_slot_1 slot_for front_right_wheel
front_right_wheel_slot_1 as wide_wheel_small_slot_1
front_right_motor_slot_1 extends front_wheel_support_slot_0
front_right_wheel_slot_1 extends front_right_motor_slot_0


################################################
##front_left_motor
################################################
front_left_motor . position [-2,0,4]
front_left_motor . rotation [0,0,90]
front_left_motor as motor

front_left_motor_slot_0 slot_for front_left_motor
front_left_motor_slot_0 as motor_slot_0
front_left_motor_slot_1 slot_for front_left_motor
front_left_motor_slot_1 as motor_slot_1

front_left_motor_angle input_for front_left_motor
front_left_motor_angle as motor_angle
front_left_motor_angle . value 0

front_left_motor_velocity input_for front_left_motor
front_left_motor_velocity as motor_velocity




################################################
##front_left_wheel
################################################
front_left_wheel . position [-4,0,4]
front_left_wheel . rotation [0,90,0]
front_left_wheel as wide_wheel_small

front_left_wheel_slot_0 slot_for front_left_wheel
front_left_wheel_slot_0 as wide_wheel_small_slot_0
front_left_wheel_slot_1 slot_for front_left_wheel
front_left_wheel_slot_1 as wide_wheel_small_slot_1
front_left_motor_slot_1 extends front_wheel_support_slot_3
front_left_wheel_slot_0 extends front_left_motor_slot_0


################################################
##front_support
################################################
front_support . position [0,0,2]
front_support . rotation [0,0,0]
front_support as continuum

front_support_slot_0 slot_for front_support
front_support_slot_0 as continuum_slot_0
front_support_slot_1 slot_for front_support
front_support_slot_1 as continuum_slot_1
front_support_slot_2 slot_for front_support
front_support_slot_2 as continuum_slot_2
front_support_slot_3 slot_for front_support
front_support_slot_3 as continuum_slot_3
front_support_slot_4 slot_for front_support
front_support_slot_4 as continuum_slot_4
front_support_slot_5 slot_for front_support
front_support_slot_5 as continuum_slot_5
front_support_slot_2 extends front_wheel_support_slot_5
core00_slot_2 extends front_support_slot_5


################################################
##hip
################################################
hip . position [0,2,2]
hip . rotation [0,0,0]
hip as continuum

hip_slot_0 slot_for hip
hip_slot_0 as continuum_slot_0
hip_slot_1 slot_for hip
hip_slot_1 as continuum_slot_1
hip_slot_2 slot_for hip
hip_slot_2 as continuum_slot_2
hip_slot_3 slot_for hip
hip_slot_3 as continuum_slot_3
hip_slot_4 slot_for hip
hip_slot_4 as continuum_slot_4
hip_slot_5 slot_for hip
hip_slot_5 as continuum_slot_5


################################################
##chest
################################################
chest . position [0,4,2]
chest . rotation [0,0,0]
chest as continuum

chest_slot_0 slot_for chest
chest_slot_0 as continuum_slot_0
chest_slot_1 slot_for chest
chest_slot_1 as continuum_slot_1
chest_slot_2 slot_for chest
chest_slot_2 as continuum_slot_2
chest_slot_3 slot_for chest
chest_slot_3 as continuum_slot_3
chest_slot_4 slot_for chest
chest_slot_4 as continuum_slot_4
chest_slot_5 slot_for chest
chest_slot_5 as continuum_slot_5

################################################
##head_support
################################################
head_support . position [0,6,2]
head_support . rotation [0,0,0]
head_support as continuum

head_support_slot_0 slot_for head_support
head_support_slot_0 as continuum_slot_0
head_support_slot_1 slot_for head_support
head_support_slot_1 as continuum_slot_1
head_support_slot_2 slot_for head_support
head_support_slot_2 as continuum_slot_2
head_support_slot_3 slot_for head_support
head_support_slot_3 as continuum_slot_3
head_support_slot_4 slot_for head_support
head_support_slot_4 as continuum_slot_4
head_support_slot_5 slot_for head_support
head_support_slot_5 as continuum_slot_5

head_support_slot_4 extends chest_slot_1

################################################
##head
################################################
head . position [0,6,3]
head . rotation [0,0,0]
head as camera

head_slot_0 slot_for head
head_slot_0 as camera_slot_0

head_slot_0 extends head_support_slot_2

hip_slot_4 extends front_support_slot_1
chest_slot_4 extends hip_slot_1

################################################
##shield_support
################################################
shield_support . position [0,2,4]
shield_support . rotation [0,0,0]
shield_support as continuum

shield_support_slot_0 slot_for shield_support
shield_support_slot_0 as continuum_slot_0
shield_support_slot_1 slot_for shield_support
shield_support_slot_1 as continuum_slot_1
shield_support_slot_2 slot_for shield_support
shield_support_slot_2 as continuum_slot_2
shield_support_slot_3 slot_for shield_support
shield_support_slot_3 as continuum_slot_3
shield_support_slot_4 slot_for shield_support
shield_support_slot_4 as continuum_slot_4
shield_support_slot_5 slot_for shield_support
shield_support_slot_5 as continuum_slot_5
shield_support_slot_4 extends front_wheel_support_slot_1


################################################
##right_light_holder
################################################
right_light_holder . position [2,4,2]
right_light_holder . rotation [0,0,0]
right_light_holder as elbow

right_light_holder_slot_0 slot_for right_light_holder
right_light_holder_slot_0 as elbow_slot_0
right_light_holder_slot_1 slot_for right_light_holder
right_light_holder_slot_1 as elbow_slot_1
right_light_holder_slot_2 slot_for right_light_holder
right_light_holder_slot_2 as elbow_slot_2
right_light_holder_slot_3 slot_for right_light_holder
right_light_holder_slot_3 as elbow_slot_3

right_light_holder_angle . value 90
right_light_holder_angle input_for right_light_holder
right_light_holder_angle as elbow_angle


right_light_holder_slot_1 extends chest_slot_0

################################################
##right_light
################################################

right_light . position [4,4,2]
right_light . rotation [0,0,270]
right_light as spot_light

right_light_iris_state input_for right_light
right_light_iris_state as spot_iris_state

right_light_slot_0 slot_for right_light
right_light_slot_0 as spot_light_slot_0

right_light_slot_0 extends right_light_holder_slot_0

################################################
##left_light_holder
################################################
left_light_holder . position [-2,4,2]
left_light_holder . rotation [0,180,0]
left_light_holder as elbow

left_light_holder_slot_0 slot_for left_light_holder
left_light_holder_slot_0 as elbow_slot_0
left_light_holder_slot_1 slot_for left_light_holder
left_light_holder_slot_1 as elbow_slot_1
left_light_holder_angle . value -90
left_light_holder_angle input_for left_light_holder
left_light_holder_angle as elbow_angle

left_light_holder_slot_1 extends chest_slot_3

################################################
##left_light
################################################

left_light . position [-4,4,2]
left_light . rotation [0,0,90]
left_light as spot_light

left_light_iris_state input_for left_light
left_light_iris_state as spot_iris_state

left_light_slot_0 slot_for left_light
left_light_slot_0 as spot_light_slot_0



left_light_slot_0 extends left_light_holder_slot_0

################################################
##crane_motor
################################################

crane_motor . position [0,2,0]
crane_motor . rotation [0,0,0]
crane_motor as motor

crane_motor_slot_0 slot_for crane_motor
crane_motor_slot_0 as motor_slot_0
crane_motor_slot_1 slot_for crane_motor
crane_motor_slot_1 as motor_slot_1

crane_motor_angle input_for crane_motor
crane_motor_angle as motor_angle

crane_motor_velocity input_for crane_motor
crane_motor_velocity as motor_velocity
crane_motor_velocity . value 5

crane_motor_slot_1 extends core00_slot_1

################################################
##point_light
################################################

light00 . position [0,3,0]
light00 . rotation [0,0,0]
light00 as point_light

light00_slot_0 slot_for light00
light00_slot_0 as point_light_slot_0

light00_slot_0 extends crane_motor_slot_0

################################################
##shield
################################################
front_shield . position [0,3,4]
front_shield . rotation [0,0,0]
front_shield as bdy_cvr_tlt_gen01_05

front_shield_slot_0 slot_for front_shield
front_shield_slot_0 as bdy_cvr_tlt_gen01_05_slot_0

front_shield_slot_0 extends shield_support_slot_2


################################################
##shovel
################################################
shovel01 . position [0,0,8]
shovel01 . rotation [0,90,0]
shovel01 as shovel

shovel01_slot_0 slot_for shovel01
shovel01_slot_0 as shovel_slot_0

shovel01_slot_0 extends front_wheel_support_slot_2

######################################################
# input_setups
######################################################

v_axis_control as input_sampler
v_axis_control_attack as input_sampler_attack
v_axis_control_attack . value 1
v_axis_control_attack input_for v_axis_control

v_axis_control_positive as input_sampler_positive
v_axis_control_positive . value W
v_axis_control_positive input_for v_axis_control

v_axis_control_negative as input_sampler_negative
v_axis_control_negative . value S
v_axis_control_negative input_for v_axis_control


v_axis_control_sample output_for v_axis_control
v_axis_control_sample as input_sampler_sample

v_axis_mapper as sample_mapper
v_axis_mapper_sample_min as sample_mapper_sample_min
v_axis_mapper_sample_min . value -1
v_axis_mapper_sample_min input_for v_axis_mapper

v_axis_mapper_sample_max as sample_mapper_sample_max
v_axis_mapper_sample_max . value 1
v_axis_mapper_sample_max input_for v_axis_mapper

v_axis_mapper_mapped_min as sample_mapper_mapped_min
v_axis_mapper_mapped_min . value -10
v_axis_mapper_mapped_min input_for v_axis_mapper

v_axis_mapper_mapped_max as sample_mapper_mapped_max
v_axis_mapper_mapped_max . value 10
v_axis_mapper_mapped_max input_for v_axis_mapper

v_axis_mapper_invert as sample_mapper_invert
v_axis_mapper_invert . value False
v_axis_mapper_invert input_for v_axis_mapper


v_axis_mapper_sample input_for v_axis_mapper
v_axis_mapper_sample as sample_mapper_sample

v_axis_mapper_mapped output_for v_axis_mapper
v_axis_mapper_mapped as sample_mapper_mapped

v_axis_mapper_inverted output_for v_axis_mapper
v_axis_mapper_inverted as sample_mapper_inverted

v_axis_mapper_sample consumes v_axis_control_sample

h_axis_control as input_sampler
h_axis_control_attack as input_sampler_attack
h_axis_control_attack . value 1
h_axis_control_attack input_for h_axis_control

h_axis_control_positive as input_sampler_positive
h_axis_control_positive . value D
h_axis_control_positive input_for h_axis_control

h_axis_control_negative as input_sampler_negative
h_axis_control_negative . value A
h_axis_control_negative input_for h_axis_control


h_axis_control_sample output_for h_axis_control
h_axis_control_sample as input_sampler_sample

h_axis_mapper as sample_mapper
h_axis_mapper_sample_min as sample_mapper_sample_min
h_axis_mapper_sample_min . value -1
h_axis_mapper_sample_min input_for h_axis_mapper

h_axis_mapper_sample_max as sample_mapper_sample_max
h_axis_mapper_sample_max . value 1
h_axis_mapper_sample_max input_for h_axis_mapper

h_axis_mapper_mapped_min as sample_mapper_mapped_min
h_axis_mapper_mapped_min . value -3
h_axis_mapper_mapped_min input_for h_axis_mapper

h_axis_mapper_mapped_max as sample_mapper_mapped_max
h_axis_mapper_mapped_max . value 3
h_axis_mapper_mapped_max input_for h_axis_mapper

h_axis_mapper_invert as sample_mapper_invert
h_axis_mapper_invert . value False
h_axis_mapper_invert input_for h_axis_mapper


h_axis_mapper_sample input_for h_axis_mapper
h_axis_mapper_sample as sample_mapper_sample

h_axis_mapper_mapped output_for h_axis_mapper
h_axis_mapper_mapped as sample_mapper_mapped

h_axis_mapper_inverted output_for h_axis_mapper
h_axis_mapper_inverted as sample_mapper_inverted

h_axis_mapper_sample consumes h_axis_control_sample

######################################################

h_axis_control as input_sampler
h_axis_control . name lift
h_axis_control_attack as input_sampler_attack
h_axis_control_attack . value 0.5
h_axis_control_attack input_for h_axis_control

h_axis_control_positive as input_sampler_positive
h_axis_control_positive . value D
h_axis_control_positive input_for h_axis_control

h_axis_control_negative as input_sampler_negative
h_axis_control_negative . value A
h_axis_control_negative input_for h_axis_control


h_axis_control_sample output_for h_axis_control
h_axis_control_sample as input_sampler_sample

h_axis_mapper as sample_mapper
h_axis_mapper_sample_min as sample_mapper_sample_min
h_axis_mapper_sample_min . value -1
h_axis_mapper_sample_min input_for h_axis_mapper

h_axis_mapper_sample_max as sample_mapper_sample_max
h_axis_mapper_sample_max . value 1
h_axis_mapper_sample_max input_for h_axis_mapper

h_axis_mapper_mapped_min as sample_mapper_mapped_min
h_axis_mapper_mapped_min . value -5
h_axis_mapper_mapped_min input_for h_axis_mapper

h_axis_mapper_mapped_max as sample_mapper_mapped_max
h_axis_mapper_mapped_max . value 5
h_axis_mapper_mapped_max input_for h_axis_mapper

h_axis_mapper_invert as sample_mapper_invert
h_axis_mapper_invert . value False
h_axis_mapper_invert input_for h_axis_mapper


h_axis_mapper_sample input_for h_axis_mapper
h_axis_mapper_sample as sample_mapper_sample

h_axis_mapper_mapped output_for h_axis_mapper
h_axis_mapper_mapped as sample_mapper_mapped

h_axis_mapper_inverted_mapped output_for h_axis_mapper
h_axis_mapper_inverted_mapped as sample_mapper_mapped_inverted

h_axis_mapper_sample consumes h_axis_control_sample

######################################################

center_right_motor_velocity consumes v_axis_mapper_mapped
front_right_motor_velocity consumes v_axis_mapper_mapped
back_right_motor_velocity consumes v_axis_mapper_mapped
center_left_motor_velocity consumes v_axis_mapper_inverted
front_left_motor_velocity consumes v_axis_mapper_inverted
back_left_motor_velocity consumes v_axis_mapper_inverted

center_right_motor_velocity consumes h_axis_mapper_inverted
front_right_motor_velocity consumes h_axis_mapper_inverted
back_right_motor_velocity consumes h_axis_mapper_inverted
center_left_motor_velocity consumes h_axis_mapper_inverted
front_left_motor_velocity consumes h_axis_mapper_inverted
back_left_motor_velocity consumes h_axis_mapper_inverted