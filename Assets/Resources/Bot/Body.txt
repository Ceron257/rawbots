######################################################
# body
######################################################

body . proxy Operants.BodyBaseProxy
body is part

######################################################
# body_gen01_1
######################################################

body_gen01_1 . view Bot/body_gen01_1
body_gen01_1 is body

body_gen01_1_slot_0 slot_for body_gen01_1
body_gen01_1_slot_0 . position [0,0,0]
body_gen01_1_slot_0 . rotation [-90,0,0]
body_gen01_1_slot_0 is slot

######################################################
# body_gen01_2
######################################################

body_gen01_2 . view Bot/body_gen01_2
body_gen01_2 is body

body_gen01_2_slot_0 slot_for body_gen01_2
body_gen01_2_slot_0 . position [0,0,0]
body_gen01_2_slot_0 . rotation [-90,0,0]
body_gen01_2_slot_0 is slot

######################################################
# body_gen01_3
######################################################

body_gen01_3 . view Bot/body_gen01_3
body_gen01_3 is body

# bottom slot
body_gen01_3_slot_0 slot_for body_gen01_3
body_gen01_3_slot_0 . position [0,0,0]
body_gen01_3_slot_0 . rotation [-90,0,0]
body_gen01_3_slot_0 is slot

######################################################
# body_gen01_4
######################################################

body_gen01_4 . view Bot/body_gen01_4
body_gen01_4 is body

body_gen01_4_slot_0 slot_for body_gen01_4
body_gen01_4_slot_0 . position [0,0,0]
body_gen01_4_slot_0 . rotation [-90,0,0]
body_gen01_4_slot_0 is slot

######################################################
# bdy_cvr_tlt_gen01_05
######################################################

bdy_cvr_tlt_gen01_05 . view Bot/bdy_cvr_tlt_gen01_05
bdy_cvr_tlt_gen01_05 is body

bdy_cvr_tlt_gen01_05_slot_0 slot_for bdy_cvr_tlt_gen01_05
bdy_cvr_tlt_gen01_05_slot_0 . position [0,-1,1]
bdy_cvr_tlt_gen01_05_slot_0 . rotation [180,0,0]
bdy_cvr_tlt_gen01_05_slot_0 is slot

######################################################
# body_gen02_1
######################################################

body_gen02_1 . view Bot/body_gen02_1
body_gen02_1 is body

body_gen02_1_slot_0 slot_for body_gen02_1
body_gen02_1_slot_0 . position [0,0,0]
body_gen02_1_slot_0 . rotation [-90,0,0]
body_gen02_1_slot_0 is slot

######################################################
# body_gen02_2
######################################################

body_gen02_2 . view Bot/body_gen02_2
body_gen02_2 is body

body_gen02_2_slot_0 slot_for body_gen02_2
body_gen02_2_slot_0 . position [0,0,0]
body_gen02_2_slot_0 . rotation [-90,0,0]
body_gen02_2_slot_0 is slot

######################################################
# body_gen02_3
######################################################

body_gen02_3 . view Bot/body_gen02_3
body_gen02_3 is body

body_gen02_3_slot_0 slot_for body_gen02_3
body_gen02_3_slot_0 . position [0,0,-1]
body_gen02_3_slot_0 . rotation [-90,0,0]
body_gen02_3_slot_0 is slot

######################################################
# body_gen03_1
######################################################

body_gen03_1 . view Bot/body_gen03_1
body_gen03_1 is body

body_gen03_1_slot_0 slot_for body_gen03_1
body_gen03_1_slot_0 . position [0,0,0]
body_gen03_1_slot_0 . rotation [-90,0,0]
body_gen03_1_slot_0 is slot

body_gen03_1_slot_1 slot_for body_gen03_1
body_gen03_1_slot_1 . position [0,0,2]
body_gen03_1_slot_1 . rotation [-90,0,0]
body_gen03_1_slot_1 is slot

body_gen03_1_slot_2 slot_for body_gen03_1
body_gen03_1_slot_2 . position [0,0,-2]
body_gen03_1_slot_2 . rotation [-90,0,0]
body_gen03_1_slot_2 is slot

######################################################
# body_gen03_2
######################################################

body_gen03_2 . view Bot/body_gen03_2
body_gen03_2 is body

body_gen03_2_slot_0 slot_for body_gen03_2
body_gen03_2_slot_0 . position [1,0,0]
body_gen03_2_slot_0 . rotation [0,180,0]
body_gen03_2_slot_0 is slot

######################################################
# body_gen03_3
######################################################

body_gen03_3 . view Bot/body_gen03_3
body_gen03_3 is body

body_gen03_3_slot_0 slot_for body_gen03_3
body_gen03_3_slot_0 . position [0,0,0]
body_gen03_3_slot_0 . rotation [-90,0,0]
body_gen03_3_slot_0 is slot

######################################################
# body_gen03_4
######################################################

body_gen03_4 . view Bot/body_gen03_4
body_gen03_4 is body

body_gen03_4_slot_0 slot_for body_gen03_4
body_gen03_4_slot_0 . position [0,0,0]
body_gen03_4_slot_0 . rotation [0,90,0]
body_gen03_4_slot_0 is slot

######################################################
# body_gen03_5
######################################################

body_gen03_5 . view Bot/body_gen03_5
body_gen03_5 is body

body_gen03_5_slot_0 slot_for body_gen03_5
body_gen03_5_slot_0 . position [0,0,0]
body_gen03_5_slot_0 . rotation [0,-90,0]
body_gen03_5_slot_0 is slot

body_gen03_5_slot_1 slot_for body_gen03_5
body_gen03_5_slot_1 . position [0,0,-2]
body_gen03_5_slot_1 . rotation [0,-90,0]
body_gen03_5_slot_1 is slot

######################################################
# body_gen03_6
######################################################

body_gen03_6 . view Bot/body_gen03_6
body_gen03_6 is body

body_gen03_6_slot_0 slot_for body_gen03_6
body_gen03_6_slot_0 . position [0,0,0]
body_gen03_6_slot_0 . rotation [0,180,0]
body_gen03_6_slot_0 is slot

######################################################
# body_gen04_1
######################################################

body_gen04_1 . view Bot/body_gen04_1
body_gen04_1 is body

body_gen04_1_slot_0 slot_for body_gen04_1
body_gen04_1_slot_0 . position [0,0,0]
body_gen04_1_slot_0 . rotation [0,0,0]
body_gen04_1_slot_0 is slot

######################################################
# bdy_cvr_tlt_gen04_01
######################################################

bdy_cvr_tlt_gen04_01 . view Bot/bdy_cvr_tlt_gen04_01
bdy_cvr_tlt_gen04_01 is body

bdy_cvr_tlt_gen04_01_slot_0 slot_for bdy_cvr_tlt_gen04_01
bdy_cvr_tlt_gen04_01_slot_0 . position [0,0,-1]
bdy_cvr_tlt_gen04_01_slot_0 . rotation [0,0,0]
bdy_cvr_tlt_gen04_01_slot_0 is slot

######################################################
# bdy_cvr_tlt_gen04_02
######################################################

bdy_cvr_tlt_gen04_02 . view Bot/bdy_cvr_tlt_gen04_02
bdy_cvr_tlt_gen04_02 is body

bdy_cvr_tlt_gen04_02_slot_0 slot_for bdy_cvr_tlt_gen04_02
bdy_cvr_tlt_gen04_02_slot_0 . position [0,-1,0]
bdy_cvr_tlt_gen04_02_slot_0 . rotation [90,0,0]
bdy_cvr_tlt_gen04_02_slot_0 is slot

######################################################
# shovel
######################################################

shovel . proxy Operants.BodyBaseProxy
shovel . view Bot/Shovel
shovel is part

shovel_slot_0 slot_for shovel
shovel_slot_0 . position [2,0,0]
shovel_slot_0 . rotation [0,90,0]
shovel_slot_0 is slot

