using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {
	
	public Camera currentCamera;
	public float force = 400f;
    public GameObject prefab;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		
		
		if(Input.GetMouseButtonDown(0)){
			
			Ray ray = currentCamera.ScreenPointToRay(Input.mousePosition);
			//Debug.DrawRay(ray.origin,ray.direction,Color.green,5f);
			
			var pos = transform.position+transform.forward*3f;
			var test = (GameObject)Instantiate (prefab, pos, Quaternion.identity);
			var bulletRigidbody = test.GetComponent<BulletRigidBody>();
			bulletRigidbody.AddForce(ray.direction*force,Vector3.zero);
			
		}
		
	
	}
}
