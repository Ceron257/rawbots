using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

[Flags]
public enum BulletLayers
{
    Part            = 1 << 0,
    GhostPart       = 1 << 1,
    World           = 1 << 2,
    Structure       = 1 << 3,
    GhostStructure  = 1 << 4,
    Water           = 1 << 5,
    Sensor          = 1 << 6,
    Camera          = 1 << 7,
    HexBlock        = 1 << 8,
}

public enum BulletObjectType {
    BulletRigidbody = 0,
    BulletConstraint = 1,
    BulletMotor = 2
}

[StructLayout(LayoutKind.Sequential, Pack=1)]
public struct BulletVector3{
    public float x;
    public float y;
    public float z;

    public BulletVector3 (float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public override string ToString () {
        return "["+x+","+y+","+ z+"]";
    }

}


[StructLayout(LayoutKind.Sequential, Pack=1)]
    public struct BulletQuaternion{
    public float x;
    public float y;
    public float z;
    public float w;
    public BulletQuaternion (float x, float y, float z, float w)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }
}



[StructLayout(LayoutKind.Sequential, Pack=1)]
 public struct BulletStatus
{
    public float rigidBodiesCount;
    public float objectsBatchAddedId;
    public float actionsBatchAddedId;
    public int collisionCount;
    public float debugValue;
    public float debugValue2;
    public float debugValue3;
    public BulletStatus (float rigidBodiesCount)
    {
        this.rigidBodiesCount = rigidBodiesCount;
        objectsBatchAddedId = 0;
        actionsBatchAddedId = 0;
        collisionCount = 0;
        debugValue = 0f;
        debugValue2 = 0f;
        debugValue3 = 0f;
    }
}


[StructLayout(LayoutKind.Sequential, Pack=1)]
public struct btRigidBody{
    public IntPtr native;
}

[StructLayout(LayoutKind.Sequential, Pack=1)]
public struct btCompoundShape{
    public IntPtr native;
}

[StructLayout(LayoutKind.Sequential, Pack=1)]
public struct btCollisionShape{
    public IntPtr native;
}

[StructLayout(LayoutKind.Sequential, Pack=1)]
public struct BulletBody{
    public int id;
    public short mask;
    public short collisionGroup;
    public BulletVector3 position;
    public BulletQuaternion rotation;
    public float linearDamping;
    public float angularDamping;
    public float restitution;
    public float friction;
	public float rollingFriction;
    public float mass;
    public int isTrigger;
    public int alwaysActive;
    public int compoundShapeId;
    public BulletVector3 compoundShapeScale;
    public int shapeCount;
    public BulletVector3 inertiaPoint;
};


[StructLayout(LayoutKind.Sequential, Pack=1)]
public struct BulletTransform{
    public int id;
    public BulletVector3 position;
    public BulletQuaternion rotation;
    public BulletVector3 velocity;
    public BulletVector3 angularVelocity;
};


[StructLayout(LayoutKind.Sequential, Pack=1)]
public struct BulletShape{
    public int id;
    public int type;
    public int vertexCount;// used if you are using a mesh collider.
    public BulletVector3 scale;
    public BulletQuaternion localRotation;
    public BulletVector3 localPosition;
};

[StructLayout(LayoutKind.Sequential, Pack=1)]
public struct CollisionAOnB{
    public int idA;
    public int idB;
    public BulletVector3 ptA;
    public BulletVector3 ptB;
    public BulletVector3 normalOnB;
};

[StructLayout(LayoutKind.Sequential, Pack=1)]
public struct BulletRaycastHit{
    public int id;
    public int hit;
    public BulletVector3 hitPoint;
    public BulletVector3 normal;
};

public class BulletRaycastHitDetails{
    public BulletRigidBody bulletRigidbody;
    public Vector3 point;
    public Vector3 normal;
}


[StructLayout(LayoutKind.Sequential, Pack=1)]
public struct BulletConstraint{
    
    public BulletConstraint(int id,int useLinearReferenceFrameA,int constraintToWorld,
                            Transform pivot, Vector3 lowerLimit,Vector3 upperLimit,
                            Vector3 lowerLinearLimit,Vector3 upperLinearLimit,
                            int disableCollisionBetweenBodies, int solverIterations){
        this.id = id;
        this.useLinearReferenceFrameA = useLinearReferenceFrameA;
        this.constraintToWorld = constraintToWorld;
        forward = new BulletVector3( pivot.forward.x,pivot.forward.y,pivot.forward.z);
        right = new BulletVector3( pivot.right.x,pivot.right.y,pivot.right.z);
        this.pivot = new BulletVector3( pivot.transform.position.x,pivot.transform.position.y,pivot.transform.position.z);
        this.lowerLimit = new BulletVector3(lowerLimit.x,lowerLimit.y,lowerLimit.z);
        this.upperLimit = new BulletVector3(upperLimit.x,upperLimit.y,upperLimit.z);
        this.lowerLinearLimit = new BulletVector3(lowerLinearLimit.x,lowerLinearLimit.y,lowerLinearLimit.z);
        this.upperLinearLimit = new BulletVector3(upperLinearLimit.x,upperLinearLimit.y,upperLinearLimit.z);
        
        this.disableCollisionBetweenBodies = disableCollisionBetweenBodies;
        this.solverIterations = 150;// solverIterations;

        rbAPos = new BulletVector3(0,0,0);
        rbBPos = new BulletVector3(0,0,0);

        rbARot = new BulletQuaternion(0,0,0,0);
        rbBRot = new BulletQuaternion(0,0,0,0);


        linearCFMValue = Vector3.zero.ToBulletVector3();
        angularCFMValue = Vector3.zero.ToBulletVector3();
        linearERPValue = Vector3.zero.ToBulletVector3();
        angularERPValue = Vector3.zero.ToBulletVector3();

        enableSpring = 0;
        springAxis =0;
        damping = 0;
        stiffness = 0;
    }
    
    public int id;
    public int useLinearReferenceFrameA;
    public int constraintToWorld;
    public BulletVector3 forward;
    public BulletVector3 right;
    public BulletVector3 pivot;
    public BulletVector3 lowerLimit;
    public BulletVector3 upperLimit;
    public BulletVector3 lowerLinearLimit;
    public BulletVector3 upperLinearLimit;


    public BulletVector3 linearCFMValue;
    public BulletVector3 angularCFMValue;
    public BulletVector3 linearERPValue;
    public BulletVector3 angularERPValue;

    public BulletVector3 rbAPos;
    public BulletVector3 rbBPos;

    public BulletQuaternion rbARot;
    public BulletQuaternion rbBRot;

    public int enableSpring;
    public int springAxis;
    public float damping;
    public float stiffness;

    public int disableCollisionBetweenBodies;
    public int solverIterations;
};
[StructLayout(LayoutKind.Sequential, Pack=1)]
public struct BulletMotor{
    public int id;
    public int constraintId;
    public int motorType;
    public int axis;
    public float kp,ki,kd;
    public BulletMotor(int id,int constraintId,int motorType,int axis,float kp,float ki,float kd){
        this.id= id;
        this.constraintId = constraintId;
        this.motorType = motorType;
        this.axis = axis;
        this.kp = kp;
        this.ki = ki;
        this.kd = kd;
    }
}

public class  Bullet: MonoBehaviour
{
    public static Bullet instance;
    static List<BulletRigidBody> rigidbodies = new List<BulletRigidBody>();

    delegate void CollisionSolverCallback( CollisionAOnB collision);

    delegate void FileLoaderCallback( int bodyId, int shapeId );

    private MemoryStream newBulletObjectsMemory;
    private byte[] newBulletObjectsList;

    private byte[] newRigidBodyBuffer;
    private byte[] newBulletShapeBuffer;
    private byte[] newBulletShapeVertexBuffer;

    private IntPtr newRigidBodyPtr;
    private IntPtr newBulletShapePtr;
    private IntPtr newBulletShapeVertexPtr;

    public float timeStep = 0.05f;
    public int maxSubSteps = 2;
    public float fixedTimeStep = 0.021f;
    public int iterations = 20;
	public float worldScale = 0.5f;// 1 means 2 meters, 0.1 means 20 cm
    public Vector3 gravity;
    
    
    GCHandle gch,collisionGch,bulletStatusGch,stateGch;

    [DllImport("BulletService")]
    private static extern void SimulateI ();

    [DllImport("BulletService")]
    extern static void RegisterCallback (CollisionSolverCallback callback);

    [DllImport("BulletService")]
    extern static void RegisterImporterCallback (FileLoaderCallback callback);

    static void FileLoader(int bodyId, int shapeId){
        byte[] data = null;
        int len = 0;
        BulletRigidBody body = GetFromBulletBodiesList(bodyId);
        if(body != default(BulletRigidBody)){
            var name = body.compoundShape.serializationName + "_" + shapeId.ToString();
            System.Console.WriteLine("Loading " + name);
            var file = Resources.Load("SerializedMeshes/"+ name ) as TextAsset;
            if(file != null){
                System.Console.WriteLine("DATA FOUND " + name);
                data = file.bytes;
                len = data.Length;
                SetLoadedData(data,len);
            }else{
                System.Console.WriteLine("NAME fail "+name);
            }
        }
    }
    static void CollisionSolver (CollisionAOnB collision) {
        Bullet.instance.CheckForCollisions(collision);
    }

    [DllImport("BulletService")]
    private static extern void SimulateStep (float step, float maxSubSteps, float fixedTimeStep);

    [DllImport("BulletService")]
    public static extern float FastInvSqrt (float x);

    [DllImport("BulletService")]
    public static extern void SetForceUpdateAllAabbs (int active);

	[DllImport("BulletService")]
    public static extern void SetWorldScaling (float scale);

    [DllImport("BulletService")]
    public static extern BulletTransform GetTransform (btRigidBody body);

    [DllImport("BulletService")]
    public  static extern void SetWorldGravity (BulletVector3 gravity);
    
    [DllImport("BulletService")]
    public static extern void EndSimulation ();

    [DllImport("BulletService")]
    public static extern void AddForce (btRigidBody body, BulletVector3 force, BulletVector3 offset);

    [DllImport("BulletService")]
    public static extern void SetVelocity (btRigidBody body, BulletVector3 velocity);

    [DllImport("BulletService")]
    public static extern void SetAngularVelocity (btRigidBody body, BulletVector3 angularVelocity);

    [DllImport("BulletService")]
    private static extern void RemoveRidigbody (btRigidBody body);

    [DllImport("BulletService")]
    public static extern void SetMotorAngle( int id, float angle );

    [DllImport("BulletService")]
    public static extern void SetMotorVelocity( int id, float velocity );

    [DllImport("BulletService")]
    public static extern void StopMotor ( int id );

    [DllImport("BulletService")]
    public static extern void ChangeLayer(btRigidBody body, short mask, short group );

    [DllImport("BulletService")]
    private static extern BulletRaycastHit BulletRaycast(short mask, short group,BulletVector3 origin,BulletVector3 direction);

    [DllImport("BulletService")]
    private static extern int BulletRaycastAll(short mask, short group,BulletVector3 origin,BulletVector3 direction,[In, Out] byte[] hits);

    [DllImport("BulletService")]
    private static extern int BulletOverlapSphere(btRigidBody body, short mask, short group, BulletVector3 center, float radius, [In, Out] byte[] hits);

    [DllImport("BulletService")]
    public static extern void SetRigidbodyPosition(btRigidBody body, BulletVector3 position);

    [DllImport("BulletService")]
    public static extern void SetRigidbodyRotation(btRigidBody body, BulletQuaternion rotation);

    [DllImport("BulletService")]
    public static extern void SetGravity(btRigidBody body, BulletVector3 g);

    [DllImport("BulletService")]
    public static extern void RemoveContraint(int id);

    [DllImport("BulletService")]
    public static extern void RemoveMotor(int id);

    [DllImport("BulletService")]
    public static extern void SetCollisionShapeScale(btRigidBody body,BulletVector3 scale);

    [DllImport("BulletService")]
    public static extern IntPtr AddRigidbody ([In, Out] byte[] newObjectsList);

    [DllImport("BulletService")]
    public static extern IntPtr CreateCompoundShape ();

    [DllImport("BulletService")]
    public static extern IntPtr GetCompoundShape (int compoundShapeId);

    [DllImport("BulletService")]
    public static extern int IsCompoundShapeLoaded (int compoundShapeId);

    [DllImport("BulletService")]
    public static extern void AddChildrenToCompoundShape(btCompoundShape compoundShape, btCollisionShape shape, BulletVector3 localPosition,BulletQuaternion localRotation, BulletVector3 centerOfMass);

    [DllImport("BulletService")]
    public static extern IntPtr CreateBoxShape (BulletVector3 scale);

    [DllImport("BulletService")]
    public static extern IntPtr CreateSphereShape (float radio);

    [DllImport("BulletService")]
    public static extern IntPtr CreateCylinderShape (BulletVector3 scale);

    [DllImport("BulletService")]
    public static extern IntPtr CreateConvexHullShape (BulletVector3[] vertices,int size);

    [DllImport("BulletService")]
    public static extern IntPtr CreateTriangleMeshShape (int id,int shapeId);

    [DllImport("BulletService")]
    public static extern IntPtr AddBulletRigidbody (BulletBody bulletRigidBody,btCompoundShape compoundShape);

    [DllImport("BulletService")]
    public static extern void AddConstraint (BulletConstraint constraint, btRigidBody bodyA, btRigidBody bodyB);

    [DllImport("BulletService")]
    public static extern void AddMotor (BulletMotor motor);

    [DllImport("BulletService")]
    public static extern void SetDamping (btRigidBody body, float linearDamping, float angularDamping );
    
    [DllImport("BulletService")]
    public static extern void SetBodyMass (btRigidBody body, float mass, BulletVector3 inertiaPoint );

    [DllImport("BulletService", CallingConvention = CallingConvention.Cdecl)]
    public static extern void SerializeShape(string path, int numVertex, int numTriangles, BulletVector3[] vertex, int[] triangleIndex);

    [DllImport("BulletService")]
    public static extern void SetLoadedData([In, Out] byte[] data, int length);
    

    [DllImport("BulletService")]
    public static extern void SetWorldIterations( int iterations);

    public static List<BulletRigidBody> OverlapSphere(btRigidBody body, BulletLayers mask, BulletLayers group,BulletVector3 origin,float radius){
        var bodies = new List<BulletRigidBody>();
        var ids = new byte[2048];
        var count = BulletOverlapSphere(body, (short)mask,(short)group,origin,radius,ids);

        int size = Marshal.SizeOf (typeof(int));
        IntPtr hitsPtr =  Marshal.AllocHGlobal ( size);
        for(int i = 0; i< count; i++){
            Marshal.Copy (ids, i*size, hitsPtr, size);
            int otherId = (int)Marshal.PtrToStructure (hitsPtr, typeof(int));
            BulletRigidBody other = GetFromBulletBodiesList(otherId);
            if(other != default(BulletRigidBody)){
                bodies.Add(other);
            }
        }
        return bodies;
    }


    void Awake(){
        instance = this;
        InitBuffers();
    }
    
    void InitBuffers(){
        
        newRigidBodyBuffer = new byte[Marshal.SizeOf(typeof( BulletBody))];
        newBulletShapeBuffer = new byte[Marshal.SizeOf(typeof( BulletShape))];
        newBulletShapeVertexBuffer = new byte[Marshal.SizeOf(typeof( BulletVector3))];

        newRigidBodyPtr = Marshal.AllocHGlobal (Marshal.SizeOf(typeof( BulletBody)));
        newBulletShapePtr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof( BulletShape)));
        newBulletShapeVertexPtr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof( BulletVector3)));
        
        
        newBulletObjectsList = new byte[1024 ];
        gch = GCHandle.Alloc(newBulletObjectsList, GCHandleType.Pinned);
        newBulletObjectsMemory = new MemoryStream (newBulletObjectsList);
         
        StartBulletEngine();
       
    }
    
    
    public void StartBulletEngine(){
        SimulateI ();
        RegisterCallback ( CollisionSolver );
        RegisterImporterCallback (  FileLoader );
        SetWorldGravity(gravity.ToBulletVector3());
        SetWorldIterations(iterations);
		SetWorldScaling(worldScale);

    }

    public void ResetMemory (MemoryStream memory){
        memory.Seek (0, SeekOrigin.Begin);  
    }

    public BulletTransform GetBulletTransform ( btRigidBody btRB ) {
        return Bullet.GetTransform(btRB);
    }

    public void AddBulletBody (BulletBody body)
    {
        int headSize = Marshal.SizeOf (body.GetType ());
        int totalSize = headSize;
        if ((totalSize + newBulletObjectsMemory .Position) >= newBulletObjectsMemory.Length) {
            newBulletObjectsMemory.Position = 0;
            Debug.LogWarning("reset!!!!");
        }
        Marshal.StructureToPtr (body, newRigidBodyPtr, false);
        Marshal.Copy (newRigidBodyPtr, newRigidBodyBuffer, 0, headSize);
        newBulletObjectsMemory.Write (newRigidBodyBuffer, 0, headSize);
    }
    public void AddBulletShape (BulletShape shape){
        int shapeSize = Marshal.SizeOf (shape.GetType ());
        int totalSize = shapeSize;
        if ((totalSize + newBulletObjectsMemory .Position) >= newBulletObjectsMemory.Length) {
            newBulletObjectsMemory.Position = 0;
            Debug.LogWarning("reset!!!!");
        }
        Marshal.StructureToPtr (shape, newBulletShapePtr, false);
        Marshal.Copy (newBulletShapePtr, newBulletShapeBuffer, 0, shapeSize);
        newBulletObjectsMemory.Write (newBulletShapeBuffer, 0, shapeSize);
    }

    public void AddBulletShapeVertex (BulletVector3 vertex){
        int vertexSize = Marshal.SizeOf (vertex.GetType ());
        int totalSize = vertexSize;
        if ((totalSize + newBulletObjectsMemory .Position) >= newBulletObjectsMemory.Length) {
         newBulletObjectsMemory.Position = 0;
         Debug.LogWarning("reset!!!!");
            Debug.Break();
        }
        Marshal.StructureToPtr (vertex, newBulletShapeVertexPtr, false);
        Marshal.Copy (newBulletShapeVertexPtr, newBulletShapeVertexBuffer, 0, vertexSize);
        newBulletObjectsMemory.Write (newBulletShapeVertexBuffer, 0, vertexSize);
    }

    void FixedUpdate (){
        SimulateStep(timeStep,maxSubSteps,fixedTimeStep);
    }

    public static int AddToBulletBodiesList(BulletRigidBody bulletRigidBody){
        int nextIndex = rigidbodies.IndexOf(default(BulletRigidBody));
        if(nextIndex < 0){
            nextIndex = rigidbodies.Count;
            rigidbodies.Add(bulletRigidBody);
        }else{
            rigidbodies[nextIndex] = bulletRigidBody;
        }

        return nextIndex;
    }

    public static void RemoveRidigbody(BulletRigidBody body){
        rigidbodies[body.Id] = default(BulletRigidBody);
        RemoveRidigbody(body.btRB);
    }

    public static BulletRigidBody GetFromBulletBodiesList(int index){
        if( index < 0 || index >= rigidbodies.Count ){
            return default(BulletRigidBody);
        }
        return rigidbodies[index];
    }
    public void AddRigidbodyToSimulation(BulletRigidBody bulletRigidBody){
        btCompoundShape compoundShape;

        var isLoaded = IsCompoundShapeLoaded(bulletRigidBody.compoundShape.compoundShapeId) > 0;
        //Debug.Log(isLoaded + " "+ bulletRigidBody.gameObject.name);

        if(!isLoaded){
            compoundShape.native = CreateCompoundShape();
        }else{
            compoundShape.native = GetCompoundShape(bulletRigidBody.compoundShape.compoundShapeId);
        }




        var centerOfMass = bulletRigidBody.compoundShape.CalculateInertia();


        var bulletTransform = bulletRigidBody.gameObject.transform;
        var body = new BulletBody();
        body.id = bulletRigidBody.Id;
        
        body.mask = (short)bulletRigidBody.mask;
        body.collisionGroup = (short)bulletRigidBody.collisionGroup;

        body.mass = bulletRigidBody.mass;
        body.friction = bulletRigidBody.friction;
        body.rollingFriction = bulletRigidBody.rollingFriction;
        body.restitution = bulletRigidBody.restitution;
        body.position = bulletTransform.position.ToBulletVector3();

        body.rotation = bulletTransform.rotation.ToBulletQuaternion();


        body.linearDamping = bulletRigidBody.linearDamping;
        body.angularDamping = bulletRigidBody.angularDamping;
        body.isTrigger = bulletRigidBody.isTrigger?1:0;
        body.alwaysActive = bulletRigidBody.alwaysActive?1:0;
        body.shapeCount = bulletRigidBody.compoundShape.collisionShapes.Length;
        body.compoundShapeId = bulletRigidBody.compoundShape.compoundShapeId;
        body.compoundShapeScale = bulletRigidBody.compoundShape.transform.localScale.ToBulletVector3();
        body.inertiaPoint =  centerOfMass.ToBulletVector3();



        if(!isLoaded){
        for(int i = 0;i<bulletRigidBody.compoundShape.collisionShapes.Length;i++){
            var collisionShape = bulletRigidBody.compoundShape.collisionShapes[i];
            btCollisionShape currentCollisionShape = new btCollisionShape();

            switch(collisionShape.type){

                case BulletCollisionShape.CollisionShapeType.Cuboid:
                    currentCollisionShape.native = CreateBoxShape((collisionShape.transform.localScale/2.0f).ToBulletVector3());

                    break;
                case BulletCollisionShape.CollisionShapeType.Sphere:
                    currentCollisionShape.native = CreateSphereShape(collisionShape.transform.localScale.x/2.0f);


                    break;
                case BulletCollisionShape.CollisionShapeType.MeshShape:

                    var vertexCount = bulletRigidBody.compoundShape.collisionShapes[i].vertexCount;

                    var mesh = bulletRigidBody.compoundShape.collisionShapes[i].mesh;
                    BulletVector3[] vertices = new BulletVector3[vertexCount];
                    for(int j = 0; j < vertexCount; j++){
                        vertices[j] =mesh.vertices[j].ToBulletVector3();
                    }

                    currentCollisionShape.native = CreateConvexHullShape(vertices,vertexCount);
                    break;
                case BulletCollisionShape.CollisionShapeType.Cylinder:
                    currentCollisionShape.native = CreateCylinderShape((collisionShape.transform.localScale/2.0f).ToBulletVector3());

                    break;
                case BulletCollisionShape.CollisionShapeType.TriangleMeshShape:
                    currentCollisionShape.native = CreateTriangleMeshShape(body.id,i);
                    break;
                default:
                    Debug.Log("this should not happen ");
                    currentCollisionShape.native = CreateBoxShape((collisionShape.transform.localScale/2.0f).ToBulletVector3());

                    break;
            }


            AddChildrenToCompoundShape(compoundShape,
                currentCollisionShape,
                collisionShape.transform.localPosition.ToBulletVector3(),
                collisionShape.transform.localRotation.ToBulletQuaternion(),
                centerOfMass.ToBulletVector3());


        }
        }
        bulletRigidBody.btRB.native = AddBulletRigidbody(body,compoundShape);
       // Debug.Log("native rb "+bulletRigidBody.btRB.native);
    }
    public void _AddRigidbodyToSimulation(BulletRigidBody bulletRigidBody){

        //AddRigidbodyToSimulation(bulletRigidBody);

        ResetMemory(newBulletObjectsMemory);
        var bulletTransform = bulletRigidBody.gameObject.transform;
        var body = new BulletBody();
        body.id = bulletRigidBody.Id;
        
        body.mask = (short)bulletRigidBody.mask;
        body.collisionGroup = (short)bulletRigidBody.collisionGroup;

        body.mass = bulletRigidBody.mass;
        body.friction = bulletRigidBody.friction;
		body.rollingFriction = bulletRigidBody.rollingFriction;
        body.restitution = bulletRigidBody.restitution;
        body.position.x = bulletTransform.position.x;
        body.position.y = bulletTransform.position.y;
        body.position.z = bulletTransform.position.z;

        body.rotation.x = bulletTransform.rotation.x;
        body.rotation.y = bulletTransform.rotation.y;
        body.rotation.z = bulletTransform.rotation.z;
        body.rotation.w = bulletTransform.rotation.w;

        body.linearDamping = bulletRigidBody.linearDamping;
        body.angularDamping = bulletRigidBody.angularDamping;
        body.isTrigger = bulletRigidBody.isTrigger?1:0;
        body.alwaysActive = bulletRigidBody.alwaysActive?1:0;
        body.shapeCount = bulletRigidBody.compoundShape.collisionShapes.Length;
        body.compoundShapeId = bulletRigidBody.compoundShape.compoundShapeId;
        body.compoundShapeScale = bulletRigidBody.compoundShape.transform.localScale.ToBulletVector3();
        body.inertiaPoint =  bulletRigidBody.compoundShape.CalculateInertia().ToBulletVector3();

        AddBulletBody(body);

        for(int i = 0;i<bulletRigidBody.compoundShape.collisionShapes.Length;i++){
            BulletShape shape = new BulletShape();
            shape.id = body.id;
            shape.type = (int)bulletRigidBody.compoundShape.collisionShapes[i].type;
            
            shape.localRotation.x = bulletRigidBody.compoundShape.collisionShapes[i].transform.localRotation.x;
            shape.localRotation.y = bulletRigidBody.compoundShape.collisionShapes[i].transform.localRotation.y;
            shape.localRotation.z = bulletRigidBody.compoundShape.collisionShapes[i].transform.localRotation.z;
            shape.localRotation.w = bulletRigidBody.compoundShape.collisionShapes[i].transform.localRotation.w;
            
            shape.localPosition.x = bulletRigidBody.compoundShape.collisionShapes[i].transform.localPosition.x;
            shape.localPosition.y = bulletRigidBody.compoundShape.collisionShapes[i].transform.localPosition.y;
            shape.localPosition.z = bulletRigidBody.compoundShape.collisionShapes[i].transform.localPosition.z;
            
            shape.scale.x = bulletRigidBody.compoundShape.collisionShapes[i].transform.localScale.x/2.0f;
            shape.scale.y = bulletRigidBody.compoundShape.collisionShapes[i].transform.localScale.y/2.0f;
            shape.scale.z = bulletRigidBody.compoundShape.collisionShapes[i].transform.localScale.z/2.0f;

            shape.vertexCount = bulletRigidBody.compoundShape.collisionShapes[i].vertexCount;

            AddBulletShape(shape);

            if(shape.type == (int)BulletCollisionShape.CollisionShapeType.TriangleMeshShape){
                continue;
            }

            var mesh = bulletRigidBody.compoundShape.collisionShapes[i].mesh;

            for(int j = 0; j < shape.vertexCount; j++){
                var unityVertex =  mesh.vertices[j];
                var vertex = new BulletVector3(unityVertex.x,unityVertex.y,unityVertex.z);
                AddBulletShapeVertex(vertex);
            }
        }
        bulletRigidBody.btRB.native = Bullet.AddRigidbody(newBulletObjectsList);
    }

    public void AddConstraintToSimulation(BulletGeneric6DofConstraint constraint){

        var constraintStructure = new BulletConstraint(
            constraint.GetHashCode(),
            (constraint.useLinearReferenceFrameA)?1:0,
            constraint.constraintToWorld?1:0,
            constraint.transform,
            constraint.lowerLimit,
            constraint.upperLimit,
            constraint.lowerLinearLimit,
            constraint.upperLinearLimit,
            (constraint.disableCollisionBetweenBodies)?1:0,
            constraint.solverIterations);

        constraintStructure.linearCFMValue = (Vector3.one*0f).ToBulletVector3();// constraint.linearCFMValue.ToBulletVector3();
        constraintStructure.angularCFMValue = (Vector3.one*0f).ToBulletVector3();//constraint.angularCFMValue.ToBulletVector3();

        constraintStructure.linearERPValue =(Vector3.one*0.5f).ToBulletVector3(); //constraint.linearERPValue.ToBulletVector3();
        constraintStructure.angularERPValue =(Vector3.one*0.8f).ToBulletVector3(); //constraint.angularERPValue.ToBulletVector3();

        constraintStructure.rbAPos = new BulletVector3(constraint.rbAPos.x,
            constraint.rbAPos.y,
            constraint.rbAPos.z);

        constraintStructure.rbBPos = new BulletVector3(constraint.rbBPos.x,
            constraint.rbBPos.y,
            constraint.rbBPos.z);

        constraintStructure.rbARot = new BulletQuaternion(constraint.rbARot.x,
            constraint.rbARot.y,
            constraint.rbARot.z,
            constraint.rbARot.w);

        constraintStructure.rbBRot = new BulletQuaternion(constraint.rbBRot.x,
            constraint.rbBRot.y,
            constraint.rbBRot.z,
            constraint.rbBRot.w);

        constraintStructure.enableSpring = constraint.enableSpring?1:0;
        constraintStructure.springAxis = (int)constraint.springAxis;
        constraintStructure.stiffness = constraint.stiffness;
        constraintStructure.damping = constraint.damping;
        var bodyA = constraint.bulletRbA.btRB;
        var bodyB = constraint.constraintToWorld?constraint.bulletRbA.btRB:constraint.bulletRbB.btRB;
        AddConstraint(constraintStructure,bodyA,bodyB);

    }

    public void AddMotorToSimulation(BulletGenericMotor motor){
        var motorStructure = new BulletMotor(motor.constraint.GetHashCode(),
        motor.constraintId,
        (int)motor.motorType,
        (int)motor.axis,
        motor.kp,
        motor.ki,
        motor.kd
        );
        AddMotor(motorStructure);

    }
    
    void OnDestroy(){
        gch.Free();
        collisionGch.Free();
        bulletStatusGch.Free();
        stateGch.Free();
    }

    public void CheckForCollisions (CollisionAOnB collision)
    {

 var bodyA = GetFromBulletBodiesList(collision.idA);
            var bodyB = GetFromBulletBodiesList(collision.idB);
		
		//Debug.Log(bodyA.name + "   " + bodyB.name + " "+ Time.time);
            var relativeVelocityAB = bodyA.GetVelocity () - bodyB.GetVelocity ();
            var relativeVelocityBA = -relativeVelocityAB;
            var ptA = collision.ptA.ToVector3 ();
            var ptB = collision.ptB.ToVector3 ();
            var lineOfActionA = (bodyA.transform.position - ptA);
            var lineOfActionB = (bodyA.transform.position - ptB);

            if (bodyA.isTrigger || bodyB.isTrigger) {
                bodyA.OnTrigger (relativeVelocityAB, bodyB,ptA,lineOfActionA);
                bodyB.OnTrigger (relativeVelocityBA, bodyA,ptB,lineOfActionB);
            } else {
                bodyA.OnCollision (relativeVelocityAB, bodyB,ptA,lineOfActionA);
                bodyB.OnCollision (relativeVelocityBA, bodyA,ptB,lineOfActionB);
            }
 
    }

    public static bool Raycast ( Ray ray, out BulletRaycastHitDetails hitDetails, float distance, BulletLayers layer, BulletLayers mask ) {
        var hitResult =  BulletRaycast((short)mask, (short)(layer), ray.origin.ToBulletVector3(),
                                       (ray.origin + ray.direction * distance ).ToBulletVector3());
        BulletRigidBody rigidbody = GetFromBulletBodiesList(hitResult.id);
        hitDetails = new BulletRaycastHitDetails();
        if ( hitResult.hit == 1 && rigidbody != default(BulletRigidBody) ) {
            hitDetails.bulletRigidbody = rigidbody;
            hitDetails.point = hitResult.hitPoint.ToVector3();
            hitDetails.normal = hitResult.normal.ToVector3();
            return true;
        }
        return false;
    }

    public static List < BulletRaycastHitDetails > RaycastAll ( Ray ray, float distance, BulletLayers layer, BulletLayers mask ) {
        
        var hitList = new List<BulletRaycastHit>();
        var hits = new byte[256];
        var count = BulletRaycastAll((short)mask,(short)layer, ray.origin.ToBulletVector3(), (ray.origin + ray.direction * distance).ToBulletVector3(), hits);
        
        int size = Marshal.SizeOf (typeof(BulletRaycastHit));
        IntPtr hitsPtr =  Marshal.AllocHGlobal ( size);
        
        for(int i = 0; i< count; i++){
            Marshal.Copy (hits, i*size, hitsPtr, size);
            BulletRaycastHit bulletHit = (BulletRaycastHit)Marshal.PtrToStructure (hitsPtr, typeof(BulletRaycastHit));
            hitList.Add(bulletHit);
        }
        
        var hitDetailsList = new List< BulletRaycastHitDetails>( hitList.Count );
        for ( int i  = 0; i< hitList.Count; i++ ) {
            BulletRigidBody rigidbody = GetFromBulletBodiesList(hitList[i].id);
            var details = new BulletRaycastHitDetails();
            if ( rigidbody != default(BulletRigidBody) ) {
                details.bulletRigidbody = rigidbody;
                details.point = hitList[ i ].hitPoint.ToVector3();
                details.normal = hitList[ i ].normal.ToVector3();
                hitDetailsList.Add( details );
            }
        }
        return hitDetailsList;
    }

}

public static class Vector3Extensions {
    public static BulletVector3 ToBulletVector3(this Vector3 v){
        return new BulletVector3(v.x,v.y,v.z);
    }

    public static Vector3 ToVector3 ( this BulletVector3 btV ) {
        return new Vector3(btV.x,btV.y,btV.z);
    }
    public static Quaternion ToQuaternion ( this BulletQuaternion btQ ) {
        return new Quaternion
            (btQ.x,btQ.y,btQ.z,btQ.w);
    }

    public static BulletQuaternion ToBulletQuaternion(this Quaternion q){
        return new BulletQuaternion(q.x,q.y,q.z,q.w);
    }
}
