using UnityEngine;
using System.Collections;

public class BulletGenericMotor : MonoBehaviour {
    
    public enum Axis {
        X = 0,
        Y = 1,
        Z = 2
    }
    
    public enum MotorType {
        Angular = 0,
        Linear = 1
    }
    
    public BulletGeneric6DofConstraint constraint;
    public MotorType motorType;
    public Axis axis;
    public float kp;
    public float ki;
    public float kd;
    public float target;
    public float velocity;
    public int constraintId;
    public bool motorInitialized = false;
    
    // Use this for initialization
    public void Initialize () {
        constraintId = constraint.GetHashCode();
        Bullet.instance.AddMotorToSimulation( this );
        motorInitialized = true;
    }
    
    public void SetVelocity ( float velocity ) {
        this.velocity = velocity;
        if ( motorInitialized ) {
            Bullet.SetMotorVelocity( constraintId, velocity );
        }
    }

    public void SetTarget ( float target ) {
        this.target = target;
        if ( motorInitialized ) {
            Bullet.SetMotorAngle( constraintId, target );
        }
    }
    
    public void Stop () {
        if ( motorInitialized ) {
            Bullet.StopMotor( constraintId );
        }
    }

    public void RemoveMotor () {
        if ( motorInitialized ) {
            Bullet.RemoveMotor( constraintId );
        }
    }

    void OnDestroy () {
        if ( motorInitialized ) {
            RemoveMotor();
        }
    }

}
