using UnityEngine;
using System.Collections;

public partial class LiveBar : LiveWidget {

    public string label;
    public Color color;
    public float min = 0;
    public float max = 100;
    public float value = 100;
    public bool test = false;
    public TextMesh labelMesh;
    public TextMesh valueMesh;

    void Start () {
    }

    void Update () {

        WidgetUpdate();

        if ( !isVisible ) {
            return;
        }

        if ( test ) {
            value = min + Mathf.PingPong( Time.time * ( max - min ), ( max - min ) );
        }

        float weight = ( value - min ) / ( max - min );
        weight = Mathf.Clamp01( weight );
        Vector2 texoff = new Vector2( 0, weight * 0.5f + 0.5f );
        GetComponent<Renderer>().material.mainTextureOffset = texoff;
        labelMesh.text = label;
        valueMesh.text = string.Format( "{0:0.00}", value );
        GetComponent<Renderer>().material.color = color;
    }
}
