using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Globalization;
using Oracle;
using Operants;
using System.IO;
using Ionic.Zlib;

public class Gaia : MonoBehaviour {

    class Command {
        public XForm xform;
        public string commands;
        public System.Action callback = null;
    }

    public static List<string> Scenes;

    public const int reserve = 20000;
    List< Command > loadQueue = new List< Command >();
    public Dictionary< string, Entity > entities = new Dictionary< string, Entity >( reserve );
    Dictionary< Entity, XForm > xforms = new Dictionary< Entity, XForm >();
    public List< Entity > recentlyAddedEntities = new List< Entity >( reserve );
    List< NodeProxy > recentlyAddedProxies = new List< NodeProxy >();
    Dictionary< string, string > typeProxyCache = new Dictionary< string, string >();
    List< NodeProxy > UpdateSubscriptionList = new List<NodeProxy>();//operant tiles io uses email-related terms (inbox, outbox, subscription...) -z26
    List< NodeProxy > FixedUpdateSubscriptionList = new List<NodeProxy>();
    XForm gaia;
    XForm context;
    UDPPacketIO udp;
    Osc osc;
    public Console console;
    static Gaia instance;
    static string activeFile = string.Empty;
    public static int scene;
    public Transform scratch;
    public Entity root;
    public static string levelName;
    bool jellyCheck = false; // jelly flood prevention and reporting -z26

    public static Gaia Instance () {
        return instance;
    }

    void Awake () {
        CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture; //z26
        CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;
        instance = this;
        UpdateScenes();
        Application.targetFrameRate = 60;//Another place where framerate is defined? it also is in projectsettings/time -z26
        gaia = context = gameObject.AddComponent< XForm >();
        gaia.enabled = false;
        udp = GetComponent< UDPPacketIO >();
        osc = GetComponent< Osc >();
        root = GetEntity( "gaia" );
        Load( "Assembler/Assembly" );
        Load( "Bot/Bot" );
        Load( "Bot/Body" );
        Load( "Bot/Earth" );
        if ( Scenes[scene] == "none" ) {
            Console.PushSuccess( "", new string[]{
                "press F1 for help",
                "press TAB to select a different universe",
                "press ENTER to start",
            } );
            console.Load();
        }
        else {
            LoadGaia();
        }
    }

    public void UpdateScenes(){
				Scenes = new List<string>(){"none","stardust","blueshift","tut-centre","tut-view","tut-build","tut-console","tut-program","tut-terraform","tut-practice","academy"};
        var folder = Path.Combine( Util.userDataPath, "defs" );
        var folders = new DirectoryInfo(folder);
        if(System.IO.Directory.Exists(folder)){
            foreach(System.IO.DirectoryInfo dInfo in folders.GetDirectories()){
                if(dInfo.GetFiles().Any(f => f.Name.StartsWith( dInfo.Name ))){
                    if(!Scenes.Contains(dInfo.Name)){
                        Scenes.Add(dInfo.Name);
                    }
                }
            }
        }
    }

    public void SubscribeToUpdateList ( NodeProxy proxy ) {
        if ( !UpdateSubscriptionList.Contains( proxy ) ) {
            UpdateSubscriptionList.Add( proxy );
        }
    }

    public void SubscribeToFixedUpdateList ( NodeProxy proxy ) {
        if ( !FixedUpdateSubscriptionList.Contains( proxy ) ) {
            FixedUpdateSubscriptionList.Add( proxy );
        }
    }

    void FixedUpdate () {
        UnityEngine.Profiling.Profiler.BeginSample( "Proxy.FixedUpdate" );
        for ( var i = 0; i< FixedUpdateSubscriptionList.Count; i++ ) {
            FixedUpdateSubscriptionList[ i ].OnFixedUpdate();
        }
        UnityEngine.Profiling.Profiler.EndSample();
    }

    List< System.Action > scheduledActions = new List< System.Action >();

    public void ScheduleAction ( System.Action action ) {
        scheduledActions.Add( action );
    }

    void Update () {
        UnityEngine.Profiling.Profiler.BeginSample( "Proxy.Update" );
        for ( var i = 0; i < UpdateSubscriptionList.Count; i++ ) {
            UpdateSubscriptionList[ i ].OnUpdate();
        }
        UnityEngine.Profiling.Profiler.EndSample();

        for ( var i = 0; i < scheduledActions.Count; ++i ) {
            scheduledActions[ i ]();
        }
        scheduledActions.Clear();

        context = gaia;
        System.Action callback = null;
        if ( loadQueue.Count > 0 ) {
            if ( loadQueue[ 0 ].xform != default( XForm ) ) {
                context = loadQueue[ 0 ].xform;
            }
            callback = loadQueue[0].callback;
            Dequeue( loadQueue[ 0 ] );
            loadQueue.RemoveAt( 0 );
        }
            //entities are initialized first, before proxies are. -z26
        {   
            
            if (jellyCheck) {
                Debug.LogError("ERROR: A Jelly Flood was blocked from happening.");
                for ( var i= 0; i < recentlyAddedEntities.Count; ++i ) {
                    Debug.Log("Jelly entity:" + recentlyAddedEntities[i].id);
                }
                for ( var i= 0; i < recentlyAddedProxies.Count; ++i ) {
                    Debug.Log("Jelly proxy:" + recentlyAddedProxies[i]);
                }
                recentlyAddedEntities.Clear();
                recentlyAddedProxies.Clear();
                Debug.Break();
            }
            jellyCheck = true;
            
            UnityEngine.Profiling.Profiler.BeginSample( "RecentlyAddedEntities" );            
            for ( var i= 0; i < recentlyAddedEntities.Count; ++i ) { 
                var entity = recentlyAddedEntities[ i ];
                
                //Debug.Log("e" + i); //continuums adds 11 entities because they have 6 slots and a slotless part = 5.
                //motors add 9 - 2 slots = 7 for the 2 halves. //the part tiles each add 1.
                //(menu tiles dont add any) energy bridges/terrain hexes add 3.
                var typeEntity = entity.Relationships()
                .Where( r => r.Predicate == "as" && r.Tail == entity )
                .Select( r => r.Head ).FirstOrDefault();

                {
                    UnityEngine.Profiling.Profiler.BeginSample( "RecentlyAddedEntities.Instance" );
                    if ( typeEntity != default( Entity ) ) {
                        {
                            UnityEngine.Profiling.Profiler.BeginSample( "RecentlyAddedEntities.ForInferredProperty" );
                            entity.instance = ( uint )entity.GetHashCode();
                            string proxyDef = null;
                            {
                                UnityEngine.Profiling.Profiler.BeginSample( "Entity.ForInferredProperty" );
                                if ( !typeProxyCache.TryGetValue( typeEntity.id, out proxyDef ) ) {
                                    entity.ForInferredProperty< string >( "proxy", Util.AsIs, proxyTypeName => {
                                        proxyDef = proxyTypeName;
                                        typeProxyCache[ typeEntity.id ] = proxyDef;
                                    } );
                                }
                                UnityEngine.Profiling.Profiler.EndSample();}

                            if ( proxyDef != null ) {
                                var proxyType = System.Reflection.Assembly.GetExecutingAssembly().GetType( proxyDef );
                                if ( proxyType != null ) {
                                    {
                                        UnityEngine.Profiling.Profiler.BeginSample( "Proxy Create: " + proxyType );
                                        var proxy = System.Activator.CreateInstance( proxyType ) as NodeProxy;
                                        var xform = context; //proxies are created here, then added to the recently created
                                        entity.proxy = proxy;//proxies list, so that their initialization continues elsewhere
                                        proxy.entity = entity;
                                        var parent = entity.Heads( p => p == "local_to" ).FirstOrDefault();
                                        if ( parent != default(Entity) ) {
                                            xform = GetXForm( parent );
                                        }
                                        proxy.xform = xform;
                                        recentlyAddedProxies.Add( proxy );
                                        UnityEngine.Profiling.Profiler.EndSample();}

                                    var slotInstancesDef = entity.Tails( p => p == "slot_for" )
                                        .Select( e => e.Heads( p => p == "as" )
                                        .FirstOrDefault() );
                                    {
                                        UnityEngine.Profiling.Profiler.BeginSample( "Slot Instancing" );
                                        var slotDefs = typeEntity.Tails( p => p == "slot_for" );
                                        var filteredSlots = slotDefs.Except( slotInstancesDef );

                                        var entityBaseId = entity.id;
                                        var index = entity.id.IndexOf( '@' );

                                        if ( index != -1 ) {
                                            entityBaseId = entity.id.Substring( 0, index );
                                        }

                                        foreach ( var slotDef in filteredSlots ) {
                                            var e = new Entity( entityBaseId + "_" + slotDef.id );
                                            new Relationship( e, "slot_for", entity );
                                            new Relationship( e, "as", slotDef );
                                            recentlyAddedEntities.Add( e );
                                        }
                                        UnityEngine.Profiling.Profiler.EndSample();}

                                }
                            }
                            UnityEngine.Profiling.Profiler.EndSample();}
                    }
                    UnityEngine.Profiling.Profiler.EndSample();}
            }
            UnityEngine.Profiling.Profiler.EndSample();}

        {
            UnityEngine.Profiling.Profiler.BeginSample( "Proxy.OnPreBuild" );
            for ( var i = 0; i < recentlyAddedProxies.Count; ++i ) {
                //Debug.Log("p" + i);// theres not as many proxies as there are entities. tiles add 1, slots each add 1
                //and physical objects add 1 (some parts have 2 halves)

                var proxy = recentlyAddedProxies[ i ];
                {
                    UnityEngine.Profiling.Profiler.BeginSample( "Proxy.OnPreBuild: " + proxy.GetType() );
                    proxy.OnPreBuild(); //this indirectly creates physical objects. commenting it out stop them from
                    UnityEngine.Profiling.Profiler.EndSample();}//physical existence, but the game still runs fine and hex tiles still work.
            }//parts still exist in unity's hierarchy, with most components and children missing.  -z26
            UnityEngine.Profiling.Profiler.EndSample();}

        {
            UnityEngine.Profiling.Profiler.BeginSample( "Proxy.OnBuild" );
            for ( var i = 0; i < recentlyAddedProxies.Count; ++i ) {
                var proxy = recentlyAddedProxies[ i ];
                {
                    UnityEngine.Profiling.Profiler.BeginSample( "Proxy.OnBuild: " + proxy.GetType() );
                    proxy.OnBuild();
                    UnityEngine.Profiling.Profiler.EndSample();}
            }
            UnityEngine.Profiling.Profiler.EndSample();}

        {
            UnityEngine.Profiling.Profiler.BeginSample( "Proxy.OnPostBuild" );
            for ( var i = 0; i < recentlyAddedProxies.Count; ++i ) {
                var proxy = recentlyAddedProxies[ i ];
                {
                    UnityEngine.Profiling.Profiler.BeginSample( "Proxy.OnPostBuild: " + proxy.GetType() );
                    proxy.OnPostBuild();
                    UnityEngine.Profiling.Profiler.EndSample();}
            }
            UnityEngine.Profiling.Profiler.EndSample();}

        {
            UnityEngine.Profiling.Profiler.BeginSample( "Name Mangling" );
            for ( var i = 0; i < recentlyAddedEntities.Count; ++i ) {
                var entity = recentlyAddedEntities[ i ];
                if ( entity.instance > 0 ) {
                    entities.Remove( entity.id );
                    entity.MangleId();
                    entities[ entity.id ] = entity;
                }
            }
            UnityEngine.Profiling.Profiler.EndSample();}
        if(callback != null){
            callback();
        }
        UpdateSubscriptionList.AddRange( recentlyAddedProxies.Where( p => p.updates ) );
        FixedUpdateSubscriptionList.AddRange( recentlyAddedProxies.Where( p => p.fixedUpdates ) );
        recentlyAddedProxies.Clear();
        recentlyAddedEntities.Clear();
        jellyCheck = false; //jelly flood happen when this whole method doesnt fully execute because of some error.
        //the lines right above can't clear the list of entities/proxies to spawn, so they keep being spawned every frame.
    }
    
    bool saving = false;

    public string Save ( string name = "" ) {
        if ( saving ) {
            return "Unable to save: save already in progress";
        }
        
        ForDaytime( daytime => {
            daytime.SetProperty( "daytime",  DayCycle.globalTime );
        } );

        saving = true;
        var local_to = "local_to";
        System.Predicate< string > defaultFilter = f => true;

        // TODO: check on merge, Remove unrelated spawnpoints
        root.Tails( p => p == local_to ).ToList().ForEach( s => {
            var nested = s.Relationships().Where( p => p.Predicate == local_to && p.Head == s );
            if ( nested.Count() == 0 ) {
                s.Relationships().ToList().ForEach( r => r.Break() );
            }
        } );

        //Get entities

        var allEntities = entities.Values.Where( e => e.Heads( p => p == "as" ).Any() )
            .Where( e => !( e.proxy is SlotProxy ) || ( ( ( SlotProxy )e.proxy ).isConnected ) );

        
        var folder = Path.Combine( Util.userDataPath, "defs" );
        System.IO.Directory.CreateDirectory( folder );
        var sceneFolder = name == "" ? Path.Combine( folder , Scenes[scene] ) : Path.Combine( folder , name );
        System.IO.Directory.CreateDirectory( sceneFolder );
        var fileName = System.IO.Path.Combine( sceneFolder, name == "" ? Scenes[scene] : name );

        string txgz = ".txt.gz"; //z26

        var backUpName = System.IO.Path.Combine( sceneFolder, Scenes[scene] + "_" + System.DateTime.Now.Ticks.ToString() );
        if ( System.IO.File.Exists( fileName ) ) {
            System.IO.File.Move( fileName , backUpName + txgz);
        }
        else if ( System.IO.File.Exists( fileName + txgz ) ) {
            System.IO.File.Move( fileName + txgz , backUpName + txgz);
        }
        // TODO: make recursively nestable, using xform entity, recalculate positions and rotations
        var xforms = root.Tails( p => p == local_to );
        foreach ( var xform in xforms ) {
            var xformEntities = xform.Tails( p => p == local_to );
            
            bool inOtherDimension = xform.Relationships().ToList().Any( p => p.Predicate == "contained_in" );
            if ( inOtherDimension ) {
                continue;
            }

            var parts = xformEntities.Select( e => e.proxy ).Where( p => p != default(NodeProxy) && p is PartProxy )
                .Cast<PartProxy>().ToList();

            var centerPart = PartProxy.GetCenterPart( parts );
            if ( centerPart != default( PartProxy ) ) {

                xform.SetProperty( "position", Util.Vector3ToProperty( centerPart.nodeView.transform.position ) );
                xform.SetProperty( "rotation", Util.Vector3ToProperty( centerPart.nodeView.transform.eulerAngles ) );

                var xformView = GetXForm( xform );

                centerPart.SetAsGroupPositionReference( parts );
                xformView.MoveXForm( centerPart.nodeView.transform.position, centerPart.nodeView.transform.rotation );

            }
        }

        allEntities = allEntities.Where( e => !e.deprecated );

        // save zlib file

        var tm = new MemoryStream( 0xF0000 );
        var sw = new StreamWriter( tm, System.Text.Encoding.ASCII );
        foreach ( var e in allEntities ) {
            sw.WriteLine( e.Serialize( defaultFilter ) );
        }
        sw.Flush();
        tm.Seek( 0, SeekOrigin.Begin );
        using ( FileStream fs = new FileStream( fileName + ".txt.gz", FileMode.Create ) ) {
            using ( GZipStream gz = new GZipStream( fs, CompressionMode.Compress, false ) ) {
                gz.Write( tm.GetBuffer(), 0, tm.GetBuffer().Length );
            }
        }
        sw.Close();

        saving = false;

        return "Snapshot saved";
    }

    void CopyStream ( System.IO.Stream src, System.IO.Stream dest ) {
        var buffer = new byte[ 0x30000 ];
        var len = src.Read( buffer, 0, buffer.Length );
        while ( len > 0 ) {
            dest.Write( buffer, 0, len );
            len = src.Read( buffer, 0, buffer.Length );
        }
        dest.Flush();
    }

    public void CreateEntity ( System.Action< Entity > pre, System.Action< Entity > post ) {
        StartCoroutine( CreateEntityCoroutine( pre, post ) );
    }

    IEnumerator CreateEntityCoroutine ( System.Action< Entity > pre, System.Action< Entity > post ) {
        var entity = new Entity();
        recentlyAddedEntities.Add( entity );
        pre( entity );
        while ( recentlyAddedEntities.IndexOf( entity ) >= 0 ) {
            yield return null;
        }
        post( entity );
    }

    void LoadGaia () { //Loading maps is kinda more complex than blueprints. They are first loaded from here. -z26
        var fileName = Scenes[scene];
        if ( activeFile != string.Empty ) {
            fileName = activeFile;
        }
        var path = Path.Combine( Path.Combine( Util.userDataPath, "defs" ), Scenes[scene] );
        var file = Path.Combine( path, fileName );

        if ( System.IO.File.Exists( file ) ) { //Load from the players' documents. -z26
            Load( fileName, () => HexBatcher.instance.BatchWorld() ); 
        }
        
        else if ( System.IO.File.Exists( file + ".txt.gz" ) ) { //Added by me. -z26
            Load( fileName + ".txt.gz", () => HexBatcher.instance.BatchWorld() ); 
        }

        else { //Otherwise, assume the map must be one of Rawbots' default maps. (In the built game, These + many other assets are compressed in rawbots_data/resources) -z26
            CreateDefaultData();
        }
    }

    public List<string> GetSavedFiles () {
        var files = new List< string >();
        var folder = Path.Combine( Util.userDataPath, "defs" );
        var sceneFolder = Path.Combine(folder, Scenes[scene] );
        if ( System.IO.Directory.Exists( sceneFolder ) ) {
            string[] filePaths = System.IO.Directory.GetFiles( sceneFolder, Scenes[scene] + "_*" );
            if ( filePaths.Length > 0 ) {
                foreach ( string f in filePaths ) {
                    var marker = f.LastIndexOf( Scenes[scene] );
                    var name = f.Substring( marker );
                    files.Add( name );
                }
            }

            var fullpath = System.IO.Path.Combine( sceneFolder, Scenes[scene] ); //changed by z26
            if ( System.IO.File.Exists(fullpath) || System.IO.File.Exists(fullpath+".txt.gz") ) {
                files.Add( "last" );
            }
        }
        return files;
    }

    public List<string> GetSavedBlueprints () {
        var files = new List< string >();
        var folder = Path.Combine( Util.userDataPath, "blueprints" );
        if ( System.IO.Directory.Exists( folder ) ) {
            string[] filePaths = System.IO.Directory.GetFiles( folder, "bp_*" );
            if ( filePaths.Length > 0 ) {
                foreach ( string f in filePaths ) {;
                    var name = Path.GetFileName(@f);
                    //name = name.Split('.')[0]; //remove any file extension -z26
                    files.Add( name );
                }
            }
        }
        return files;
    }

    public void Reset ( string argument ) {
        activeFile = argument;
        GameModeManager.ChangeMode( GameModeManager.ModeId.World );
        levelName = Application.loadedLevelName;
        Application.LoadLevel( "CleanSimulation" );
    }
    
    public void Eval ( XForm context, string commands ) {
        loadQueue.Add( new Command(){ xform = context , commands = commands} );
    }

    public void Load ( XForm xform ) {
        var data = default(string);
        data = loadTextAsset( xform.name );
        xform.transform.parent = transform;

        if(data == default(string)){
            Debug.LogError( "Error definition not found: " + xform.name );
            Destroy( xform.gameObject );
            return;
        }

        loadQueue.Add( new Command(){ xform = xform , commands = data } );
    }

    public void Load ( string def, System.Action callback = null) {
        string data = default( string );
        data = loadTextAsset( def );
        if ( data == default( string ) ) { //next line actually loads maps from the players' document folder. -z26
            data = LoadTextFile(Path.Combine(Path.Combine(Path.Combine(Util.userDataPath,"defs"),Scenes[scene]), def) );
        }
        if ( data == default( string ) ) {
            Debug.LogError( "Error definition not found: " + def );
            return;
        }
        loadQueue.Add( new Command(){ xform = default( XForm ) , commands = data, callback = callback } );
    }

    public List<Entity> ParseBlueprint ( string blueprint ) {
        var addedEntities = new List<Entity>();
        var data = LoadTextFile( Path.Combine( Path.Combine(Util.userDataPath,"blueprints"), blueprint ));

        if(data == default(string)){
            return addedEntities;
        }
        foreach ( var expression in data.Split( '\n' ) ) {
            List<Entity> newEntities;
            if ( expression.Length > 0 && expression[ 0 ] == 0 ) {
                break;
            }
            Parse( expression, out newEntities );
            addedEntities.AddRange( newEntities );
        }
        addedEntities = addedEntities.Distinct().ToList();
        return addedEntities;
    }

    void Dequeue ( Command command ) { //this is where the recentlyAddedEntities list is populated from. -z26
        var local_to = "local_to";//the for loop to empty this list is at line 138.
        var addedEntities = new List<Entity>();
        foreach ( var expression in command.commands.Split( '\n' ) ) {
            List<Entity> newEntities;
            if ( expression.Length > 0 && expression[ 0 ] == 0 ) {
                break;
            }
            var result = Parse( expression, out newEntities );
            if ( result != default( string ) && result.StartsWith( "Error\t" ) ) {
//                foreach ( var c in expression ) {
//                    Debug.Log( ( int )c );
//                }
                Debug.Log("Error in " + expression );
            }
            addedEntities.AddRange( newEntities );
        }
        addedEntities = addedEntities.Distinct().ToList();
        if ( command.xform == default(  XForm ) ) {
            var spawners = root.Tails( p => p == local_to );
            foreach ( var s in spawners ) {
                GetXForm( s );
            }
        }
        else {
            var xform = entities[ "xform" ];
            var eXform = addedEntities
                .Where( e => e.Relationships()
                    .Any( r => e != xform && r.Predicate == "as" && r.Head == xform ) ).FirstOrDefault();
            if ( eXform != default(Entity) ) {
                eXform.SetProperty( "position", Util.Vector3ToProperty( command.xform.transform.position ) );
                eXform.SetProperty( "rotation", Util.Vector3ToProperty( command.xform.transform.eulerAngles ) );
                if ( !xforms.ContainsKey( eXform ) ) {
                    xforms.Add( eXform, command.xform );
                }
            }
        }

        addedEntities = SubPartImporter.FilterSubparts(addedEntities);
        //This doesnt repeat during a jelly flood, so I suspect the issue happens later down the process. -z26
        recentlyAddedEntities.AddRange( addedEntities );
    }

    Regex subPreObj = new Regex( @"^[a-z][a-z0-9_\@]+\ [a-z][a-z0-9_]+\ [a-z][a-z0-9_\@]+", RegexOptions.Compiled );
    Regex subProVal = new Regex( @"^[a-z][a-z0-9_\@]+\ \.\ [a-z][a-z0-9_\@]+\ .*", RegexOptions.Compiled );

    int IndexOfValue ( string input, char chr, int start, int count ) {
        int index = input.IndexOf( chr, start );
        if ( count > 0 ) {
            return IndexOfValue( input, chr, ++index, --count );
        }
        return index + 1;
    }

    string Parse ( string expression, out List<Entity> newEntities ) {
        newEntities = new List<Entity>();
        if ( expression.StartsWith( "#" ) ) {
            return default( string );
        }
        expression = expression.Trim();
        if ( subPreObj.IsMatch( expression ) ) {
            var parts = expression.Split( ' ' );
            var sub = GetEntity( parts[ 0 ] );
            var obj = GetEntity( parts[ 2 ] );
            newEntities.Add( sub );
            newEntities.Add( obj );
            var pre = parts[ 1 ];
            if ( !sub.Heads( p => p == pre ).Where( h => h == obj ).Any() ) {
                new Relationship( sub, pre, obj );
            }
            return default( string );
        }
        else if ( subProVal.IsMatch( expression ) ) {
            var parts = expression.Split( ' ' );
            var value = expression.Substring( IndexOfValue( expression, ' ', 0, 2 ) );
            var entity = GetEntity( parts[ 0 ] );
            newEntities.Add( entity );
            float number;
            if ( float.TryParse( value, out number ) ) {
                entity.SetProperty( parts[ 2 ], number );
            }
            else {
                entity.SetProperty( parts[ 2 ], value );
            }
            return default( string );
        }
        else if ( expression.Length > 2 && expression[ 0 ] != '#' ) {
            return "Error\t" + expression;
        }
        return default( string );
    }

    private void DeleteBot ( PartProxy part ) {
        var parent = part.entity.Heads( p => p == "local_to" ).FirstOrDefault();
        if ( parent == default(Entity) ) {
            return;
        }
        var bot = GetXForm( parent );
        parent.Relationships().ToList().ForEach( r => r.Break() );
        System.Predicate<string> entitiesFilter = f => f != "local_to" && f != "as";
        var allEntities = part.entity.EntitiesToSave( entitiesFilter );
        foreach ( var e in allEntities ) {
            if ( entities.ContainsKey( e.id ) ) {
                entities.Remove( e.id );
            }
            e.Relationships().ToList().ForEach( r => r.Break() );
        }
        foreach ( var child in bot.gameObject.GetComponentsInChildren<Transform>() ) {
            child.gameObject.SetActive(false);
        }
        bot.gameObject.SetActive(false);
    }

    void CreateDefaultData () {
        Load( "Bots/" + Scenes[scene], () => HexBatcher.instance.BatchWorld() );
    }

    Entity GetXFormEntity ( XForm xform ) {
        Entity eXForm = default(Entity);
        var keys = xforms.Keys;
        foreach ( var k in keys ) {
            if ( xforms[ k ] == xform ) {
                eXForm = k;
                break;
            }
        }

        return eXForm;
    }

    Entity GetXFormEntity ( string botName ) {
        var botEntity = default(Entity);
        var botEntityId = "";
        if ( botName.Contains( "/" ) ) {
            botEntityId = botName.Split( '/' )[ 1 ];
        }
        else {
            botEntityId = botName;
        }
        if ( !entities.TryGetValue( botEntityId, out botEntity ) ) {
            botEntity = GetEntity( botEntityId );
        }
        return botEntity;
    }

    public Entity GetEntity ( string id ) { //Lets you access an entity if you know its name@hash? -z26
        Entity entity;
        if ( entities.TryGetValue( id, out entity ) ) {
            return entity;
        }
        entity = new Entity( id );
        entities[ id ] = entity;
        return entity;
    }

    public XForm GetXForm ( Entity entity ) {

        XForm xform;
        if ( xforms.TryGetValue( entity, out xform ) ) {
            return xform;
        }


        var obj = new GameObject( entity.id );
        obj.transform.parent = transform;
        xform = obj.AddComponent< XForm >();
        xforms[ entity ] = xform;

        entity.ForProperty<string>( "position", value => {
            var position = Util.PropertyToVector3( value );
            obj.transform.position = position;
        } );
        entity.ForProperty<string>( "rotation", value => {
            var rotation = Util.PropertyToVector3( value );
            obj.transform.eulerAngles = rotation;
        } );

        if ( !entity.Heads( p => p == "as" ).Any() ) {
            new Relationship( entity, "as", entities[ "xform" ] );
        }

        return xform;
    }

    string loadTextAsset ( string assetName ) {
        var result = default(string);
        var resourceFile = Resources.Load( assetName, typeof( TextAsset ) ) as TextAsset;
        if ( resourceFile != null ) {
            result = resourceFile.text;
        }
        return result;
    }

    string LoadTextFile ( string path) {
        if ( !System.IO.File.Exists( path ) ) {
            return default( string );
        }
        var fs = new FileStream( path, FileMode.Open );
        var zm = new System.IO.MemoryStream( 0x40000 );
        var zip = new GZipStream( fs, CompressionMode.Decompress );
        CopyStream( zip, zm );
        fs.Close();
        return System.Text.Encoding.ASCII.GetString( zm.ToArray() );
    }

    public void ForPassCode ( string passcode, System.Action<bool> action ) {
        ForCodes( codes => {
            codes.ForProperty<string>( "codes", value => {
                var found = false;
                var chain = value.Split( '#' );
                if ( chain.Any( c => c == passcode ) ) {
                    found = true;
                }
                action( found );
            } );
        } );
    }

    public void SaveCode ( string passcode ) {
        if ( passcode == string.Empty ) {
            return;
        }
        ForCodes( codes => {
            var keychain = ( string )codes.GetProperty<string>( "codes" );
            keychain += "#" + passcode;
            var keys = keychain.Split( '#' ).Where( s => s != string.Empty ).Distinct();
            var newKeychain = "";
            foreach ( var k in keys ) {
                newKeychain += "#" + k;
            }
            codes.SetProperty( "codes", newKeychain );
        } );
    }

    void ForCodes ( System.Action<Entity> callback ) {
        var settings = entities[ "settings" ].Tails( p => p == "as" ).FirstOrDefault();
        if ( settings == null ) {
            CreateEntity( e => {
                e .id = "settings0";
                new Relationship( e, "as", entities[ "settings" ] );
                new Relationship( e, "for", root );
                e.SetProperty( "permissions", ( float )( Sec.Permission.all ) );
            },
            e => {
                CreateEntity( codes => {
                    codes.id = "codes0";
                    new Relationship( codes, "as", entities[ "settings_codes" ] );
                    new Relationship( codes, "for", e );
                    codes.SetProperty( "codes", "#" );
                }, codes => {
                    callback( codes );
                } );
            } );
        }
        else {
            var codes = settings.Tails( p => p == "for" )
                .Where( t => t.InmediateParent() == entities[ "settings_codes" ] )
                .FirstOrDefault();
            if ( codes != default(Entity) ) {
                callback( codes );
            }
            else {
                CreateEntity( c => {
                    c.id = "codes0";
                    new Relationship( c, "as", entities[ "settings_codes" ] );
                    new Relationship( c, "for", settings );
                    c.SetProperty( "codes", "#" );
                }, c => {
                    callback( c );
                } );
            }
        }
    }

    public void ForDaytime ( System.Action< Entity > callback ) {
        var settings = entities[ "settings" ].Tails( p => p == "as" ).FirstOrDefault();
        if ( settings == null ) {
            CreateEntity( e => {
                e .id = "settings0";
                new Relationship( e, "as", entities[ "settings" ] );
                new Relationship( e, "for", root );
                e.SetProperty( "permissions", ( float )( Sec.Permission.all ) );
            },
            e => {
                CreateEntity( daytime => {
                    daytime.id = "daytime0";
                    new Relationship( daytime, "as", entities[ "settings_daytime" ] );
                    new Relationship( daytime, "for", e );
                    daytime.SetProperty( "daytime", 0f );
                }, daytime => {
                    callback( daytime );
                } );
            } );
        }
        else {
            var daytime = settings.Tails( p => p == "for" )
                .Where( t => t.InmediateParent() == entities[ "settings_daytime" ] )
                .FirstOrDefault();
            if ( daytime != default(Entity) ) {
                callback( daytime );
            }
            else {
                CreateEntity( dt => {
                    dt.id = "daytime0";
                    new Relationship( dt, "as", entities[ "settings_daytime" ] );
                    new Relationship( dt, "for", settings );
                    dt.SetProperty( "daytime", 0f );
                }, dt => {
                    callback( dt );
                } );
            }
        }
    }

    public void SetAddressHandler ( string id, OscMessageHandler handler ) {
        osc.SetAddressHandler( id, handler );
    }

    public void SetupOSC ( string remoteIP, int listenPort, int sendPort = 9000 ) {
        osc.Close();
        udp.Close();
        udp.Restart( remoteIP, sendPort, listenPort );
        osc.Restart( udp );
    }

}
