using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class Rocks : MonoBehaviour {

    public int numOfRocks = 100;
    public float radius = 100;
    public Waveform scale;
    public Waveform rotationX;
    public Waveform rotationY;
    public Waveform rotationZ;
    public Waveform distributionX;
    public Waveform distributionY;
    public Waveform distributionZ;
    public List< GameObject > rockAssets;

    void Start () {
        for ( float i = 0; i < numOfRocks; ++i ) {
            var rock = Instantiate( rockAssets[ Random.Range( 0, rockAssets.Count ) ] ) as GameObject;
            rock.transform.parent = transform;
            var x = distributionX.Sample( i / numOfRocks ).sample;
            var y = distributionY.Sample( i / numOfRocks ).sample;
            var z = distributionZ.Sample( i / numOfRocks ).sample;
            var position = new Vector3( x, y, z );
            position = position + position.normalized * radius;
            rock.transform.localPosition = position;
            x = y = z = scale.Sample( i / numOfRocks ).sample;
            rock.transform.localScale = new Vector3( x, y, z );
            rock.transform.rotation = Random.rotation;
            x = rotationX.Sample( i / numOfRocks ).sample;
            y = rotationY.Sample( i / numOfRocks ).sample;
            z = rotationZ.Sample( i / numOfRocks ).sample;
            rock.transform.localRotation = Quaternion.Euler( x, y, z );
        }
    }
 
    void Update () {
 
    }
}
