import library

with open('spider.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addContinuum("core","0,0,0","0,0,0")
	obj.extend("box1","3","core","0")
	obj.addContinuum("box1","4,0,0","0,0,0")


	obj.addContinuum("leg1","8,0,3","0,0,0")
	obj.extend("leg1","5","box1","2")

	obj.addMotor("rightbackleg","8,0,5","-90,180,0")
	obj.extend("rightbackleg","1","leg1","2")
	obj.addContinuum("leg1_1","8,0,8","0,0,0")
	obj.extend("leg1_1","5","rightbackleg","0")
	obj.addPiston("rightbackfoot","10,0,8","0,0,-90")
	obj.extend("rightbackfoot","1","leg1_1","0")


	obj.addContinuum("leg2","8,0,-3","0,0,0")
	obj.extend("leg2","2","box1","5")

	obj.addMotor("leftbackleg","8,0,-5","-90,0,0")
	obj.extend("leftbackleg","1","leg2","5")
	obj.addContinuum("leg2_1","8,0,-8","0,0,0")
	obj.extend("leg2_1","2","leftbackleg","0")

	obj.addPiston("leftbackfoot","10,0,-8","0,0,-90")
	obj.extend("leftbackfoot","1","leg2_1","0")


	obj.addContinuum("leg3","-4,0,3","0,0,0")
	obj.extend("leg3","5","core","2")

	obj.addMotor("rightfrontleg","-4,0,5","-90,180,0")
	obj.extend("rightfrontleg","1","leg3","2")
	obj.addContinuum("leg3_1","-4,0,8","0,0,0")
	obj.extend("leg3_1","5","rightfrontleg","0")

	obj.addPiston("rightfrontfoot","-6,0,8","0,0,90")
	obj.extend("rightfrontfoot","1","leg3_1","3")
#	obj.addContinuum("leg3_2","-6,0,8","0,0,0")
#	obj.extend("leg3_2","3","leg3_1","0")
#	obj.addContinuum("leg3_3","-8,0,8","0,0,0")
#	obj.extend("leg3_3","3","leg3_2","0")	


	obj.addContinuum("leg4","-4,0,-3","0,0,0")
	obj.extend("leg4","2","core","5")


	obj.addMotor("leftfrontleg","-4,0,-5","-90,0,0")
	obj.extend("leftfrontleg","1","leg4","5")
	obj.addContinuum("leg4_1","-4,0,-8","0,0,0")
	obj.extend("leg4_1","2","leftfrontleg","0")
	obj.addPiston("leftfrontfoot","-6,0,-8","0,0,90")
	obj.extend("leftfrontfoot","1","leg4_1","3")


	obj.addPistol("pistol1","-3,4,0","0,-90,0")
	obj.extend("pistol1","1","core","1")


#	obj.addContinuum("jet_holder","8,6,0","0,0,0")
#	obj.extend("jet_holder","4","box1","1")
#
#	obj.addMotor("leftjetmotor","8,6,-2","-90,0,0")
#	obj.extend("leftjetmotor","1","jet_holder","0")
#	obj.addMotor("rightjetmotor","8,6,2","-90,180,0")
#	obj.extend("rightjetmotor","1","jet_holder","2")
#
#	obj.addJet("left_jet","10,6,-5","0,0,90")
#	obj.extend("left_jet","0","leftjetmotor","0")
#	obj.addJet("right_jet","10,6,5","0,0,90")
#	obj.extend("right_jet","1","rightjetmotor","0")

	obj.addOscillator(name = "osc1",
		pos = "0,10,0",
		rot= "0,0,0",
		type="Triangle",
		invert= "True",
		frequency_range = "5,10,0.1",
		frequency_value = "1",
		amplitude_range = "-360,360,1",
		amplitude_value = "45",
		offset_range= "-200,200,1",
		offset_value = "-90",
		phase_range = "0,10,0.1",
		phase_value = "0.75")


	obj.addOscillator(name = "osc2",
		pos = "0,10,0",
		rot= "0,0,0",
		type="Triangle",
		invert= "False",
		frequency_range = "5,10,0.1",
		frequency_value = "1",
		amplitude_range = "-360,360,1",
		amplitude_value = "45",
		offset_range= "-200,100,1",
		offset_value = "90",
		phase_range = "0,10,0.1",
		phase_value = "0.50")

	obj.addOscillator(name = "osc3",
		pos = "0,10,0",
		rot= "0,0,0",
		type="Triangle",
		invert= "True",
		frequency_range = "5,10,0.1",
		frequency_value = "1",
		amplitude_range = "-360,360,1",
		amplitude_value = "45",
		offset_range= "-200,200,1",
		offset_value = "90",
		phase_range = "0,10,0.1",
		phase_value = "0.25")


	obj.addOscillator(name = "osc4",
		pos = "0,10,0",
		rot= "0,0,0",
		type="Triangle",
		invert= "False",
		frequency_range = "5,10,0.1",
		frequency_value = "1",
		amplitude_range = "-360,360,1",
		amplitude_value = "45",
		offset_range= "-200,100,1",
		offset_value = "-90",
		phase_range = "0,10,0.1",
		phase_value = "0")

#-----------------------------------------------------------------------------

	obj.addOscillator(name = "osc_a",
		pos = "0,10,0",
		rot= "0,0,0",
		type="Sawtooth",
		invert= "False",
		frequency_range = "5,10,0.1",
		frequency_value = "1",
		amplitude_range = "-0.5,0.5,0.01",
		amplitude_value = "0.5",
		offset_range= "-0.5,0.5,0.01",
		offset_value = "0.5",
		phase_range = "0,1,0.1",
		phase_value = "0.4")

	obj.addOscillator(name = "osc_b",
		pos = "0,10,0",
		rot= "0,0,0",
		type="Sawtooth",
		invert= "False",
		frequency_range = "5,10,0.1",
		frequency_value = "1",
		amplitude_range = "-0.5,0.5,0.01",
		amplitude_value = "0.5",
		offset_range= "-0.5,0.5,0.01",
		offset_value = "0.5",
		phase_range = "0,1,0.1",
		phase_value = "0.4")

	obj.addOscillator(name = "osc_c",
		pos = "0,10,0",
		rot= "0,0,0",
		type="Sawtooth",
		invert= "False",
		frequency_range = "5,10,0.1",
		frequency_value = "1",
		amplitude_range = "-0.5,0.5,0.01",
		amplitude_value = "0.5",
		offset_range= "-0.5,0.5,0.01",
		offset_value = "0.5",
		phase_range = "0,1,0.1",
		phase_value = "0.1")


	obj.addOscillator(name = "osc_d",
		pos = "0,10,0",
		rot= "0,0,0",
		type="Sawtooth",
		invert= "False",
		frequency_range = "5,10,0.1",
		frequency_value = "1",
		amplitude_range = "-0.5,0.5,0.01",
		amplitude_value = "0.5",
		offset_range= "-0.5,0.5,0.01",
		offset_value = "0.5",
		phase_range = "0,1,0.1",
		phase_value = "0.75")



	obj.consume("rightbackleg","angle","osc1","sample")
	obj.consume("leftbackleg","angle","osc2","sample")
	obj.consume("rightfrontleg","angle","osc3","sample")
	obj.consume("leftfrontleg","angle","osc4","sample")



	obj.consume("rightbackfoot","position","osc_a","sample")
	obj.consume("leftbackfoot","position","osc_b","sample")
	obj.consume("rightfrontfoot","position","osc_c","sample")
	obj.consume("leftfrontfoot","position","osc_d","sample")

