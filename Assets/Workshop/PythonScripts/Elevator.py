import library

with open('../elevator.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()







	obj.addCore("master","0,0,2","0,0,0")

	obj.addElevator("ele","0,0,0","0,0,0")

	obj.extend("master","5","ele","0")

	obj.addOscillator(name = "elevator_control",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "0.2",
		amplitude_range = "0,1,1",
		amplitude_value = "1",
		offset_range= "0,0,0",
		offset_value = "0.5",
		phase_range = "0,0,0")
	obj.consume("ele","position","elevator_control","sample")


	obj.addHook("hook001","0,-12,0","0,0,0")
	obj.extend("hook001","0","ele","1")