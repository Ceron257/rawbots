import library

with open('../../BotDefinitions/chopper.txt', 'w') as f:
    obj = library.Workshop(f)
    obj.libs()

    obj.addCore("core001","0,0,0","0,0,0")
    obj.addContinuum("box01","2,0,0","0,0,0")
    obj.extend("box01","3","core001","0")

    obj.addContinuum("box02","4,0,0","0,0,0")
    obj.extend("box02","3","box01","0")

    obj.addContinuum("box03","6,0,0","0,0,0")
    obj.extend("box03","3","box02","0")

    obj.addContinuum("box04","8,0,0","0,0,0")
    obj.extend("box04","3","box03","0")

    obj.addContinuum("box05","10,0,0","0,0,0")
    obj.extend("box05","3","box04","0")

    obj.addContinuum("box06","-2,0,0","0,0,0")
    obj.extend("box06","0","core001","3")

    obj.addContinuum("box07","-4,0,0","0,0,0")
    obj.extend("box07","0","box06","3")

    obj.addContinuum("box08","-6,0,0","0,0,0")
    obj.extend("box08","0","box07","3")

    obj.addContinuum("box09","-8,0,0","0,0,0")
    obj.extend("box09","0","box08","3")

    obj.addContinuum("box10","-10,0,0","0,0,0")
    obj.extend("box10","0","box09","3")

    obj.addMotor("motor01","0,0,2","0,90,90")
    obj.extend("motor01","1","core001","0")

    obj.addMotor("motor02","0,0,-2","0,270,90")
    obj.extend("motor02","1","core001","5")

    obj.addCannon("cannon01","2,0,5","0,90,90")
    obj.extend("cannon01","0","motor01","0")

    obj.addCannon("cannon02","2,0,-5","0,90,270")
    obj.extend("cannon02","0","motor02","0")

    obj.addMotor("motor03","10,2,0","0,90,0")
    obj.extend("motor03","1","box05","1")

    obj.addTripleHelix("helix01","10,4,0","0,90,0")
    obj.extend("helix01","0","motor03","0")

    obj.addMotor("motor04","4,-2,0","0,90,0")
    obj.extend("motor04","0","box02","4")

    obj.addContinuum("box11","4,-4,0","0,0,0")
    obj.extend("box11","1","motor04","1")

    obj.addMotor("motor05","4,-4,2","0,90,90")
    obj.extend("motor05","1","box11","2")

    obj.addMotor("motor06","4,-4,-2","0,270,90")
    obj.extend("motor06","1","box11","5")

    obj.addSensor("sensor01","4,-4,4","0,90,90")
    obj.extend("sensor01","0","motor05","0")

    obj.addSensor("sensor02","4,-4,-4","0,90,270")
    obj.extend("sensor02","0","motor06","0")

    obj.addMotor("motor07","-10,2,0","0,270,0")
    obj.extend("motor07","1","box10","1")
    
    obj.addTripleHelix("helix02","-10,4,0","0,90,0")
    obj.extend("helix02","0","motor07","0")

    obj.addMotor("motor08","-10,-2,0","0,90,0")
    obj.extend("motor08","0","box10","4")

    obj.addContinuum("box12","-10,-4,0","0,0,0")
    obj.extend("box12","1","motor08","1")

    obj.addMotor("motor09","-10,-4,2","0,90,90")
    obj.extend("motor09","1","box12","2")

    obj.addMotor("motor10","-10,-4,-2","0,270,90")
    obj.extend("motor10","1","box12","5")

    obj.addJet("jet01","-12,-4,4","0,0,270")
    obj.extend("jet01","1","motor09","0")

    obj.addJet("jet02","-12,-4,-4","0,0,270")
    obj.extend("jet02","0","motor10","0")

    # --- Shields

    obj.addBody_gen03_1("shield01","6,1,0","0,270,180")
    obj.extend("shield01","0","box03","1")

    obj.addBody_gen03_1("shield02","0,1,0","0,270,180")
    obj.extend("shield02","0","core001","1")

    obj.addBody_gen03_1("shield03","-6,1,0","0,270,180")
    obj.extend("shield03","0","box08","1")

    obj.addBody_gen03_1("shield04","-11,-4,0","0,180,90")
    obj.extend("shield04","0","box12","3")
    
