import library

with open('../../BotDefinitions/bike.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("core00","0,0,0","0,0,0")
	obj.addContinuum("right_support","2,0,0","0,0,0")
	obj.addContinuum("left_support","-2,0,0","0,0,0")
	obj.extend("right_support","3","core00","0")
	obj.extend("left_support","0","core00","3")

	obj.addContinuum("right_front_wheel_base","5,0,6","315,0,0")
	obj.addMotor("right_front_wheel_motor","3,0,6","0,0,90")
	obj.extend("right_front_wheel_base","3","right_front_wheel_motor","1")
	obj.addPiston("right_front_damper","5,2.87,3.24","45,180,180",initial_values=[["position","1"]])
	obj.extend("right_front_damper","0","right_front_wheel_base","1")
	obj.addWideWheel("front_wheel","0,0,6","0,90,0")
	obj.extend("front_wheel","0","right_front_wheel_motor","0")

	obj.addContinuum("left_front_wheel_base","-5,0,6","315,0,0")
	obj.addMotor("left_front_wheel_motor","-3,0,6","0,0,270")
	obj.extend("left_front_wheel_base","0","left_front_wheel_motor","1")
	obj.addPiston("left_front_damper","-5,2.87,3.24","45,180,180",initial_values=[["position","1"]])
	obj.extend("left_front_damper","0","left_front_wheel_base","1")
	obj.extend("front_wheel","1","left_front_wheel_motor","0")

	obj.addContinuum("right_piston_top","5,4.20,1.82","315,0,0")
	obj.extend("right_piston_top","4","right_front_damper","1")
	obj.extend("right_piston_top","5","right_support","1")

	obj.addContinuum("left_piston_top","-5,4.20,1.82","315,0,0")
	obj.extend("left_piston_top","4","left_front_damper","1")
	obj.extend("left_piston_top","5","left_support","1")

	obj.addContinuum("center_connector01","0,0,-6","0,0,0")
	obj.extend("center_connector01","2","core00","5")
	obj.addContinuum("center_connector02","0,2,-6","0,0,0")
	obj.extend("center_connector02","4","center_connector01","1")

	obj.addWideWheel("back_wheel","0,0,-12","0,90,0")
	obj.addMotor("right_back_wheel_motor","3,0,-12","0,0,90")
	obj.addMotor("left_back_wheel_motor","-3,0,-12","0,0,270")
	obj.extend("back_wheel","0","right_back_wheel_motor","0")
	obj.extend("back_wheel","1","left_back_wheel_motor","0")

	obj.addJet("jet001","5,0,-14","0,90,90")
	obj.extend("jet001","1","right_back_wheel_motor","1")
	obj.addJet("jet002","-5,0,-14","0,90,90")
	obj.extend("jet002","0","left_back_wheel_motor","1")

	obj.extend("jet001","0","center_connector01","0")
	obj.extend("jet002","1","center_connector01","3")