import library

with open('../../Resources/Bots/lasertrap.txt', 'w') as f:
    obj = library.Workshop(f)
    obj.libs()

    obj.addElbow("elbow0","0,35.98281,42.94009","0,180,90")
    obj.addElbow("elbow1","0,35.98282,5.89284","0,180,90")
    obj.addHexColumn("hexcolumn2","1.5,40.5,4","0,0,30")
    obj.addHexColumn("hexcolumn3","0,42,48","90,330,0")
    obj.addHexColumn("hexcolumn4","0,42,0","90,30,0")
    obj.addLaser("laser5","0,34,42.9388","0,180,180")
    obj.addLaser("laser6","0,34,5.927941","0,180,180")

    obj.extend("hexcolumn4","03","hexcolumn2","01")
    obj.extend("hexcolumn3","05","hexcolumn2","00")
    obj.extend("hexcolumn2","04","elbow0","0")
    obj.extend("hexcolumn2","05","elbow1","0")
    obj.extend("elbow0","1","laser5","0")
    obj.extend("elbow1","1","laser6","0")

    obj.addOscillator(name = "leg01",
        pos = "0,4,5",
        rot= "0,0,0",
        type="Sine",
        invert= "False",
        frequency_range = "0,10,0.1",
        frequency_value = "1",
        amplitude_range = "0,10,1",
        amplitude_value = "50",
        offset_range= "-1500,1500,1",
        offset_value = "10",
        phase_value = "0.5",
        phase_range = "0,10,0.1")

    obj.consume("elbow0","angle","leg01","sample")
    obj.consume("elbow1","angle","leg01","sample")