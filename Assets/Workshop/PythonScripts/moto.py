import library

with open('../motoBasic.txt', 'w') as f:
    obj = library.Workshop(f)
    obj.libs()

    obj.addCore("core1","0,0,0","45,0,0")
    obj.addContinuum("box01","2,0,0","45,0,0")
    obj.extend("box01","3","core1","0")

    obj.addContinuum("box02","0,-1.5,1.5","45,0,0")
    obj.extend("box02","5","core1","4")

    obj.addContinuum("box03","2,-1.5,1.5","45,0,0")
    obj.extend("box03","5","box01","2")

    obj.addContinuum("box04","-2,-1.5,1.5","45,0,0")
    obj.extend("box04","0","box02","3")

    obj.addContinuum("box05","-4,-1.5,1.5","45,0,0")
    obj.extend("box05","0","box04","3")

    obj.addContinuum("box06","4,-1.5,1.5","45,0,0")
    obj.extend("box06","3","box03","0")

    obj.addContinuum("box07","6,-1.5,1.5","45,0,0")
    obj.extend("box07","3","box06","0")

    obj.addContinuum("box08","6,-3,3","45,0,0")
    obj.extend("box08","5","box07","2")

    obj.addContinuum("box09","-4,-3,3","45,0,0")
    obj.extend("box09","5","box05","2")

    obj.addContinuum("box10","6,-4.5,4.5","45,0,0")
    obj.extend("box10","5","box08","2")

    obj.addContinuum("box11","-4,-4.5,4.5","45,0,0")
    obj.extend("box11","5","box09","2")

    obj.addContinuum("box12","6,-6,6","45,0,0")
    obj.extend("box12","5","box10","2")

    obj.addContinuum("box13","-4,-6,6","45,0,0")
    obj.extend("box13","5","box11","2")

    obj.addMotor("motor01","-2,-6,6","90,90,0")
    obj.extend("motor01","1","box13","0")

    obj.addMotor("motor02","4,-6,6","90,270,0")
    obj.extend("motor02","1","box12","3")

    obj.addWideWheel("widewheel01","1,-6,6","0,90,0")
    obj.extend("widewheel01","1","motor01","0")
    obj.extend("widewheel01","0","motor02","0")

    obj.addContinuum("box14","0,1.5,-1.5","45,0,0")
    obj.extend("box14","2","core1","5")

    obj.addContinuum("box15","2,1.5,-1.5","45,0,0")
    obj.extend("box15","2","box01","5")
    
    obj.addContinuum("box16","0,0,-3","45,0,0")
    obj.extend("box16","1","box14","4")

    obj.addContinuum("box17","2,0,-3","45,0,0")
    obj.extend("box17","1","box15","4")

    obj.addContinuum("box18","4,0,-3","45,0,0")
    obj.extend("box18","3","box17","0")

    obj.addContinuum("box19","6,0,-3","45,0,0")
    obj.extend("box19","3","box18","0")

    obj.addContinuum("box20","-2,0,-3","45,0,0")
    obj.extend("box20","0","box16","3")

    obj.addContinuum("box21","-4,0,-3","45,0,0")
    obj.extend("box21","0","box20","3")

    obj.addContinuum("box22","-4,-1.5,-4.5","45,0,0")
    obj.extend("box22","1","box21","4")

    obj.addContinuum("box23","-4,-3,-6","45,0,0")
    obj.extend("box23","1","box22","4")

    obj.addContinuum("box24","-4,-4.5,-7.5","45,0,0")
    obj.extend("box24","1","box23","4")

    obj.addContinuum("box25","-4,-6,-9","45,0,0")
    obj.extend("box25","1","box24","4")

    
    obj.addContinuum("box26","6,-1.5,-4.5","45,0,0")
    obj.extend("box26","1","box19","4")

    obj.addContinuum("box27","6,-3,-6","45,0,0")
    obj.extend("box27","1","box26","4")

    obj.addContinuum("box28","6,-4.5,-7.5","45,0,0")
    obj.extend("box28","1","box27","4")

    obj.addContinuum("box29","6,-6,-9","45,0,0")
    obj.extend("box29","1","box28","4")

    obj.addMotor("motor03","-2,-6,-9","90,90,0")
    obj.extend("motor03","1","box25","0")

    obj.addMotor("motor04","4,-6,-9","90,270,0")
    obj.extend("motor04","1","box29","3")

    obj.addWideWheel("widewheel02","1,-6,-9","0,90,0")
    obj.extend("widewheel02","1","motor03","0")
    obj.extend("widewheel02","0","motor04","0")


    obj.addMotor("motor05","-6,-1.5,1.5","90,270,0")
    obj.extend("motor05","1","box05","3")

    obj.addMotor("motor06","8,-1.5,1.5","90,90,0")
    obj.extend("motor06","1","box07","0")

    obj.addShotgun("cannon1","-8,-1.5,3","0,0,90")
    obj.extend("cannon1","0","motor05","0")
    
    obj.addShotgun("cannon2","10,-1.5,3","0,0,270")
    obj.extend("cannon2","0","motor06","0")

    #shields

    obj.addBody_gen03_1("shield1","7,-4.5,4.5","45,0,90")
    obj.extend("shield1","0","box10","0")

    obj.addBody_gen03_1("shield2","-5,-4.5,4.5","45,0,270")
    obj.extend("shield2","0","box11","3")

    obj.addBody_gen03_1("shield3","7,-3,-6","45,180,270")
    obj.extend("shield3","0","box27","0")

    obj.addBody_gen03_1("shield4","-5,-3,-6","45,180,90")
    obj.extend("shield4","0","box23","3")

    obj.addBody_gen03_2("shield5","6,-0.5,2","310,0,180")
    obj.extend("shield5","0","box07","1")

    obj.addBody_gen03_2("shield6","-4,-0.5,2","310,0,0")
    obj.extend("shield6","0","box05","1")
    



    
    
