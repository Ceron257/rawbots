import library

with open('../planet001.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()
	obj.addBot("bot001","-34.6417,605.2211,102.36","14.98,5.524035,4.1542", "ldu-2")
	obj.addBot("bot002","25.6417,605.2211,102.36","14.98,5.524035,4.1542", "sentinel")
	obj.addBot("extractor001","81.99,598.69,100.22","14.98,5.5,355.25", "extractor")
	obj.addBot("processor002","60.85551,603.6802,-70.70389","1.8,4.58,351.7", "generator")

	obj.addCrystal("crystal001","62.85551,603.6802,-70.70389","1.8,4.58,351.7")

	obj.addBot("lightpost001","-67.26,631.52,34.86","0,0,7.82", "militarLightPostA")


	obj.addCamera("camera001","0,0,0","0,0,0")
	obj.consume("camera001","target","bot001","self")
	obj.consume("camera001","target","bot002","self")
	obj.consume("camera001","target","extractor001","self")