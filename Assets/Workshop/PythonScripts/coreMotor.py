import library

with open('../../BotDefinitions/coreMotor.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("master","0,0,0","0,0,0")
	obj.addMotor("motor001","0,2,0","0,0,0",initial_values =[["velocity","100"]] )
	obj.extend("motor001","1","master","1")
	obj.addContinuum("box01","0,4,0","0,0,0")
	obj.extend("box01","4","motor001","0")

