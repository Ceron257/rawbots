import library

freq="0.5"

with open('../runner.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("core00","0,0,4","0,0,0")
	obj.addContinuum("vertebrae00","0,0,-4","0,0,0")
	obj.addElbow("vertebrae01","0,0,0","0,270,0")
	obj.addContinuum("vertebrae02","0,0,12","0,0,0")
	obj.addElbow("vertebrae03","0,0,8","0,90,0")
	obj.addMotor("vertebraemotor00","0,0,10","90,0,0",initial_values=[["angle","0"]])
	obj.addMotor("vertebraemotor01","0,0,-2","270,0,0",initial_values=[["angle","0"]])
	obj.extend("vertebrae03","0","core00","2")
	obj.extend("vertebrae01","0","core00","5")
	obj.extend("vertebraemotor00","1","vertebrae03","1")
	obj.extend("vertebraemotor01","1","vertebrae01","1")
	obj.extend("vertebrae00","2","vertebraemotor01","0")
	obj.extend("vertebrae02","5","vertebraemotor00","0")

	obj.addContinuum("rightclavicle","2,0,12","0,0,0")
	obj.extend("rightclavicle","3","vertebrae02","0")
	obj.addMotor("rightshouldermotor","4,0,12","0,0,270",initial_values=[["angle","0"]])
	obj.extend("rightshouldermotor","1","rightclavicle","0")
	obj.addContinuum("rightshoulder","6,0,12","0,0,0")
	obj.extend("rightshoulder","3","rightshouldermotor","0")
	obj.addPiston("rightarmextensor","6,-2,12","180,0,0",initial_values=[["position","0.25"]])
	obj.extend("rightarmextensor","1","rightshoulder","4")
	obj.addElbow("rightelbow","6,-6,12","0,180,270",initial_values=[["angle","-45"]])
	obj.extend("rightelbow","1","rightarmextensor","0")
	obj.addElbow("rightfrontankle","6,-8,12","0,0,270",initial_values=[["angle","-45"]])
	obj.extend("rightfrontankle","1","rightelbow","0")
	obj.addFoot("rightpaw","6,-10,12","0,0,0")
	obj.extend("rightpaw","0","rightfrontankle","0")

	obj.addContinuum("leftclavicle","-2,0,12","0,0,0")
	obj.extend("leftclavicle","0","vertebrae02","3")
	obj.addMotor("leftshouldermotor","-4,0,12","0,0,90",initial_values=[["angle","0"]])
	obj.extend("leftshouldermotor","1","leftclavicle","3")
	obj.addContinuum("leftshoulder","-6,0,12","0,0,0")
	obj.extend("leftshoulder","0","leftshouldermotor","0")
	obj.addPiston("leftarmextensor","-6,-2,12","180,0,0",initial_values=[["position","0.25"]])
	obj.extend("leftarmextensor","1","leftshoulder","4")
	obj.addElbow("leftelbow","-6,-6,12","0,180,270",initial_values=[["angle","-45"]])
	obj.extend("leftelbow","1","leftarmextensor","0")
	obj.addElbow("leftfrontankle","-6,-8,12","0,0,270",initial_values=[["angle","-45"]])
	obj.extend("leftfrontankle","1","leftelbow","0")
	obj.addFoot("leftpaw","-6,-10,12","0,0,0")
	obj.extend("leftpaw","0","leftfrontankle","0")

	obj.addContinuum("righthipjoin","2,0,-4","0,0,0")
	obj.extend("righthipjoin","3","vertebrae00","0")
	obj.addMotor("righthipmotor","4,0,-4","0,0,270","-45,45,1",initial_values=[["angle","15"]])
	obj.extend("righthipmotor","1","righthipjoin","0")
	obj.addContinuum("righthip","6,0,-4","0,0,0")
	obj.extend("righthip","3","righthipmotor","0")
	obj.addPiston("rightlegextensor","6,-2,-4","180,0,0",initial_values=[["position","0.25"]])
	obj.extend("rightlegextensor","1","righthip","4")
	obj.addElbow("rightknee","6,-6,-4","0,180,270",initial_values=[["angle","-45"]])
	obj.extend("rightknee","1","rightlegextensor","0")
	obj.addElbow("rightbackankle","6,-8,-4","0,0,270",initial_values=[["angle","-20"]])
	obj.extend("rightbackankle","1","rightknee","0")
	obj.addFoot("rightfoot","6,-10,-4","0,180,0")
	obj.extend("rightfoot","0","rightbackankle","0")

	obj.addContinuum("lefthipjoin","-2,0,-4","0,0,0")
	obj.extend("lefthipjoin","0","vertebrae00","3")
	obj.addMotor("lefthipmotor","-4,0,-4","0,0,90",initial_values=[["angle","-15"]])
	obj.extend("lefthipmotor","1","lefthipjoin","3")
	obj.addContinuum("lefthip","-6,0,-4","0,0,0")
	obj.extend("lefthip","0","lefthipmotor","0")
	obj.addPiston("leftlegextensor","-6,-2,-4","180,0,0",initial_values=[["position","0.25"]])
	obj.extend("leftlegextensor","1","lefthip","4")
	obj.addElbow("leftknee","-6,-6,-4","0,180,270",initial_values=[["angle","-45"]])
	obj.extend("leftknee","1","leftlegextensor","0")
	obj.addElbow("leftbackankle","-6,-8,-4","0,0,270",initial_values=[["angle","-20"]])
	obj.extend("leftbackankle","1","leftknee","0")
	obj.addFoot("leftfoot","-6,-10,-4","0,180,0")
	obj.extend("leftfoot","0","leftbackankle","0")
	
	#obj.addContinuum("tailbase00","0,2,-2","0,0,0")
	#obj.extend("tailbase00","4","vertebrae00","1")
	#obj.addContinuum("tailbase01","-2,2,-2","0,0,0")
	#obj.addContinuum("tailbase02","2,2,-2","0,0,0")
	#obj.extend("tailbase01","0","tailbase00","3")
	#obj.extend("tailbase02","3","tailbase00","0")
#
	#obj.addContinuum("tailbase05","2,0,0","0,0,0")
	#obj.addContinuum("tailbase06","2,2,0","0,0,0")
	#obj.extend("tailbase05","5","righthipjoin","2")
	#obj.extend("tailbase06","5","tailbase02","2")
#
	#obj.addContinuum("tailbase09","-2,0,0","0,0,0")
	#obj.addContinuum("tailbase10","-2,2,0","0,0,0")
	#obj.extend("tailbase09","5","lefthipjoin","2")
	#obj.extend("tailbase10","5","tailbase01","2")
#
	#obj.addMotor("guntailmotor00","2,4,-2","0,0,0")
	#obj.addShotgun("guntail00","2,7,0","0,0,0")
	#obj.extend("guntailmotor00","1","tailbase02","1")
	#obj.extend("guntail00","0","guntailmotor00","0")
#
	#obj.addMotor("guntailmotor01","-2,4,-2","0,0,0")
	#obj.addShotgun("guntail01","-2,7,0","0,0,0")
	#obj.extend("guntailmotor01","1","tailbase01","1")
	#obj.extend("guntail01","0","guntailmotor01","0")
#
	#obj.addBody_gen03_1("coreshield","0,1,4","0,0,180")
	#obj.extend("coreshield","0","core00","1")
	
	obj.addBody_gen03_2("righthipshield","6,1,-3","270,90,0")
	obj.extend("righthipshield","0","righthip","3")
	obj.addBody_gen03_2("lefthipshield","-6,1,-3","270,90,0")
	obj.extend("lefthipshield","0","lefthip","1")

	obj.addBody_gen03_2("rightshouldershield","3,1,12","270,0,0")
	obj.extend("rightshouldershield","0","rightclavicle","1")
	obj.addBody_gen03_2("leftshouldershield","-3,1,12","270,180,0")
	obj.extend("leftshouldershield","0","leftclavicle","1")

	obj.addMotor("neckmotor","0,2,12","0,0,0")
	obj.extend("neckmotor","1","vertebrae02","1")
	obj.addCannon("head","0,5,14","0,0,0")
	obj.extend("head","0","neckmotor","0")



	obj.addOscillator(name = "righthiposc",
		pos = "0,0,8",
		rot= "0,0,0",
		type="Sine",
		invert= "True",
		frequency_range = "5,10,1",
		frequency_value = freq,
		amplitude_range = "0,45,1",
		amplitude_value = "15",
		offset_range= "-200,200,1",
		offset_value = "10",
		phase_range = "0,10,1",
		phase_value = "0")
	obj.addOscillator(name = "rightshoulderosc",
		pos = "0,0,8",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "5,10,1",
		frequency_value = freq,
		amplitude_range = "0,45,1",
		amplitude_value = "5",
		offset_range= "-200,200,1",
		offset_value = "0",
		phase_range = "0,10,1",
		phase_value = "0")
	obj.addOscillator(name = "leftshoulderosc",
		pos = "0,0,8",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "5,10,1",
		frequency_value = freq,
		amplitude_range = "0,45,1",
		amplitude_value = "-5",
		offset_range= "-200,200,1",
		offset_value = "0",
		phase_range = "0,10,1",
		phase_value = "0.5")
	obj.addOscillator(name = "lefthiposc",
		pos = "0,0,8",
		rot= "0,0,0",
		type="Sine",
		invert= "True",
		frequency_range = "5,10,1",
		frequency_value = freq,
		amplitude_range = "0,45,1",
		amplitude_value = "-15",
		offset_range= "-200,200,1",
		offset_value = "-10",
		phase_range = "0,10,1",
		phase_value = "0.5")
	obj.addOscillator(name = "osc4",
		pos = "0,0,8",
		rot= "0,0,0",
		type="Sine",
		invert= "True",
		frequency_range = "5,10,1",
		frequency_value = freq,
		amplitude_range = "0,45,1",
		amplitude_value = "-45",
		offset_range= "-200,200,1",
		offset_value = "-45",
		phase_range = "0,10,1",
		phase_value = "0")
	obj.addOscillator(name = "osc5",
		pos = "0,0,8",
		rot= "0,0,0",
		type="Sine",
		invert= "True",
		frequency_range = "5,10,1",
		frequency_value = freq,
		amplitude_range = "0,45,1",
		amplitude_value = "-45",
		offset_range= "-200,200,1",
		offset_value = "-45",
		phase_range = "0,10,1",
		phase_value = "0.5")
	obj.addOscillator(name = "piston_osc0",
		pos = "0,0,8",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "5,10,1",
		frequency_value = freq,
		amplitude_range = "0,1,0.25",
		amplitude_value = "0.125",
		offset_range= "-200,200,1",
		offset_value = "0.125",
		phase_range = "0,10,1",
		phase_value = "0")
	obj.addOscillator(name = "piston_osc1",
		pos = "0,0,8",
		rot= "0,0,0",
		type="Sine",
		invert= "True",
		frequency_range = "5,10,1",
		frequency_value = freq,
		amplitude_range = "0,1,0.25",
		amplitude_value = "0.125",
		offset_range= "-200,200,1",
		offset_value = "0.125",
		phase_range = "0,10,1",
		phase_value = "0")
#
	obj.addOscillator(name = "motorosc00",
		pos = "0,0,8",
		rot= "0,0,0",
		type="Sine",
		invert= "True",
		frequency_range = "5,10,1",
		frequency_value = freq,
		amplitude_range = "0,45,1",
		amplitude_value = "30",
		offset_range= "-200,200,1",
		offset_value = "0",
		phase_range = "0,10,1",
		phase_value = "0")
#
	obj.addOscillator(name = "vertebraeosc00",
		pos = "0,0,8",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "5,10,1",
		frequency_value = freq,
		amplitude_range = "0,45,1",
		amplitude_value = "5",
		offset_range= "-200,200,1",
		offset_value = "0",
		phase_range = "0,10,1",
		phase_value = "0")
#
	obj.addOscillator(name = "vertebraeosc01",
		pos = "0,0,8",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "5,10,1",
		frequency_value = freq,
		amplitude_range = "0,45,1",
		amplitude_value = "-5",
		offset_range= "-200,200,1",
		offset_value = "0",
		phase_range = "0,10,1",
		phase_value = "0")
#
	obj.consume("vertebraemotor00","angle","motorosc00","sample")
	obj.consume("vertebraemotor01","angle","motorosc00","sample")
#
	obj.consume("vertebrae03","angle","vertebraeosc00","sample")
	obj.consume("vertebrae01","angle","vertebraeosc01","sample")
#
	obj.consume("rightarmextensor","position","piston_osc0","sample")
	obj.consume("leftlegextensor","position","piston_osc0","sample")
	obj.consume("leftarmextensor","position","piston_osc1","sample")
	obj.consume("rightlegextensor","position","piston_osc1","sample")
	
	obj.consume("righthipmotor","angle","righthiposc","sample")
	obj.consume("rightshouldermotor","angle","rightshoulderosc","sample")
	obj.consume("leftshouldermotor","angle","leftshoulderosc","sample")
	obj.consume("lefthipmotor","angle","lefthiposc","sample")
	
	obj.consume("rightelbow","angle","osc4","sample")
	obj.consume("leftelbow","angle","osc5","sample")
	obj.consume("rightknee","angle","osc5","sample")
	obj.consume("leftknee","angle","osc4","sample")
	
	obj.consume("rightfrontankle","angle","osc4","sample")
	obj.consume("leftbackankle","angle","osc4","sample")
	obj.consume("leftfrontankle","angle","osc5","sample")
	obj.consume("rightbackankle","angle","osc5","sample")
#
	#obj.addBody_gen03_4("back_shield","0,1,-4","0,0,-90")
	#obj.extend("back_shield","0","vertebrae00","1")
#
	#obj.addBody_gen03_4("core_shield","0,1,4","0,0,-90")
	#obj.extend("core_shield","0","core00","1")
#
	#obj.addBody_gen03_3("front_shield","0,1,14","90,180,0")
	#obj.extend("front_shield","0","vertebrae02","2")