import library

with open('../../Resources/Bots/saucerbase.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("core01","0,0,0","0,0,0")
	obj.addContinuum("cont01","6,0,0","0,0,0")
	obj.addContinuum("cont02","4,0,4","0,45,0")
	obj.addContinuum("cont03","0,0,6","0,0,0")
	obj.addContinuum("cont04","-4,0,4","0,45,0")
	obj.addContinuum("cont05","-6,0,0","0,0,0")
	obj.addContinuum("cont06","-4,0,-4","0,45,0")
	obj.addContinuum("cont07","0,0,-6","0,0,0")
	obj.addContinuum("cont08","4,0,-4","0,45,0")

	obj.extend("core01","2","cont03","5")
	obj.extend("core01","3","cont05","0")
	obj.extend("core01","5","cont07","2")
	obj.extend("core01","0","cont01","3")

	obj.extend("cont01","2","cont02","0")
	obj.extend("cont02","3","cont03","0")

	obj.extend("cont07","3","cont06","0")
	obj.extend("cont05","5","cont06","3")

	obj.extend("cont04","2","cont03","3")
	obj.extend("cont04","5","cont05","2")

	obj.extend("cont08","2","cont01","5")
	obj.extend("cont08","5","cont07","0")

	obj.addJet("toprightjet","4,0,7","0,45,-90")
	obj.addJet("bottomrightjet","4,0,-7","0,135,90")
	obj.addJet("topleftjet","-4,0,7","0,135,270")
	obj.addJet("bottomleftjet","-4,0,-7","0,45,90")

	obj.extend("toprightjet","1","cont02","2")
	obj.extend("topleftjet","0","cont04","3")
	obj.extend("bottomrightjet","1","cont08","0")
	obj.extend("bottomleftjet","0","cont06","5")
