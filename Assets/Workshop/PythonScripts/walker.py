import library

with open('walker.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("core00","0,0,0","0,0,0")
	obj.addMotor("mt02","0,2,0","0,0,0")
	obj.extend("mt02","1","core00","1")
	
	obj.addContinuum("top00","0,4,0","0,0,0")
	obj.extend("top00","4","mt02","0")

	obj.addContinuum("top01","2,4,0","0,0,0")
	obj.addContinuum("top02","-2,4,0","0,0,0")
	obj.extend("top01","3","top00","0")
	obj.extend("top02","0","top00","3")

	obj.addMotor("mt00","4,4,0","0,0,-90")
	obj.extend("mt00","1","top01","0")

	obj.addMotor("mt01","-4,4,0","0,0,90")
	obj.extend("mt01","1","top02","3")

	obj.addPistol("lgun","6,4,1","0,0,-90")
	obj.extend("lgun","1","mt00","0")

	obj.addPistol("rgun","-6,4,1","0,0,90")
	obj.extend("rgun","1","mt01","0")

	obj.addContinuum("base00","0,-2,0","0,0,0")
	obj.extend("base00","1","core00","4")
	obj.addContinuum("base01","2,-2,0","0,0,0")
	obj.extend("base01","3","base00","0")
	obj.addContinuum("base02","-2,-2,0","0,0,0")
	obj.extend("base02","0","base00","3")
	
	obj.addContinuum("base03","2,-4,0","0,0,0")
	obj.extend("base03","1","base01","4")
	obj.addContinuum("base04","-2,-4,0","0,0,0")
	obj.extend("base04","1","base02","4")


	obj.addContinuum("wheelbase00","2,-4,8","0,0,0")
	obj.extend("wheelbase00","5","base03","2")
	obj.addContinuum("wheelbase01","2,-4,-8","0,0,0")
	obj.extend("wheelbase01","2","base03","5")
	obj.addContinuum("wheelbase02","-2,-4,8","0,0,0")
	obj.extend("wheelbase02","5","base04","2")
	obj.addContinuum("wheelbase03","-2,-4,-8","0,0,0")
	obj.extend("wheelbase03","2","base04","5")

	obj.addMotor("wheelmotor02","4,-4,0","0,0,-90")
	obj.extend("wheelmotor02","1","base03","0")
	obj.addMotor("wheelmotor00","4,-4,8","0,0,-90")
	obj.extend("wheelmotor00","1","wheelbase00","0")
	obj.addMotor("wheelmotor01","4,-4,-8","0,0,-90")
	obj.extend("wheelmotor01","1","wheelbase01","0")

	obj.addMotor("wheelmotor05","-4,-4,0","0,0,90")
	obj.extend("wheelmotor05","1","base04","3")
	obj.addMotor("wheelmotor03","-4,-4,8","0,0,90")
	obj.extend("wheelmotor03","1","wheelbase02","3")
	obj.addMotor("wheelmotor04","-4,-4,-8","0,0,90")
	obj.extend("wheelmotor04","1","wheelbase03","3")

	obj.addWideWheel("wheel00","7,-4,8","0,-90,0")
	obj.extend("wheel00","0","wheelmotor00","0")
	obj.addWideWheel("wheel01","7,-4,-8","0,-90,0")
	obj.extend("wheel01","0","wheelmotor01","0")
	obj.addWideWheel("wheel02","7,-4,0","0,-90,0")
	obj.extend("wheel02","0","wheelmotor02","0")

	obj.addWideWheel("wheel03","-7,-4,8","0,90,0")
	obj.extend("wheel03","0","wheelmotor03","0")
	obj.addWideWheel("wheel04","-7,-4,-8","0,90,0")
	obj.extend("wheel04","0","wheelmotor04","0")
	obj.addWideWheel("wheel05","-7,-4,0","0,90,0")
	obj.extend("wheel05","0","wheelmotor05","0")

	obj.extend("wheelbase00","3","wheelbase02","0")
	obj.extend("wheelbase01","3","wheelbase03","0")
	
	obj.addOscillator(name = "osc1",
		pos = "0,0,0",
		rot= "0,0,0",
		type="Sawtooth",
		invert= "False",
		frequency_range = "5,10,1",
		frequency_value = "1",
		amplitude_range = "0,360,1",
		amplitude_value = "360",
		offset_range= "-200,200,1",
		offset_value = "0",
		phase_range = "0,10,1",
		phase_value = "0")

	obj.addOscillator(name = "osc2",
		pos = "0,0,0",
		rot= "0,0,0",
		type="Sawtooth",
		invert= "True",
		frequency_range = "5,10,1",
		frequency_value = "1",
		amplitude_range = "0,360,1",
		amplitude_value = "360",
		offset_range= "-200,200,1",
		offset_value = "0",
		phase_range = "0,10,1",
		phase_value = "0")

	#obj.addMidiInput("midi001",controls=[["up_map","1"],["down_map","2"],["left_map","3"],["right_map","4"],["hip_map","6"],["fire_map","5"]])
	#obj.addMidiMap("up_map",map="0,1,1",control="1")
	#obj.addMidiMap("down_map",map="0,1,2",control="2")
	#obj.addMidiMap("left_map",map="0,1,3",control="3")
	#obj.addMidiMap("right_map",map="0,1,4",control="4")
	#obj.addMidiMap("fire_map",map="0,1,5",control="5")
	#obj.addMidiMap("hip_map",map="-180,180,6",control="6")
#
	#obj.consume("up_map","input","midi001","up_map")
	#obj.consume("down_map","input","midi001","down_map")
	#obj.consume("left_map","input","midi001","left_map")
	#obj.consume("right_map","input","midi001","right_map")
	#obj.consume("hip_map","input","midi001","hip_map")
	#obj.consume("fire_map","input","midi001","fire_map")


	obj.addKeyBoardControl()
	obj.consume("wheelmotor00","velocity","km01","left_control")
	obj.consume("wheelmotor01","velocity","km01","left_control")
	obj.consume("wheelmotor02","velocity","km01","left_control")
	obj.consume("wheelmotor03","velocity","km01","right_control")
	obj.consume("wheelmotor04","velocity","km01","right_control")
	obj.consume("wheelmotor05","velocity","km01","right_control")

	#obj.consume("wheelmotor00","velocity","left_map","sample")
	#obj.consume("wheelmotor01","velocity","left_map","sample")
	#obj.consume("wheelmotor02","velocity","left_map","sample")
	#obj.consume("wheelmotor03","velocity","right_map","sample")
	#obj.consume("wheelmotor04","velocity","right_map","sample")
	#obj.consume("wheelmotor05","velocity","right_map","sample")
	
	obj.consume("mt02","angle","hip_map","sample")
	obj.consume("lgun","shoot","fire_map","sample")
	obj.consume("rgun","shoot","fire_map","sample")


	obj.addBody_gen03_4("front_shield",pos = "0,4,1",rot="-90,90,0")
	obj.extend("front_shield","0","top00","2")


	obj.addBody_gen03_3("back_shield",pos = "0,4,-1",rot="270,180,0")
	obj.extend("back_shield","0","top00","5")


	obj.addBody_gen03_6("front_right_shield",pos = "2,-4,9",rot="0,0,0")
	obj.extend("front_right_shield","0","wheelbase00","2")


	obj.addBody_gen03_6("front_left_shield",pos = "-2,-4,9",rot="0,0,0")
	obj.extend("front_left_shield","0","wheelbase02","2")




	obj.addBody_gen03_2("inner_right_shield",pos = "2,-3,9",rot="-90,90,0")
	obj.extend("inner_right_shield","0","wheelbase00","1")


	obj.addBody_gen03_2("inner_left_shield",pos = "-2,-3,9",rot="-90,90,0")
	obj.extend("inner_left_shield","0","wheelbase02","1")