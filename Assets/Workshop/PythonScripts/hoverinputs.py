import library

with open('../../Resources/Bots/hoverinputs.txt', 'w') as f:
    obj = library.Workshop(f)

    obj.addInputSampler("forwardpushinput",{"positive" : "W"}, "5")

    obj.addSampleMapper("jetforwardmap","0","500","0","1")

    obj.addSampleMapper("finreactionmap","0","90","0","1")

    obj.consume("jetforwardmap", "sample","forwardpushinput", "sample")
    obj.consume("finreactionmap", "sample","forwardpushinput", "sample")

    obj.addInputSampler("sidemotioninput",{"positive" : "D", "negative" : "A"}, "1")

    obj.addSampleMapper("topmotormap","0","90","-1","1")

    obj.consume("topmotormap","sample","sidemotioninput","sample")

    obj.addInputSampler("landinggearinput",{"positive" : "Z", "negative" : "X"}, "1")
    obj.addSampleMapper("landinggermap","0","90","0","1")
    obj.consume("landinggermap","sample","landinggearinput","sample")


    