import library

with open('../../BotDefinitions/lilfish7.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addContinuum("continuum001","0,0,0","0,0,0")
	obj.addElbow("right_fin","0,0,2","0,90,0",initial_values=[["angle","0"]])
	obj.addFin("fin001","0,0,4","90,270,0")
	obj.addFin("fin002","0,2,0","0,0,90")

	obj.extend("right_fin","0","continuum001","2")
	obj.extend("fin001","0","right_fin","1")
	obj.extend("fin002","0","continuum001","1")

	obj.addOscillator(name = "jaw_osc",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "2",
		amplitude_range = "0,10,1",
		amplitude_value = "30",
		offset_range= "-1500,1500,1",
		offset_value = "0",
		phase_range = "0,10,0.1")

	obj.consume("right_fin","angle","jaw_osc","sample")



