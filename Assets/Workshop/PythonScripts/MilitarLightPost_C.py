import library

with open('../militarLightPostC.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("master","0,0,0","0,0,0")
	obj.addContinuum("left_box","-2,0,0","0,0,0")
	obj.addContinuum("right_box","2,0,0","0,0,0")

	obj.extend("right_box","3","master","0")
	obj.extend("left_box","0","master","3")

	obj.addMotor("right_motor","2,2,0","0,0,0",initial_values = [["angle","40"]])
	obj.extend("right_motor","1","right_box","1")

	obj.addMotor("left_motor","-2,2,0","0,0,0",initial_values = [["angle","-40"]])
	obj.extend("left_motor","1","left_box","1")

	obj.addElbow("right_elbow","2,4,0","0,0,90",initial_values = [["angle","40"]])
	obj.addElbow("left_elbow","-2,4,0","0,0,90",initial_values = [["angle","40"]])

	obj.extend("right_elbow","1","right_motor","0")

	obj.extend("left_elbow","1","left_motor","0")

	obj.addContinuum("left_upper_box","-2,6,0","0,0,0")
	obj.addContinuum("right_upper_box","2,6,0","0,0,0")

	obj.extend("left_upper_box","4","left_elbow","0")
	obj.extend("right_upper_box","4","right_elbow","0")


	obj.addSpotLight("left_light","-2,6,2","90,0,0")
	obj.addSpotLight("right_light","2,6,2","90,0,0")

	obj.extend("left_light","0","left_upper_box","2")
	obj.extend("right_light","0","right_upper_box","2")	



	obj.addCSColumn("column001","0,-15,0","0,0,0")
	obj.extend("column001","0","master","4")	

	obj.addCSColumn("column002","0,-29,0","0,0,0")
	obj.extend("column002","0","column001","1")

	obj.addBody_gen02_3("front_shield","0,0,3","-90,0,0")
	obj.extend("front_shield","0","master","2")
	obj.addBody_gen02_3("back_shield","0,0.5,-3","-90,180,0")
	obj.extend("back_shield","0","master","5")




#	obj.addBody_gen03_1("left_lamp_shield","-2,7,0","0,0,180")
#	obj.extend("right_upper_box","1","right_lamp_shield","0")
#	obj.addBody_gen03_1("right_lamp_shield","2,7,0","0,0,180")
#	obj.extend("left_upper_box","1","left_lamp_shield","0")


	obj.addHook("hook01","0,-31,0","0,0,0")
	obj.extend("hook01","0","column002","1")	