import library

with open('../MidiBehavior.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("core00","0,0,0","0,0,0")
	obj.addMotor("hip_motor","0,2,0","0,0,0")
	obj.addContinuum("chest00","0,4,0","0,0,0")
	obj.addContinuum("chest01","0,6,0","0,0,0")

	obj.extend("hip_motor","1","core00","1")
	obj.extend("chest00","4","hip_motor","0")
	obj.extend("chest01","4","chest00","1")

	obj.addMotor("neck_motor","0,8,0","0,0,0")
	obj.addSensor("head","0,10,0","0,0,0")

	obj.extend("neck_motor","1","chest01","1")
	obj.extend("head","0","neck_motor","0")

	obj.addContinuum("right_clavicle","2,6,0","0,0,0")
	obj.addMotor("right_clavicle_motor","4,6,0","0,0,270")
	obj.addElbow("right_shoulder","6,6,0","90,180,0",initial_values=[["angle","90"]])
	obj.addContinuum("right_arm","8,6,0","0,0,0")
	obj.addMotor("right_arm_motor","10,6,0","0,0,270")
	obj.addElbow("right_elbow","12,6,0","0,0,180",initial_values=[["angle","45"]])
	obj.addContinuum("right_wrist","14,6,0","0,0,0")
	obj.addFist("right_fist","17,6,0","0,90,180")

	obj.extend("right_clavicle","3","chest01","0")
	obj.extend("right_clavicle_motor","1","right_clavicle","0")
	obj.extend("right_shoulder","0","right_clavicle_motor","0")
	obj.extend("right_arm","3","right_shoulder","1")
	obj.extend("right_arm_motor","1","right_arm","0")
	obj.extend("right_elbow","0","right_arm_motor","0")
	obj.extend("right_wrist","3","right_elbow","1")
	obj.extend("right_fist","0","right_wrist","0")

	obj.addContinuum("left_clavicle","-2,6,0","0,0,0")
	obj.addMotor("left_clavicle_motor","-4,6,0","0,0,90")
	obj.addElbow("left_shoulder","-6,6,0","90,0,0",initial_values=[["angle","90"]])
	obj.addContinuum("left_arm","-8,6,0","0,0,0")
	obj.addMotor("left_arm_motor","-10,6,0","0,0,90")
	obj.addElbow("left_elbow","-12,6,0","0,0,0",initial_values=[["angle","45"]])
	obj.addContinuum("left_wrist","-14,6,0","0,0,0")
	obj.addFist("left_fist","-17,6,0","0,270,180")

	obj.extend("left_clavicle","0","chest01","3")
	obj.extend("left_clavicle_motor","1","left_clavicle","3")
	obj.extend("left_shoulder","0","left_clavicle_motor","0")
	obj.extend("left_arm","0","left_shoulder","1")
	obj.extend("left_arm_motor","1","left_arm","3")
	obj.extend("left_elbow","0","left_arm_motor","0")
	obj.extend("left_wrist","0","left_elbow","1")
	obj.extend("left_fist","0","left_wrist","3")

	obj.addMotor("right_hip_motor","2,0,0","0,0,270",initial_values=[["angle","0"]])
	obj.addContinuum("right_hip","4,0,0","0,0,0")
	obj.addElbow("right_thigh","4,-2,0","0,0,90")
	obj.addContinuum("right_knee","4,-4,0","0,0,0")
	obj.addElbow("right_leg","4,-6,0","0,0,90")
	obj.addContinuum("right_ankle","4,-8,0","0,0,0")
	obj.addFoot("right_foot","4,-10,0","0,0,0")

	obj.extend("right_hip_motor","1","core00","0")
	obj.extend("right_hip","3","right_hip_motor","0")
	obj.extend("right_thigh","0","right_hip","4")
	obj.extend("right_knee","1","right_thigh","1")
	obj.extend("right_leg","0","right_knee","4")
	obj.extend("right_ankle","1","right_leg","1")
	obj.extend("right_foot","0","right_ankle","4")

	obj.addMotor("left_hip_motor","-2,0,0","0,0,90",initial_values=[["angle","0"]])
	obj.addContinuum("left_hip","-4,0,0","0,0,0")
	obj.addElbow("left_thigh","-4,-2,0","0,0,90")
	obj.addContinuum("left_knee","-4,-4,0","0,0,0")
	obj.addElbow("left_leg","-4,-6,0","0,0,90")
	obj.addContinuum("left_ankle","-4,-8,0","0,0,0")
	obj.addFoot("left_foot","-4,-10,0","0,0,0")

	obj.extend("left_hip_motor","1","core00","3")
	obj.extend("left_hip","0","left_hip_motor","0")
	obj.extend("left_thigh","0","left_hip","4")
	obj.extend("left_knee","1","left_thigh","1")
	obj.extend("left_leg","0","left_knee","4")
	obj.extend("left_ankle","1","left_leg","1")
	obj.extend("left_foot","0","left_ankle","4")

	obj.addBody_gen03_6("plate00","8,6,1","0,0,90",setColors=True,initial_values=[["_channel_a","#FFB619"],["_channel_b","#2E4130"],["_channel_c","#0E1528"]])
	obj.addBody_gen03_6("plate01","-8,6,1","0,0,270",setColors=True,initial_values=[["_channel_a","#FFB619"],["_channel_b","#2E4130"],["_channel_c","#0E1528"]])
	obj.addBody_gen03_6("plate02","4,-4,1","0,0,0",setColors=True,initial_values=[["_channel_a","#FFB619"],["_channel_b","#2E4130"],["_channel_c","#0E1528"]])
	obj.addBody_gen03_6("plate03","-4,-4,1","0,0,0",setColors=True,initial_values=[["_channel_a","#FFB619"],["_channel_b","#2E4130"],["_channel_c","#0E1528"]])
	obj.addBody_gen03_6("plate04","4,-8,1","0,0,0",setColors=True,initial_values=[["_channel_a","#FFB619"],["_channel_b","#2E4130"],["_channel_c","#0E1528"]])
	obj.addBody_gen03_6("plate05","-4,-8,1","0,0,0",setColors=True,initial_values=[["_channel_a","#FFB619"],["_channel_b","#2E4130"],["_channel_c","#0E1528"]])
	obj.addBody_gen03_2("plate06","0,4,1","0,0,270",setColors=True,initial_values=[["_channel_a","#FFB619"],["_channel_b","#2E4130"],["_channel_c","#0E1528"]])
	obj.addBody_gen03_1("plate07","0,0,-1","0,270,270",setColors=True,initial_values=[["_channel_a","#FFB619"],["_channel_b","#2E4130"],["_channel_c","#0E1528"]])
	obj.addBody_gen04_1("plate08","0,6,-1","0,0,0",setColors=True,initial_values=[["_channel_a","#FFB619"],["_channel_b","#2E4130"],["_channel_c","#0E1528"]])

	obj.extend("plate00","0","right_arm","2")
	obj.extend("plate01","0","left_arm","2")
	obj.extend("plate02","0","right_knee","2")
	obj.extend("plate03","0","left_knee","2")
	obj.extend("plate04","0","right_ankle","2")
	obj.extend("plate05","0","left_ankle","2")
	obj.extend("plate06","0","chest00","2")
	obj.extend("plate07","0","core00","5")
	obj.extend("plate08","0","chest01","5")



	obj.addMidiInput("lizardmidi",controls = [["sample_a","72"],["sample_b","8"],["sample_c","74"],["sample_d","71"],["sample_e","20"],["sample_f","22"],["sample_g","86"],["sample_h","73"]]);

	obj.addMidiMap("sample_a",map="-90,90,72",control="72")
	obj.addMidiMap("sample_b",map="-90,90,8",control="8")
	obj.addMidiMap("sample_c",map="-90,90,74",control="74")
	obj.addMidiMap("sample_d",map="-90,90,71",control="71")
	obj.addMidiMap("sample_e",map="-90,90,20",control="20")
	obj.addMidiMap("sample_f",map="-90,90,22",control="22")
	obj.addMidiMap("sample_g",map="-90,90,86",control="86")
	obj.addMidiMap("sample_h",map="-90,90,73",control="73")



	obj.consume("sample_a","input","lizardmidi","sample_a")
	obj.consume("sample_b","input","lizardmidi","sample_b")
	obj.consume("sample_c","input","lizardmidi","sample_c")
	obj.consume("sample_d","input","lizardmidi","sample_d")
	obj.consume("sample_e","input","lizardmidi","sample_e")
	obj.consume("sample_f","input","lizardmidi","sample_f")
	obj.consume("sample_g","input","lizardmidi","sample_g")
	obj.consume("sample_h","input","lizardmidi","sample_h")



	#Timeline for Elbows


	obj.consume("right_shoulder","angle","sample_a","sample")


	obj.consume("right_elbow","angle","sample_b","sample")


	obj.consume("left_shoulder","angle","sample_c","sample")


	obj.consume("left_elbow","angle","sample_d","sample")


	obj.consume("right_thigh","angle","sample_e","sample")


	obj.consume("right_leg","angle","sample_f","sample")


	obj.consume("left_thigh","angle","sample_g","sample")


	obj.consume("left_leg","angle","sample_h","sample")