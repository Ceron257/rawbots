import library

with open('../extractor.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("master","0,11,0","0,0,0")
	obj.addExtractor("extractor001","0,0,0","0,0,0")
	obj.extend("extractor001","0","master","4")	

	obj.addSwitch("switch001","0,13,0","0,0,90")
	obj.extend("master","1", "switch001","1")
	obj.consume("extractor001","trigger","switch001","trigger")
