using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ConnectPartsMode : MonoBehaviour
{


    private BulletLayers partMask;
    public GameCamera gameCamera;
    Slot lastTargetSlot = null;
    bool click = false;
    private Part firstPart;
    private Part secondPart;
    BuilderState currentState = BuilderState.None;
    private GhostPart ghostPart;
    Builder.SlotsMarkerWrapper firstPartSlots = new Builder.SlotsMarkerWrapper();
    Builder.SlotsMarkerWrapper secondPartSlots = new Builder.SlotsMarkerWrapper();
    private TargetPosPID currentPosPid;
    private Part posiblePart;
    private float angle = 0;
    public GameObject soundSuccessAsset;
    public GameObject soundFailedAsset;
    public AudioClip soundStartMove;
    private List<Part> parts = new List<Part>();
    private List<Part> partsToClean = new List<Part>();
    public List<Builder.TemporalRelationShip> temporal = new List<Builder.TemporalRelationShip>();
    BulletLayers partLayers = (BulletLayers.Part | BulletLayers.World | BulletLayers.Structure | BulletLayers.Water | BulletLayers.Sensor | BulletLayers.HexBlock);

    public enum BuilderState
    {
        None,
        OnePart,
        TwoParts,
        Connecting
    }

    void Awake()
    {
        var mode = new GameModeManager.Mode()
        {
            id = GameModeManager.ModeId.PartCloseConnect,
            transitions = new List<GameModeManager.Transition>(){
                new GameModeManager.Transition(){ key = KeyCode.Alpha1 , state = GameModeManager.KeyState.KeyUp , mode = GameModeManager.ModeId.World},
            }
        };
        GameModeManager.RegisterMode(mode);
    }

    // Use this for initialization
    void Start()
    {
        partMask = BulletLayers.Part;
    }

    // Update is called once per frame
    void Update()
    {
        ConnectPartsMachine();
    }

    void ConnectPartsMachine()
    {

        var builderActive = GameModeManager.InMode(m => m == GameModeManager.ModeId.PartCloseConnect);

        BulletRaycastHitDetails partHit;

        var ray = gameCamera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        var clickUp = Input.GetMouseButtonDown(0);
        click = Input.GetMouseButton(0);

        var partHitStatus = Bullet.Raycast(ray, out partHit, 1500, partMask, partMask);

        switch (currentState)
        {
            case BuilderState.None:
                if (builderActive)
                {
                    if (partHitStatus)
                    {
                        var part = partHit.bulletRigidbody.GetComponent<Part>();
                        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
                        if (subPart != null)
                        {
                            part = subPart.part;
                        }


                        HighlightElegiblePartFirstMode(part, Highlight.HighlightType.Normal);
                    }
                    else
                    {
                        DisableHighlightToElegiblePartFirstMode();
                    }
                    if (partHitStatus && clickUp)
                    {
                        DisableHighlightToElegiblePartFirstMode();

                        var part = partHit.bulletRigidbody.GetComponent<Part>();
                        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
                        if (subPart != null)
                        {
                            part = subPart.part;
                        }

                        var validPart = SelectFirstPart(part);
                        if (validPart)
                        {
                            Builder.SetHighLightToPartAndSubpart(firstPart.gameObject, true, Highlight.HighlightType.Blue);
                            Builder.instance.SwitchSlot(0, firstPartSlots);

                            Sec.ForPermission(firstPart.proxy.entity, Sec.Permission.connect, () => {

                                currentState = BuilderState.OnePart;
                            }, () => {
                                Console.PushError("", new string[] { "access denied - check permissions" });
                                firstPart.isProtected = false;
                                currentState = BuilderState.None;
                                Builder.SetHighLightToPartAndSubpart(firstPart.gameObject, false);
                                firstPart.proxy.RemoveUnUsedSlots();
                                Builder.CleanSlotsMarker(firstPartSlots.slots);
                                Destroy(ghostPart.gameObject);
                            });
                            //Debug.Break();
                        }
                    }
                }
                else
                {
                    DisableHighlightToElegiblePartFirstMode();
                }

                break;
            case BuilderState.OnePart:
                if (builderActive)
                {

                    if (partHitStatus)
                    {
                        var part = partHit.bulletRigidbody.GetComponent<Part>();
                        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
                        if (subPart != null)
                        {
                            part = subPart.part;
                        }

                        if (part != firstPart)
                        {
                            HighlightElegiblePartFirstMode(part, Highlight.HighlightType.Yellow);
                        }
                        else
                        {
                            DisableHighlightToElegiblePartFirstMode();
                        }
                        SwitchSlotFirstPart(part);
                    }
                    else
                    {
                        DisableHighlightToElegiblePartFirstMode();
                    }




                    if (click && partHitStatus && clickUp)
                    {
                        DisableHighlightToElegiblePartFirstMode();
                        var part = partHit.bulletRigidbody.GetComponent<Part>();
                        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
                        if (subPart != null)
                        {
                            part = subPart.part;
                        }

                        var validPart = SelectSecondPart(part, true);

                        if (validPart)
                        {
                            //Builder.SetHighLightToPartAndSubpart( secondPart.gameObject, true, Highlight.HighlightType.Green );1
                            Sec.ForPermission(secondPart.proxy.entity, Sec.Permission.connect, () => {
                                PlaceGhostFirstTime();
                                currentState = BuilderState.TwoParts;
                            }, () => {
                                Console.PushError("", new string[] { "access denied - check permissions" });
                                firstPart.isProtected = false;
                                secondPart.isProtected = false;
                                currentState = BuilderState.None;
                                Builder.SetHighLightToPartAndSubpart(firstPart.gameObject, false);
                                Builder.SetHighLightToPartAndSubpart(secondPart.gameObject, false);
                                firstPart.proxy.RemoveUnUsedSlots();
                                secondPart.proxy.RemoveUnUsedSlots();
                                Builder.CleanSlotsMarker(firstPartSlots.slots);
                                Destroy(ghostPart.gameObject);
                            });

                        }
                    }
                }
                else
                {
                    firstPart.isProtected = false;
                    currentState = BuilderState.None;
                    Builder.SetHighLightToPartAndSubpart(firstPart.gameObject, false);
                    Destroy(ghostPart.gameObject);
                    firstPart.proxy.RemoveUnUsedSlots();
                    Builder.CleanSlotsMarker(firstPartSlots.slots);
                }
                break;
            case BuilderState.TwoParts:
                if (builderActive)
                {

                    if (partHitStatus)
                    {
                        var part = partHit.bulletRigidbody.GetComponent<Part>();
                        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
                        if (subPart != null)
                        {
                            part = subPart.part;
                        }
                        //part.proxy.SetSlots();
                        if (part == secondPart)
                        {
                            var slot = part.GetClosestFreeSlot(partHit.point);
                            if (part == secondPart)
                            {
                                PlaceGhost(slot);

                            }
                        }

                    }
                    if (lastTargetSlot != default(Slot) && secondPart != null)
                    {

                        RotateGhost();
                        PlaceGhost(lastTargetSlot);

                    }



                    if (click && clickUp && secondPart != null)
                    {
                        currentState = BuilderState.Connecting;
                        SetPidComponent();
                        Builder.SetHighLightToPartAndSubpart(firstPart.gameObject, false);
                        Builder.SetHighLightToPartAndSubpart(secondPart.gameObject, false);
                        Builder.CleanSlotsMarker(firstPartSlots.slots);
                    }





                    //                    if ( partHitStatus && currentState != BuilderState.Connecting && currentState != BuilderState.None ) {
                    //                        var part = partHit.bulletRigidbody.GetComponent<Part>();
                    //                        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
                    //                        if ( subPart != null ) {
                    //                            part = subPart.part;
                    //                        }
                    //
                    //                        if ( part == firstPart ) {
                    //                            SwitchSlotFirstPart( part );
                    //                        }
                    //                        else if ( click && clickUp ) {
                    //                            if ( secondPart != default(Part) ) {
                    //                                secondPart.isProtected = false;
                    //
                    //                                Builder.CleanSlotsMarker( secondPartSlots.slots );
                    //                                secondPart.proxy.RemoveUnUsedSlots();
                    //                                Builder.SetHighLightToPartAndSubpart( secondPart.gameObject, false );
                    //                                ghostPart.gameObject.SetActive( false );
                    //
                    //                            }
                    //
                    //                            var validPart = SelectSecondPart( part );
                    //
                    //                            if ( validPart ) {
                    //                                PlaceGhostFirstTime();
                    //                                //Debug.Log( "changing part " + Time.time + " "+ currentState );
                    //                                //Builder.SetHighLightToPartAndSubpart( secondPart.gameObject, true, Highlight.HighlightType.Green );
                    //                            
                    //                                Sec.ForPermission( secondPart.proxy.entity, Sec.Permission.connect, () => {
                    //
                    //                                }
                    //                                , () => {
                    //                                    Console.PushError( new string[]{  "access denied - check permissions" } );
                    //                                    firstPart.isProtected = false;
                    //                                    secondPart.isProtected = false;
                    //                                    currentState = BuilderState.None;
                    //                                    Builder.SetHighLightToPartAndSubpart( firstPart.gameObject, false );
                    //                                    Builder.SetHighLightToPartAndSubpart( secondPart.gameObject, false );
                    //
                    //                                    firstPart.proxy.RemoveUnUsedSlots();
                    //                                    secondPart.proxy.RemoveUnUsedSlots();
                    //                                    Builder.CleanSlotsMarker( firstPartSlots.slots );
                    //                                    Destroy( ghostPart.gameObject );
                    //                                } );
                    //                            }
                    //                            else {
                    //                                currentState = BuilderState.OnePart;
                    //                            }
                    //
                    //                        }
                    //                    }
                }
                else
                {
                    currentState = BuilderState.None;
                    firstPart.isProtected = false;
                    secondPart.isProtected = false;
                    Builder.SetHighLightToPartAndSubpart(firstPart.gameObject, false);
                    Builder.SetHighLightToPartAndSubpart(secondPart.gameObject, false);

                    firstPart.proxy.RemoveUnUsedSlots();
                    secondPart.proxy.RemoveUnUsedSlots();
                    Builder.CleanSlotsMarker(firstPartSlots.slots);
                    Destroy(ghostPart.gameObject);
                }
                break;
            case BuilderState.Connecting:
                if (builderActive)
                {

                }
                else
                {
                    currentPosPid.breakPid = true;
                    currentState = BuilderState.None;

                    firstPart.proxy.RemoveUnUsedSlots();
                    secondPart.proxy.RemoveUnUsedSlots();
                    firstPart.isProtected = false;
                    secondPart.isProtected = false;

                }

                break;
            default:
                break;

        }
    }

    public void HighlightElegiblePartFirstMode(Part part, Highlight.HighlightType type)
    {

        if (part == posiblePart)
        {
            return;
        }
        if (posiblePart != null)
        {
            Builder.SetHighLightToPartAndSubpart(posiblePart.gameObject, false);
        }
        Builder.SetHighLightToPartAndSubpart(part.gameObject, true, type);
        posiblePart = part;
    }

    public void DisableHighlightToElegiblePartFirstMode()
    {
        if (posiblePart != null)
        {
            Builder.SetHighLightToPartAndSubpart(posiblePart.gameObject, false);
            posiblePart = default(Part);
        }
    }

    public bool SelectFirstPart(Part part)
    {

        firstPart = part;
        firstPart.proxy.SetSlots();
        firstPartSlots.slotIndex = 0;
        firstPartSlots.slots = firstPart.proxy.GetAvailableSlots().Select(p => p.nodeView).ToList();

        if (firstPartSlots.slots.Count > 0)
        {
            //ghostPiece = firstPart.proxy.CreateGhostPiece();
            CreateGhostTree(firstPart);


            ghostPart.gameObject.SetActive(false);
            angle = 0;
            firstPart.isProtected = true;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void SwitchSlotFirstPart(Part part)
    {

        if (part != firstPart)
        {
            return;
        }

        var leftClick = Input.GetMouseButtonUp(1);

        if (leftClick)
        {
            Builder.instance.SwitchSlot(1, firstPartSlots);
        }
    }

    private void SetGhostPosition(Slot targetSlot, GameObject targetPartView)
    {
        var dummyObject = new GameObject("Dummy");

        var firstSlot = firstPartSlots.slots[firstPartSlots.slotIndex];
        var secondSlot = secondPartSlots.slots[secondPartSlots.slotIndex];
        ghostPart.transform.position = firstPart.gameObject.transform.position;
        ghostPart.transform.rotation = firstPart.gameObject.transform.rotation;


        var slotInitialPosition = Vector3.zero;
        var slotInitialRotation = Quaternion.identity;

        firstSlot.proxy.entity.ForInferredProperty<string>("position", Util.AsIs, position => {
            slotInitialPosition = Util.PropertyToVector3(position);
        });

        firstSlot.proxy.entity.ForInferredProperty<string>("rotation", Util.AsIs, rotation => {
            slotInitialRotation = Quaternion.Euler(Util.PropertyToVector3(rotation));
        });

        dummyObject.transform.position = firstPart.gameObject.transform.TransformPoint(slotInitialPosition);
        dummyObject.transform.rotation = firstPart.gameObject.transform.rotation * slotInitialRotation;



        ghostPart.transform.parent = dummyObject.transform;
        dummyObject.transform.parent = targetSlot.transform;
        dummyObject.transform.localPosition = Vector3.zero;
        dummyObject.transform.localRotation = Quaternion.identity;


        //ghostPart.transform.parent = firstPart.gameObject.transform.parent;
        ghostPart.transform.parent = secondSlot.transform;// targetPartView.gameObject.transform;




        var targetSlotPos = targetSlot.transform.position;
        var targetSlotUp = targetSlot.transform.up;
        ghostPart.transform.RotateAround(targetSlotPos, targetSlotUp, 180);

        lastTargetSlot = targetSlot;
        var axis = lastTargetSlot.transform.forward;
        var anchor = lastTargetSlot.transform.position;
        ghostPart.transform.RotateAround(anchor, axis, angle);
        Destroy(dummyObject);
    }

    private void SetPidComponent()
    {

        var slotOwner = firstPartSlots.slots[firstPartSlots.slotIndex].transform.parent.gameObject;//.parent.GetComponent<DragComponent>();

        var target = ghostPart.partComponent[slotOwner.GetHashCode()];
        var posPid = slotOwner.AddComponent<TargetPosPID>();

        //        Debug.Log(slot.name + Time.time);
        //var slot = slotList.slots[ slotList.slotIndex ];

        currentPosPid = posPid;
        CheckPossibilityOfBadConnexion(secondPart); //teleportation allows exploits, so if needed this toggles the old force connect as a fallback -z26

        firstPart.GetComponent<BulletRigidBody>().ChangeMaskAndGroup(BulletLayers.GhostPart, BulletLayers.GhostPart);
        firstPart.GetComponent<PhysicalObject>().multiplier = 0;
        //        firstPart.GetComponent<BulletRigidBody>().SetConstantForce(Vector3.zero);
        firstPart.linkedParts.ForEach(s => {
            s.GetComponent<BulletRigidBody>().ChangeMaskAndGroup(BulletLayers.GhostPart, BulletLayers.GhostPart);
            s.GetComponent<PhysicalObject>().multiplier = 0;
            //            s.GetComponent<BulletRigidBody>().SetConstantForce(Vector3.zero);
        });

        firstPart.proxy.StorePosition();
        secondPart.proxy.StorePosition();

        var selectedPart = firstPart;
        var ownPiece = slotOwner;
        var currentGhost = ghostPart;



        posPid.GoalFunc = () => {


            //I (-z26) added these two for loops to softly move the whole assembly in the right spot.
            //Before, only the selected part was moved and the other were violently dragged into place by the physics joints.

            //the firstPart that the user selected is used as the reference point to move all the others.
            //I store the relative position of each part in a list, then reapply the positions once the reference part has been moved.
            //I use that complex method because it allows me not to worry about how the parts are arranged in the hierarchy.


            var relativePos = new Vector3[parts.Count]; //fill the list.
            for (int i = 0; i < parts.Count; i++) {
                relativePos[i] = ownPiece.transform.InverseTransformPoint(parts[i].transform.position);        
            }

            var rotationBetween = target.transform.rotation * Quaternion.Inverse(ownPiece.transform.rotation); 


            //I nedd to place the selected(first) part at the right spot for the loop below to work.
            ownPiece.GetComponent<BulletRigidBody>().SetPosition(target.transform.position); 

            for (int i = 0; i < parts.Count; i++) {
                parts[i].bulletRigidBody.SetPosition(ownPiece.transform.TransformPoint(relativePos[i]));
                parts[i].bulletRigidBody.SetRotation(rotationBetween * parts[i].transform.rotation);
            } 
            //I originally used "parts[i].transform.rotation * rotationBetween" instead and it took me ages to find the problem -z26
            //The code I added code ends here. -z26


            //firstPart.proxy.ResetMovablePart();

            //            Debug.Log( "did connect ! " + Time.time );
            selectedPart.GetComponent<BulletRigidBody>().ChangeMaskAndGroup(partLayers, BulletLayers.Part);
            firstPart.GetComponent<PhysicalObject>().multiplier = 1;
            selectedPart.linkedParts.ForEach(s => {
                s.GetComponent<BulletRigidBody>().ChangeMaskAndGroup(partLayers, BulletLayers.Part);
                s.GetComponent<PhysicalObject>().multiplier = 1;
            });

            CheckSlots(selectedPart);//This actually creates the connection(s). If the part isnt placed correctly this fails.
            //I think this automatically creates connections between properly aligned neighbours? (like if making a continuum cube)

            selectedPart.GetComponent<BulletRigidBody>().SetVelocity(Vector3.zero);
            selectedPart.GetComponent<BulletRigidBody>().SetAngularVelocity(Vector3.zero);


            firstPart.linkedParts.ForEach(s => {
                s.GetComponent<BulletRigidBody>().SetVelocity(Vector3.zero);
                s.GetComponent<BulletRigidBody>().SetAngularVelocity(Vector3.zero);
            });


            var otherBulletRigidbody = secondPart.GetComponent<BulletRigidBody>();
            otherBulletRigidbody.SetVelocity(Vector3.zero);
            otherBulletRigidbody.SetAngularVelocity(Vector3.zero);

            secondPart.linkedParts.ForEach(s => {
                s.GetComponent<BulletRigidBody>().SetVelocity(Vector3.zero);
                s.GetComponent<BulletRigidBody>().SetAngularVelocity(Vector3.zero);
            });


            posPid.Destroy();
            Destroy(currentGhost.gameObject);
            currentState = BuilderState.None;
            firstPart.isProtected = false;
            secondPart.isProtected = false;

            Instantiate(soundSuccessAsset, selectedPart.transform.position, selectedPart.transform.rotation);
        };
        posPid.FailFunc = () => {
            Destroy(currentGhost.gameObject);
            ownPiece.GetComponent<BulletRigidBody>().SetVelocity(Vector3.zero);
            ownPiece.GetComponent<BulletRigidBody>().SetAngularVelocity(Vector3.zero);
            firstPart.GetComponent<BulletRigidBody>().ChangeMaskAndGroup(partLayers, BulletLayers.Part);
            firstPart.GetComponent<PhysicalObject>().multiplier = 1;
            selectedPart.linkedParts.ForEach(s => {
                s.GetComponent<BulletRigidBody>().ChangeMaskAndGroup(partLayers, BulletLayers.Part);
                s.GetComponent<PhysicalObject>().multiplier = 1;
            });


            posPid.Destroy();
            firstPart.isProtected = false;
            secondPart.isProtected = false;

            Instantiate(soundFailedAsset, selectedPart.transform.position, selectedPart.transform.rotation);
        };



        posPid.Initialize(
                            () => target.transform.position,
                            () => target.transform.rotation
                        );
    }

    public void HideGhost()
    {
        if (ghostPart.gameObject.activeSelf)
        {
            ghostPart.gameObject.SetActive(false);
        }
    }

    public void PlaceGhostFirstTime()
    {
        NodeView slot = secondPartSlots.slots[0];
        var father = slot.proxy.entity.Heads(p => p == "slot_for")
                                                                    .Select(e => e.proxy)
                                                                    .First();

        var targetPartView = father.nodeView.gameObject;
        var targetSlot = slot.gameObject.GetComponent<Slot>();
        if (targetPartView == secondPart.gameObject)
        {
            if (!ghostPart.gameObject.activeSelf)
            {
                ghostPart.gameObject.SetActive(true);
            }
            UnityEngine.Profiling.Profiler.BeginSample("ghost position");
            SetGhostPosition(targetSlot, targetPartView);
            UnityEngine.Profiling.Profiler.EndSample();
        }
    }

    public void PlaceGhost(Slot slot)
    {

        var targetSlotNodeView = slot.gameObject.GetComponent<NodeView>();

        var father = targetSlotNodeView.proxy.entity.Heads(p => p == "slot_for")
                                                                    .Select(e => e.proxy)
                                                                    .First();

        var targetPartView = father.nodeView.gameObject;
        if (targetPartView == secondPart.gameObject)
        {
            if (!ghostPart.gameObject.activeSelf)
            {
                ghostPart.gameObject.SetActive(true);
            }
            UnityEngine.Profiling.Profiler.BeginSample("ghost position");
            SetGhostPosition(slot, targetPartView);
            UnityEngine.Profiling.Profiler.EndSample();
        }
    }

    public bool SelectSecondPart(Part part, bool allowSameBot = false)
    {
        var master = firstPart.proxy.entity.Heads(p => p == "local_to").FirstOrDefault();
        var secondMaster = part.proxy.entity.Heads(p => p == "local_to").FirstOrDefault();
        if (part == firstPart || (master == secondMaster && !allowSameBot))
        {
            return false;
        }
        part.proxy.SetSlots();
        var otherPartSlots = part.proxy.GetAvailableSlots();
        secondPartSlots.slotIndex = 0;
        secondPartSlots.slots = otherPartSlots.Select(p => p.nodeView).ToList();
        if (secondPartSlots.slots.Count > 0)
        {
            secondPart = part;
            secondPart.isProtected = true;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void RotateGhost()
    {

        var mouseWheel = Input.GetAxis("Mouse ScrollWheel");
        var leftClick = Input.GetMouseButtonUp(1);
        var shift = Input.GetKey(KeyCode.LeftShift);

        if (mouseWheel != 0 && shift)
        {
            angle -= mouseWheel * 50;
        }
        else if (mouseWheel != 0)
        {
            angle -= mouseWheel * 150;
        }

        if (leftClick && shift)
        {
            angle += 15f;
        }
        else if (leftClick)
        {
            angle += 45f;
        }

    }

    private void CheckPossibilityOfBadConnexion(Part secondPart) //-z26
    {
        currentPosPid.useOldConnectMode = false;

        for (int i = 0; i < parts.Count; i++)
        {
            //if the first and second parts are from the same assembly, dont teleport part a to part b. Apply a force instead,
            //So that if the assembly is properly articulated the connection can still be made but otherwise the connection fails.
            if(parts[i] == secondPart)
            {
                currentPosPid.useOldConnectMode = true;
            }   

            //If the assembly to teleport includes a currently tethered hook, activate the same fallback.
            if(parts[i].proxy is Operants.HookProxy)
            {
                if(parts[i].GetComponent<Hook>().isActive)
                {
                    currentPosPid.useOldConnectMode = true;
                }
            }
        }
    }

    private void CreateGhostTree(Part part)
    {


        parts.Clear();
        foreach(var proxy in part.proxy.PhysicallyLinked()) {
            //Unlike proxies, Nodeview is attached to the gameobject so you can access other script components from it. -z26
            parts.Add(proxy.nodeView.GetComponent<Part>());
        }

        for (int i = 0; i < parts.Count; i++)
        {

            if (i == 0)
            {
                var cGhost = parts[i].CreateGhostPiece(true);
                ghostPart = cGhost;
            }
            else
            {
                var cGhost = parts[i].CreateGhostPiece(false);
                cGhost.transform.parent = ghostPart.transform;
            }
        }
    }





    public void CheckSlots(Part partToBeConnected)
    {
        var slotsToBeConnected = GetSlotsToBeConnected(partToBeConnected);

        foreach (var slotPair in slotsToBeConnected)
        {
            Builder.SetPartEntityData(slotPair.tail, slotPair.head); //responsible for putting the part in the right spot.
            //if only the foreach below this one is disabled, connecting parts will seemingly do nothing, but after reload
            //the 1st part will be positionned right next to the choosen slot (with the actual joint missing)
            //this line manipulates xforms to do that, but does the different xform hierarchy only updates after a reload?

            //Builder.RemoveCurrentXFormsBySlot(slotPair.tail);
            //Builder.RemoveCurrentXFormsBySlot(slotPair.head);
        }
        foreach (var temp in slotsToBeConnected)
        {
            new Oracle.Relationship(temp.tail, temp.predicate, temp.head); //responsible for actually making the physics joint
        }


        partToBeConnected.proxy.CreateVoltaicArcsAndJoints();
        partsToClean.ForEach(part => part.proxy.RemoveUnUsedSlots());
        partToBeConnected.proxy.RemoveUnUsedSlots();
    }

    private List<Builder.TemporalRelationShip> GetSlotsToBeConnected(Part partToBeConnected)
    {
        var slotsPairs = new List<Builder.TemporalRelationShip>();
        var nearByParts = partToBeConnected.GetNearByParts();
        foreach (var proxy in nearByParts.Select(p => p.proxy))
        {
            proxy.SetSlots();
        }
        partsToClean = nearByParts;
        var partProxy = partToBeConnected.nodeView.proxy as Operants.PartProxy;
        var slots = partProxy.GetAvailableSlots();
        var otherSlots = nearByParts.Select(p => p.nodeView.proxy)
                                    .Cast<Operants.PartProxy>()
                                    .SelectMany(pp => pp.GetAvailableSlots())
                                    .ToList();

        for (var i = 0; i < slots.Count(); i++)
        {
            var slot = slots[i].nodeView;
            for (var j = 0; j < otherSlots.Count; j++)
            {
                var otherSlot = otherSlots[j].nodeView;
                if (Vector3.Distance(otherSlot.transform.position, slot.transform.position) < 0.2f)
                {
                    slotsPairs.Add(new Builder.TemporalRelationShip(slot.proxy.entity, "extends", otherSlot.proxy.entity));
                }
            }
        }
        return slotsPairs;
    }

    private List<Operants.PartProxy> GetPartsToBeConnected(Part partToBeConnected)
    {
        temporal.Clear();
        var partsToBeConnected = new List<Operants.PartProxy>();

        var nearByParts = partToBeConnected.GetNearByParts();
        foreach (var proxy in nearByParts.Select(p => p.proxy))
        {
            proxy.SetSlots();
        }

        var partProxy = partToBeConnected.nodeView.proxy as Operants.PartProxy;
        var slots = partProxy.GetAvailableSlots();

        var otherSlots = nearByParts.Select(p => p.nodeView.proxy)
                                    .Cast<Operants.PartProxy>()
                                    .SelectMany(pp => pp.GetAvailableSlots())
                                    .ToList();


        for (var i = 0; i < slots.Count(); i++)
        {
            var slot = slots[i].nodeView;
            for (var j = 0; j < otherSlots.Count; j++)
            {
                var otherSlot = otherSlots[j].nodeView;
                if (Vector3.Distance(otherSlot.transform.position, slot.transform.position) < 0.2f)
                {
                    var slotProxy = otherSlots[j];
                    partsToBeConnected.Add(slotProxy.ownerPartProxy);
                    temporal.Add(new Builder.TemporalRelationShip(slots[i].entity, "extends", slotProxy.entity));
                }
            }
        }
        return partsToBeConnected.Distinct().ToList();
    }


}
