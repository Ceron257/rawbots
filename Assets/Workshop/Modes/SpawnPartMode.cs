using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnPartMode : MonoBehaviour {

    public GameCamera gameCamera;
    public Transform pointer;
    public Material pointerMaterial;
    bool inMode = false;
    bool gridOn = false;
    Color partColor = new Color(0.64f, 0.31f, 0, 1);
    Color bpColor = new Color(0, 0.62f, 0.65f, 1);
    HexGrid grid;

    void Awake () {
        var mode = new GameModeManager.Mode(){
            id = GameModeManager.ModeId.SpawnPart,
            transitions = new List<GameModeManager.Transition>(){
                new GameModeManager.Transition(){ key = KeyCode.LeftControl , state = GameModeManager.KeyState.KeyUp , mode = GameModeManager.ModeId.World},
            }};
        GameModeManager.RegisterMode( mode );
    }

    void Update () {
        BulletRaycastHitDetails hit;
        var shift = Input.GetKey(KeyCode.LeftShift);
        pointerMaterial.SetColor( "_TintColor", shift ? bpColor : partColor );
        if ( GameModeManager.InMode( m => m == GameModeManager.ModeId.SpawnPart )) {
            var clickUp = Input.GetMouseButtonUp(0);
            var clickDown = Input.GetMouseButton(0);
            var ray = gameCamera.GetComponent<Camera>().ScreenPointToRay( Input.mousePosition ); //added structure -z26
            var hitStatus = Bullet.Raycast( ray, out hit, 500, BulletLayers.Camera, (BulletLayers.World | BulletLayers.HexBlock | BulletLayers.Structure) );

            if(!inMode){
                inMode = true;
                gridOn = false;
                pointer.gameObject.SetActive( true );
                gameCamera.EnableGridCamera( null, GridCamera.GridType.SpawnPart,false);
                grid = gameCamera.gridCamera.grid;
                grid.transform.position = gameCamera.transform.position;
                grid.camera.gridPosition = Vector3.zero;
                grid.transform.rotation = gameCamera.transform.rotation;
            }
            if(hitStatus){
                pointer.transform.position = hit.point;
                pointer.transform.up = hit.normal;
                pointer.localScale = clickDown ? Vector3.one * 0.7f : pointer.localScale = Vector3.one;
                if(clickUp && !gridOn){
                    if(shift){
                        inMode = false;
                        pointer.gameObject.SetActive( false );
                        gameCamera.DisableGridCamera( GridCamera.GridType.Programming , false );
                        GameModeManager.ChangeMode( GameModeManager.ModeId.Grid );
                        gameCamera.gridCamera.console.LoadBlueprint(pointer.transform.position, pointer.transform.rotation, pointer.transform.up);
                    }else{
                        pointer.gameObject.SetActive( false );
                        var pointerScreen = gameCamera.GetComponent<Camera>().WorldToScreenPoint( pointer.transform.position );
                        pointerScreen.z = grid.transform.position.z;
                        var pointerWorld = grid.camera.GetComponent<Camera>().ScreenToWorldPoint( pointerScreen );
                        var localPosition = grid.camera.transform.InverseTransformPoint( pointerWorld );
                        localPosition.x = -localPosition.x;
                        var tiling = grid.ToTiling( localPosition );
                        tiling.z = 0;
                        var position = pointer.transform.position + pointer.transform.up * 5;
                        var rotation = pointer.transform.rotation;
                        grid.GenerateTilesParts( tiling, position, rotation );
                        gridOn = true;
                    }
                }
            }

        }
        else {
            if ( inMode && !Input.GetKey( KeyCode.LeftControl )) {
                pointer.gameObject.SetActive( false );
                gameCamera.DisableGridCamera( GridCamera.GridType.Programming , false );
                GameModeManager.ChangeMode( GameModeManager.ModeId.World );
                inMode = false;
                gridOn = false;
            }
        }
 
    }
}
