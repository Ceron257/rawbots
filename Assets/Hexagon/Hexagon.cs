using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Hexagon : MonoBehaviour {

    public GameObject blockAsset;
    List< Transform > blocks = new List< Transform >();
    public int count = 100;
    public float height = 550;

    void Start () {

        int n = count;
        float inc = Mathf.PI * ( 3 - Mathf.Sqrt( 5 ) );
        float off = 2f / n;
        float x;
        float y;
        float z;
        float r;
        float phi;
         
        for ( int k = 0; k < n; k++ ) {
            y = k * off - 1 + ( off / 2 );
            r = Mathf.Sqrt( 1 - y * y );
            phi = k * inc;
            x = Mathf.Cos( phi ) * r;
            z = Mathf.Sin( phi ) * r;
            var blockObj = Instantiate( blockAsset ) as GameObject;
            blockObj.transform.parent = transform;
            var up = new Vector3( x, y, z );
            blocks.Add( blockObj.transform );
            blockObj.transform.position = transform.position;
            blockObj.transform.up = up;
            blockObj.transform.position = transform.position + blockObj.transform.up * height;
        }
        
        blockAsset.SetActive( false );
    }

//    void Update () {
//        for ( var i = 0; i < blocks.Count; ++i ) {
//            var block = blocks[ i ];
//            var distSqr = ( Camera.mainCamera.transform.position - block.position ).sqrMagnitude;
//            if ( distSqr > 1000000 ) {
//                if ( block.gameObject.active ) {
//                    block.gameObject.SetActiveRecursively( false );
//                }
//            }
//            else {
//                if ( !block.gameObject.active ) {
//                    block.gameObject.SetActiveRecursively( true );
//                }
//            }
//        }
//    }

}
