Shader "Rozgo/WaterSurface" {

    Properties {
  	  _TintColor ("Tint Color", Color) = (0.5,0.5,0.5,0.5)
      _MainTex ("Texture", 2D) = "white" {}
      _BumpMap ("Bumpmap", 2D) = "bump" {}
      _RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
      _RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
      
    }
    
    SubShader {

    
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	LOD 400
	
	Fog { Mode Off }
    Cull [_INWATER]
    //the shader can be told whenever to render inside faces by gamecamera.cs thanks to this -z26
	
	CGPROGRAM
	
      
      #pragma surface surf BlinnPhong alpha
      
      struct Input {
          float2 uv_MainTex;
          float2 uv_BumpMap;
          float3 viewDir;
      };
      
	float _InvFade = 0.01f;
      
      fixed4 _TintColor;
      sampler2D _MainTex;
      sampler2D _BumpMap;
      float4 _RimColor;
      float _RimPower;
      
      void surf (Input IN, inout SurfaceOutput o) {
      
	      float2 uv_SecondMain = IN.uv_MainTex;
      	  uv_SecondMain -= _Time.x * 0.5f;
      	  
      	  o.Albedo = tex2D( _MainTex, uv_SecondMain ).rgb * _TintColor.rgb;
      	  
      	  o.Alpha = _TintColor.a; 
          
          float2 uv_MainBump = IN.uv_BumpMap;
          uv_MainBump -= _Time.x * 0.5f;
          
          float2 uv_SecondBump = IN.uv_BumpMap;
          uv_SecondBump.x += _SinTime.x * 0.2f;
          uv_SecondBump.y -= _CosTime.x * 0.2f;
          
          float3 mnormal = UnpackNormal (tex2D (_BumpMap, uv_MainBump));
          float3 snormal = UnpackNormal (tex2D (_BumpMap, uv_SecondBump));
          
          o.Normal = ( mnormal + snormal ) / 2;

          float positivedotproduct = abs(dot (normalize(IN.viewDir), o.Normal));
          half rim = 1.0 - saturate(positivedotproduct); //modified shader so that it doesnt look weird
          o.Emission = _RimColor.rgb * pow (rim, _RimPower); //when it's viewed inside out. -z26
      }
      
      

    ENDCG
      
      
      
      
    } 
    
	FallBack "Transparent/VertexLit"
}

