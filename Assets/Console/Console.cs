using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Ionic.Zlib;
using Operants;
using Oracle;
using UnityEngine;

public class Console : MonoBehaviour {

    List<Context> history = new List<Context> ();
    string[] output = new string[2] { "", "" };
    string _suggestion = string.Empty;
    public GameCamera gameCamera;
    public TextMesh textMesh;
    public TextMesh contextMesh;
    public TextMesh aidTextMesh;
    public GameObject logObj;
    List<TextMesh> logMeshes = new List<TextMesh> ();
    static Queue<LogLoad> logs = new Queue<LogLoad> ();
    string cursor = "";
    string cursorCharacter;
    string inputString = string.Empty;
    bool waitingForClearInput = false;
    float lastCommandRun;

    public class Context {

        public string name = string.Empty;
        public string prompt = ":";
        public string input = string.Empty;
        public string aid = string.Empty;
        public List<string> options = new List<string> ();
        public HexGridTile tile;
        public System.Action<string, Context> onInput = (s, c) => { };

        public Context () { }

        public Context (Context other) {
            name = other.name;
            prompt = other.prompt;
            input = other.input;
            options = other.options;
            aid = other.aid;
            tile = other.tile;
            onInput = other.onInput;
        }
    }

    class Command {
        public CommandDelegate command;
        public string input;
    }

    public class LogLoad {
        public string contextName;
        public string line;
        public Color color;
    }

    class KeyBinding {
        public bool leftControl = false;
        public bool leftAlt = false;
        public KeyCode keyCode = KeyCode.None;
        public KeybindingDelegate keybindingDelegate;

        public int repeatCounter = 0;
        public RepeatDelegate repeatDelegate;
    }

    string suggestion {
        get { return _suggestion; }
        set {
            _suggestion = value;
            if (_suggestion != null && _suggestion != string.Empty) {
                aidTextMesh.text = context.prompt + " " + input._DontUseReplaceLastWord (_suggestion);
            } else {
                aidTextMesh.text = "";
            }
        }
    }

    string _input = string.Empty;

    string input {
        get { return _input; }
        set {
            _input = value;
            output = input._DontUseSplitAt (offset, out cursorCharacter);
            suggestion = ProcessInput ();
        }
    }

    int _offset = 0;

    int offset {
        get { return _offset; }
        set {
            _offset = value;
            cursor = " ";
            if (_offset > input.Length) {
                _offset = input.Length;
            }
            if (_offset < 0) {
                _offset = 0;
            }
            output = input._DontUseSplitAt (offset, out cursorCharacter);
        }
    }

    int _hOffset = 0;

    int hOffset {
        get { return _hOffset; }
        set {
            if (history.Count == 0) {
                _hOffset = 0;
                return;
            }
            _hOffset = value;
            if (_hOffset > history.Count - 1) {
                _hOffset = 0;
            }
            if (_hOffset < 0) {
                _hOffset = history.Count - 1;
            }
        }
    }

    delegate void CommandDelegate (string command);

    Queue<Command> waitQueue = new Queue<Command> ();
    Dictionary<string, CommandDelegate> commands;
    List<KeyBinding> keyBindings;
    [System.NonSerializedAttribute]
    public List<string> spawnableParts = new List<string> () {
        "continuum",
        "motor",
        "elbow",
        "piston",
        "grenade",
        "hover",
        "jet",
        "hydro_jet",
        "wheel",
        "hook",
        "wide_wheel",
        "wide_wheel_small",
        "foot",
        "fist",
        "fin",
        "spot_light",
        /*"pressure_plate",*/
        "floater",
        "hyper_cannon",
        "point_light",
        "camera",
        "plasma_cannon",
        "flux_capacitor",
        "laser",
        "hypercube",
        "projector",
        "extension_cross",
        /*"pivot",*/
        "extension_line",
        "extension_elbow",
        "extension_t",
        "speaker",
        "magnet",
        "antenna"
    };

    delegate void KeybindingDelegate ();
    delegate void RepeatDelegate (int repeatCount);

    void Awake () {
        lastCommandRun = Time.time;
        spawnableParts.Sort ();
        keyBindings = new List<KeyBinding> () {
            new KeyBinding { leftControl = true, keyCode = KeyCode.A, keybindingDelegate = StartOfInput },
            new KeyBinding { leftControl = true, keyCode = KeyCode.E, keybindingDelegate = EndOfInput },
            new KeyBinding { leftControl = true, keyCode = KeyCode.L, keybindingDelegate = ClearLog },
            new KeyBinding { leftControl = true, keyCode = KeyCode.U, keybindingDelegate = ClearContext },
            new KeyBinding { leftAlt = true, keyCode = KeyCode.LeftArrow, keybindingDelegate = CursorToPreviousWord },
            new KeyBinding { leftAlt = true, keyCode = KeyCode.RightArrow, keybindingDelegate = CursorToNextWord },
            new KeyBinding { leftAlt = true, keyCode = KeyCode.Backspace, keybindingDelegate = DeleteWord },
            new KeyBinding { keyCode = KeyCode.UpArrow, keybindingDelegate = HistoryPrevious },
            new KeyBinding { keyCode = KeyCode.DownArrow, keybindingDelegate = HistoryNext },
            new KeyBinding { keyCode = KeyCode.LeftArrow, repeatDelegate = CursorMoveLeft },
            new KeyBinding { keyCode = KeyCode.RightArrow, repeatDelegate = CursorMoveRight },
            new KeyBinding { leftControl = true, keyCode = KeyCode.C, keybindingDelegate = Copy },
            new KeyBinding { leftControl = true, keyCode = KeyCode.V, keybindingDelegate = Paste }
        };

        commands = new Dictionary<string, CommandDelegate> () { 
        { "save", CommandSave },
        { "reset", CommandReset },
        { "help", CommandHelp },
        { "shutdown", CommandShutdown },
        { "settings", CommandSettings },
        { "load", CommandLoad },
        { "list_saves", CommandListSaves },
        { "delete_save", CommandDeleteSaves },
        //            { "host", CommandHost },
        //            { "join", CommandJoin },
        { "invertcamera", CommandInvertY },
        { "spawn", CommandSpawn }
        };
    }

    public static void ForInstance (System.Action<Console> action) {
        var console = FindObjectOfType (typeof (Console)) as Console;
        if (console != null) {
            action (console);
        }
    }

    public static Context context = new Context ();

    string CreateOptionsString (List<string> options) {
        var optionsString = string.Empty;
        int idx = options.IndexOf (input);

        if (idx != -1) {
            for (int i = idx; i < options.Count; ++i) {
                if (i == idx) {
                    continue;
                }
                optionsString += "|" + options[i];
            }
            for (int i = 0; i < idx; ++i) {
                optionsString += "|" + options[i];
            }
        }
        return optionsString;
    }

    public void ForInput (string name, string input, List<string> options, System.Action<string> onInput) {
        context = new Context ();
        context.name = name;
        context.input = input;
        context.options = options.ToList ();
        context.onInput = (s, c) => onInput (s);
        if (options.Count > 0) {
            if (options.IndexOf (input) < 0) {
                input = options[0];
            }
            this.input = input;
            context.aid = CreateOptionsString (context.options);
        }
        this.input = input;
    }

    public void ForInput (string name, string input, List<string> options, System.Action<string, Context> onInput) {
        ForInput (name, input, options, i => { });
        context.onInput = onInput;
    }

    public static void PushLog (string contextName, string[] result) {
        EnqueueLog (contextName, result, Color.gray);
    }

    public static void PushError (string contextName, string[] result) {
        EnqueueLog (contextName, result, new Color (197f / 256f, 88f / 256f, 88f / 256f));
    }

    public static void PushSuccess (string contextName, string[] result) {
        EnqueueLog (contextName, result, new Color (109f / 256f, 197f / 256f, 115f / 256f));
    }

    static void EnqueueLog (string contextName, string[] data, Color color) {
        for (int i = 0; i < data.Length; ++i) {
            logs.Enqueue (new LogLoad () { contextName = contextName, line = data[i], color = color });
        }
    }

    void Update () {
        ProcessLogs ();
        ProcessQueue ();
        if (GameModeManager.InMode (m => m != GameModeManager.ModeId.Grid) || waitingForClearInput) {
            if (GetComponent<Camera> ().enabled) {
                GetComponent<Camera> ().enabled = false;
            }
            return;
        }
        if (!GetComponent<Camera> ().enabled) {
            GetComponent<Camera> ().enabled = true;
        }
        // process key commands    Input.GetKeyUp( keyBinding.keyCode ) 
        foreach (var keyBinding in keyBindings) {
            if (Input.GetKey (KeyCode.LeftControl) == keyBinding.leftControl &&
                Input.GetKey (KeyCode.LeftAlt) == keyBinding.leftAlt) {

                if (keyBinding.keybindingDelegate != null && Input.GetKeyUp (keyBinding.keyCode)) {
                    keyBinding.keybindingDelegate ();
                }

                if (keyBinding.repeatDelegate != null && Input.GetKey (keyBinding.keyCode)) {

                    keyBinding.repeatDelegate (keyBinding.repeatCounter);
                    keyBinding.repeatCounter++;
                } else {
                    keyBinding.repeatCounter = 0;
                }

            }

        }

        bool altgr = Input.GetKey (KeyCode.RightAlt) && Input.GetKey (KeyCode.LeftControl);
        bool control = Input.GetKey (KeyCode.LeftControl) && !altgr;
        bool alt = Input.GetKey (KeyCode.LeftAlt);
        bool leftShift = Input.GetKey (KeyCode.LeftShift);
        if (control || alt) { } else {
            var rawString = Input.inputString.Replace ("\t", "").Replace (((char) 25).ToString (), "")
                .Replace (((char) 27).ToString (), "").Replace ("`", "");
            inputString = string.Empty;

            foreach (char c in rawString) {
                if ((int) c < 126) {
                    inputString += c.ToString ();
                }
            }

            if (Input.GetKeyUp (KeyCode.Tab)) {
                inputString = inputString + "\t";
            }

            foreach (char c in inputString) {
                // backspace
                if (c == "\b" [0]) {
                    if (input.Length > 0 && input.Length - offset > 0) {
                        input = input.Remove ((input.Length - offset) - 1, 1);
                    } else if (input.Length == 0) {
                        ClearContext ();
                    }
                }
                // tab
                else if (c == "\t" [0]) {
                    if (context.options.Count > 0) {
                        int idx = context.options.IndexOf (input);
                        if (idx == -1) {
                            var closets = context.options
                                .Where (o => {
                                    var item = o.Replace (">", "").Replace ("<", "");
                                    var typed = input.Replace (">", "").Replace ("<", "");
                                    return item.StartsWith (typed);
                                })
                                .FirstOrDefault ();
                            if (closets != default (string)) {
                                idx = context.options.IndexOf (closets);
                            } else {
                                idx = 0;
                            }
                        } else {
                            idx = leftShift ? idx - 1 : idx + 1;
                        }
                        idx = idx < 0 ? context.options.Count - 1 : idx;
                        idx = idx >= context.options.Count ? 0 : idx;
                        input = context.options[idx];
                        context.aid = CreateOptionsString (context.options);
                    } else {
                        continue;
                    }
                }

                // enter
                else if (c == "\n" [0] || c == "\r" [0]) {

                    input = input.Trim ();

                    bool inContext = context.name != string.Empty;
                    var lastInput = input;
                    input = string.Empty;
                    var process = true;
                    AddToHistory (lastInput);

                    if (context.options.Count > 0) {
                        if (context.options.IndexOf (lastInput) < 0) {
                            process = false;
                            PushError (context.name, new string[] { lastInput + " is not a valid option" });
                        }
                    }

                    if (process) {
                        PushLog (context.name, new string[] { lastInput });
                        var previousContext = context;
                        context = new Context ();
                        previousContext.onInput (lastInput, previousContext);
                    }

                    // console command
                    if (!inContext) {
                        var match = Regex.Match (lastInput + "\n", @"^[a-z][a-z0-9_]+[ \n]");
                        if (match.Success) {
                            CommandDelegate commandDelegate;
                            if (commands.TryGetValue (match.Value.Trim (), out commandDelegate)) {
                                if (Gaia.scene > 0 ||
                                    (commandDelegate == CommandLoad ||
                                        //                                    commandDelegate == CommandHost ||
                                        commandDelegate == CommandJoin ||
                                        commandDelegate == CommandShutdown ||
                                        commandDelegate == CommandHelp)) {
                                    commandDelegate (lastInput);
                                } else {
                                    PushError (context.name, new string[] { "command disabled" });
                                }
                            } else {
                                PushError (context.name, new string[] { "bad command" });
                            }

                        } else if (lastInput != string.Empty) {
                            PushError (context.name, new string[] { "bad command" });
                        }
                    }
                    offset = 0;
                    hOffset = 0;
                } else {
                    string filtered = c.ToString ();
                    input = input.Insert (input.Length - offset, filtered);
                }
            }
        }

        textMesh.text = context.prompt + " " + output.Aggregate ((s, next) => s + cursor + next);
        aidTextMesh.text = new string (' ', textMesh.text.Count ()) + context.aid;
        contextMesh.text = context.name;
        int blinkTick = (int) (Time.time * 5) % 2;
        if (blinkTick == 0) {
            cursor = "_";
        } else {
            cursor = cursorCharacter;
        }
    }

    void ProcessLogs () {
        if (logs.Count > 0) {
            var log = logs.Dequeue ();
            var logText = (Instantiate (logObj) as GameObject).GetComponent<TextMesh> ();
            var logContextText = logText.transform.Find ("ContextLogLine").GetComponent<TextMesh> ();
            logText.transform.parent = logObj.transform.parent;
            logText.text = context.prompt + " " + log.line;
            logContextText.text = log.contextName;
            logText.GetComponent<Renderer> ().material.color = log.color;
            logContextText.GetComponent<Renderer> ().material.color = log.color;
            logMeshes.Add (logText);
            RedrawLogs ();
        }
    }

    void RedrawLogs () {
        for (int i = 0; i < logMeshes.Count; ++i) {
            var logItem = logMeshes[logMeshes.Count - 1 - i];
            if (logItem != default (TextMesh)) {
                logItem.transform.localPosition = logObj.transform.localPosition + new Vector3 (0, i * 0.1f, 0);
                logItem.transform.localRotation = Quaternion.identity;
                logItem.transform.localScale = Vector3.one;
            }
        }
    }

    void ProcessQueue () {
        var time = Time.time;
        if (time - lastCommandRun > 0.5f && waitQueue.Count > 0) {
            var c = waitQueue.Dequeue ();
            c.command (c.input);
            lastCommandRun = time;
        }
    }

    void CursorMoveLeft (int repeatCount) {
        if (repeatCount == 0 || repeatCount >= 15) {
            if(repeatCount % 3 != 2) {
                offset++;
            }
        }
    }

    void CursorMoveRight (int repeatCount) {
        if (repeatCount == 0 || repeatCount >= 15) {
            if(repeatCount % 3 != 2) {
                offset--;
            }
        }
    }

    void HistoryPrevious () {
        hOffset--;
        context = GetFromHistory ();
        input = context.input;
        offset = 0;
    }

    void HistoryNext () {
        hOffset++;
        context = GetFromHistory ();
        input = context.input;
        offset = 0;
    }

    void CursorToNextWord () {
        for (int i = offset - 1; i > 0; --i) {
            if (input[(input.Length) - i] == ' ') {
                offset = i;
                return;
            }
        }
        offset = 0;
    }

    void CursorToPreviousWord () {
        for (int i = offset + 1; i < input.Length; ++i) {
            if (input[(input.Length) - i] == ' ') {
                offset = i;
                return;
            }
        }
        offset = input.Length;
    }

    void Copy () {
        GUIUtility.systemCopyBuffer = input;
    }

    void Paste () {
        string pastebin = GUIUtility.systemCopyBuffer;
        input += pastebin;
    }

    void ClearLog () {
        foreach (var log in logMeshes) {
            Destroy (log.gameObject);
        }
        logMeshes.Clear ();
    }

    void ClearContext () {
        input = string.Empty;
        inputString = string.Empty;
        context = new Context ();
        offset = 0;
        hOffset = 0;
    }

    void StartOfInput () {
        offset = input.Count ();
    }

    public void EndOfInput () {
        offset = 0;
    }

    public void RunCommand (string command, string parameter) {
        CommandDelegate commandDelegate = default (CommandDelegate);
        if (commands.TryGetValue (command, out commandDelegate)) {
            if (Gaia.scene > 0) {
                var stringInput = parameter == string.Empty ? command : command + " " + parameter;
                waitQueue.Enqueue (new Command () { command = commandDelegate, input = stringInput });
            }
        } else {
            PushError ("", new string[] { "invalid command" });
        }
    }

    void DeleteWord () {
        int targetPositon = 0;
        for (int i = offset + 1; i < input.Length; ++i) {
            if (input[(input.Length) - i] == ' ') {
                targetPositon = ((input.Length) - i) + 1;
                break;
            }
        }
        input = input.Remove (targetPositon, ((input.Length - offset) - targetPositon));
        offset = input.Length - targetPositon;
    }

    Regex filesRegex = new Regex (@"^[a-z0-9]+$", RegexOptions.Compiled);

    void CommandSave (string command) {
        var parts = command.Split (' ');
        if (parts.Length == 1) {
            var log = Gaia.Instance ().Save ();
            PushLog ("save", new string[] { log });
        } else if (parts.Length == 2) {
            if (parts[1] == "-?") {
                PushSuccess ("save", new string[] {
                    "usage: save [-?][new_name]",
                    "creates a snapshot of the current game state",
                    "\t-?\t\tprints help",
                    "\tnew_name\t\tcreates a new file based on the loaded data",
                });
            } else if (filesRegex.IsMatch (parts[1])) {
                var log = Gaia.Instance ().Save (parts[1]);
                PushLog ("save", new string[] { log });
            } else {
                PushError ("save", new string[] { "invalid option, use -? for details" });
            }
        } else {
            PushError ("save", new string[] { "invalid option, use -? for details" });
        }
    }

    void CommandReset (string command) {
        var fileNames = new Dictionary<string, string> ();
        var parts = command.Split (' ');
        var files = Gaia.Instance ().GetSavedFiles ();
        var options = new List<string> ();
        if (files.Count > 0) {
            if (files.Contains ("last")) {
                options.Add ("last");
                fileNames.Add ("last", Gaia.Scenes[Gaia.scene]);
                files.Remove ("last");
            }
            files.OrderByDescending (f => f).ToList ().ForEach (f => {
                int startIndex = f.IndexOf ("_") + 1;
                int endIndex;

                if (f.Contains (".txt.gz")) {
                    endIndex = f.IndexOf (".txt.gz");
                } else {
                    endIndex = f.Length;
                }

                var ticks = f.Substring (startIndex, endIndex - startIndex);
                long date;
                string hint;
                if (long.TryParse (ticks, out date)) {
                    hint = (new System.DateTime (date)).ToString ();
                } else {
                    hint = ticks;
                }
                if (!fileNames.ContainsKey (hint)) {
                    fileNames.Add (hint, f);
                    options.Add (hint);
                }
            });
        }
        if (parts.Length == 1) {
            if (options.Count > 0) {
                ForInput ("reset", "", options, input => {
                    var fileName = string.Empty;
                    if (fileNames.ContainsKey (input)) {
                        fileName = fileNames[input];
                    }
                    Gaia.Instance ().Reset (fileName);
                });
            } else {
                PushError ("reset", new string[] { "snapshots not found" });
            }
        } else if (parts.Length == 2) {
            if (parts[1] == "-?") {
                PushSuccess ("reset", new string[] {
                    "usage: reset [-? | --hard | last]",
                    "restore a previously saved game state",
                    "\t-?\t\tprints help",
                    "\t--hard\t\trestore factory default",
                    "\tlast\t\trestore last saved or factory default if exist",
                });
            } else if (parts[1] == "--hard") {
                Gaia.Instance ().Reset ("default");
            } else if (parts[1] == "last") {
                var fileName = string.Empty;
                if (fileNames.ContainsKey (parts[1])) {
                    fileName = fileNames[parts[1]];
                    Gaia.Instance ().Reset (fileName);
                } else {
                    Gaia.Instance ().Reset ("default");
                }

            } else {
                PushError ("reset", new string[] { "invalid option, use -? for details" });
            }
        } else {
            PushError ("reset", new string[] { "invalid option, use -? for details" });
        }
    }

    void CommandHelp (string command) {
        var parts = command.Split (' ');
        if (parts.Length == 1) {
            PushSuccess ("help", commands.Keys.ToArray ());
        }
        //        Gaia.Instance().helper.Enable();
    }

    void CommandShutdown (string command) {
        Application.Quit ();
    }

    void CommandDeleteSaves (string command) {
        var parts = command.Split (' ');
        if (parts.Length == 2 && parts[1] == "-?") {
            PushSuccess ("deletesave", new string[] {
                "usage: deletesave [-? | name1 name2 name3...]",
                "delete every save of a specific universe"
            });
        } else if (parts.Length > 1) {
            var defsFolder = Path.Combine (Util.userDataPath, "defs");
            for(int i = 1; i < parts.Length; i++ ) {
                string universeFolder =  Path.Combine (defsFolder, parts[i]);

                if(Directory.Exists(universeFolder)) {
                    Directory.Delete(universeFolder, true);
                    PushSuccess (parts[i], new string[] { "save deleted" });

                } else {
                    PushError (parts[i], new string[] { "no saves of this universe" });
                }
            }
        } else {
            PushError ("reset", new string[] { "invalid option, use deletesave -? for details" });
        }

    }

     void CommandListSaves (string command) {
        string defsFolder = Path.Combine (Util.userDataPath, "defs");
        string[] universes = Directory.GetDirectories(defsFolder);

        if(universes.Length == 0) {
            PushLog ("listsaves", new string[] { "no existing saves" });
        }
        foreach(string universe in universes) {
            string name = universe.Split(Path.DirectorySeparatorChar).Last();
            PushLog ("", new string[] {name});
        }
    }

    void CommandInvertY (string command) {
        string input = command.Split (' ').Last ();

        if (input == "true" || input == "false") {
            bool invertY = bool.Parse (input);
            PlayerPrefs.SetString ("invertY", input);
            GameObject.Find ("CameraGhost").GetComponent<GhostCamera> ().invertY = invertY;
        } else if (input == "-?") {
            PushSuccess ("invertcamera", new string[] {
                "usage: invertcamera true | invertcamera false",
                "Inverts the 3rd person camera's vertical axis"
            });
        } else {
            PushError ("invertcamera", new string[] { "invalid option, use invertcamera -? for help" });
        }
    }

    public static bool hosting = false;

    //    void CommandHost ( string command ) {
    //        hosting = true;
    //        SceneLoad( "host" );
    //    }

    void CommandLoad (string command) {
        Gaia.Instance ().UpdateScenes ();
        var lastLoadedScene = PlayerPrefs.GetString ("scene", string.Empty);
        var options = Gaia.Scenes.Where (i => i != "none").ToList ();
        if (options.Contains (lastLoadedScene)) {
            options = options.OrderByDescending (s => s == lastLoadedScene).ToList ();
        }
        var parts = command.Split (' ');
        if (parts.Length == 1) {
            hosting = false;
            SceneLoad ("load", options);
        } else if (parts.Length == 2) {
            var index = options.IndexOf (parts[1]);
            if (parts[1] == "-?") {
                PushSuccess ("load", new string[] {
                    "usage: load [-? | name]",
                    "loads the main gamemode",
                    "\t-?\t\tprints help",
                    "\tname\t\tloads specific universe",
                });
            } else if (index != -1) {
                context = new Context ();
                Gaia.scene = Gaia.Scenes.IndexOf (options[index]);
                PlayerPrefs.SetString ("scene", Gaia.Scenes[Gaia.scene]);
                Gaia.Instance ().Reset (string.Empty);
            } else {
                PushError ("load", new string[] { "invalid option, use -? for details" });
            }
        } else {
            PushError ("load", new string[] { "invalid option, use -? for details" });
        }
    }

    void CommandSpawn (string command) {
        var parts = command.Split (' ');
        if (parts.Length == 1) {
            ForInput ("spawn", "", spawnableParts, (input) => {
                SpawnPart (input);
            });
        } else if (parts.Length == 2) {
            if (parts[1] == "-?") {
                PushSuccess ("spawn", new string[] {
                    "usage: spawn [-?]",
                    "spawns available parts at mouse position, the mouse should be placed on top of any ground",
                    "\t-?\t\tprints help"
                });
            } else if (spawnableParts.IndexOf (parts[1]) != -1) {
                SpawnPart (parts[1]);
            } else {
                PushError ("spawn", new string[] { "invalid option, use -? for details" });
            }
        } else {
            PushError ("spawn", new string[] { "invalid option, use -? for details" });
        }
    }

    public void SpawnPart (string partName, Vector3 position, Quaternion rotation) {
        var obj = new GameObject ("Base/" + partName + "_base"); //when a part is mouse spawned, the first step is to
        obj.transform.parent = transform.parent; //create this "base" gameObject with the xform and bot script attached to it
        obj.transform.position = position; //the "base" isnt a physical object, rather it contains a basic part definition
        obj.transform.rotation = rotation; //template like you'd see in a map file.
        obj.AddComponent<XForm> ();
        obj.AddComponent<Bot> ();
    }

    void SpawnPart (string partName) {
        BulletRaycastHitDetails hit;
        Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition); //added structure to layer check,doesnt work-z26
        if (Bullet.Raycast (ray, out hit, 500, BulletLayers.World | BulletLayers.HexBlock | BulletLayers.Structure, BulletLayers.World | BulletLayers.HexBlock | BulletLayers.Structure)) {
            SpawnPart (partName, hit.point + hit.normal * 10, hit.bulletRigidbody.transform.rotation);
        } else {
            PushError ("spawn", new string[] { "place the mouse pointer above ground" });
        }
    }

    public void Load () {
        GameModeManager.ChangeMode (GameModeManager.ModeId.Grid);
        CommandLoad (string.Empty);
    }

    IEnumerator OpenSettings () {
        var settings = Gaia.Instance ().entities["settings"].Tails (p => p == "as").FirstOrDefault ();
        if (settings == null) {
            settings = new Entity ("settings0");
            new Relationship (settings, "as", Gaia.Instance ().entities["settings"]);
            new Relationship (settings, "for", Gaia.Instance ().root);
            settings.SetProperty ("permissions", (float)
                (Sec.Permission.connect | Sec.Permission.disconnect | Sec.Permission.edit));
            Gaia.Instance ().recentlyAddedEntities.Add (settings);
            while (Gaia.Instance ().recentlyAddedEntities.IndexOf (settings) >= 0) {
                yield return 0;
            }
        }
        gameCamera.EnableGridCamera (settings);
    }

    void CommandSettings (string command) {
        var parts = command.Split (' ');
        if (parts.Length == 1) {
            StartCoroutine (OpenSettings ());
        } else if (parts.Length == 2) {
            if (parts[1] == "-?") {
                PushSuccess ("settings", new string[] {
                    "usage: settings [-?]",
                    "exposes the settings subgraph",
                    "\t-?\t\tprints help"
                });
            } else {
                PushError ("settings", new string[] { "invalid option, use -? for details" });
            }
        } else {
            PushError ("settings", new string[] { "invalid option, use -? for details" });
        }
    }

    void AddToHistory (string command) {
        if (history == null) {
            history = new List<Context> ();
        }
        context.input = command;
        history.Add (context);
        if (history.Count > 20) {
            history = history.Skip (1).ToList ();
        }
    }

    Context GetFromHistory () {
        if (history.Count == 0 || hOffset > history.Count) {
            return new Context ();
        }
        return new Context (history[hOffset]);
    }

    string ProcessInput () {
        string candidate = string.Empty;
        if (input.Length > 0 && Regex.Match (input, @"^[\+\-][ ][a-z0-9]+|^[\:][a-z0-9]+|^[\?][ ]*").Success) {
            Match filtered = Regex.Match (input, @"[a-z0-9_]+$|^[\:][a-z0-9_]+$");
            if (filtered.Success) {
                candidate = commands.Keys
                    .Where (s => (s.StartsWith (filtered.Value) && s.Length > filtered.Value.Length))
                    .OrderBy (s => s.Length).FirstOrDefault ();
            }
        }
        return candidate;
    }

    public void SetNewPasscode (Entity entity) {
        if (entity == default (Entity)) {
            return;
        }
        ForInput ("input new passcode", "", new List<string> (), input => {
            if (Regex.Match (input, @"^[0-9][0-9][0-9][0-9]$").Success || input == "") {
                if (input == "") {
                    entity.RemoveProperty ("passcode");
                    PushLog ("passcode", new string[] { "passcode cleared" });
                } else {
                    entity.SetProperty ("passcode", "#" + input);
                    Gaia.Instance ().SaveCode (input);
                    PushLog ("passcode", new string[] { "passcode changed" });
                }
            } else {
                PushError ("passcode", new string[] { "insert four digits or nothing for clearing" });
            }
        });
    }

    public void ForPasscode (Entity entity, System.Action success, System.Action fail) {
        if (entity == default (Entity)) {
            success ();
            return;
        }
        var passcode = string.Empty;
        entity.ForProperty<string> ("passcode", value => passcode = value.Substring (1));
        if (passcode != string.Empty) {
            Gaia.Instance ().ForPassCode (passcode, value => {
                if (value) {
                    success ();
                } else {
                    fail ();
                    ForInput ("passcode", "", new List<string> (), (i) => {
                        if (Regex.Match (i, @"^[0-9][0-9][0-9][0-9]$").Success) {
                            if (i == passcode) {
                                Gaia.Instance ().SaveCode (passcode);
                                PushSuccess ("passcode", new string[] { "access granted" });
                                success ();
                            } else {
                                PushError ("passcode", new string[] { "access denied" });
                            }
                        } else {
                            PushError ("passcode", new string[] { "insert four digits" });
                        }
                    });
                }
            });
        } else {
            success ();
        }
    }

    public void SaveBlueprint (HashSet<Entity> entites) {
        waitingForClearInput = true;

        input = string.Empty;
        inputString = string.Empty;
        context = new Context ();
        offset = 0;
        hOffset = 0;
        StartCoroutine (SaveBP (entites));
    }

    IEnumerator SaveBP (HashSet<Entity> entites) {
        string bpName = string.Empty;
        System.Predicate<string> defaultFilter = f => true;
        while (Input.anyKey) {
            yield return null;
        }
        waitingForClearInput = false;
        ForInput ("name", string.Empty, new List<string> (), i => {
            var parts = i.Split (' '); //this part proccesses
            if (parts.Length == 1 && Regex.Match (i, @"^[A-Za-z_0-9]*$").Success) {
                bpName = "bp_" + parts[0]; //this part check if the user-choosen name of the bp is okay -z26

                var connectedParts = entites.Where (e => e.nodeView != default (NodeView))
                    .Where (e => e.proxy is PartProxy)
                    .Select (e => e.proxy).Cast<PartProxy> ().ToList ();

                var xForms = new List<Entity> ();
                connectedParts.ForEach (pp => {
                    var xFormEntity = pp.entity.Heads (p => p == "local_to").FirstOrDefault ();
                    if (xFormEntity != default (Entity)) {
                        if (!xForms.Contains (xFormEntity)) {
                            xForms.Add (xFormEntity);
                            entites.Add (xFormEntity);
                        }
                    }
                });

                Blueprint bp = new Blueprint ();
                bp.name = bpName;

                var bpData = new List<string> ();
                foreach (var e in entites) {
                    bpData.AddRange (e.MangledSerialize (defaultFilter));
                }
                bp.blueprint = bpData.ToArray ();

                var BPFolder = Path.Combine( Util.userDataPath, "blueprints" );
                System.IO.Directory.CreateDirectory( BPFolder );

                //check for existing files
                string fileName = System.IO.Path.Combine( BPFolder, bpName);
                bool extensionless = File.Exists(fileName);
                bool gzip = File.Exists(fileName +  ".txt.gz");


                if(extensionless ^ gzip) { //XOR

                    string autosave = Path.Combine( BPFolder, "bp_auto-save.txt.gz" );
                    
                    if (System.IO.File.Exists(autosave)) {
                        System.IO.File.Delete(autosave);
                    }

                    PushError("blueprint", new string[]{"Previous blueprint with this name renamed auto-save"});
                    PushError("blueprint", new string[]{"auto-save may be overwritten next time you save!"});

                    if(extensionless) {
                        File.Move(fileName, autosave);
                    } else {
                        File.Move(fileName + ".txt.gz", autosave);
                    }
                }
                                
                if (extensionless && gzip) {
                    PushError("blueprint", new string[]{  "several files in with this name but a different file extension."});
                    PushError("blueprint", new string[]{  "blueprint was not saved."});
                    PushError("blueprint", new string[]{ "Please open your bp folder " + BPFolder + " and fix this."});
                    return;
                }
 
                var tm = new MemoryStream (0xF0000);
                var sw = new StreamWriter (tm, System.Text.Encoding.ASCII);

                foreach (var line in bp.blueprint) {
                    sw.WriteLine (line);
                    sw.Flush (); //flushing at each iteration ensure newlines -z26
                }

                tm.Seek (0, SeekOrigin.Begin);
                using (FileStream fs = new FileStream (fileName + ".txt.gz", FileMode.Create)) {
                    using (GZipStream gz = new GZipStream (fs, CompressionMode.Compress, false)) {
                        gz.Write (tm.GetBuffer (), 0, tm.GetBuffer ().Length);
                    }
                }
                sw.Close ();
                PushSuccess ("blueprint", new string[] { bpName + " saved successfully" });
            } else {
                PushError ("blueprint", new string[] { "invalid name" });
            }
        });
    }

    public void LoadBlueprint (Vector3 position, Quaternion rotation, Vector3 up) {
        waitingForClearInput = true;
        input = string.Empty;
        inputString = string.Empty;
        context = new Context ();
        offset = 0;
        hOffset = 0;
        StartCoroutine (CreateGhost (position, rotation, up));

    }

    IEnumerator CreateGhost (Vector3 position, Quaternion rotation, Vector3 up) {
        var fileNames = new Dictionary<string, string> ();
        while (Input.anyKey) {
            yield return null;
        }

        waitingForClearInput = false;

        var files = Gaia.Instance ().GetSavedBlueprints ();
        if (files.Count > 0) {
            var options = new List<string> ();
            files.OrderByDescending (f => f).ToList ().ForEach (f => {

                int startIndex = f.IndexOf ("_") + 1;
                int endIndex;

                if (f.Contains (".txt.gz")) { //z26
                    endIndex = f.LastIndexOf (".txt.gz");
                } else {
                    endIndex = f.Length;
                }

                var name = f.Substring (startIndex, endIndex - startIndex);
                if (!fileNames.ContainsKey (name)) {
                    fileNames.Add (name, f);
                    options.Add (name);
                } else {
                    PushError (name, new string[] { "more than one blueprint has this name" });
                }
            });

            ForInput ("bp_name", "", options, input => {
                if (fileNames.ContainsKey (input)) {
                    var bpName = fileNames[input];
                    var newEntities = Gaia.Instance ().ParseBlueprint (bpName);
                    if (newEntities.Count > 0) {

                        var root = new GameObject (bpName);
                        root.transform.position = position;
                        root.transform.rotation = rotation; // * Quaternion.Euler(euler);
                        var bpLoader = root.AddComponent<BluePrintLoader> ();
                        bpLoader.console = this;
                        bpLoader.entities = newEntities;
                        bpLoader.up = up;
                    } else {
                        PushError ("blueprint", new string[] { "cannot load the blueprint" });
                    }
                }
            });
        } else {
            PushError ("blueprint", new string[] { "blueprint files not found" });
        }
    }

    void SceneLoad (string name, List<string> options) {
        ForInput (name, "", options, (input) => {
            var index = options.IndexOf (input);
            if (index != -1) {
                context = new Context ();
                Gaia.scene = Gaia.Scenes.IndexOf (options[index]);
                PlayerPrefs.SetString ("scene", Gaia.Scenes[Gaia.scene]);
                Gaia.Instance ().Reset (string.Empty);
            }
        });
    }

    void CommandJoin (string command) {
        // var options = new List< string >();
        // ForInput( "join", "localhost", options, input => {
        //     PushLog("", new string[] {"Trying to join " + input } );
        //     Network.Connect( input, 57668 );
        //            ForInput( "passcode", "", options, password => {
        //            } );
        // } );
    }

    // void OnServerInitialized () {
    //     PushSuccess("", new string[] {"Hosting" } );
    // }

    // void OnConnectedToServer () {
    //     PushSuccess("", new string[] {"Joined" } );
    // }

    // void OnFailedToConnect ( NetworkConnectionError error ) {
    //     PushError("", new string[] {"Could not join " + error} );
    // }
}

public static class StringEx {

    public static string[] _DontUseSplitAt (this string s, int offset, out string cursor) {
        cursor = " ";
        string[] splitted = new string[2] { "", "" };
        if (offset == 0 || s == string.Empty) {
            splitted[0] = s;
        } else {
            string tail = s.Substring (s.Length - offset);
            cursor = (tail.First ()).ToString ();
            splitted[0] = s.Substring (0, s.Length - offset);
            splitted[1] = tail.Remove (0, 1);
        }
        return splitted;
    }

    public static string _DontUseReplaceLastWord (this string s, string replacement) {
        var lastWord = s.Substring (s.LastIndexOf (' ') + 1);
        if (lastWord == string.Empty) {
            return "";
        } else {
            return Regex.Replace (s, Regex.Escape (lastWord) + "+$", replacement);
        }
    }

    public static byte[] GetBytes (this string str) {
        byte[] bytes = new byte[str.Length * sizeof (char)];
        System.Buffer.BlockCopy (str.ToCharArray (), 0, bytes, 0, bytes.Length);
        return bytes;
    }

}

public class Blueprint {
    public string name;
    public string[] blueprint;
}