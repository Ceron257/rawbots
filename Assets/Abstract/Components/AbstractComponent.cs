using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Operants {

    public abstract class AbstractComponent {
     
        public NodeProxy Proxy
        { get; protected set; }
        
        public AbstractComponent ( NodeProxy proxy ) {
            Proxy = proxy;
        }
        
        public virtual void OnUpdate () {
        }

        public virtual void OnFixedUpdate () {
        }

        public virtual void OnDestroy () {
        }
        
        public virtual void OnPreBuild () {
        }
     
        public virtual void OnBuild () {
        }
        
        public virtual void OnPostBuild () {
        }
        
        public virtual void OnInbox ( Inbox inbox ) {
        }

        public virtual void OnInboxDestroy ( Inbox inbox ) {
        }

        public virtual void OnInboxDisconnect( Inbox inbox ){
        }

    }

}
