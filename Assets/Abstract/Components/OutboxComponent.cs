using UnityEngine;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class Outbox : AbstractComponent {
    
        public string property = string.Empty;
        public List< Inbox > inboxes = new List< Inbox >();
        List< System.Delegate > actions = new List< System.Delegate >();
    
        public Outbox ( NodeProxy proxy ) : base( proxy ) {
        }

        public void RemoveAction< T > ( System.Action< T > action ) {
            actions.Remove( action );
        }

        public void ClearActions () {
            actions.Clear();
        }
        
        public override void OnPreBuild () {
            base.OnPreBuild();
        }
        
        public void Produce< T > ( T value ) {
             for(int i = 0; i< actions.Count; ++i){
                var castedAction = (System.Action< T > )actions[i];
                castedAction( value );

            }

//            foreach ( var action in actions ) {
//                var castedAction = (System.Action< T > )action;
//                castedAction( value );
//            }
            for(int i = 0; i< inboxes.Count; ++i){
                if ( inboxes[i] == null ) {
                    Debug.LogError( "outbox: " + Proxy.entity.id + " cannot connect to inbox for property: " + property );
                }
                else {
                    inboxes[i].Consume< T >( this, value );
                }

            }
//            foreach ( var inbox in inboxes ) {
//                if ( inbox == null ) {
//                    Debug.LogError( "outbox: " + Proxy.entity.id + " cannot connect to inbox for property: " + property );
//                }
//                else {
//                    inbox.Consume< T >( this, value );
//                }
//            }
        }

        public void For< T > ( bool predicate, System.Action< T > action ) {
            if ( predicate ) {
                actions.Add( action );
            }
        }
    
    }
 
}
