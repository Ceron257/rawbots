using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class VelocimeterProxy : NodeProxy {

        PartProxy part;
        IOComponent io;
        Vector3 lastVelocity = Vector3.zero;
        Vector3 lastAngularVelocity = Vector3.zero;
        float lowpassFactor = 2;
        Quaternion attitude;
        bool setNewAttitude = false;
        GameObject gyroscope;

        public VelocimeterProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();

        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< PartProxy >( property == "part_a", ( output, value ) => part = value );
            } );
        }

        public override void OnUpdate () {
            var lin_velocity_up = 0f;
            var lin_velocity_side = 0f;
            var lin_velocity_front = 0f;
            var ang_velocity_up = 0f;
            var ang_velocity_side = 0f;
            var ang_velocity_front = 0f;
            var velocity = Vector3.zero;
            var angularVelocity = Vector3.zero;
            var localAngularVelocity = Vector3.zero;
            if ( part != default(PartProxy) && part.nodeView != default(NodeView) ) {
                var bulletRigidbody = part.nodeView.GetComponent<PhysicalObject>().bulletRigidbody;
                var centerOfMass =bulletRigidbody.compoundShape.centerOfMass;

                velocity =bulletRigidbody.GetVelocity();// Vector3.Lerp( lastVelocity, physicalObject.bulletRigidbody.GetVelocity(), Time.deltaTime * lowpassFactor );
                angularVelocity = bulletRigidbody.GetAngularVelocity();//Vector3.Lerp( lastAngularVelocity, physicalObject.bulletRigidbody.GetAngularVelocity(), Time.deltaTime * lowpassFactor );
                lastVelocity = velocity;
                lastAngularVelocity = angularVelocity;

                lin_velocity_up = Vector3.Dot( velocity, centerOfMass.transform.up );
                 lin_velocity_side = Vector3.Dot( velocity, centerOfMass.transform.right );
                 lin_velocity_front = Vector3.Dot( velocity, centerOfMass.transform.forward );
                localAngularVelocity = centerOfMass.transform.InverseTransformDirection(angularVelocity);

            }
            part = default( PartProxy );
            io.Produce<float>( "v", velocity.magnitude );
            io.Produce<float>( "w", angularVelocity.magnitude );
            io.Produce<float>( "v_up", lin_velocity_up );
            io.Produce<float>( "v_side", lin_velocity_side );
            io.Produce<float>( "v_front", lin_velocity_front );
            io.Produce<float>( "w_up", localAngularVelocity.y );
            io.Produce<float>( "w_side", localAngularVelocity.x );
            io.Produce<float>( "w_front", localAngularVelocity.z );
        }






    }



}