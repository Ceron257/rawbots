using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Operants {

    public enum ContactFX{
        None = 0,
        Dust = 1,
        Volcanic = 2,
        Ice = 3,
        Metal = 4,
        Grass = 5,
    }


public class StructureProxy : NodeProxy {

        public IOComponent io;

        public StructureProxy(){
            io = new IOComponent(this);
            components.Add(io);
        }

        public override void OnPreBuild () {
            base.OnPreBuild();


            CreateView();

        }

        public override void OnDestroy () {
            base.OnDestroy();
            foreach ( var r in entity.Relationships().Where( r =>
                    r.Predicate == "input_for" ||
                    r.Predicate == "output_for")  ) {
                var e = r.Head == entity ? r.Tail : r.Head;
                e.deprecated = true;
            }
            entity.deprecated = true;
            entity.Relationships().ToList().ForEach(r => r.Break());
            GameObject.Destroy(nodeView.gameObject);
        }

    }   
}
