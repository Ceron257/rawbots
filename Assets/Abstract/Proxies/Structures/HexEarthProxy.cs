using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Operants;

namespace Operants {

    public class HexEarthProxy : StructureProxy {

        public enum Faces {
            Face0,//At 60
            Face1,//At 120
            Face2,//At 180
            Face3,//At 240
            Face4,//At 300
            Face5,//At 360
            BottomFace,
            TopFace,
        }

        public HexEarth earth;
        public bool[] availableFaces = new bool[ 8 ];
        public ContactFX fx;

        public HexEarthProxy () {
        }

        public override void OnPostBuild () {
            base.OnPostBuild();
            entity.InmediateParent().ForProperty<float>("contact_fx", value => fx = (ContactFX)((int)value));
        }

    }
}
