using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class CommandProxy : NodeProxy {
    
        IOComponent io;
        Pulse pulse;
        string command = string.Empty;
        string parameter = string.Empty;

        public CommandProxy () {
            io = new IOComponent( this );
            components.Add( io );
            pulse = new Pulse();
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            pulse = new Pulse();
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "trigger", ( output, value ) => {
                    pulse.ForPulse(value, () => {
                        if(command != string.Empty){
                            Gaia.Instance().console.RunCommand(command,parameter);
                        }
                    });
                });
                inbox.For< string >( property == "name", (output, value) => command = value.Trim() );
                inbox.For< string >( property == "parameter", (output, value) => parameter = value.Trim() );
            } );
        }
   
    }
}
