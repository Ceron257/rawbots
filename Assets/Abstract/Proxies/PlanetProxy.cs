using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public enum PlanetType {
        BodyDefault,
        BodyVolcanic,
        BodyRocky,
        Water,
    }

    public class PlanetProxy : NodeProxy {

        IOComponent io;
        Planet planet;

        public PlanetProxy () {
            io = new IOComponent( this );
            components.Add( io );
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            CreateView();
            ForView<Planet>( ref planet, () => {
                var btRigidBody = planet.water.GetComponent<BulletRigidBody>();
                btRigidBody.SetPosition(planet.transform.position);
                btRigidBody.SetRotation(planet.transform.rotation);
            } );
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {

                inbox.For< string >( property == "type", ( outbox, value ) => {
                    ForView< Planet >( ref planet, () => {
                        planet.SetType( ( PlanetType )System.Enum.Parse( typeof( PlanetType ), value.Split( '.' ).Last() ) );
                    } );
                } );

                inbox.For< float >( property == "size", ( outbox, value ) => {
                    ForView< Planet >( ref planet, () => {
                        planet.SetSize( value );
                    } );
                } );

                inbox.For< float >( property == "roots", ( outbox, value ) => {
                    ForView< Planet >( ref planet, () => {
                        planet.roots = ( int )value;
                    } );
                } );

            } );
        }

    }
}
