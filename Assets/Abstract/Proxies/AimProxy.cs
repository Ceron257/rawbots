﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine; //removethis

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using UnityEngine;

namespace Operants {

    public enum Arrows {
        Red,
        Reverse_Red,
        Green,
        Reverse_Green,
        Blue,
        Reverse_Blue
    }

    public class AimProxy : NodeProxy {

        IOComponent io;

        PartProxy proxy_a; 
        PartProxy proxy_target;

        int front = 0;
        int up = 2;
        int right = 4;

        bool initFront = false;
        bool initUp = false;
        bool initRight = false;

        bool validArrows = false;

        Matrix4x4 matrix = new Matrix4x4(); //only 4x4 because unity doesn't have 3x3 built-in.


        public Dictionary<string, Arrows> arrowsDict = new Dictionary<string, Arrows> {
            { "Red", Arrows.Red },
            { "Reverse_Red", Arrows.Reverse_Red },
            { "Green", Arrows.Green },
            { "Reverse_Green", Arrows.Reverse_Green },
            { "Blue", Arrows.Blue },
            { "Reverse_Blue", Arrows.Reverse_Blue }
        };

        public AimProxy () {
            io = new IOComponent (this);
            components.Add (io);
            updates = true;
        }


        void Configure (string input, int axisToChange) {

            //Update choosen axis.
            if (axisToChange == 2) {
                front = (int)arrowsDict[input];
                initFront = true;
            } else if (axisToChange == 1) {
                up = (int)arrowsDict[input];
                initUp = true;
            } else {
                right = (int)arrowsDict[input];
                initRight = true;
            }

           
            int frontColor = front/2;
            int upColor = up/2;
            int rightColor = right/2;
            //verify if 3 axes are all a different color. 
            if (frontColor != upColor && frontColor != rightColor && upColor != rightColor) {


                //This matrix lets you swap axes around and invert them. Left/right-handed coord are both supported.
                //Mathematicians have weird conventions. In a matrix, columns act like rows, unlike what we're used to.
                matrix.SetColumn(0,InitVector(right));
                matrix.SetColumn(1,InitVector(up));
                matrix.SetColumn(2,InitVector(front));
                matrix.SetColumn(3,new Vector4(0,0,0,1));

                validArrows = true;

            } else {
                validArrows = false;

                if(initFront && initUp && initRight) { //Don't spam on initialization.
                    Console.PushError("aim", new string[] { "invalid configuration, each input must be a different color" });
                }
            }
        }



        Vector4 InitVector (int arrow) { //4th term is useless for this operant, only there cause Unity matricies are 4x4.
            switch (arrow) {
                case 0:
                    return new Vector4 (1, 0, 0, 0);
                case 1:
                    return new Vector4 (-1, 0, 0, 0);
                case 2:
                    return new Vector4 (0, 1, 0, 0);
                case 3:
                    return new Vector4 (0, -1, 0, 0);
                case 4:
                    return new Vector4 (0, 0, 1, 0);
                case 5:
                    return new Vector4 (0, 0, -1, 0);
                default:
                    throw new System.ArgumentException ();
            }
        }


        public override void OnInbox (Inbox inbox) {
            base.OnInbox (inbox);
            inbox.Proxy.entity.ForInferredProperty<string> ("property", Util.AsIs, property => {
                inbox.For<PartProxy> (property == "part_a", (output, value) => proxy_a = value);
                inbox.For<PartProxy> (property == "target", (output, value) => proxy_target = value);

                inbox.For<string> (property == "arrow_right", (output, value) => Configure (value, 0));
                inbox.For<string> (property == "arrow_up", (output, value) => Configure (value, 1));
                inbox.For<string> (property == "arrow_front", (output, value) => Configure (value, 2));
            });
        }

        public override void OnUpdate () {

            if(proxy_a == null ||
               proxy_a.nodeView == null ||
               proxy_target == null ||
               proxy_target.nodeView == null)
               { return; }


            var body_a = proxy_a.nodeView.GetComponent<PhysicalObject>().bulletRigidbody;
            var body_target = proxy_target.nodeView.GetComponent<PhysicalObject>().bulletRigidbody;

            if (validArrows) {

                DebugAxes(body_a, body_target);

                //Position of target relative to part_a, in Unity's global coordinate space.
                Vector3 cartesian = (body_target.transform.position - body_a.transform.position);

                
                //Quaternion and matrix are both inverted cause I'm doing a global->local conversion.
                //Quaternion must be applied first, matrix second. (opposite than local->global conversion)

                cartesian =  Quaternion.Inverse(body_a.transform.rotation) * cartesian; //orientation of part_a.
                cartesian = matrix.inverse.MultiplyPoint3x4(cartesian); //swap axes around.

                CartesianToPolar(cartesian);
            }

            
            float distance = Vector3.Distance (body_a.transform.position, body_target.transform.position);
            io.Produce<float> ("distance", distance);
        }


        void CartesianToPolar(Vector3 cartesian) {
            //atan2 is usually given (y,x), but this is yaw and I want pure z (forward) vectors to = 0 degrees.
            var yaw = Mathf.Rad2Deg * Mathf.Atan2(cartesian.x,cartesian.z);

            //Calculated from different vectors than yaw(azimuth), visual here: https://en.wikipedia.org/wiki/Azimuth
            var hypotenuse = Mathf.Sqrt(cartesian.x * cartesian.x + cartesian.z * cartesian.z);
            var pitch = Mathf.Rad2Deg * Mathf.Atan2(cartesian.y,hypotenuse);

            io.Produce<float> ("yaw", yaw);
            io.Produce<float> ("pitch", pitch);
            io.Produce<float> ("front_distance", cartesian.z);  
        }



        void DebugAxes(BulletRigidBody body_a, BulletRigidBody body_target) {
            Vector3 DebugX = body_a.transform.rotation * matrix.MultiplyPoint3x4(new Vector3(5,0,0));
            Debug.DrawLine(body_a.transform.position, body_a.transform.position + DebugX, Color.red);

            Vector3 DebugY = body_a.transform.rotation * matrix.MultiplyPoint3x4(new Vector3(0,5,0));
            Debug.DrawLine(body_a.transform.position, body_a.transform.position + DebugY, Color.green);

            Vector3 DebugZ = body_a.transform.rotation * matrix.MultiplyPoint3x4(new Vector3(0,0,5));
            Debug.DrawLine(body_a.transform.position, body_a.transform.position + DebugZ, Color.blue);
        }

    }
}