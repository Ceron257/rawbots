﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

	//by z26.
    public class MemoryProxy : NodeProxy {

        public IOComponent io;
        float sample_in;
        float sample_out;
        int index;
		int write;
		int clear_all;

        Pulse writePulse = new Pulse();//triggers an action if the signal just transistioned from 0 to 1.
		Pulse clear_allPulse = new Pulse();



        public MemoryProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
        	sample_in = 0f;
        	sample_out = 0f;
        	index = 0;
			write = 0;
			clear_all = 0;
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "sample_in", ( outbox, value ) => HandleInput(value) );
                inbox.For< float >( property == "index", ( outbox, value ) => FetchIndex(value) );
	            inbox.For< float >( property == "write", ( outbox, value ) => write = Mathf.RoundToInt( Mathf.Clamp01( value ) ) );
			    inbox.For< float >( property == "clear_all", ( outbox, value ) => clear_all = Mathf.RoundToInt( Mathf.Clamp01( value ) ) );
            } );
        }

		void HandleInput(float newValue) {

			if (newValue == sample_in) {
				return;
			}

			sample_in = newValue;
			if (write == 1) {
				Debug.Log("save value to index "+ index);
			}
		}

		void FetchIndex(float newValue) {

			if ((int)Mathf.Floor(newValue) == index) {
				return;
			}
			
			index = (int)Mathf.Floor(newValue);
			Debug.Log("update index");
		}

		void ClearAll() {
			Debug.Log("clear the memory and update output.");
		}

		void RefreshOutput() {
			//refresh output every frame,
		}

        public override void OnUpdate () {

			writePulse.ForPulse( write, () => Debug.Log("save value to index "+ index) );
			clear_allPulse.ForPulse( clear_all, () => ClearAll() );

			RefreshOutput();
			io.Produce< float >( "sample_out", sample_out );
        }

    }
}
