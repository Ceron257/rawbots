using UnityEngine;
using System.Collections;
using System.Linq;
using Oracle;

namespace Operants {
    public class CoreProxy : NodeProxy {

        IOComponent io;
        Core core;

        public CoreProxy () {
            io = new IOComponent( this );
            components.Add( io );
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            CreateView();
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "size", 
                                 ( outbox, value ) => 
                                 ForView< Core >( ref core, () => core.SetSize(Mathf.Clamp(value,1,100) ) ) 
                                 );
                inbox.For< float >( property == "face_0",
                                   ( outbox, value) =>
                                   ForView< Core >( ref core, () => core.SetFaceState(0,Mathf.Clamp01(value) ) )
                                   );
                inbox.For< float >( property == "face_1",
                                   ( outbox, value) =>
                                   ForView< Core >( ref core, () => core.SetFaceState(1,Mathf.Clamp01(value) ) )
                                   );
                inbox.For< float >( property == "face_2",
                                   ( outbox, value) =>
                                   ForView< Core >( ref core, () => core.SetFaceState(2,Mathf.Clamp01(value) ) )
                                   );
                inbox.For< float >( property == "face_3",
                                   ( outbox, value) =>
                                   ForView< Core >( ref core, () => core.SetFaceState(3,Mathf.Clamp01(value) ) )
                                   );
                inbox.For< float >( property == "face_4",
                                   ( outbox, value) =>
                                   ForView< Core >( ref core, () => core.SetFaceState(4,Mathf.Clamp01(value) ) )
                                   );
                inbox.For< float >( property == "face_5",
                                   ( outbox, value) =>
                                   ForView< Core >( ref core, () => core.SetFaceState(5,Mathf.Clamp01(value) ) )
                                   );
            } );
        }

    }
}
