using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Oracle;

namespace Operants {

    public class PartProxy : NodeProxy {

        public static List< PartProxy > allParts = new List< PartProxy >();

        public static IEnumerable< PartProxy > All () {
            for ( var i = 0; i < allParts.Count; ) {
                var part = allParts[ i ];
                if ( part == default( PartProxy ) ) {
                    allParts.RemoveAt( i );
                }
                else {
                    yield return part;
                    ++i;
                }
            }
        }

        struct JointExtension {
            public BulletGeneric6DofConstraint joint;
            public GameObject otherPart;
        }

        struct Extension {
            public EdgeView edgeView;
            public SlotProxy tailSlot;
            public SlotProxy headSlot;
        }

        List< Extension > extensions = new List< Extension >();
        List< JointExtension > jointExtensions = new List< JointExtension >();
        public IOComponent io;
        public GameObject auxiliar;
        public bool isRoot = false;
        public bool inHypercube = false;
        protected string passcode = string.Empty;
        protected float energy = 1000;
        protected float maxEnergy;

        Part view;
        Pulse disconnectPulse = new Pulse();

        public string Passcode { get { return passcode; } }

        public virtual void ResetMovablePart () {
        }

        public PartProxy () {
            io = new IOComponent( this );
            components.Add( io );
            allParts.Add( this );
            updates = true;
        }

        public void unList()
        {
            allParts.Remove(this);
        }

        public override void OnUpdate () {
            io.Produce< PartProxy >( "part", this );
            io.Produce< float >( "health", (float)energy );
        }

        public override void OnDestroy()
        {
            //Console.PushError("Destroyed", new string[] { this.ToString() });
        }





        //I altered how health (energy) works. -z26
        public virtual float Energy () {
            return energy;
        }

        public virtual float MaxEnergy () {
            return maxEnergy;
        }

        public void OnHit ( float damage ) {
            DrainEnergy( damage );
        }

        public virtual void DrainEnergy ( float amount ) {
            float drain = amount;
            if (amount > maxEnergy) {//Need at least 2 hits to destroy a part, cause I like grenade showers. -z26
                drain = maxEnergy;
            }

            if ( energy < 1 ) {//Destroy if a part has no hp even before the hit.
                Disconnect();
                Builder.instance.DeleteCluster(entity, false, false);
            }

            energy -= drain;
            energy = Mathf.Clamp(energy, 0, maxEnergy);

            if ( energy < 1 ) {//Disconnect if hp below 1.
                Disconnect();
            }
        }

        public void EnergyToSavefile () {
            if (!Mathf.Approximately(energy, maxEnergy)) {//pointless to store if at full health.
                entity.SetProperty("energy",energy.ToString("#"));
            }
        }
        
        public virtual void EnergyRecharge ( float amount ) {
            energy += amount;
            energy = Mathf.Clamp(energy, 0, maxEnergy);
        }









        public override void OnPreBuild () {
            base.OnPreBuild();
            entity.ForProperty< float >( "hyper_space", hyper_space => inHypercube = ( hyper_space == 1 ) );
            if ( !inHypercube ) {
                CreateView();
            }



            //Find if the health of the part was stored in the savefile.
            bool energyWasSaved = false;
            float savedEnergyValue = 0;
            entity.ForInferredProperty< float >( "energy", Util.AsIs, value => {
                energyWasSaved = true;
                savedEnergyValue = value;
            } );

            //To know the actual max health of a part, I delete any stored health value, so that reading the property
            //gets the default value for this type of part. Even if the health in the savefile is higher (cheaters)
            //or lower (part already damaged), I still know the true value.
            entity.RemoveProperty("energy");
            entity.ForInferredProperty< float >( "energy", Util.AsIs, value => {
                maxEnergy = value;
                energy = value;
            } );
            
            //restore the energy property to the saved value.
            if(energyWasSaved && savedEnergyValue > maxEnergy) {
                energy = savedEnergyValue;
                EnergyToSavefile();
            }


            entity.ForProperty<string>( "passcode", value => passcode = value.Substring( 1 ) );
        }

        public override void OnViewDestroyed()
        {
            base.OnViewDestroyed();
        }

        public void SetSlots () {
            foreach ( var slot in Slots() ) {
                ( slot as SlotProxy ).OnSlot();
            }
        }

        public void RemoveUnUsedSlots () {
            foreach ( var slot in Slots() ) {
                ( slot as SlotProxy ).RemoveUnUsedSlot();
            }
        }

//        public override void OnBuild () {
//            base.OnBuild();
//        }

        public override void OnBuild () {
            base.OnBuild();
            isRoot = true;
            UnityEngine.Profiling.Profiler.BeginSample( "createVoltaicArcs" );
            if ( !inHypercube ) {
                ForView< Part >( ref view, () => {
					CreateVoltaicArcsAndJoints();
                } );
            }
            UnityEngine.Profiling.Profiler.EndSample();
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "disconnect", ( outbox, value ) => {
                    disconnectPulse.ForPulse( value, () => {
                        ForView< Part >( ref view, () => {
                            var builder = Gaia.Instance().GetComponent< Builder >();
                            builder.Disconnect( view, false );
                        } );
                    } );
                } );
                inbox.For< Color >( property == "color_a", ( outbox, value ) => {
                    ForView< Part >( ref view, () => view.ApplyChannel( "_ChannelA", value ));
                } );
                inbox.For< Color >( property == "color_b", ( outbox, value ) => {
                    ForView< Part >( ref view, () => view.ApplyChannel( "_ChannelB", value ) );
                } );
                inbox.For< Color >( property == "highlight", ( outbox, value ) => {
                    ForView< Part >( ref view, () => view.ApplyChannel( "_ChannelC", value ) );
                } );
                inbox.For< Color >( property == "glow", ( outbox, value ) => {
                    ForView< Part >( ref view, () => view.ApplyChannel( "_IllumTint", value ) );
                } );
                inbox.For< string >( property == "comment", ( outbox, value ) => {
                    //nodeView.GetComponent<>
                } );
            } );
        }

        public void SetPasscode ( string code ) {
            passcode = code;
            entity.SetProperty( "passcode", "#" + passcode );
        }

        public bool IsPasscodeSet () {
            return passcode != string.Empty;
        }

        public void CreateVoltaicArcsAndJoints () {
            var extends = Slots().SelectMany( s => s.entity.Relationships()
                .Where( r => r.Predicate == "extends" && r.Tail == s.entity ) );
            foreach ( var extend in extends ) {
                var tailSlot = extend.Tail.proxy as SlotProxy;
                var headSlot = extend.Head.proxy as SlotProxy;

                if ( tailSlot == null ) {
                    return;
                }
                if ( headSlot == null ) {
                    return;
                }


                var extensionExist = extensions.Any( ext => ( ext.headSlot == headSlot && ext.tailSlot == tailSlot ) );
                if ( !extensionExist ) {
                    var arcObj = UnityEngine.GameObject.Instantiate( Resources.Load( "Bot/VoltaicArc", typeof( GameObject ) ) ) as GameObject;


                    tailSlot.OnSlot();
                    headSlot.OnSlot();
                    AddJointsToParts( tailSlot, headSlot );
                    arcObj.transform.parent = extend.Tail.nodeView.transform;
                    var arc = arcObj.GetComponent< VoltaicArc >();
                    arc.edgeView.tail = extend.Tail.nodeView;
                    arc.edgeView.head = extend.Head.nodeView;
                    var extension = new Extension();
                    extension.edgeView = arc.edgeView;
                    extension.tailSlot = extend.Tail.proxy as SlotProxy;
                    extension.headSlot = extend.Head.proxy as SlotProxy;
                    extensions.Add( extension );
                }
            }
        }

         void OnNeighborDisconnect ( PartProxy neighbor ) {
            SearchAndDisconnect( neighbor );
            RemoveUnUsedSlots();
            neighbor.RemoveUnUsedSlots();
        }

        void OnRemoveJointsAndExtends ( PartProxy neighbor ) {
            SearchAndDetach( neighbor );
        }

        public void RemoveJointsAndExtends () {
            foreach ( var neighbor in Neighbors() ) {
                neighbor.OnRemoveJointsAndExtends( this );
                SearchAndDetach( neighbor );
            }
        }

        public virtual void Disconnect () {
            var part = nodeView.GetComponent<Part>();
            part.Disconnect();
            var parts = Parts().ToList();
            if(parts.Count > 1){
            foreach ( var neighbor in Neighbors() ) {
                neighbor.OnNeighborDisconnect( this );
                SearchAndDisconnect( neighbor );
            }
            foreach ( var p in parts ) {
                p.RemoveUnUsedSlots();
            }
            RecalculateClusters( parts );
            }
        }

        public virtual void DisconnectSingle(Part second)
        {
            var subpartEntity = second.proxy.entity.Tails(e => e == "linked_to").FirstOrDefault();
            PartProxy subpartProxy = default(PartProxy);
            PartProxy masterPartProxy = default(PartProxy);

            if (subpartEntity != default(Entity))
            {
                subpartProxy = subpartEntity.proxy as PartProxy;
            }
            var masterPartEntity = second.proxy.entity.Heads(e => e == "linked_to").FirstOrDefault();
            if (masterPartEntity != default(Entity))
            {
                masterPartProxy = masterPartEntity.proxy as PartProxy;
            }                                                                   //Get proxies for all subparts


            var part = nodeView.GetComponent<Part>();
            part.Disconnect();
            var parts = Parts().ToList();
            if (parts.Count > 1)
            {
                foreach (var neighbor in Neighbors())
                {
                    if(neighbor == second.proxy)
                    {
                        neighbor.OnNeighborDisconnect(this);
                        SearchAndDisconnect(neighbor);
                    }
                    else if ((neighbor == subpartProxy) && (subpartProxy != default(PartProxy)))
                    {
                        neighbor.OnNeighborDisconnect(this);
                        SearchAndDisconnect(neighbor);
                    }
                    else if ((neighbor == masterPartProxy) && (masterPartProxy != default(PartProxy)))
                    {
                        neighbor.OnNeighborDisconnect(this);
                        SearchAndDisconnect(neighbor);
                    }

                }
                foreach (var p in parts)
                {
                    p.RemoveUnUsedSlots();
                }
                RecalculateClusters(parts);
            }
        }

        public void SearchAndDetach ( PartProxy neighbor ) {
            var relationshipsToRemove = new List<Relationship>();
            if ( CheckForExtendees( neighbor, out relationshipsToRemove ) ) {
                DestroyExtends( relationshipsToRemove );
                RemoveJoints( neighbor );
            }
        }

         void SearchAndDisconnect ( PartProxy neighbor ) {
            var relationshipsToRemove = new List<Relationship>();
            if ( CheckForExtendees( neighbor, out relationshipsToRemove ) ) {
                DestroyExtends( relationshipsToRemove );
                DestroyRelationships( relationshipsToRemove );
                RemoveJoints( neighbor );
            }
        }

        bool CheckForExtendees ( PartProxy neighbor, out List<Relationship> relationships ) {
            relationships = neighbor.Slots().SelectMany( s => s.entity.Relationships()
                .Where( r => r.Predicate == "extends" && entity.Tails( p => p == "slot_for" )
                .Any( t => t == r.Tail ) && neighbor.entity.Tails( pn => pn == "slot_for" )
                .Any( h => h == r.Head ) ) ).ToList();
            return relationships.Count > 0 ? true : false;
        }

        void DestroyExtends ( List< Relationship > relationships ) {
            relationships.ForEach( r => {
                var extension = extensions.Where( e => e.tailSlot == r.Tail.proxy && e.headSlot == r.Head.proxy ).ToList();
                foreach ( var e in extension ) {
                    GameObject.Destroy( e.edgeView.gameObject );
                    extensions.Remove( e );
                }
            } );
        }

        void DestroyRelationships ( List< Relationship > relationships ) {
            relationships.ForEach( r => r.Break() );
        }

        void  RemoveJoints ( PartProxy neighbor ) {
//            var neighborParts = neighbor.nodeView.GetComponent< Part >()
//                .subparts.Select( s => s.gameObject )
//                .ToList();

            //var neighborParts = new List<GameObject>();
            //neighborParts.Add( neighbor.nodeView.gameObject );
            var jointsToDestroy = jointExtensions.Where( j => j.otherPart == neighbor.nodeView.gameObject ).ToList();
            jointsToDestroy.ForEach( j => {
              j.joint.RemoveBulletConstraint();
                GameObject.Destroy( j.joint );
                jointExtensions.Remove( j );
            } );
        }

        public void AddJointsToParts ( SlotProxy slotA, SlotProxy slotB ) {

            var objectA = slotA.ownerGameObject;
            var objectB = slotB.ownerGameObject;

            var connect = true;

            if ( jointExtensions.Any( j => j.otherPart == objectB ) ) {
                connect = false;
            }
            if(connect){
                var constraint = slotA.nodeView.gameObject.AddComponent<BulletGeneric6DofConstraint>();
                constraint.bulletRbA = objectA.GetComponent<BulletRigidBody>();
                constraint.bulletRbB = objectB.GetComponent<BulletRigidBody>();

                var engineA = constraint.bulletRbA.GetComponent<Engine>();
                var engineB = constraint.bulletRbB.GetComponent<Engine>();

                if(engineA != null || engineB != null){

                    constraint.solverIterations = 100;

                }else{
                    constraint.solverIterations = 80;
                }



                constraint.rbAPos = objectA.transform.position;
                constraint.rbBPos = objectB.transform.position;
                constraint.rbBRot = objectB.transform.rotation;
                constraint.rbARot = objectA.transform.rotation;

                //constraint.solverIterations = 20;
                constraint.useLinearReferenceFrameA = false;
                constraint.disableCollisionBetweenBodies = true;

                constraint.angularERPValue = Vector3.one*0.8f;
                constraint.linearERPValue = Vector3.one*0.8f;

                constraint.angularCFMValue = Vector3.zero; //Vector3.one*0.01f;
                constraint.linearCFMValue = Vector3.zero;//Vector3.one*0.01f;

                constraint.Initialize();

                var extension = new JointExtension();
                extension.otherPart = objectB;
                extension.joint = constraint;
                jointExtensions.Add( extension );
            }


        }

        public IEnumerable< SlotProxy > Slots () {
            return this.entity.Tails( p => p == "slot_for" ).Select( e => e.proxy ).Cast<SlotProxy>();
        }

        public IEnumerable< PartProxy > Neighbors () {//return immediate connections (connections between halves of actuators don't count)
            var slots = entity.Tails( p => p == "slot_for" );
            var extends = slots.SelectMany( s => s.Relationships().Where( r => r.Predicate == "extends" ) );
            var neighbors = extends.SelectMany( r => r.Entities().Where( e => !slots.Contains( e ) ) )
                .SelectMany( e => e.Relationships()
                .Where( r => r.Predicate == "slot_for" ) )
                .Select( r => r.Head.proxy ).Where( p => p is PartProxy ).Cast<PartProxy>().ToList();
//            entity.Relationships().Where(r => r.Predicate == "linked_to").ToList().ForEach(r => {
//                var e = r.Head == entity ? r.Tail : r.Head;
//                neighbors.Add(e.proxy as PartProxy);
           // });
            return neighbors;
        }

        public IEnumerable<PartProxy> GridParts () {//return itself + indirect connections of all types (even indirect connections by
            var dependants = new HashSet< Entity >();//code operants, but the operants themselves are not returned)
            Hyperspace.Entities( entity, dependants,new string[]{"input_for","output_for","extends","slot_for","linked_to","consumes"} );
            return dependants.Select( d => d.proxy )
                .Where( p => p != null && p is PartProxy ).Cast<PartProxy>();
        }

        public IEnumerable< PartProxy > Parts () {//indirect version of Neighbors(), i think. Connections between actuators halves are not followed.
            var parts = new List< PartProxy >();
            foreach ( var p in Parts( parts ) ) {
                yield return p;
            }
        }

        IEnumerable< PartProxy > Parts ( List< PartProxy > parts ) {
            if ( !parts.Contains( this ) ) {
                parts.Add( this );
                yield return this;
                foreach ( var n in Neighbors() ) {
                    foreach ( var p in n.Parts( parts ) ) {
                        yield return p;
                    }
                }
            }
        }

        public IEnumerable<PartProxy> PhysicallyLinked () {//by z26, generic function to use anywhere.
            var dependants = new HashSet< Entity >();//TODO: Add option to filter out subparts
            Hyperspace.Entities( entity, dependants,new string[]{"extends","slot_for","linked_to"} );
            var output = dependants.Select( d => d.proxy )
                .Where( p => p != null && p is PartProxy ).Cast<PartProxy>();
            
            return output;
        }

        //by z26. If there is any actuators in the input IEnumerable, only the "base" half is kept while the other half is discarded.
        //This is used so that cameras don't count actuators twice when counting how many parts they see.
        public static IEnumerable<PartProxy> ExcludeSubparts (IEnumerable<PartProxy> parts) {
            var subparts = parts.Where(p => DetectIfSubpart(p.entity));
            // foreach(var sp in subparts) {
            //     Debug.Log(sp);
            // }
            return parts.Except(subparts);
        }
        public static bool DetectIfSubpart(Entity entity) {
            return entity.Relationships().Any (r => r.Predicate == "linked_to" && r.Tail == entity);
            }



        // TODO: should be IEnumerable< SlotProxy >
        public List< SlotProxy > GetAvailableSlots () {
            return Slots().Where( sl => {
                var isEmpty = !sl.entity.Heads( p => p == "extends" ).Any();
                return isEmpty = isEmpty && !sl.entity.Tails( p => p == "extends" ).Any();
            } ).ToList();
        }

        public void SetAsGroupPositionReference () {
            var cluster = Parts().ToList();
            SetAsGroupPositionReference( cluster );
        }

        // TODO: has to be public?
        public void SetAsGroupPositionReference ( List<PartProxy> cluster ) {

            var masterPos = Vector3.zero;
            var masterRot = Quaternion.identity;

            entity.ForProperty< string >( "position", position => {
                masterPos = Util.PropertyToVector3( position );
            } );

            entity.ForProperty< string >( "rotation", rotation => {
                masterRot = Quaternion.Euler( Util.PropertyToVector3( rotation ) );
            } );

            var masterDummy = new GameObject();
            masterDummy.transform.position = masterPos;
            masterDummy.transform.rotation = masterRot;

            foreach ( var partProxy in cluster ) {

                var slavePos = Vector3.zero;
                var slaveRot = Quaternion.identity;

                partProxy.entity.ForProperty< string >( "position", position => {
                    slavePos = Util.PropertyToVector3( position );
                } );

                partProxy.entity.ForProperty< string >( "rotation", rotation => {
                    slaveRot = Quaternion.Euler( Util.PropertyToVector3( rotation ) );
                } );


                var newSlavePos = masterDummy.transform.InverseTransformPoint( slavePos );
                var newSlaveRot = Quaternion.Inverse( masterRot ) * slaveRot;

                
                partProxy.entity.SetProperty( "position", Util.Vector3ToProperty( newSlavePos ) );
                partProxy.entity.SetProperty( "rotation", Util.Vector3ToProperty( newSlaveRot.eulerAngles ) );
            }
            GameObject.Destroy( masterDummy );
        }

        public void RecalculateClusters () {
            RecalculateClusters( Parts().ToList() );
        }

        void RecalculateClusters ( List<PartProxy> cluster ) {
            List<List<PartProxy>> clusters = new List<List<PartProxy>>();
            var local_to = "local_to";
            for ( int i = 0; i < cluster.Count; ++i ) {
                var part = cluster[ i ];
                if ( clusters.Any( c => c.Contains( part ) ) ) {
                    continue;
                }
                var newCluster = part.Parts().ToList();
                clusters.Add( newCluster );
            }

            for ( int i = 0; i < clusters.Count; ++i ) {
                var centerPart = GetCenterPart( clusters[ i ] );
                centerPart.SetAsGroupPositionReference( clusters[ i ] );
                var spawn = new Entity( "a_" + ( ( uint )centerPart.GetHashCode() ).ToString() );
                Gaia.Instance().recentlyAddedEntities.Add( spawn );
                new Relationship( spawn, local_to, Gaia.Instance().root );
                spawn.SetProperty( "position", Util.Vector3ToProperty( centerPart.nodeView.transform.position ) );
                spawn.SetProperty( "rotation", Util.Vector3ToProperty( centerPart.nodeView.transform.eulerAngles ) );
                new Relationship( spawn, "as", Gaia.Instance().entities[ "xform" ] );
                for ( int j = 0; j < clusters[i].Count; ++j ) {
                    var currentPart = clusters[ i ][ j ];
                    currentPart.entity.Relationships()
                        .Where( r => r.Tail == currentPart.entity && r.Predicate == local_to ).ToList()
                        .ForEach( r => r.Break() );
                    new Relationship( currentPart.entity, local_to, spawn );
                }
            }
        }
        
        public static PartProxy GetCenterPart ( List<PartProxy> cluster ) {
            var center = Vector3.zero;
            var count = 0;

            foreach ( var part in cluster ) {
                ++count;
                center += part.nodeView.transform.position;
            }
            center /= count;
            var closestDistance = float.MaxValue;
            var closestPart = default( PartProxy );
            foreach ( var part in cluster ) {
                var distance = ( part.nodeView.transform.position - center ).sqrMagnitude;
                if ( distance < closestDistance ) {
                    closestDistance = distance;
                    closestPart = part;
                }
            }
            return closestPart;
        }

        public static Vector3 GetCenter ( List<Operants.PartProxy> cluster ) {
            Vector3 center = Vector3.zero;
            var count = 0;
            foreach ( var part in cluster ) {
                ++count;
                center += part.nodeView.transform.position;
            }
            center /= count;
            return center;
        }

        public bool Extends ( PartProxy other ) {
            //No linq because is used on every update
            var isConnected = false;
            for ( var i = 0; i < extensions.Count; ++i ) {
                var head = extensions[ i ].headSlot;
                if ( head != default(SlotProxy) ) {
                    if ( head.ownerPartProxy == other ) {
                        isConnected = true;
                        break;
                    }
                }
            }
            return isConnected;
        }

        public bool IsConnected ( PartProxy other ) {
            return ( this.Extends( other ) || other.Extends( this ) );
        }

        public void EnableInputs () {
            
        }

    }
}
