using UnityEngine;
using System.Collections;
using System.Linq;

namespace Operants {

    public class EnergyBridgeProxy : StructureProxy {

        EnergyBridge energyBridge;

        public EnergyBridgeProxy () {
            //updates = true;
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "power", ( outbox, value ) => {
                    ForView< EnergyBridge >( ref energyBridge, () => {
                        energyBridge.SwitchState( value );
                    } );
                } );
				inbox.For< float >( property == "length", ( outbox, value ) => {
                    ForView< EnergyBridge >( ref energyBridge, () => {
                        energyBridge.length = value;
                    } );
                } );
				inbox.For< float >( property == "width", ( outbox, value ) => {
                    ForView< EnergyBridge >( ref energyBridge, () => {
                        energyBridge.width = value;
                    } );
                } );
            } );
        }
		
		
		public override void OnPreBuild ()
		{
			base.OnPreBuild ();
			if (nodeView != null) {

			}
			
			entity.ForInferredProperty< string > ("scale", Util.AsIs, scale => {
				var initialScale = Util.PropertyToVector3 (scale);
				
				ForView< EnergyBridge > (ref energyBridge, () => {
					
					energyBridge.SetInitialScale(initialScale);
					
				});
			});

			
			
		}
		
		
		
//        public override void OnUpdate () {
//            base.OnUpdate();
//            var length = 0.0f;
//            ForView< EnergyBridge >( ref energyBridge, () => {
//                length = energyBridge.length;
//            } );
//            io.Produce< float >( "length", length );
//        }
    }
}