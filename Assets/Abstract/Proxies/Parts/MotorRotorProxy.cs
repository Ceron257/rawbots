using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Operants {

    public class MotorRotorProxy : PartProxy {
        
        public MotorRotorProxy () {
            updates = false;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            
        }

        public override void OnPostBuild () {
            base.OnPostBuild();
        }

        public override void OnViewDestroyed () {
            base.OnViewDestroyed();
        }





        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
        }

        public override void OnInboxDestroy ( Inbox inbox ) {
            base.OnInboxDestroy( inbox );
        }
    }
    
}
