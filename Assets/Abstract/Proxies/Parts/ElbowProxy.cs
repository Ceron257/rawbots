using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Operants {

    public class ElbowProxy : PartProxy {

        public Elbow elbow;

        public ElbowProxy () {
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            if ( nodeView != null ) {
                elbow = nodeView.GetComponent<Elbow>();
                elbow.rotorObject.transform.parent = elbow.gameObject.transform.parent;

                elbow.producePartAngle = angle => io.Produce( "angle", angle );
                auxiliar = elbow.rotorObject;
            }
        }



        public override void OnViewDestroyed () {
            base.OnViewDestroyed();
        }

        public override void StorePosition () {
            ForView< Elbow >( ref elbow, () => {
                elbow.StoreElbowStatus();
            } );
        }



        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued<float>( property == "angle", ( outbox, value ) => {

                    ForView< Elbow >( ref elbow, () => {
                        elbow.SetElbowAngle(Util.Sum(value) );
                    } );
                } );
                inbox.ForQueued<float>( property == "force", ( outbox, value ) => {

                } );
                inbox.ForQueued<float>( property == "velocity", ( outbox, value ) => {
                    ForView< Elbow >( ref elbow, () => {
                        elbow.SetElbowVelocity(Util.Sum(value) );
                    } );
                } );
            } );
        }

        public override void OnInboxDestroy ( Inbox inbox ) {
            base.OnInboxDestroy( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                Util.ForPredicate( property == "angle", () => {
                    ForView< Elbow >( ref elbow, () => {
                        elbow.motor.Stop();
                    } );
                } );
                Util.ForPredicate( property == "velocity", () => {
                    ForView< Elbow >( ref elbow, () => {
                        elbow.motor.Stop();
                    } );
                } );
            } );
        }

        public override void ResetMovablePart () {
            ForView< Elbow >( ref elbow, () => {
                elbow.ResetElbowJoint();
            } );
        }
    }
    
}
