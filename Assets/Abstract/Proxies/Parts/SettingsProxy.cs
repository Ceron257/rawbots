using UnityEngine;
using System.Collections;

namespace Operants {

    public class SettingsProxy : NodeProxy {

        public IOComponent io;
        string remoteIP = "127.0.0.1";
        int listenPort = 8000;
        int sendPort = 9000;

        public SettingsProxy () {
            io = new IOComponent( this );
            components.Add( io );
            Gaia.Instance().SetupOSC( remoteIP, listenPort, sendPort );
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "sound_volume", ( outbox, value ) => {
                } );
                inbox.For< float >( property == "music_volume", ( outbox, value ) => {
                } );
                inbox.For< float >( property == "daytime", ( outbox, value ) => {
                    DayCycle.globalTime = value;
                } );
                inbox.For< float >( property == "osc_incoming_port", ( outbox, value ) => {
                    var port = ( int )value;
                    if ( port >= 8000 && port < 9000 ) {
                        listenPort = port;
                        Gaia.Instance().SetupOSC( remoteIP, listenPort, sendPort );
                    }
                    else {
                        Console.PushError("osc_incoming_port", new string[]{ "invalid port, use within [8000,8999]" } );
                    }
                } );
            } );
        }

        void StartHostingSession () {

//             Network.incomingPassword = "";
//            bool useNat = !Network.HavePublicAddress();
            // var useNat = false;
            // Network.InitializeServer( 32, 57668, useNat );
        }

    }
}
