using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Operants {

        public enum LaserRange{ 
        Short,
        Medium,
        Long,
        Pointer
    }

    public class LaserProxy : PartProxy {

        Laser laser;
        float[] ranges = new[]{3f,10f,50f,100f};
        int[] damages = new[]{90,35,5,0};
        float[] thicknesses = new[]{2f,1f,0.5f,0.25f};


        public LaserProxy () {
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
             if ( nodeView != null ) {
                laser = nodeView.GetComponent<Laser>();
                laser.maxLength = ranges[(int)LaserRange.Medium];
                laser.damageEachFrame = damages[(int)LaserRange.Medium];
                laser.thickness = thicknesses[(int)LaserRange.Medium];
            }
            
        }

        public override void OnPostBuild () {
            base.OnPostBuild();
            //ForView<Laser>( ref laser, () => laser.beam.transform.parent = laser.transform.parent );
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued<float>( property == "length", ( outbox, value ) => {
                    ForView< Laser >( ref laser, () => laser.inputLength =  Util.Sum( value )  );
                } );
                inbox.For<Color>( property == "color", (outbox,value) => {
                    ForView< Laser >( ref laser, () => laser.beamColor = value );
                } );
                inbox.For< string >( property == "mode", ( outbox, value ) => {
                var multiplier = ( LaserRange )System.Enum.Parse( typeof( LaserRange ), value.Split( '.' ).Last() );
                laser.maxLength = ranges[(int)multiplier];
                laser.damageEachFrame = damages[(int)multiplier];
                laser.thickness = thicknesses[(int)multiplier];
                } );
            } );
        }

        public override void OnUpdate () {
            base.OnUpdate();
            ForView< Laser >( ref laser, () => io.Produce< float >( "length", laser.currentLength));
            io.Produce< float > ("damage", laser.damageEachFrame);
            io.Produce( "detected", laser.detected );
        }
    }
}
