using UnityEngine;
using System.Collections;


namespace Operants {

    public class HoloProjectorProxy : PartProxy {

        HoloProjector projector;
        public System.Action<float> SetIntensity = (intensity) => {};


        public HoloProjectorProxy () {
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            ForView<HoloProjector>( ref projector, () => {
                SetIntensity = projector.SetIntensity;
            } );
        }

        public override void OnViewDestroyed () {
            base.OnViewDestroyed();
            SetIntensity = (intensity) => {};
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "intensity", ( outbox, value ) => {
                    this.SetIntensity( value );} );
                inbox.ForQueued< NodeProxy >(property == "label", ( outbox, value ) => ForView<HoloProjector>(
                    ref projector, () => value.ForEach(v => projector.inFrameLabelProxies.Add((LabelProxy) v) )) );
                inbox.ForQueued< NodeProxy >(property == "texture", ( outbox, value ) => ForView<HoloProjector>(
                    ref projector, () => value.ForEach(v => projector.inFrameTextureProxies.Add((TextureProxy) v) )) );
            } );
        }

    }
}
