using UnityEngine;
using System.Collections;

namespace Operants
{
	public class HookProxy : PartProxy
	{

		
		public Vector3 hookPoint;
		public Quaternion hookRotation;
		public bool attached;
		
		public HookProxy (){
			updates = true;
		}

		Hook hook;

		public override void OnPreBuild ()
		{
			base.OnPreBuild ();
			if (nodeView != null) {

			}
			
			entity.ForInferredProperty< string > ("hook_point", Util.AsIs, hook_point => {
				hookPoint = Util.PropertyToVector3 (hook_point);
			});
			entity.ForInferredProperty< string > ("hook_rotation", Util.AsIs, hook_rotation => {
				hookRotation = Quaternion.Euler( Util.PropertyToVector3 (hook_rotation));
			});
			
			entity.ForInferredProperty< float > ("attached", Util.AsIs, isAttached => {
				attached = (isAttached == 0) ? false : true;
			});
			
			
		}

		public override void OnPostBuild ()
		{
			base.OnPostBuild ();
            
            if (attached)
            {
                ForView<Hook>(ref hook, () =>
                {
                    var distance = Vector3.Magnitude(hook.transform.position - hookPoint);  //Get distance between current point and last recorded position
                    if (distance < 10)                          //Only attach if hook hasn't moved (i.e. been blueprinted)
                    {
                        hook.attachAtBeginning = true;
                        hook.initialAttachedPos = hookPoint;
                        hook.initialAttachedRot = hookRotation;
                    }
                });
            }
        }

		public override void OnViewDestroyed ()
		{
			base.OnViewDestroyed ();
		}

		public override void OnInbox (Inbox inbox)
		{
			base.OnInbox (inbox);
			inbox.Proxy.entity.ForInferredProperty< string > ("property", Util.AsIs, property => {
				inbox.For< float > (property == "attach", ( outbox, value ) => {
					ForView< Hook > (ref hook, () => {
						var jointState = Mathf.RoundToInt (Mathf.Clamp01 (value));
						hook.SetJointState (jointState);
					});
				});

			});
		}

		public override void OnUpdate ()
		{
			base.OnUpdate ();



			ForView< Hook > (ref hook, () => {

				io.Produce< float > ("distance", hook.distance);
				io.Produce< float > ("angle", hook.angle);
				io.Produce< float > ("attached", hook.isActive ? 1 : 0);

			});

           
		}


	}

}
