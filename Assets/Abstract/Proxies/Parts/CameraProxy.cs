using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Operants {

    public class CameraProxy : PartProxy {

        CameraView camera;

        public CameraProxy () {
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            ForView< CameraView >( ref camera, () => {
                camera.produceActivity = value => io.Produce( "activity", value );
                camera.produceVisiblePart = value => io.Produce( "detected", value );
            } );
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "fov", ( outbox, value ) =>
                    ForView< CameraView >( ref camera, () => camera.SetFOV( value ) ) );
                inbox.For< float >( property == "size", ( outbox, value ) =>
                    ForView< CameraView >( ref camera, () => camera.SetSize( value ) ) );
                inbox.For< float >( property == "near", ( outbox, value ) =>
                    ForView< CameraView >( ref camera, () => camera.SetNearPlane( value ) ) );
                inbox.For< float >( property == "far", ( outbox, value ) =>
                    ForView< CameraView >( ref camera, () => camera.SetFarPlane( value ) ) );
            } );
        }

    }
 
}