using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Oracle;

namespace Operants {

    public class HyperCubeProxy : PartProxy {

        HyperCube cube;
        Pulse pipePulse = new Pulse();
        Pulse popPulse = new Pulse();
        PartProxy pipePart;

        public HyperCubeProxy () {
            updates = true;
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {

                inbox.For< float >( property == "pipe_pop", ( outbox, value ) => {
                    ForView< HyperCube >( ref cube, () => {

                            pipePulse.ForPulse( value, () => {
//                                var localToEntity = bag[ 0 ];
//                                var pickedPart = localToEntity.Tails( p => p == "local_to" ).FirstOrDefault();
//                                pipePart = pickedPart.proxy as PartProxy;
//                                bag.RemoveAt( 0 );

                                var containedEntity = entity.Tails( p => p == "contained_in" ).FirstOrDefault();
                                if ( containedEntity != default(Entity) ) {
                                    var pickedPart = containedEntity.Tails( p => p == "local_to" ).FirstOrDefault();
                                    pipePart = pickedPart.proxy as PartProxy;
                                }

                            } );

                    } );
                } );

                inbox.For< float >( property == "pop", ( outbox, value ) => {

                        popPulse.ForPulse( value, () => {
                            ForView< HyperCube >( ref cube, () => {
                                var containedEntity = entity.Tails( p => p == "contained_in" ).FirstOrDefault();
                                if ( containedEntity != default(Entity) ) {
                                    cube.Pop( containedEntity );
                                }

                            } );
                        } );

                } );

            } );
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
        }

        public override void OnUpdate () {
            base.OnUpdate();
            io.Produce< PartProxy >( "pipe", pipePart );
            pipePart = default( PartProxy );
        }

    }


}
