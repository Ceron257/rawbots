using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Operants {

    public class TextureProxy : NodeProxy {
        IOComponent io;
        public Vector3 position = Vector3.zero;
        public Vector3 size = Vector3.one;
        public string url = string.Empty;
        public bool tryLocal = false;
        public bool refresh = false;
        public bool isMovie = false;

        public TextureProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }
        
        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< string >( property == "url", ( outbox, value ) =>  ValidateUrl(value));
                inbox.For< string >( property == "position", ( outbox, value ) => ValidatePosition(value) );
                inbox.For< float >( property == "width", ( outbox, value ) =>  size.x = Mathf.Clamp(value,0.5f,2) * 4 );
                inbox.For< float >( property == "height", ( outbox, value ) =>  size.z = Mathf.Clamp(value,0.5f,2) * 4 );
            } );
        }

        Regex urlRegex = new Regex( @"^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$", RegexOptions.Compiled );
        public void ValidateUrl(string value){
            tryLocal = !urlRegex.IsMatch(value);
            if(url != value){
                refresh = true;
            }
            url = value;
        }
        
        public void ValidatePosition(string value){
            char[] separator = {',',' ',};
            char[] brackets = {'[',']'};
            var values = value.Trim(brackets).Split(separator);
            float x = 0;
            float y = 0;
            float z = 0;
            bool success = false;

            if (values.Count() == 3) { success = true; }
            success = success && Single.TryParse(values[0], out x);
            success = success && Single.TryParse(values[1], out y);
            success = success && Single.TryParse(values[2], out z);

            if(success){
                position.x = Mathf.Clamp(x, -3 , 3)  * 4;
                position.y = Mathf.Clamp(y, -3 , 3) * 4;
                var temp = Mathf.Clamp( z , -3 ,3 );
                temp /= 2;
                position.z = position.y;
                position.y = temp;
            }else{
                Console.PushError("position",new string[]{"invalid format use: 0.0, 0.0, 0.0" });
            }
        }

        
        public override void OnUpdate () {
            io.Produce<NodeProxy>("texture",this);
        }

    }
    
}
