using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public enum HyperFieldType {
//        White,
        Black,
    }

    public class HyperFieldProxy : PartProxy {

        public List< Entity > bag = new List< Entity >();
        public HyperField hyperField;
        PartProxy pipePart;
        public HyperFieldType fieldType = HyperFieldType.Black;
        Pulse pipePulse = new Pulse();

        public HyperFieldProxy () {
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            fieldType = HyperFieldType.Black;
            pipePulse = new Pulse();
            bag.Clear();
            var proxies = entity.Tails( p => p == "contained_in" ).ToList();
            bag.AddRange( proxies );
        }

        public override void OnBuild () {
            base.OnBuild();
            if ( !IsPasscodeSet() ) {
                SetPasscode( UnityEngine.Random.Range( 0, 9999 ).ToString( "0000" ) );
            }
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                
                inbox.For< float >( property == "pipe_pop", ( outbox, value ) => {
                    ForView< HyperField >( ref hyperField, () => {
                        if ( bag.Count > 0 ) {
                            pipePulse.ForPulse( value, () => {
                                var localToEntity = bag[ 0 ];
                                var pickedPart = localToEntity.Tails( p => p == "local_to" ).FirstOrDefault();
                                pipePart = pickedPart.proxy as PartProxy;
                                bag.RemoveAt( 0 );
                            } );
                        }
                    } );
                } );

                inbox.ForQueued< float >( property == "size", ( outbox, value ) => {
                    ForView< HyperField >( ref hyperField, () => hyperField.SetSize( Util.Sum( value ) ) );
                } );

                inbox.For< string >( property == "type", ( outbox, value ) => {
                    fieldType = ( HyperFieldType )System.Enum.Parse(
                        typeof( HyperFieldType ), value.Split( '.' ).Last()
                    );
                } );

            } );
        }

        public override void OnUpdate () {
            base.OnUpdate();
            io.Produce< PartProxy >( "pipe", pipePart );
            pipePart = default( PartProxy );
            Capture();
        }

        void Capture () {
            ForView< HyperField >( ref hyperField, () => {
                if ( hyperField.partsToTransport.Count > 0 ) {
                    var partProxy = hyperField.partsToTransport[ 0 ];
                    hyperField.partsToTransport.RemoveAt( 0 );
                    if ( partProxy.nodeView != default( NodeView ) ) {
                        HyperspacedFX.Create( partProxy );
                        Hyperspace.DestroyPartsView( partProxy.nodeView.GetComponent< Part >(), this );
                    }
                }
            } );
        }
    }

}
