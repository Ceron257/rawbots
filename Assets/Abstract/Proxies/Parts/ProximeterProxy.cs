using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Oracle;

namespace Operants {

    public class ProximeterProxy : NodeProxy {

        IOComponent io;
        PartProxy proxyPartA;
        PartProxy proxyPartB;

        public ProximeterProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< PartProxy >( property == "part_a", ( output, value ) => proxyPartA = value );
                inbox.For< PartProxy >( property == "part_b", ( output, value ) => proxyPartB = value );
            } );
        }

        public override void OnUpdate () {
            var phyObjA = proxyPartA != null && proxyPartA.nodeView != null ?
                proxyPartA.nodeView.GetComponent< PhysicalObject >() : null;
            var phyObjB = proxyPartB != null && proxyPartB.nodeView != null ?
                proxyPartB.nodeView.GetComponent< PhysicalObject >() : null;
            float distance = 0f; //this will cause less bugs in player programs than 65535 -z26
            if ( phyObjA != null && phyObjB != null ) {
                distance = Vector3.Distance( phyObjA.bulletRigidbody.transform.position, phyObjB.bulletRigidbody.transform.position);
                proxyPartA = null;
                proxyPartB = null;
            }
            io.Produce< float >( "proximity", distance );
        }

    }
}
