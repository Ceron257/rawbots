using UnityEngine;
using System.Collections;
using System.Linq;

namespace Operants {

    public class GrenadeProxy : PartProxy {

        Grenade grenade;
        System.Action<float> tryExplode = (v) => {};

        public GrenadeProxy () {
        }


        public override void OnPreBuild () {
            base.OnPreBuild();
             if ( nodeView != null ) {
                grenade = nodeView.GetComponent<Grenade>();
            }      
        }

        public override void DrainEnergy (float amount) {
            energy -= amount;
            if ( energy < 1 ) {
                grenade.Explode();
                Disconnect();
                Builder.instance.DeleteCluster(entity, false, false);
            }
        }


        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "detonate", ( outbox, value ) => {
                    if(value > 0.5) { grenade.SelfDestruct();}
                } );
            } );
        }




    }
}
