using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class PlasmaCannonProxy : PartProxy {

        PlasmaCannon cannon;
        Pulse pulse = new Pulse();


        public override void OnPreBuild () {
            base.OnPreBuild ();
            pulse = new Pulse();
            ForView< PlasmaCannon >( ref cannon, () => cannon.produceShootForce = f => io.Produce<float>("shoot_force",f ) );
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "shoot", ( outbox, value ) => {
                    pulse.ForPulse( value, () => {
                        ForView< PlasmaCannon >( ref cannon, () => cannon.TryShoot() );
                    } );
                } );
                inbox.For< PartProxy >( property == "energy_source", ( outbox, value ) => {
                    ForView< PlasmaCannon >( ref cannon, () => {
                        cannon.inputProxy = value;
                    } );
                } );
            } );
        }

    }

}
