using UnityEngine;
using System.Collections;
using System.Linq;
 
namespace Operants {

    public class SpotLightProxy : PartProxy {

        SpotLight spotlight;
        public System.Action<float> SetProperties = (aperture) => {};

        public SpotLightProxy () {
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            if ( nodeView != null ) {
                spotlight = nodeView.GetComponent< SpotLight >();
                spotlight.light.intensity = 0;
                SetProperties = spotlight.SetProperties;
            }
        }

        public override void OnViewDestroyed () {
            base.OnViewDestroyed();
            SetProperties = (aperture) => {};
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "iris", ( outbox, value ) =>
                {

                this.SetProperties(Util.Sum(value) );

                    });
            } );
        }
    }
}
