using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Operants {

    public class ElbowBaseProxy : PartProxy {

        public ElbowBase elbow;

        public ElbowBaseProxy () {
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
        }

        public override void OnViewDestroyed () {
            base.OnViewDestroyed();
        }
     
        public override void OnPostBuild () {

         
            var rotorProxy = this.entity.Tails( e => e == "linked_to" ).Select( e => e.proxy ).Cast<ElbowRotorProxy>().First();


             if(rotorProxy.nodeView == default(NodeView)){
                base.OnPostBuild ();
                return;
            }

            ForView< ElbowBase >( ref elbow, () => {
                elbow.rotor = rotorProxy.nodeView.gameObject;
                elbow.producePartAngle = ( angle ) => io.Produce( "angle", angle );
            } );
            GameObject constraintHolder = null;
            ForView< ElbowBase >( ref elbow, () => {
                constraintHolder = elbow.constraintHolder;
            } );
            ForView< ElbowBase >( ref elbow, () => {
                rotorProxy.nodeView.btRigidBody.nodeview = nodeView;
                var partHighLight = nodeView.GetComponent<Highlight>();
                var subpartHighLight = rotorProxy.nodeView.GetComponent<Highlight>();

                partHighLight.RegisterRenderers(subpartHighLight.renderers);
                //nodeView.GetComponent<Highlight>().subpartRenderers.AddRange( rotorProxy.nodeView.GetComponent<Highlight>().renderers );




                var part = nodeView.GetComponent<Part>();
                var subpart = rotorProxy.nodeView.GetComponent<Part>();
                part.partMaterials.AddRange( subpart.partMaterials );
                part.linkedParts.Add( subpart );
                subpart.linkedParts.Add(part);
            } );
            var constraint = constraintHolder.AddComponent<BulletGeneric6DofConstraint>();

            elbow.motor.constraint = constraint;
            constraint.bulletRbA = nodeView.GetComponent<BulletRigidBody>();
            constraint.bulletRbB = rotorProxy.nodeView.GetComponent<BulletRigidBody>();

            var objectA = nodeView.gameObject;
            var objectB = rotorProxy.nodeView.gameObject;
             
            GameObject dummyRotor = new GameObject();
            dummyRotor.transform.parent = objectA.transform;
            dummyRotor.transform.localPosition = new Vector3( 0f, 0f, 0f );
            dummyRotor.transform.localRotation = Quaternion.identity;


            constraint.rbAPos = objectA.transform.position;
            constraint.rbBPos = dummyRotor.transform.position;// objectB.transform.position;
            constraint.rbBRot = dummyRotor.transform.rotation;//objectB.transform.rotation;
            constraint.rbARot = objectA.transform.rotation;

            var rotorCurrentRotation = Quaternion.Inverse( objectA.transform.rotation ) * objectB.transform.rotation;
            var currentAngle = rotorCurrentRotation.eulerAngles.y;


            objectB.GetComponent<BulletRigidBody>().SetPosition( dummyRotor.transform.position );
            objectB.GetComponent<BulletRigidBody>().SetRotation( dummyRotor.transform.rotation );
         
            //constraint.solverIterations = 20;
            constraint.useLinearReferenceFrameA = false;
            constraint.disableCollisionBetweenBodies = true;

            constraint.angularERPValue = Vector3.one * 0.8f;
            constraint.linearERPValue = Vector3.one * 0.8f;

            constraint.angularCFMValue = Vector3.zero; //Vector3.one*0.01f;
            constraint.linearCFMValue = Vector3.zero;//Vector3.one*0.01f;
         
            constraint.lowerLimit.z = -1.57f;
            constraint.upperLimit.z = 1.57f;
         
            constraint.Initialize();
         
            var newRotation = Quaternion.AngleAxis( currentAngle, Vector3.up );
            dummyRotor.transform.localRotation = newRotation;
            objectB.GetComponent<BulletRigidBody>().SetPosition( dummyRotor.transform.position );
            objectB.GetComponent<BulletRigidBody>().SetRotation( dummyRotor.transform.rotation );
            GameObject.Destroy( dummyRotor );

            base.OnPostBuild();
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued<float>( property == "angle", ( outbox, value ) => {

                    ForView< ElbowBase >( ref elbow, () => {
                        elbow.SetElbowAngle(Util.Sum(value)  );
                    } );
                } );
                inbox.ForQueued<float>( property == "force", ( outbox, value ) => {

                } );
                inbox.ForQueued<float>( property == "velocity", ( outbox, value ) => {
                    ForView< ElbowBase >( ref elbow, () => {
                        elbow.SetElbowVelocity( Util.Sum(value)  );
                    } );
                } );
            } );
        }

        public override void OnInboxDestroy ( Inbox inbox ) {
            base.OnInboxDestroy( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                Util.ForPredicate( property == "angle", () => {
                    ForView< ElbowBase >( ref elbow, () => {
                        elbow.motor.Stop();
                    } );
                } );
                Util.ForPredicate( property == "velocity", () => {
                    ForView< ElbowBase >( ref elbow, () => {
                        elbow.motor.Stop();
                    } );
                } );
            } );
        }



    }
    
}
