using UnityEngine;
using System.Collections;

namespace Operants {

    public class ContinuumProxy : PartProxy {

        PhysicalObject physicalObject;
        float mass = 0.01f;
        
        public ContinuumProxy () {
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
        }

        public override void OnPostBuild () {
            base.OnPostBuild();
            ForView< PhysicalObject >( ref physicalObject, () => {
                mass = physicalObject.bulletRigidbody.mass;
            } );
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "mass", ( outbox, value ) => {
                    ForView< PhysicalObject >( ref physicalObject, () => {
                        mass = Mathf.Clamp( Util.Sum( value ), 0.01f, 1000 );
                        physicalObject.bulletRigidbody.SetBodyMass(mass,physicalObject.bulletRigidbody.compoundShape.CalculateInertia());//.mass = mass;
                    } );
                } );
            } );
        }

        public override void OnUpdate () {
            base.OnUpdate();
            io.Produce< float >( "mass", mass );
        }

    }

}
