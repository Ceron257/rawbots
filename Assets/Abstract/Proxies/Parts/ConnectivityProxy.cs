using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Oracle;

namespace Operants {

    public class ConnectivityProxy : NodeProxy {

        IOComponent io;
        PartProxy proxyPartA;
        PartProxy proxyPartB;

        public ConnectivityProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< PartProxy >( property == "part_a", ( output, value ) => proxyPartA = value );
                inbox.For< PartProxy >( property == "part_b", ( output, value ) => proxyPartB = value );
            } );
        }

        public override void OnUpdate () {
            var isConnected = false;
            var assembly_a = (proxyPartA != null)? proxyPartA.PhysicallyLinked() : null;
            if ( proxyPartA != null && proxyPartB != null ) {
                isConnected = assembly_a.Contains(proxyPartB);
            }
            io.Produce< float >( "connectivity", isConnected ? 1 : 0 );
            io.Produce< float >( "partcount_a", (proxyPartA != null)?  PartProxy.ExcludeSubparts(assembly_a).Count() : 0);
        }
        
    }
}
