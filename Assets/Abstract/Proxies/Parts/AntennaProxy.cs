using UnityEngine;
using System.Collections;
using System.Linq;

namespace Operants
{
    public class AntennaProxy : PartProxy
    {
        float channel_rx = 0;
        float channel_tx = 0;
        float width_rx = 0;
        float width_tx = 0;

        public float channelInLow = 0;
        public float channelInHigh = 0;
        public float dataIn = 0;
        public float channelOutLow = 0;
        public float channelOutHigh = 0;
        public float dataOut = 0;
        bool deleted = false;
        Antenna antenna;
        //Color color;

        public AntennaProxy()
        {
            updates = true;
        }

        public override void OnPreBuild()
        {
            base.OnPreBuild();

            antenna = nodeView.GetComponent<Antenna>();

            //entity.ForInferredProperty<float>("tx_channel", Util.AsIs, value =>
            //{
            //    channelOutLow = value;
            //    //color = Util.PropertyToColor(value);
            //    //transceiver.ledRenderer.material.SetColor("_Color", color);
            //});
        }

        public override void OnDestroy()
        {
            deleted = true;
 //           Console.PushError("Destroyed", new string[] { this.ToString() });
            channel_rx = 0;
            channel_tx = 0;
            width_rx = 0;
            width_tx = 0;

            channelInLow = 0;
            channelInHigh = 0;
            dataIn = 0;
            channelOutLow = 0;
            channelOutHigh = 0;
            dataOut = 0;
        }

        public override void OnPostBuild()
        {
            base.OnPostBuild();
        }

        public override void OnViewDestroyed()
        {
            base.OnViewDestroyed();
        }

        public override void OnInbox(Inbox inbox)
        {
            base.OnInbox(inbox);
            //inbox.Proxy.entity.ForInferredProperty<string>("property", Util.AsIs, property =>
            //{
            //    inbox.For<object>(property == "broadcast", value =>
            //    {
            //        Antenna.instance.Transmit(this, value);
            //        transceiver.StartCoroutine(transceiver.VisualFeedBack(color));
            //    });
            //});

            inbox.Proxy.entity.ForInferredProperty<string>("property", Util.AsIs, property => {
                inbox.ForQueued<float>(property == "channel_tx", (outbox, value) => {
                    ForView<Antenna>(ref antenna, () => {
                        channel_tx = Util.Sum(value);
                    });
                });
            });

            inbox.Proxy.entity.ForInferredProperty<string>("property", Util.AsIs, property => {
                inbox.ForQueued<float>(property == "channel_rx", (outbox, value) => {
                    ForView<Antenna>(ref antenna, () => {
                        channel_rx = Util.Sum(value);
                    });
                });
            });

            inbox.Proxy.entity.ForInferredProperty<string>("property", Util.AsIs, property => {
                inbox.ForQueued<float>(property == "width_rx", (outbox, value) => {
                    ForView<Antenna>(ref antenna, () => {
                        width_rx = Util.Sum(value);
                    });
                });
            });

            inbox.Proxy.entity.ForInferredProperty<string>("property", Util.AsIs, property => {
                inbox.ForQueued<float>(property == "width_tx", (outbox, value) => {
                    ForView<Antenna>(ref antenna, () => {
                        width_tx = Util.Sum(value);
                    });
                });
            });

            inbox.Proxy.entity.ForInferredProperty<string>("property", Util.AsIs, property => {
                inbox.ForQueued<float>(property == "data_tx", (outbox, value) => {
                    ForView<Antenna>(ref antenna, () => {
                        dataOut = Util.Sum(value);
                    });
                });
            });

            channelInLow = Mathf.Max(0.0f, (channel_rx - (0.5f * width_rx)));
            channelInHigh = Mathf.Max(0.0f, (channel_rx + (0.5f * width_rx)));
            channelOutLow = Mathf.Max(0.0f, (channel_tx - (0.5f * width_tx)));
            channelOutHigh = Mathf.Max(0.0f, (channel_tx + (0.5f * width_tx)));
            //Console.PushLog("InLow", new string[] { channelInLow.ToString() });
            //Console.PushLog("InHigh", new string[] { channelInHigh.ToString() });
            //Console.PushLog("OutLow", new string[] { channelOutLow.ToString() });
            //Console.PushLog("OutHigh", new string[] { channelOutHigh.ToString() });
        }

        //public void ReceiveMessage<T>(T message)
        //{
        //    transceiver.StartCoroutine(transceiver.VisualFeedBack(color));
        //    io.Produce("message", message);
        //}

        public override void OnUpdate()
        {
            var antennas = Gaia.Instance().entities["antenna"]
            .Tails(p => p == "as")
            .Where(e => e.proxy != null) //bugfix, blueprint ghosts are proxyless entities -z26
            .Select(e => e.proxy as AntennaProxy).ToList();

            var data = 0.0f;
            if (antennas.Count > 1)
            {
                channelInLow = Mathf.Max(0.0f, (channel_rx - (0.5f * width_rx)));
                channelInHigh = Mathf.Max(0.0f, (channel_rx + (0.5f * width_rx)));
                channelOutLow = Mathf.Max(0.0f, (channel_tx - (0.5f * width_tx)));
                channelOutHigh = Mathf.Max(0.0f, (channel_tx + (0.5f * width_tx)));
                foreach (AntennaProxy ant in antennas)
                {
                    if (ant != this)     //Don't loopback internally
                    {
                        if (ant.nodeView != default(NodeView))
                        {
                            if ((channelInHigh >= ant.channelOutLow) && (channelInLow <= ant.channelOutHigh))    //If bands overlap
                            {
                                data += ant.dataOut;
                            }  
                        }
                    }
                }
            }
            dataIn = data;
            base.OnUpdate();
            io.Produce<float>("data_rx", data);
        }
    }
}
