using UnityEngine;
using System.Collections;

namespace Operants {

    public class FinProxy : PartProxy {

        public FinProxy () {
        }

        public override void OnBuild () {
            base.OnBuild();
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
                inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
            } );
        }

    }
}
