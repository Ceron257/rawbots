using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class ControllerProxy : NodeProxy {

        public PIDScalar pid;
        IOComponent io;
        float feedback = 0;
        Pulse resetPulse = new Pulse();

        public ControllerProxy () {
            pid = new PIDScalar();
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            pid = new PIDScalar();
            resetPulse = new Pulse();
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "setpoint", ( output, value ) => pid.setpoint = Util.Sum( value ) );
                inbox.ForQueued< float >( property == "feedback", ( output, value ) => feedback = Util.Sum( value ) );
                inbox.ForQueued< float >( property == "kp", ( output, value ) => pid.kp = Util.Sum( value ) );
                inbox.ForQueued< float >( property == "ki", ( output, value ) => pid.ki = Util.Sum( value ) );
                inbox.ForQueued< float >( property == "kd", ( output, value ) => pid.kd = Util.Sum( value ) );
                inbox.For< float >( property == "reset",
                    ( output, value ) => resetPulse.ForPulse( value, () => pid.Reset( feedback ) ) );
            } );
        }

        public override void OnUpdate () {
            var control = pid.Compute( feedback, Time.deltaTime );
            //Debug.Log("setpoint:"+pid.setpoint+" kp:"+pid.kp+" kd:"+pid.kd+" ki:"+pid.ki+" feedback:"+feedback+ " control:"+control);
            io.Produce< float >( "control", control );
        }

    }
}