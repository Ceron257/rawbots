using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class ToggleProxy : NodeProxy {

        public IOComponent io;
        float sample_a;
        float sample_b;
        string string_a;
        string string_b;
        Color color_a;
        Color color_b;
        PartProxy part_a;
        PartProxy part_b;
        float selector = 0;
        bool toggle = true;
        Pulse pulse = new Pulse();

        public ToggleProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            pulse = new Pulse();
            selector = 0;
            sample_a = 0f;
            sample_b = 0f;
            string_a = string.Empty;
            string_b = string.Empty;
            part_a = default( PartProxy );
            part_b = default( PartProxy );

            //get value the toggle was in the last save. -z26
            //entity.ForInferredProperty< string >( "toggle", Util.AsIs, value => toggle = bool.Parse(value) );
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "selector", ( outbox, value ) => selector = value );
                inbox.For< float >( property == "sample_a", ( outbox, value ) => sample_a = value );
                inbox.For< float >( property == "sample_b", ( outbox, value ) => sample_b = value );
                inbox.For< string >( property == "string_a", ( outbox, value ) => string_a = value );
                inbox.For< string >( property == "string_b", ( outbox, value ) => string_b = value );
                inbox.For< Color >( property == "color_a", ( outbox, value ) => color_a = value );
                inbox.For< Color >( property == "color_b", ( outbox, value ) => color_b = value );
                inbox.For< PartProxy >( property == "part_a", ( outbox, value ) => part_a = value );
                inbox.For< PartProxy >( property == "part_b", ( outbox, value ) => part_b = value );
            } );
        }

        public override void OnUpdate () {
            pulse.ForPulse( selector, () => toggle = !toggle );
            entity.SetProperty("toggle",toggle.ToString());//store current value of toggle in the savefile. -z26
            io.Produce< float >( "sample", toggle ? sample_a : sample_b );
            io.Produce< Color >( "color", toggle ? color_a : color_b );
            io.Produce< PartProxy >( "part", toggle ? part_a : part_b );
            io.Produce< string >( "string",toggle ? string_a : string_b );
        }

    }
}
