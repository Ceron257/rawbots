using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class TargetProxy : NodeProxy {


        PartProxy part;
        float range;
        IOComponent io;

        public TargetProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }



        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< PartProxy >( property == "part", ( output, value ) => CallForCameraFocus(value) );
                inbox.For< float >( property == "range", ( output, value ) => range = value );
            } );
        }

        public void CallForCameraFocus(PartProxy part){
            Debug.Log("helloo "+ part.nodeView.name);
            //part = part; what's the point? -z26
        }



    }
}