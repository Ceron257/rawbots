using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class MultiplexerProxy : NodeProxy {

        public IOComponent io;

        float sample_a;
        float sample_b;
        string string_a;
        string string_b;
        Color color_a;
        Color color_b;
        PartProxy part_a;
        PartProxy part_b;
        int selector = 0;

        public MultiplexerProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            selector = 0;
            sample_a = 0f;
            sample_b = 0f;
            string_a = string.Empty;
            string_b = string.Empty;
            part_a = default( PartProxy );
            part_b = default( PartProxy );
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "selector", ( outbox, value ) => selector = Mathf.RoundToInt( Mathf.Clamp01( value ) ) );
                inbox.For< float >( property == "sample_a", ( outbox, value ) => sample_a = value );
                inbox.For< float >( property == "sample_b", ( outbox, value ) => sample_b = value );
                inbox.For< string >( property == "string_a", ( outbox, value ) => string_a = value );
                inbox.For< string >( property == "string_b", ( outbox, value ) => string_b = value );
                inbox.For< Color >( property == "color_a", ( outbox, value ) => color_a = value );
                inbox.For< Color >( property == "color_b", ( outbox, value ) => color_b = value );
                inbox.For< PartProxy >( property == "part_a", ( outbox, value ) => part_a = value );
                inbox.For< PartProxy >( property == "part_b", ( outbox, value ) => part_b = value );
            } );
        }

        public override void OnUpdate () {
            io.Produce< float >( "sample", selector == 0 ? sample_a : sample_b );
            io.Produce< Color >( "color", selector == 0 ? color_a : color_b );
            io.Produce< PartProxy >( "part", selector == 0 ? part_a : part_b );
            io.Produce< string >( "string",selector == 0 ? string_a : string_b );
        }

    }
}
