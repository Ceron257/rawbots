using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public enum LogicGateType {
        NOT,
        AND,
        NAND,
        OR,
        NOR,
        XOR,
        XNOR,
    }

    public class LogicGateProxy : NodeProxy {

        public IOComponent io;
        LogicGateType gateType = LogicGateType.NOT;
        int a;
        int b;

        public LogicGateProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "in_a", ( outbox, value ) => a = Mathf.RoundToInt( Mathf.Clamp01( value ) ) );
                inbox.For< float >( property == "in_b", ( outbox, value ) => b = Mathf.RoundToInt( Mathf.Clamp01( value ) ) );
                inbox.For< string >( property == "type", ( outbox, value ) => {
                    gateType = ( LogicGateType )System.Enum.Parse( typeof( LogicGateType ), value.Split( '.' ).Last() );
                } );
            } );
        }

        public override void OnUpdate () {
            base.OnUpdate();

            var o = 0;

            switch ( gateType ) {

                case LogicGateType.NOT:
                    o = a == 0 ? 1 : 0;
                    break;

                case LogicGateType.AND:
                    o = a == 1 && b == 1 ? 1 : 0;
                    break;

                case LogicGateType.NAND:
                    o = a == 1 && b == 1 ? 0 : 1;
                    break;

                case LogicGateType.OR:
                    o = a == 1 || b == 1 ? 1 : 0;
                    break;

                case LogicGateType.NOR:
                    o = a == 0 && b == 0 ? 1 : 0;
                    break;

                case LogicGateType.XOR:
                    o = a != b ? 1 : 0;
                    break;

                case LogicGateType.XNOR:
                    o = a == b ? 1 : 0;
                    break;
            }

            io.Produce< float >( "out", o );
        }

    }
}
