using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class InputSamplerProxy : NodeProxy {

        public enum deviceType {
            osc,
            keyboard
        }

        List<string> validInputKeys = new List<string>(){
            "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
            "1","2","3","4","5","6","7","8","9","0",
            "[1]","[2]","[3]","[4]","[5]","[6]","[7]","[8]","[9]","[0]",
            "up","down","left","right",
            "right shift","left shift","right ctrl","left ctrl","right alt","left alt","right cmd","left cmd",
            "backspace","tab","return","escape","space","delete","enter","insert","home","end","page up","page down",
            "f1","f2","f3","f4","f5","f6","f7","f8","f9","f10","f11","f12"
        };
        int positiveIndex = 0;
        int negativeIndex = 0;
        float sample = 0;
        string positive = string.Empty;
        string negative = string.Empty;
        float attack = 1;
        float release = 1;
        float scale = 1;
        float offset = 0;
        public IOComponent io;
        deviceType positiveDevice;
        deviceType negativeDevice;
        //UDPPacketIO udp; -z26
        public bool hasFocus = false;
        public static Dictionary<string,float> midiStatus = new Dictionary<string, float>();

        public InputSamplerProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;

        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            sample = 0;
            positive = string.Empty;
            negative = string.Empty;
            attack = 1;
            release = 1;
            scale = 1;
            offset = 0;
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< string >( property == "positive", ( outbox, value ) => AssignPositive( value ) );
                inbox.For< string >( property == "negative", ( outbox, value ) => AssignNegative( value ) );
                inbox.For< float >( property == "attack", ( outbox, value ) => attack = value );
                inbox.For< float >( property == "release", ( outbox, value ) => release = value );
                inbox.For< float >( property == "scale", ( outbox, value ) => scale = value );
                inbox.For< float >( property == "offset", ( outbox, value ) => offset = value );
            } );
        }

        void AssignPositive ( string value ) {
            positive = string.Empty;
            positiveIndex = 0;
            if ( validInputKeys.Contains( value.ToLower() ) ) {
                positive = value;
                positiveDevice = deviceType.keyboard;
            }
            else {
                positive = ParseAddress( value, out positiveIndex );
                Gaia.Instance().SetAddressHandler( value, OscPositiveHandler );
                positiveDevice = deviceType.osc;
            }
        }

        void AssignNegative ( string value ) {
            negative = string.Empty;
            negativeIndex = 0;
            if ( validInputKeys.Contains( value.ToLower() ) ) {
                negative = value;
                negativeDevice = deviceType.keyboard;
            }
            else {
                negative = ParseAddress( value, out negativeIndex );
                Gaia.Instance().SetAddressHandler( value, OscNegativeHandler );
                negativeDevice = deviceType.osc;
            }
        }

        public override void OnUpdate () {
            base.OnUpdate();
            bool released = true;


            if ( positive != string.Empty && negative != string.Empty
                && positive == negative && positiveIndex == negativeIndex ) {
                sample = GetAxisValue( positive.ToLower() + "_" + positiveIndex.ToString(), positiveDevice );
            }
            else {
                if ( positive != string.Empty && CheckIfActive( positive.ToLower(), positiveIndex, positiveDevice ) ) {
                    sample += attack * Time.deltaTime;
                    if ( sample > 1 ) {
                        sample = 1;
                    }
                    released = false;
                }
                if ( negative != string.Empty && CheckIfActive( negative.ToLower(), negativeIndex, negativeDevice ) ) {
                    sample -= attack * Time.deltaTime;
                    if ( sample < -1 ) {
                        sample = -1;
                    }
                    released = false;
                }
                if ( released ) {
                    if ( sample > 0 ) {
                        sample -= release * Time.deltaTime;
                        if ( sample < 0 ) {
                            sample = 0;
                        }
                    }
                    else {
                        sample += release * Time.deltaTime;
                        if ( sample > 0 ) {
                            sample = 0;
                        }
                    }
                }
            }
            if ( hasFocus ) {
                io.Produce< float >( "sample", sample * scale + offset );
                io.Produce< float >( "inverted", sample * -scale + offset );
            }
        }

        public float GetAxisValue ( string inputId, deviceType device ) {
            float midiValue = 0;
            midiStatus.TryGetValue( inputId, out midiValue );
            return midiValue;
        }

        public bool CheckIfActive ( string inputId, int index, deviceType device ) {
            float midiValue = 0;
            bool active = false;
            var key = inputId + "_" + index.ToString();
            midiStatus.TryGetValue( key, out midiValue );
            if ( midiValue > 0 ) {
                active = true;
            }
            else if ( device == deviceType.keyboard && Input.GetKey( inputId ) ) {
                active = true;
            }
            return active;
        }

        void OscPositiveHandler ( OscMessage message ) {
            float value = 0;
            var key = positive + "_" + positiveIndex.ToString();
            if ( message.Values.Count > positiveIndex ) {
                value = ( float )message.Values[ positiveIndex ];
            }
            else {
                value = ( float )message.Values[ 0 ];
            }
            if ( !midiStatus.ContainsKey( key ) ) {
                midiStatus.Add( key, value );
            }
            else {
                midiStatus[ key ] = value;
            }
        }

        void OscNegativeHandler ( OscMessage message ) {
            float value = 0;
            var key = negative + "_" + negativeIndex.ToString();
            if ( message.Values.Count > positiveIndex ) {
                value = ( float )message.Values[ negativeIndex ];
            }
            else {
                value = ( float )message.Values[ 0 ];
            }
            if ( !midiStatus.ContainsKey( key ) ) {
                midiStatus.Add( key, value );
            }
            else {
                midiStatus[ key ] = value;
            }
        }

        public static string ParseAddress ( string address, out int index ) {
            index = 0;
            var oscAddress = string.Empty;
            var startIndex = address.IndexOf( '[' );
            var endIndex = address.IndexOf( ']' );
            if ( startIndex == -1 && endIndex == -1 ) {
                oscAddress = address;
            }
            else if ( startIndex != -1 && endIndex != -1 ) {
                ++startIndex;
                var stringIndex = address.Substring( startIndex, endIndex - startIndex );
                int.TryParse( stringIndex, out index );
                oscAddress = address.Substring( 0, startIndex - 1 );
            }
            return oscAddress;
        }

    }
}
