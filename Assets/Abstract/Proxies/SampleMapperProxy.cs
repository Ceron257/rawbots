using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public enum SampleMapperInvert {
        False,
        True,
    }

    public class SampleMapperProxy : NodeProxy {

        SampleMapperInvert invert = SampleMapperInvert.False;
        float sample_min = -1;
        float sample_max = 1;
        float mapped_min = -1;
        float mapped_max = 1;
        float sample = 0;
        public IOComponent io;

        public SampleMapperProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }
  
        public override void OnPreBuild () {
            base.OnPreBuild();
        }
        
        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {

                inbox.For< float >( property == "sample_min", ( outbox, value ) => sample_min = value );
                inbox.For< float >( property == "sample_max", ( outbox, value ) => sample_max = value );
                inbox.For< float >( property == "mapped_min", ( outbox, value ) => mapped_min = value );
                inbox.For< float >( property == "mapped_max", ( outbox, value ) => mapped_max = value );
                inbox.For< string >( property == "invert", ( outbox, value ) => {
                    invert = ( SampleMapperInvert )System.Enum
                    .Parse( typeof( SampleMapperInvert ), value.Split( '.' ).Last() );
                } );
                inbox.ForQueued < float >( property == "sample", ( outbox, value ) => sample = Util.Sum( value ) );
            } );
        }

        public override void OnUpdate () {
            base.OnUpdate();
            var divisor = sample_max - sample_min;
            if ( divisor == 0 ) {
                return;
            }
            var normalizedSample = ( sample - sample_min ) / Mathf.Abs( divisor );
            var invertedNormalizedSample = 1 - normalizedSample;
            var mapped = mapped_min + normalizedSample * Mathf.Abs( mapped_max - mapped_min );
            var mappedInverted = mapped_min + invertedNormalizedSample * Mathf.Abs( mapped_max - mapped_min );
            io.Produce< float >( "mapped", invert == SampleMapperInvert.False ? mapped : mappedInverted );
            io.Produce<float>( "inverted", invert == SampleMapperInvert.True ? mapped : mappedInverted );
        }
    }
}
