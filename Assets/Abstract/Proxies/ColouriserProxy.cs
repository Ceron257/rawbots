using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class ColouriserProxy : NodeProxy {

        public IOComponent io;

        float red = 0;
		float green = 0;
		float blue = 0;

		public ColouriserProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "red", ( outbox, value ) => red = value );
				inbox.For< float >( property == "green", ( outbox, value ) => green = value );
				inbox.For< float >( property == "blue", ( outbox, value ) => blue = value );
			} );
        }

        public override void OnUpdate () {
			Color colour;
			colour.a = 255;
            colour.r = Mathf.Clamp((red * 0.003922f), 0, 1); //Scale to 0-255
            colour.g = Mathf.Clamp((green * 0.003922f), 0, 1); //Scale to 0-255
            colour.b = Mathf.Clamp((blue * 0.003922f), 0, 1); //Scale to 0-255

            io.Produce< Color >( "colour", colour);
        }

    }
}
