using UnityEngine;

using System.Collections;

public class CameraAspectRatio : MonoBehaviour {

// Use this for initialization
    public bool adjustX = true;
    public bool adjustY = true;
    public bool adjustWidth = true;
    public bool adjustHeight = true;

    void Start () {

        //Set as needed
        float targetaspect = 16.0f / 9.0f;

        float windowaspect = ( float )Screen.width / ( float )Screen.height;

        float scaleheight = ( windowaspect / targetaspect ) * GetComponent<Camera>().rect.height;


        // if scaled height is less than current height, add letterbox

        if ( scaleheight < 1.0f ) {
            Rect rect = GetComponent<Camera>().rect;
            if ( adjustX ) {
                rect.x = 0;
            }
            if ( adjustWidth ) {
                rect.width = 1.0f;
            }
            if ( adjustY ) {
                rect.y = ( 1.0f - scaleheight ) / 2.0f;
            }
            if ( adjustHeight ) {
                rect.height = scaleheight;
            }
            GetComponent<Camera>().rect = rect;
        }
        else { // add pillarbox
            float scalewidth = 1.0f / scaleheight;
            Rect rect = GetComponent<Camera>().rect;
            if ( adjustX ) {
                rect.x = ( 1.0f - scalewidth ) / 2.0f;
            }
            if ( adjustWidth ) {
                rect.width = scalewidth;
            }
            if ( adjustY ) {
                rect.y = 0;
            }
            if ( adjustHeight ) {
                rect.height = 1.0f;
            }
            GetComponent<Camera>().rect = rect;
        }

    }

}