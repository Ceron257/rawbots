using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;

public class GameCamera : MonoBehaviour {

    public BulletLayers pickMask;
    public PhysicalObject physicalObject;
    public GridCamera gridCamera;
    public GameObject watery;
    PartProxy target;
    public TitleScreen titleScreen;
    public GameObject caustics;
    Transform cameraViewTarget;
    private DragComponent hoverDragComponent;
    public GameObject targetProxyTransform;
    List<InputSamplerProxy> inputSamplers;
 

    public enum GameCameraState {
        Init,
        Following,
        FirstPerson,
        FreeCamera,
        PopFromCamera,
        ChangeTargetMode
    }

    public GameCameraState state = GameCameraState.Init;
 
    void Awake () {
        watery.SetActive( false );
        var mode = new GameModeManager.Mode(){
            id = GameModeManager.ModeId.ChangeCameraFocus,
            transitions = new List<GameModeManager.Transition>(){
                new GameModeManager.Transition(){ key = KeyCode.Tab , state = GameModeManager.KeyState.KeyUp , mode = GameModeManager.ModeId.World},
            }};
        GameModeManager.RegisterMode( mode );
     
    }
 
    void Start () {
        gridCamera.GetComponent<Camera>().enabled = false;
        gridCamera.pinned.enabled = true;
        GetComponent<BulletRigidBody>().AddOnTriggerDelegate( OnTrigger );
        NextState();
    }

    DragComponent pickPart;
    Part hoverTabPart;
    Vector3 oldMousePosition = Vector3.zero;

    public void EnableGridCamera ( Entity entity, GridCamera.GridType type = GridCamera.GridType.Programming, bool darken = true ) {
        gridCamera.SwitchGrid( type );
        gridCamera.RemoveDeprecated();
        if ( darken ) {
            GetComponent<Camera>().GetComponent< ColorCorrectionEffect >().grid = true;
        }
        gridCamera.GetComponent<Camera>().enabled = true;
        gridCamera.pinned.enabled = false;

        if ( entity != null ) {
            gridCamera.ShowGrid( entity );
        }
    }

    public void DisableGridCamera ( GridCamera.GridType type = GridCamera.GridType.Programming, bool darken = true ) {
        gridCamera.SwitchGrid( type );
        if ( darken ) {
            GetComponent<Camera>().GetComponent< ColorCorrectionEffect >().grid = false;
        }
        gridCamera.GetComponent<Camera>().enabled = false;
        gridCamera.pinned.enabled = true;
    }

    void UnlockCamera ( CameraProxy camera ) {
        if ( !Gaia.Instance().entities[ "unlocked" ].Tails( p => p == "was" ).Any( e => e == camera.Proxy.entity ) ) {
            new Relationship( camera.Proxy.entity, "was", Gaia.Instance().entities[ "unlocked" ] );
        }
    }

    int cameraIdx = -1;

    PartProxy SelectTarget () {
        if ( target != null ) {


            HashSet<Entity> entities = new HashSet<Entity>();
            Hyperspace.Entities( target.Proxy.entity, entities, new string[]{"input_for","output_for","extends","slot_for","linked_to","consumes"} );

            var connectedParts = entities.Where( e => e.proxy is PartProxy ).Select( e => e.proxy ).Cast<PartProxy>();



            var cameraProxy = connectedParts
                .Where( p => p.nodeView != null && p is CameraProxy )
                .FirstOrDefault();
         
            return cameraProxy;
         
        }
        //Console.PushError("", new string[] { "No cams" });
        return default(PartProxy);
     
    }
 
    bool tabPressed = false;
    bool tabShiftPressed = false;
    bool partPicked = false;
    int tabUpdateState = -1;
    bool inWater = false;
    bool wateryEyesEnabled = false;
 
    void Update () {
     
        if ( tabUpdateState == -1 && !tabPressed ) {
            tabUpdateState = 0;
        }
        if ( tabUpdateState == 0 && tabPressed ) {
            tabUpdateState = 1;
        }
        if ( tabUpdateState == 1 && !tabPressed ) {
            tabUpdateState = 2;
        }
        if ( tabUpdateState == 2 && tabPressed ) {
            tabUpdateState = 1;
        }
     
        if ( GameModeManager.InMode( m => m == GameModeManager.ModeId.World ) && gridCamera.GetComponent<Camera>().enabled ) {
            DisableGridCamera();
        }
        else if ( GameModeManager.InMode( m => m == GameModeManager.ModeId.Grid ) && !gridCamera.GetComponent<Camera>().enabled ) {
            EnableGridCamera( null );
        }
        HightlightAndGrabPart( tabUpdateState );
     
        if ( Input.GetKey( KeyCode.Tab ) ) {
            if ( Input.GetKey( KeyCode.LeftShift ) ) {
                tabShiftPressed = true;
                tabPressed = false;
            }
            else {
                tabPressed = true;
                tabShiftPressed = false;
            }
        }
        else {
            tabPressed = false;
            tabShiftPressed = false;
        }
     
    }
 
    void FixedUpdate () {
        if ( target != null ) {
        if ( target.nodeView != null ) {
            targetProxyTransform.transform.position = target.nodeView.transform.position;
            targetProxyTransform.transform.rotation = target.nodeView.transform.rotation;
        }
        }
     
        if ( inWater && !wateryEyesEnabled ) {
            OnWaterEnter();
            wateryEyesEnabled = true;
         
        }
        else if ( !inWater && wateryEyesEnabled ) {
            OnWaterExit();
            wateryEyesEnabled = false;
        }
        
        
        //This sends a message to the shader not to cull (cull: don't render hidden faces for perf reason)
        //the back of water meshes when you are submerged. That way, you can see the air/water boundary.
        //When out of water, culling improves the aesthetic, so the else statement enables it. The message
        //needs to be repetitively sent to the shader every frame. -z26
        
        if (inWater) {
            Shader.SetGlobalInt("_INWATER", (int)UnityEngine.Rendering.CullMode.Front);
        } else {
            Shader.SetGlobalInt("_INWATER", (int)UnityEngine.Rendering.CullMode.Back);
        }
     
     
        inWater = false;
    }
 
    void HightlightAndGrabPart ( int tabState ) {
        var touch = new TouchData();
        touch.camera = GetComponent<Camera>();
        touch.deltaPosition = oldMousePosition - Input.mousePosition;
        touch.position = Input.mousePosition;
        DragComponent hitDragComponent = null;
        var pickRay = GetComponent<Camera>().ScreenPointToRay( Input.mousePosition );
        BulletRaycastHitDetails hit;
        if ( Bullet.Raycast( pickRay, out hit, 2000, pickMask, pickMask ) ) {
            touch.worldPosition = hit.point;
            hitDragComponent = hit.bulletRigidbody.GetComponent<DragComponent>();// Part.FindOwner( hit.transform, -1 );
        }
        if ( hitDragComponent != default(DragComponent) && GameModeManager.InMode( m => m == GameModeManager.ModeId.ChangeCameraFocus ) ) {
            if ( hoverTabPart != hitDragComponent.part ) {
                if ( hoverTabPart != default(Part) ) {
                    hoverTabPart.gameObject.GetComponent<Highlight>().SetHighlight( false );
                }
                hitDragComponent.part.gameObject.GetComponent<Highlight>().SetHighlight( true, Highlight.HighlightType.Normal );
                hoverTabPart = hitDragComponent.part;
            }
        }
        else if ( hoverTabPart != default(Part) ) {
            hoverTabPart.gameObject.GetComponent<Highlight>().SetHighlight( false );
            hoverTabPart = default(Part);
        }
        if ( pickPart != null && !Input.GetMouseButton( 0 ) ) {
            if ( GameModeManager.InMode( m => m == GameModeManager.ModeId.ChangeCameraFocus ) ) {
                var po = pickPart.GetComponent< PhysicalObject >();
                if ( po != null ) {
                    EnableInputSamplers( pickPart.part.proxy.entity );
                 
                    target = pickPart.part.proxy as PartProxy;
                    partPicked = true;
                    //followDistance = 10;
//                    followLook = transform.parent.position + Vector3
//                        .Dot( target.nodeView.transform.position - transform.parent.position, transform.forward )
//                        * transform.forward;
//                    positionPID.Reset( transform.parent.position );

                    HashSet<Entity> entities = new HashSet<Entity>();
                    Hyperspace.Entities( target.Proxy.entity, entities, new string[]{"input_for","output_for","extends","slot_for","linked_to","consumes"} );

                    var connectedParts = entities.Where( e => e.proxy is PartProxy ).Select( e => e.proxy ).Cast<PartProxy>();


                    var cameras = connectedParts
                        .Select( p => p.nodeView )
                        .Where( n => n != null )
                        .Select( n => n.proxy.GetComponent< CameraProxy >() )
                        .Where( c => c != null );
                    foreach ( var c in cameras ) {
                        UnlockCamera( c );
                    }
                }
            }
            pickPart.SendMessage( "OnMouseUpCustom", touch, SendMessageOptions.DontRequireReceiver );
            pickPart = null;
        }
        else if ( pickPart != null && touch.deltaPosition.sqrMagnitude > 1 ) {
            pickPart.SendMessage( "OnMouseDragCustom", touch, SendMessageOptions.DontRequireReceiver );
        }
        else if ( pickPart != null ) {
            pickPart.SendMessage( "OnMouseStayCustom", touch, SendMessageOptions.DontRequireReceiver );
        }
        else if ( pickPart == null && hitDragComponent != null && Input.GetMouseButton( 0 ) && GameModeManager.InMode( m => m != GameModeManager.ModeId.Grid ) ) {
            pickPart = hitDragComponent;
            pickPart.SendMessage( "OnMouseDownCustom", touch, SendMessageOptions.DontRequireReceiver );
        }
        else if ( pickPart == null && hoverDragComponent != null && hitDragComponent != hoverDragComponent ) {
            hoverDragComponent.SendMessage( "OnMouseExitCustom", touch, SendMessageOptions.DontRequireReceiver );
            hoverDragComponent = null;
        }
        else if ( pickPart == null && hoverDragComponent == null && hitDragComponent != null ) {
            hoverDragComponent = hitDragComponent;
            hoverDragComponent.SendMessage( "OnMouseEnterCustom", touch, SendMessageOptions.DontRequireReceiver );
        }
        else if ( pickPart == null && hoverDragComponent != null ) {
            hoverDragComponent.SendMessage( "OnMouseMoveCustom", touch, SendMessageOptions.DontRequireReceiver );
        }

        oldMousePosition = touch.position;

    }

    public void EnableInputSamplers ( Entity entity ) {
        DisableInputSamplers();
     
        OperantFinder finder = new OperantFinder();
        var samplers = finder.FindOperants( entity, "input_sampler" );
     
        foreach ( var sampler in samplers ) {
            var inputSampler = ( InputSamplerProxy )sampler;
            inputSampler.hasFocus = true;
            inputSamplers.Add( inputSampler );
        }
     
    }

    public void DisableInputSamplers () {
        if ( inputSamplers != default(List<InputSamplerProxy>) ) {
            inputSamplers.ForEach( sampler => sampler.hasFocus = false );
        }
        inputSamplers = new List<InputSamplerProxy>();
    }
 
    public bool FindTarget () {

        var cameras = Gaia.Instance().entities[ "camera" ]
            .Tails( p => p == "as" )
            .Select( e => e.proxy as CameraProxy ).ToList();
        //Debug.Log(cameras.Count+" " +Time.time);
//        var unlockedCameras = Gaia.Instance().entities[ "unlocked" ]
//            .Tails( p => p == "was" )
//            .Select( e => e.proxy as CameraProxy )
//            .Where( p => p != null ).ToList();
//        if ( unlockedCameras.Count > 0 && !DevTools.IsOptionEnabled( DevTools.Options.CameraCycleFPS ) ) {
//            cameras = unlockedCameras;
//        }

        for ( var tries = 0; tries < 2; ++tries ) {
            var cameraView = default( CameraView );
            var cameraProxy = default( CameraProxy );
         
            while ( ++cameraIdx < cameras.Count ) {
                if ( cameras[ cameraIdx ].nodeView != default( NodeView ) ) {
                    cameraProxy = cameras[ cameraIdx ];
                    cameraView = cameraProxy.nodeView.GetComponent< CameraView >();
                    if ( cameraView != default( CameraView ) ) {
                        break;
                    }
                }
            }
            if ( cameraView == default( CameraView ) ) {
                cameraViewTarget = null;
                cameraIdx = -1;
            }
            else {
                EnableInputSamplers( cameraProxy.entity );
                UnlockCamera( cameraProxy );
                cameraViewTarget = cameraView.transform;
                target = cameraProxy;
                return true;
            }
        }
        return false;
    }
 
    IEnumerator InitState () {
        var timeout = 600;          //10s timeout to find a camera
        bool cameraAdded = false;
        //Console.PushLog("Cam", new string[] { "Init" });
        while ( state == GameCameraState.Init ) {
            if ((Gaia.Instance().entities.ContainsKey("camera")))
            {
                var cameras = Gaia.Instance().entities[ "camera" ]
        .Tails( p => p == "as" )
        .Select( e => e.proxy as CameraProxy ).Count();
                if ( cameras > 0 ) {
                    titleScreen.Fading = true;
                 
                    var cameraFound = FindTarget();
                    if ( cameraFound ) {
                        state = GameCameraState.PopFromCamera;
                        GhostCamera.instance.Initialize( target.nodeView.gameObject, transform.gameObject );
                    }
                    else {
                     
                        state = GameCameraState.FreeCamera;
                    }
                }
            }
            if ((Gaia.scene > 0) && (timeout < 0) && (!cameraAdded))            //If not on the start screen
            {
                titleScreen.Fading = true;                  //test
                var obj = new GameObject("Base/" + "camera" + "_base");
                obj.transform.parent = transform.parent;
                obj.transform.position = Vector3.zero;
                obj.transform.rotation = Quaternion.Euler(0, 0, 0);     //Spawn a camera at 0,0,0 if none are present
                obj.AddComponent<XForm>();
                obj.AddComponent<Bot>();
                cameraAdded = true;
            }
            timeout--;
            //Console.PushLog("Timer", new string[] { timeout.ToString() });
            yield return 0;
        }
        NextState();
    }
 
    IEnumerator PopFromCameraState () {
        while ( state == GameCameraState.PopFromCamera ) {
            state = GameCameraState.Following;
            yield return 0;
        }
        NextState();
    }
 
    IEnumerator FollowingState () {
        //Console.PushLog("Cam", new string[] { "Following" });
        int tabState = -1;
        int shiftTabState = -1;
     
        while ( state == GameCameraState.Following ) {
         
            var inWorldMode = GameModeManager.InMode( m => m == GameModeManager.ModeId.World );
            var inChangeCameraFocusMode = GameModeManager.InMode( m => m == GameModeManager.ModeId.ChangeCameraFocus );
            if ( inWorldMode || inChangeCameraFocusMode ) {
                if ( tabState == -1 && !tabPressed ) {
                    tabState = 0;
                }
                if ( tabState == 0 && tabPressed ) {
                    tabState = 1;
                }
                if ( tabState == 1 && !tabPressed ) {
                    tabState = 2;
                }
                if ( tabState == 2 && tabPressed ) {
                    tabState = 1;
                }
         
         
                if ( shiftTabState == -1 && !tabShiftPressed ) {
                    shiftTabState = 0;
                }
                if ( shiftTabState == 0 && tabShiftPressed ) {
                    shiftTabState = 1;
                }
                if ( shiftTabState == 1 && !tabShiftPressed ) {
                    shiftTabState = 2;
                }
                if ( shiftTabState == 2 && tabShiftPressed ) {
                    shiftTabState = 1;
                }
            }
            else {
                tabState = -1;
                shiftTabState = -1;
            }
         
         
         
 
            if ( inWorldMode && shiftTabState == 2 ) {
                if ( FindTarget() ) {
                    state = GameCameraState.PopFromCamera;
                    GhostCamera.instance.ChangeCamera( target.nodeView.gameObject );
                }

            }
            else if ( inWorldMode && tabState == 2 ) {
                var camera = SelectTarget();
                if ( camera != default(PartProxy) ) {
                    var cameraProxy = camera;
                    cameraViewTarget = cameraProxy.nodeView.GetComponent< CameraView >().transform;
                    target = camera;
                    state = GameCameraState.FirstPerson;
                    GhostCamera.instance.SetFPS( target.nodeView.gameObject );
                }
            }
            else if ( partPicked ) {
                state = GameCameraState.ChangeTargetMode;
            }
         

            yield return new WaitForFixedUpdate();
        }
        NextState();
     
     
    }
 
    IEnumerator ChangeTargetModeState () {
        GhostCamera.instance.ChangeTarget( target.nodeView.gameObject );
        partPicked = false;
        while ( state == GameCameraState.ChangeTargetMode ) {
         
     
            state = GameCameraState.Following;
         
            yield return new WaitForFixedUpdate();
        }
        NextState();
    }
 
    IEnumerator FirstPersonState () {
        int tabState = -1;
        int shiftTabState = -1;
        while ( state == GameCameraState.FirstPerson ) {
         
            var inWorldMode = GameModeManager.InMode( m => m == GameModeManager.ModeId.World );
            var inChangeCameraFocusMode = GameModeManager.InMode( m => m == GameModeManager.ModeId.ChangeCameraFocus );
            if ( inWorldMode || inChangeCameraFocusMode ) {
                if ( tabState == -1 && !tabPressed ) {
                    tabState = 0;
                }
                if ( tabState == 0 && tabPressed ) {
                    tabState = 1;
                }
                if ( tabState == 1 && !tabPressed ) {
                    tabState = 2;
                }
                if ( tabState == 2 && tabPressed ) {
                    tabState = 1;
                }
         
         
                if ( shiftTabState == -1 && !tabShiftPressed ) {
                    shiftTabState = 0;
                }
                if ( shiftTabState == 0 && tabShiftPressed ) {
                    shiftTabState = 1;
                }
                if ( shiftTabState == 1 && !tabShiftPressed ) {
                    shiftTabState = 2;
                }
                if ( shiftTabState == 2 && tabShiftPressed ) {
                    shiftTabState = 1;
                }
            }
            else {
                tabState = -1;
                shiftTabState = -1;
            }
         
         
         
            if ( inWorldMode && tabState == 2 ) {
                //if (FindTarget ()) {
                state = GameCameraState.PopFromCamera;
                GhostCamera.instance.CancelFPS();
                //}
            }
            else if ( inWorldMode && shiftTabState == 2 ) {
             
                if ( cameraViewTarget == default(Transform) ) {
                    if ( FindTarget() ) {
                        state = GameCameraState.PopFromCamera;
                    }
                 
                }
                else {
                    state = GameCameraState.PopFromCamera;
                }
            }
     
         

            yield return new WaitForFixedUpdate();
        }
        NextState();
     
     
    }
 
    IEnumerator FreeCameraState () {
        //Console.PushLog("Cam", new string[] { "Free" });
        int tabState = -1;
        while ( state == GameCameraState.FreeCamera ) {
         
         
            var inWorldMode = GameModeManager.InMode( m => m == GameModeManager.ModeId.World );
            var inChangeCameraFocusMode = GameModeManager.InMode( m => m == GameModeManager.ModeId.ChangeCameraFocus );
            if ( inWorldMode || inChangeCameraFocusMode ) {
                if ( tabState == -1 && !tabPressed ) {
                    tabState = 0;
                }
                if ( tabState == 0 && tabPressed ) {
                    tabState = 1;
                }
                if ( tabState == 1 && !tabPressed ) {
                    tabState = 2;
                }
                if ( tabState == 2 && tabPressed ) {
                    tabState = 1;
                }
         
         
            }
            else {
                tabState = -1;
            }

         
            if ( partPicked ) {
                state = GameCameraState.Following;
                partPicked = false;
            }
         
            if ( tabShiftPressed && inWorldMode && tabState == 2 ) {
                if ( FindTarget() ) {
                    state = GameCameraState.PopFromCamera;
                }
            }
            else if ( tabPressed && inWorldMode ) {
             
             
            }
     
         
            yield return new WaitForFixedUpdate();
        }
        NextState();
     
     
    }

    void NextState () {
        string methodName = state.ToString() + "State";
        System.Reflection.MethodInfo info =
            GetType().GetMethod( methodName,
                                System.Reflection.BindingFlags.NonPublic |
                                System.Reflection.BindingFlags.Instance );
        StartCoroutine( ( IEnumerator )info.Invoke( this, null ) );
    }
 
    public void OnTrigger ( Vector3 relativeVelocity, BulletRigidBody other, Vector3 contactPoint, Vector3 lineOfAction ) {
        if ( other.collisionGroup == BulletLayers.Water ) {
            inWater = true;
        }
    }
 
    void OnWaterEnter () {
        //Debug.Log("water enter "+ Time.time);
        caustics.SetActive( true );
        watery.SetActive( true );
    }

    void OnWaterExit () {
        caustics.SetActive( false );
        watery.SetActive( false );
    }
 
 
 
 

}
