using UnityEngine;
using System.Collections;
using System.IO;

public class MeshSerializer : MonoBehaviour {

    public BulletCompoundShape compoundShape;


    void Awake(){
        compoundShape = GetComponent<BulletCompoundShape>();
    }

	void Start () {
        for(int i = 0 ; i < compoundShape.collisionShapes.Length ; ++i) {
            var shape = compoundShape.collisionShapes[i];
            if(shape.mesh == default(Mesh)){
                continue;
            }
            var folder = Path.Combine( Util.userDataPath, "meshes" );
            System.IO.Directory.CreateDirectory( folder );
            string path = Path.Combine(folder,  compoundShape.serializationName + "_" + i + ".bullet");
            var numVertex = shape.mesh.vertexCount;
            var numTriangles = shape.mesh.triangles.Length / 3;
            BulletVector3[] vertex = new BulletVector3[numVertex];
            for(int j = 0 ; j < numVertex ; ++j){
                vertex[j] = shape.mesh.vertices[j].ToBulletVector3();
            }
            var triangleIndex = shape.mesh.triangles;

            Bullet.SerializeShape(path,numVertex,numTriangles,vertex,triangleIndex);

        }
	}
	
}
