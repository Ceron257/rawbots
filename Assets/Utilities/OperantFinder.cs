using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;


public class OperantFinder  {
	
	
	Dictionary<Entity,int> visited;
    public string operantId = "input_sampler";
	
	
	public OperantFinder(){
		visited = new Dictionary<Entity, int>();
	}
	
	public List<Operants.NodeProxy> FindOperants( Entity entity, string operantId){
		this.operantId = operantId;
		List<Operants.NodeProxy> operants = new List<Operants.NodeProxy>();
		FindOperantsRecursive(entity,operants);
		return operants;
	}
	
	
	void FindOperantsRecursive ( Entity entity, List<Operants.NodeProxy> operants ) {
        var num = 0;
		
		
        if ( visited.TryGetValue( entity, out num ) ) {
            return;
        }

        visited.Add( entity, 0 );

        var definition = entity.Heads( p => p == "as" ).FirstOrDefault();
        if ( definition != default(Entity) ) {
            if ( definition.id == operantId ) {
                operants.Add( entity.proxy );
            }
        }

        foreach ( var r in entity.Relationships().Where( r =>
            r.Predicate == "input_for" ||
            r.Predicate == "output_for" ||
            r.Predicate == "extends" ||
            r.Predicate == "slot_for" ||
            r.Predicate == "linked_to" ||
            r.Predicate == "consumes" ) ) {
            var e = r.Head == entity ? r.Tail : r.Head;
            FindOperantsRecursive( e,operants );
        }
    }
	
	
	
}
