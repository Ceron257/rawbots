using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;

public class GhostCamera : MonoBehaviour
{
	
	
	public enum GameCameraState
	{
		Init,
		Following,
		FirstPerson,
		FreeCamera,
		PopFromCamera,
		ChangeTargetMode,
		ChangeCameraMode
	}
	
	BulletRigidBody bulletRigidbody;
	public PID positionPID;
	public PID obstaclePID;
	public FastFollower fastFollower;
	Vector3 targetTestPosition = Vector3.zero;
	public GameCameraState state = GameCameraState.Init;
	public GameObject ghostTarget;
	public GameObject secondGhostTarget;
	private GameObject newTarget;
	public GameObject targetProxyTransform;
	public GameObject targetCube;
	public BulletRigidBody gameCamera;
	
	private bool newPart = false;
	private bool goToFps = false;
	PhysicalObject cameraPhysicalObject;
	PhysicalObject targetPhysicalObject;
	GameObject targetProxy;
	float followPitch = 0f;
	public float followPitchSpeed = 7f;
	float followYaw = 0f;
	public float followYawSpeed = 15f;
	float distanceToCamera = 0;
	public float followDistance = 0.1f;
	public float minFollowDistance = 15;
	public float maxFollowDistance = 100f;
	public static GhostCamera instance;
	private bool movingYaw = false;
	private bool movingPitch = false;
	private bool movingDistance = false;
	private int pitchDirection = 0;
	private int yawDirection = 0;
	private int distanceDirection = 0;

	public bool invertY = false;
	
	

	public bool obstacleFound = false;


	void Awake ()
	{
		instance = this;
		if(PlayerPrefs.HasKey("invertY")) {
		invertY = bool.Parse(PlayerPrefs.GetString("invertY"));
		}
		
	}

	public void Initialize (GameObject realTarget, GameObject realCamera)
	{
		
		cameraPhysicalObject = realCamera.GetComponent<PhysicalObject> ();
		targetPhysicalObject = realTarget.GetComponent<PhysicalObject> ();
		bulletRigidbody = GetComponent<BulletRigidBody> ();
		
		
		
		
		SyncronizeGhosts ();
		bulletRigidbody.SetRotation (ghostTarget.transform.rotation);
		bulletRigidbody.SetPosition (ghostTarget.transform.position + ghostTarget.transform.forward * 2f);
		
		
		NextState ();
	}
	
	public void ChangeTarget (GameObject newTarget)
	{
		
		this.newTarget = newTarget;
		newPart = true;
		state = GameCameraState.ChangeTargetMode;
		
		
		
	}
	
	public void ChangeCamera (GameObject newTarget)
	{
		
		this.newTarget = newTarget;
		state = GameCameraState.ChangeCameraMode;
	}
	
	public void SetFPS (GameObject newTarget)
	{
		this.newTarget = newTarget; 
		goToFps = true;
	}

	public void CancelFPS ()
	{
		goToFps = true;
		
	}

	void OnDrawGizmos ()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawSphere (targetTestPosition, 0.5f);
	}
	
	void SyncronizeGhosts ()
	{
		
		ghostTarget.transform.rotation = targetPhysicalObject.transform.rotation;
	}
	
	
	public Vector3 GetMirrorCameraPosition ()
	{
		
		var localPos = ghostTarget.transform.InverseTransformPoint (transform.position);
		
		
		
		return targetPhysicalObject.transform.TransformPoint (localPos);
	}
	
	public Vector3 GetMirrorCameraTargetPosition ()
	{
		var localPos = ghostTarget.transform.InverseTransformPoint (targetTestPosition);	
		return targetPhysicalObject.transform.TransformPoint (localPos);
	}
	
	
	public Quaternion GetMirrorCameraRotation ()
	{
		
		return  transform.rotation;
	}
	
	public void SyncronizeRealCameraAndTarget(){
		targetCube.transform.position = GetMirrorCameraTargetPosition ();
		gameCamera.SetPosition( GetMirrorCameraPosition());
		gameCamera.SetRotation(GetMirrorCameraRotation());
		//gameCamera.transform.position = GetMirrorCameraPosition();
		//gameCamera.transform.rotation = GetMirrorCameraRotation();
	}
	
	
	Vector3 GetYawPitchDistanceFromPoint (Vector3 point)
	{
		SyncronizeGhosts ();
		//var cameraTargetUp = cameraPhysicalObject.Up ();
		var targetPhysical = targetPhysicalObject;
		var targetUp = targetPhysical.Up ();
		var distanceVector = (point - ghostTarget.transform.position);
//		var targetToCamera = distanceVector.normalized;
		var rotationAdjustment = Quaternion.FromToRotation (targetProxy.transform.up, targetUp);
		targetProxy.transform.position = ghostTarget.transform.position;
		targetProxy.transform.rotation = rotationAdjustment * targetProxy.transform.rotation;
//		var targetSide = targetProxy.transform.right;
		var frontAxis = targetProxy.transform.forward;// lastTargetToCamera;
		var toCamera2 = point - ghostTarget.transform.position;
		//var dot = Vector3.Dot (toCamera2.normalized, targetUp);
		//var acos = Mathf.Acos (dot);
		
		//var acos = Vector3.Angle(toCamera2.normalized,targetUp);
		var angle =Vector3.Angle(toCamera2.normalized,targetUp);// acos * 180 / Mathf.PI;
		//Debug.Log ("pitch " + (-90 + angle) + " " + followPitch);
		var firstCross = Vector3.Cross (targetUp, toCamera2).normalized;
		var secondCross = Vector3.Cross (firstCross, targetUp).normalized;
		Debug.DrawLine (ghostTarget.transform.position, ghostTarget.transform.position + targetUp * 4f);
		Debug.DrawLine (ghostTarget.transform.position, ghostTarget.transform.position + secondCross * 4f);
		var thirdCross = Vector3.Cross (secondCross, frontAxis);
		//var acos2 =Vector3.Angle(secondCross,frontAxis);// Mathf.Acos (dot2);
		//var acos2_u =Vector3.Angle(secondCross,frontAxis);
		//Debug.Log(acos2* 180 / Mathf.PI + "  "+ acos2_u);
		var calculatedYaw = Vector3.Angle(secondCross,frontAxis);// acos2 * 180 / Mathf.PI;
		var filteredFollowYaw = followYaw;
		if (filteredFollowYaw < 0)
			filteredFollowYaw += 360;
		var localThirdCross = targetProxy.transform.InverseTransformDirection (thirdCross.normalized);
		if (localThirdCross.y > 0) {
			calculatedYaw = 360 - calculatedYaw;
		}
		
		var yawPitchDistance = new Vector3 (calculatedYaw, -90 + angle, distanceVector.magnitude);
		//Debug.Log ("yaw pitch distance " + yawPitchDistance);
		return yawPitchDistance;
	}
	
	Vector3 GetYawPitchDistanceOfCamera ()
	{
		SyncronizeGhosts ();

		var targetPhysical = targetPhysicalObject;
		var targetUp = targetPhysical.Up ();
		var distanceVector = (transform.position - ghostTarget.transform.position);

		var rotationAdjustment = Quaternion.FromToRotation (targetProxy.transform.up, targetUp);
		targetProxy.transform.position = ghostTarget.transform.position;
		targetProxy.transform.rotation = rotationAdjustment * targetProxy.transform.rotation;

		var frontAxis = targetProxy.transform.forward;
		var toCamera2 = transform.position - ghostTarget.transform.position;

		var angle = Vector3.Angle(toCamera2.normalized,targetUp);

		var firstCross = Vector3.Cross (targetUp, toCamera2).normalized;
		var secondCross = Vector3.Cross (firstCross, targetUp).normalized;
		Debug.DrawLine (ghostTarget.transform.position, ghostTarget.transform.position + targetUp * 4f);
		Debug.DrawLine (ghostTarget.transform.position, ghostTarget.transform.position + secondCross * 4f);
		var thirdCross = Vector3.Cross (secondCross, frontAxis);

		
		var calculatedYaw = Vector3.Angle(secondCross,frontAxis);//  acos2 * 180 / Mathf.PI;
		var filteredFollowYaw = followYaw;
		if (filteredFollowYaw < 0)
			filteredFollowYaw += 360;
		var localThirdCross = targetProxy.transform.InverseTransformDirection (thirdCross.normalized);
		if (localThirdCross.y > 0) {
			calculatedYaw = 360 - calculatedYaw;
		}
		var yawPitchDistance = new Vector3 (calculatedYaw, -90 + angle, distanceVector.magnitude);
		//Debug.Log ("calculatedYaw " + calculatedYaw);
		return yawPitchDistance;
		
	}

	void CheckCameraInputs (bool vpGridHidden)
	{


		if (Input.GetKey (KeyCode.DownArrow) && vpGridHidden) {
			if (Input.GetKey (KeyCode.LeftShift)) {
				movingDistance = true;
				distanceDirection = invertY ? -1 : 1;
				movingPitch = false;
			} else {
				pitchDirection = -1;
				movingPitch = true;
				movingDistance = false;
			}
			
		} else if (Input.GetKey (KeyCode.UpArrow) && vpGridHidden) {
			if (Input.GetKey (KeyCode.LeftShift)) {
				movingDistance = true;
				distanceDirection = invertY ? 1 : -1;
				movingPitch = false;
			} else {
				pitchDirection = 1;
				movingPitch = true;
				movingDistance = false;
			}
			
		} else {
			movingPitch = false;
			movingDistance = false;
		}
		if (Input.GetKey (KeyCode.LeftArrow) && vpGridHidden) {
			movingYaw = true;
			yawDirection = 1;
		} else if (Input.GetKey (KeyCode.RightArrow) && vpGridHidden) {
			movingYaw = true;
			yawDirection = -1;
		} else {
			movingYaw = false;
		}

		
	}
        /*float scrollWheelZoom() { //doesnt work properly yet -z26
            var output = 0f;
            if ( GameModeManager.InMode( m => m == GameModeManager.ModeId.World ) ) {
                output = Input.GetAxis( "Mouse ScrollWheel" ) *20;
                Debug.Log(output);
                output *= Mathf.Abs(output);
            }
            return output;            
        }*/

        
        
	IEnumerator InitState ()
	{
		
		float timer = Time.time;
		
		while (state == GameCameraState.Init) {
			if ((Time.time - timer) < 2.0f) {
				state = GameCameraState.PopFromCamera;
			}
			yield return 0;
		}
		NextState ();
	}
	
	IEnumerator PopFromCameraState ()
	{
		SyncronizeGhosts ();
		
		Destroy (targetProxy);
		targetProxy = new GameObject ();
		
		targetProxy.transform.position = ghostTarget.transform.position;
		
		followPitch = 30f;
		followYaw = 0;
		distanceToCamera = 10;
		bulletRigidbody.SetPosition (ghostTarget.transform.position + ghostTarget.transform.transform.forward * -2f+ghostTarget.transform.transform.up*1f);
		bulletRigidbody.SetRotation (ghostTarget.transform.rotation);
		bulletRigidbody.SetVelocity (Vector3.zero);
		positionPID.setpoint = ghostTarget.transform.position + ghostTarget.transform.transform.forward * -2f+ghostTarget.transform.transform.up*1f;
		positionPID.Reset (ghostTarget.transform.position + ghostTarget.transform.transform.forward * -2f+ghostTarget.transform.transform.up*1f);
		
		
		SyncronizeRealCameraAndTarget();
//		var followerPos = ghostTarget.transform.InverseTransformPoint (ghostTarget.transform.position);
//		var targetGlobal =  targetPhysicalObject.transform.TransformPoint (followerPos);
//		
		fastFollower.GetComponent<BulletRigidBody>().SetPosition(gameCamera.transform.position);
		fastFollower.followPid.Reset(gameCamera.transform.position);
		
		

		while (state == GameCameraState.PopFromCamera) {
			state = GameCameraState.Following;
			yield return 0;
		}
		NextState ();
	}
	
	IEnumerator FollowingState ()
	{
		Destroy (targetProxy);
		targetProxy = new GameObject ("TargetProxy");
		

		targetProxy.transform.position = ghostTarget.transform.position;
		var yawPitchDistance = GetYawPitchDistanceOfCamera ();
		
		followYaw = yawPitchDistance.x;
		followPitch = yawPitchDistance.y;
		
		
		var timer = Time.time;
		
		
		while (state == GameCameraState.Following) {
			
			if (targetPhysicalObject == default(PhysicalObject) /*|| state != GameCameraState.Following*/) {
				//Debug.Log ("cameraview target is null " + Time.time);
				
				state = GameCameraState.FreeCamera;
				continue;
			}
			
			//GetYawPitchDistanceOfCamera();
			SyncronizeGhosts ();
			SyncronizeRealCameraAndTarget();
			
			var inWorldMode = GameModeManager.InMode (m => m == GameModeManager.ModeId.World || m == GameModeManager.ModeId.PartCloseConnect || m == GameModeManager.ModeId.PartExtendConnect);
            CheckCameraInputs (inWorldMode);

			

			
			
			if ((transform.position - ghostTarget.transform.position).magnitude > maxFollowDistance * 8f) {
				distanceToCamera = 10;
				state = GameCameraState.PopFromCamera;
			}
			
//			var cameraTargetUp = cameraPhysicalObject.Up ();
			var targetPhysical = targetPhysicalObject;
			var targetUp = targetPhysical.Up ();
			

//			var targetToCamera = distanceVector / distanceMagnitude;
			var rotationAdjustment = Quaternion.FromToRotation (targetProxy.transform.up, targetUp);
			targetProxy.transform.position = ghostTarget.transform.position;
			targetProxy.transform.rotation = rotationAdjustment * targetProxy.transform.rotation;
			var targetSide = targetProxy.transform.right;
			var frontAxis = targetProxy.transform.forward;
			

			
			var currentYawPitchDistance = GetYawPitchDistanceOfCamera ();
			
			
			
			var dummy = new GameObject();
			dummy.transform.parent = cameraPhysicalObject.transform;
			dummy.transform.position = fastFollower.transform.position;// cameraPhysicalObject.transform.position + rejectDirection*1.5f;
			
			var secondDummy = new GameObject();
			secondDummy.transform.parent = transform;
			secondDummy.transform.localPosition = dummy.transform.localPosition;
			secondDummy.transform.localRotation = dummy.transform.localRotation;
			

			Debug.DrawLine(transform.position,secondDummy.transform.position);

			if(obstacleFound){
				followYaw = (currentYawPitchDistance.x );
				followPitch = (currentYawPitchDistance.y);
			}
			
			if (movingYaw) {
				followYaw = (currentYawPitchDistance.x ) + 10f * yawDirection;
			}
			

			if (movingPitch) {
				followPitch = (currentYawPitchDistance.y) + 10f * pitchDirection;
			}
			
			
			
			Destroy (dummy);
		
			followPitch = Mathf.Clamp (followPitch, -80, 80);
            
			if (movingDistance) { //tweaked the zooming speed, the slope is somewhere between straight and quadratic -z26
                var delta = (distanceDirection * 0.45f) * Mathf.Pow(currentYawPitchDistance.z, 0.8f);
				var newFollowDistance = currentYawPitchDistance.z + delta;
				if (newFollowDistance > minFollowDistance && newFollowDistance < maxFollowDistance) {
					distanceToCamera = newFollowDistance;
				}
			}
			
			
			if (followPitch > 360) {
				followPitch = 0;
			}
			if (followYaw > 360) {
				followYaw -= 360;
			}

			if (followPitch < -360) {
				followPitch = 0;
			}
			if (followYaw < -360) {
				followYaw += 360;
			}
			
			
			
		//Debug.Log(followYaw+ " "+ targetUp);
			var targetYaw = Quaternion.AngleAxis (followYaw, targetUp);
			var targetPitch = Quaternion.AngleAxis (followPitch, targetSide);
			var maxDistance = maxFollowDistance;
			var targetDistance = Mathf.Clamp (distanceToCamera, minFollowDistance, maxDistance);
			var targetPosition = ghostTarget.transform.position + targetYaw * targetPitch * frontAxis * targetDistance;
			//Debug.Log(ghostTarget.transform.position + " " + targetYaw * targetPitch * frontAxis * targetDistance + " "+targetYaw +" "+ targetPitch +" "+ frontAxis +" "+ targetDistance );
			positionPID.setpoint = targetPosition;
			var distanceForce = positionPID.Compute (transform.position, Time.deltaTime);
			distanceForce = Vector3.ClampMagnitude (distanceForce, 1000);
			
			
			obstaclePID.setpoint = secondDummy.transform.position;
			var restoreForce = obstaclePID.Compute(transform.position,Time.deltaTime);

//			var cameraDrag = Vector3.zero;
//			
//			var targetVelocityDirection = targetPhysicalObject.bulletRigidbody.GetVelocity().normalized;
//			cameraDrag = targetVelocityDirection*-1f*10f;
			
			Destroy(secondDummy);
			
			if(!obstacleFound)restoreForce = Vector3.zero;
			
			
			if((Time.time-timer)< 0.2f)restoreForce = Vector3.zero;
			
			bulletRigidbody.AddForce (distanceForce+ restoreForce , Vector3.zero);	
			//Debug.Log(targetPosition);
			targetTestPosition = targetPosition;
			
			var followerPos = ghostTarget.transform.InverseTransformPoint (targetTestPosition);
			var targetGlobal =  targetPhysicalObject.transform.TransformPoint (followerPos);
			fastFollower.Calculate(targetGlobal);

			var rotation = Quaternion.LookRotation (ghostTarget.transform.position - transform.position, targetUp);
			if((Time.time-timer)< 0.15f){
				rotation = Quaternion.Slerp (transform.rotation, rotation, 0.2f);
			}
			bulletRigidbody.SetRotation (rotation);
			
			SyncronizeRealCameraAndTarget();
			
			if (newPart) {
				state = GameCameraState.ChangeTargetMode;
				newPart = false;
			}
			if (goToFps) {
				
				state = GameCameraState.FirstPerson;
				goToFps = false;
			}
			obstacleFound = false;
			yield return new WaitForFixedUpdate();
		}
		Destroy(targetProxy);
		NextState ();
		
		
	}
	
	IEnumerator ChangeTargetModeState ()
	{
	
		
		var dummy = new GameObject ();
		dummy.transform.position = newTarget.transform.position;
		dummy.transform.rotation = newTarget.transform.rotation;
		
		if(targetPhysicalObject != null){
			dummy.transform.parent = targetPhysicalObject.transform;
		}else{
			dummy.transform.parent = targetProxyTransform.transform;	
		}
		
		
		secondGhostTarget.transform.parent = ghostTarget.transform;
		secondGhostTarget.transform.localPosition = dummy.transform.localPosition;
		secondGhostTarget.transform.localRotation = dummy.transform.localRotation;
		
		secondGhostTarget.transform.parent = ghostTarget.transform.parent;
		
		var pos = secondGhostTarget.transform.position;
		var rot = secondGhostTarget.transform.rotation;
		
		secondGhostTarget.transform.position = ghostTarget.transform.position;
		secondGhostTarget.transform.rotation = ghostTarget.transform.rotation;
		
		ghostTarget.transform.position = pos;
		ghostTarget.transform.rotation = rot;
		targetPhysicalObject = newTarget.GetComponent<PhysicalObject> ();
		
		Destroy (dummy);
		
		
		while (state == GameCameraState.ChangeTargetMode) {
			
			
		
			
			state = GameCameraState.Following;
			
			yield return new WaitForFixedUpdate();
		}
		NextState ();
		
		
	}
	
	IEnumerator ChangeCameraModeState ()
	{
		
		var dummy = new GameObject ();
		dummy.transform.position = newTarget.transform.position;
		dummy.transform.rotation = newTarget.transform.rotation;
		
		if(targetPhysicalObject != null){
			dummy.transform.parent = targetPhysicalObject.transform;
		}else{
			dummy.transform.parent = targetProxyTransform.transform;	
		}
		
		secondGhostTarget.transform.parent = ghostTarget.transform;
		secondGhostTarget.transform.localPosition = dummy.transform.localPosition;
		secondGhostTarget.transform.localRotation = dummy.transform.localRotation;
		
		secondGhostTarget.transform.parent = ghostTarget.transform.parent;
		
		var pos = secondGhostTarget.transform.position;
		var rot = secondGhostTarget.transform.rotation;
		
		secondGhostTarget.transform.position = ghostTarget.transform.position;
		secondGhostTarget.transform.rotation = ghostTarget.transform.rotation;
		
		ghostTarget.transform.position = pos;
		ghostTarget.transform.rotation = rot;
		
		targetPhysicalObject = newTarget.GetComponent<PhysicalObject> ();
		
		Destroy (dummy);
		
		
		while (state == GameCameraState.ChangeCameraMode) {
			
			
			
			
			state = GameCameraState.PopFromCamera;
			
			yield return new WaitForFixedUpdate();
		}
		NextState ();
		
		
	}
	
	IEnumerator FirstPersonState ()
	{
		
		var dummy = new GameObject ();
		dummy.transform.position = newTarget.transform.position;
		dummy.transform.rotation = newTarget.transform.rotation;
		
		dummy.transform.parent = targetPhysicalObject.transform;
		
		secondGhostTarget.transform.parent = ghostTarget.transform;
		secondGhostTarget.transform.localPosition = dummy.transform.localPosition;
		secondGhostTarget.transform.localRotation = dummy.transform.localRotation;
		
		secondGhostTarget.transform.parent = ghostTarget.transform.parent;
		
		var pos = secondGhostTarget.transform.position;
		var rot = secondGhostTarget.transform.rotation;
		
		secondGhostTarget.transform.position = ghostTarget.transform.position;
		secondGhostTarget.transform.rotation = ghostTarget.transform.rotation;
		
		ghostTarget.transform.position = pos;
		ghostTarget.transform.rotation = rot;
		targetPhysicalObject = newTarget.GetComponent<PhysicalObject> ();
		
		while (state == GameCameraState.FirstPerson) {
			
			
			
			SyncronizeGhosts ();
			SyncronizeRealCameraAndTarget();
			
			if (ghostTarget != default(Transform)) {
				bulletRigidbody.SetRotation (ghostTarget.transform.rotation);
				bulletRigidbody.SetPosition (ghostTarget.transform.position + ghostTarget.transform.forward * 2f);
			} else {
				state = GameCameraState.FreeCamera;
			}
			
			if (goToFps) {
				state = GameCameraState.PopFromCamera;
				goToFps = false;
			}
			
			yield return new WaitForFixedUpdate();
		}
		NextState ();
		
		
	}
	
	IEnumerator FreeCameraState ()
	{
		Destroy (targetProxy);
		targetProxy = new GameObject ();
		targetProxy.transform.position = bulletRigidbody.transform.position;
		PhysicalObject cameraPhysicalObj = cameraPhysicalObject;// bulletRigidbody.GetComponent<PhysicalObject> ();
		positionPID.Reset (targetProxy.transform.position);
		distanceToCamera = 10;
		while (state == GameCameraState.FreeCamera) {

			
			var inWorldMode = GameModeManager.InMode (m => m == GameModeManager.ModeId.World);
            CheckCameraInputs (inWorldMode);

			
			if (newPart) {
				state = GameCameraState.Following;
				newPart = false;
			}
			

			var targetUp = cameraPhysicalObj.Up ();

			var rotationAdjustment = Quaternion.FromToRotation (targetProxy.transform.up, targetUp);
			targetProxy.transform.rotation = rotationAdjustment * targetProxy.transform.rotation;//*rotationAdjustment;
			var targetSide = targetProxy.transform.right;
			var frontAxis = targetProxy.transform.forward;// lastTargetToCamera;
			
			
			

			
			var targetYaw = Quaternion.AngleAxis (followYaw, targetUp);
			var targetPitch = Quaternion.AngleAxis (followPitch, targetSide);

			//var targetDistance = Mathf.Clamp (distanceToCamera, minFollowDistance, maxDistance);
			var targetPosition = targetProxy.transform.position + targetYaw * targetPitch * frontAxis * 10f;
			positionPID.setpoint = targetPosition;
			
//			var followerPos = target.transform.InverseTransformPoint (targetTestPosition);
//			var targetGlobal =  targetPhysicalObject.transform.TransformPoint (followerPos);
//			fastFollower.GetComponent<FastFollower>().Calculate(targetGlobal);

			
			var distanceForce = positionPID.Compute (transform.position, Time.deltaTime);
			distanceForce = Vector3.ClampMagnitude (distanceForce, 1000);
			bulletRigidbody.AddForce (distanceForce /* resultForce*/, Vector3.zero);
			
			
			var rotation = Quaternion.LookRotation (targetProxy.transform.position - transform.position, targetUp);

			rotation = Quaternion.Slerp (transform.rotation, rotation, 0.1f);

			bulletRigidbody.SetRotation (rotation);
			
			
			yield return new WaitForFixedUpdate();
		}
		NextState ();
		
		
	}

	void NextState ()
	{
		string methodName = state.ToString () + "State";
		System.Reflection.MethodInfo info =
            GetType ().GetMethod (methodName,
                                System.Reflection.BindingFlags.NonPublic |
                                System.Reflection.BindingFlags.Instance);
		StartCoroutine ((IEnumerator)info.Invoke (this, null));
	}
}
