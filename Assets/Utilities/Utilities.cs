using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using System.Text.RegularExpressions;

public static class Util
{

	public static string userDataPath = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments), "rawbots");

	public static void LogStack ()
	{
		var trace = new System.Diagnostics.StackTrace ();
		foreach (var frame in trace.GetFrames()) {
			var method = frame.GetMethod ();
			if (method.Name.Equals ("LogStack")) {
				continue;
			}
			Debug.Log (string.Format ("{0}::{1}", method.ReflectedType != null ? method.ReflectedType.Name : string.Empty, method.Name));
		}
	}

	public static void DrawAxis (Transform xform)
	{
		Debug.DrawLine (xform.position, xform.position + xform.right * 10, Color.red);
		Debug.DrawLine (xform.position, xform.position + xform.up * 10, Color.green);
		Debug.DrawLine (xform.position, xform.position + xform.forward * 10, Color.blue);
	}
 
	public static void DrawRay (Transform origin, Vector3 direction, Color color)
	{
		//Debug.DrawLine( origin.position, origin.position + direction.normalized * 10, color );
	}

	public static void SetLayerRecursively (GameObject obj, int newLayer)
	{
		obj.layer = newLayer;
		foreach (Transform child in obj.transform) {
			SetLayerRecursively (child.gameObject, newLayer);
		}
	}

	public static float MapValue0X (float value, float maxValue, float minMapValue, float maxMapValue)
	{
		return (value / maxValue) * (maxMapValue - minMapValue) + minMapValue;
	}

	public static Vector3 CubicBezierCurve (float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
	{
		return ((1 - t) * (1 - t) * (1 - t)) * p0
            + 3 * ((1 - t) * (1 - t)) * t * p1
            + 3 * (1 - t) * (t * t) * p2 + (t * t * t) * p3;
	}

    public static Regex vector3Regex = new Regex( @"^\[-?[0-9]+(?:\.[0-9]+)?,-?[0-9]+(?:\.[0-9]+)?,-?[0-9]+(?:\.[0-9]+)?\]$", RegexOptions.Compiled );
	public static Vector3 PropertyToVector3 (string property){
        if(vector3Regex.IsMatch(property)){
		    property = property.Substring (1, property.Length - 2);
            var vs = property.Split (',');
            return new Vector3 (float.Parse (vs [0]), float.Parse (vs [1]), float.Parse (vs [2]));
        }
        return Vector3.zero;
	}

	public static string Vector3ToProperty (Vector3 vector)
	{
		return string.Format ("[{0:F6},{1:F6},{2:F6}]", vector.x, vector.y, vector.z);
	}

	public static Vector3 TouchToWorld (Vector3 position, TouchData touchData)
	{
		var camera = touchData.camera;
		var projected = Vector3
            .Dot (position - camera.transform.position, camera.transform.forward) * camera.transform.forward;
		var screenPosition = touchData.position;
		screenPosition.z = projected.magnitude;
		return camera.ScreenToWorldPoint (screenPosition);
	}
    
	public static Vector3 TouchToWorld (Vector3 position, Vector3 touchPosition, Camera camera)
	{
		var projected = Vector3
            .Dot (position - camera.transform.position, camera.transform.forward) * camera.transform.forward;
		var screenPosition = touchPosition;
		screenPosition.z = projected.magnitude;
		return camera.ScreenToWorldPoint (screenPosition);
	}

	public static void ForPredicate (bool predicate, System.Action action)
	{
		if (predicate) {
			action ();
		}
	}

	public static void ForEnumValue (string expression, System.Action<System.Type , string> action)
	{
		var index = expression.LastIndexOf (".");
		var typeName = expression.Substring (0, index);
		var valueString = expression.Substring (++index);
		var enumType = System.Type.GetType (typeName);
		var enumValue = System.Enum.Parse (enumType, valueString);
		action (enumType, enumValue.ToString ());
	}

	public static T EnumParse<T> (string value)
	{
		return (T)System.Enum.Parse (typeof(T), value);
	}

	public static Color PropertyToColor (string sColor)
	{
        var colorRegex = "#[A-Fa-f0-9]{6}"; //copied the check in hexinputtile.cs to here -z26
        var isValid = Regex.Match( sColor, colorRegex ).Success && sColor.Length == 7;
        int argb = 0;
		if (isValid) {
            argb = System.Int32.Parse (sColor.Substring (1), System.Globalization.NumberStyles.HexNumber);
        }
        else {Debug.Log ("bad color input:" + sColor);}
		return new Color (
         ((argb & 0xFF0000) >> 16) / 255f,
         ((argb & 0x00FF00) >> 8) / 255f,
         (argb & 0x0000FF) / 255f, 1);
	}

	public static string ColorToProperty (Color color)
	{
		return "#" +
            Mathf.RoundToInt (color.r * 255).ToString ("X2") +
            Mathf.RoundToInt (color.g * 255).ToString ("X2") +
            Mathf.RoundToInt (color.b * 255).ToString ("X2");
	}

	public static float Sum (List< float > numbers)
	{
		var total = 0f;
		for (var i = 0; i < numbers.Count; ++i) {
            float temp = numbers[i];
            if ((!(float.IsNaN(temp)))&&(!(float.IsInfinity(temp))))    //This is the NaN fix
            {
                total += temp;
            }
		}
		return total;
	}

	public static bool AsIs (string predicate)
	{
		return predicate == "as" || predicate == "is";
	}

	public static void MoveToLayer (Transform root, int to_layer, int from_layer)
	{
		if (root.gameObject.layer == from_layer) {
			root.gameObject.layer = to_layer;
		}
		for (int i = 0; i< root.childCount; i++) {
			MoveToLayer (root.GetChild (i), to_layer, from_layer);
		}
	}
	/*this is dirty! */
	public static float FastInvSqrt (float x)
	{
		/*0x5f3759df*/ 
		/*
		float xhalf = 0.5f * x;
		int i = BitConverter.ToInt32 (BitConverter.GetBytes (x), 0);
		i =0x5f375a86 - (i >> 1);
		x = BitConverter.ToSingle (BitConverter.GetBytes (i), 0);
		x = x * (1.5f - xhalf * x * x);
		return x;
		*/
		return Bullet.FastInvSqrt(x);
	}

	public static Vector3 FastNormalize (Vector3 vector)
	{
		
		return vector * FastInvSqrt (vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
	}

	public static void ClonePiece (GameObject clone, GameObject original, LayerMask layer)
	{

		clone.layer = layer;
		var originalRenderer = original.GetComponent< MeshRenderer > ();
		var originalFilter = original.GetComponent< MeshFilter > ();

		if (originalFilter != null && originalRenderer != null && (originalRenderer.castShadows || originalRenderer.receiveShadows)) {
			var renderer = clone.AddComponent< MeshRenderer > ();
			var filter = clone.AddComponent< MeshFilter > ();

			filter.mesh = originalFilter.mesh;
            renderer.sharedMaterial = originalRenderer.sharedMaterial;
			renderer.enabled = originalRenderer.enabled;
		}

		clone.transform.position = original.transform.position;
		clone.transform.rotation = original.transform.rotation;
		clone.transform.localScale = original.transform.localScale;
		clone.name = "(Ghost)" + original.name;

		for (int i =0; i< original.transform.childCount; i++) {
			var child = new GameObject ();
			child.transform.parent = clone.transform;
			ClonePiece (child, original.transform.GetChild (i).gameObject, layer);
		}
	}

    public static string GetString(byte[] bytes)
    {
        char[] chars = new char[bytes.Length / sizeof(char)];
        System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
        return new string(chars);
    }

    public static byte[] CopyIntToByteArray(int value) {
        var result = new byte[4];
        result[ 0 ] = (byte)(value & 0xFF);
        result[ 1 ] = (byte)((value & 0xFF00) >> 8 );
        result[ 2 ] = (byte)((value & 0xFF0000) >> 16 );
        result[ 3 ] = (byte)((value & 0xFF000000) >> 24 );
        return result;
    }

    public static int ab2int(byte[] bytes) {
        return (bytes[0] | (bytes[1] << 8) | ( bytes[2] << 16 ) | ( bytes[3] << 24 ));
    }
}
